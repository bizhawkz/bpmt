$(document).ready(function () {

    $(function () {
        $('.daterange').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });
    });

    $('.timepicker').mdtimepicker();
    // $('.logouttime').timepicker();

    $('#multiselect3-all').multiselect({
        includeSelectAllOption: true,
    });

    $('[data-toggle="tooltip"]').tooltip();

    $(".datepicker").datepicker({
        autoclose: true,
        startDate: new Date()
    });

    $(".backdatepicker").datepicker({
        autoclose: true,
        minDate: 0
    });

    $(".commondatepicker").datepicker({
        autoclose: true
    });


    $("#clientbl").change(function () {
        var val = $(this).val();
        $(".hideme2").hide();
        $(".assignpro").prop("checked", false);
        $("#nomessage").hide();
        $("#addbltn").prop('disabled', true);

        if (val == "") {

            $(".hideme").hide();
        } else {

            $("#proassign").show();
        }
    })


    /* Check Paid & Total hours */
    $("#project").change(function () {
        var val = $(this).val();
        var refscrf = $("input[name='_csrfToken']").val();
        $(".hideme3").hide();

        if (val != "") {
            $.ajax({
                data: {
                    val: val,
                    _csrfToken: refscrf
                },
                url: "/bpmt/billing/getprojects_data",
                type: "post",
                success: function (response) {
                    if (response.result == "success") {
                        $("#total").val(response.total);
                        $("#pending").val(response.pending);
                        $("#paid").val(response.paid);
                        $(".hideme3").show();
                        $("#addbltn").prop('disabled', false);

                    }


                }

            })
        }



    })
    /* END */


    /* Assign Project Change*/

    $(".assignpro").change(function () {
        var val = $(this).val();
        var client = $("#clientbl").val();
        var refscrf = $("input[name='_csrfToken']").val();
        $("#nomessage").hide();
        $("#addbltn").prop("disabled", true);

        $(".hideme2").hide();

        if (val == "yes") {
            $.ajax({
                data: {
                    val: val,
                    client: client,
                    _csrfToken: refscrf
                },
                url: "/bpmt/billing/getprojects",
                type: "post",
                success: function (response) {
                    if (response.result == "success") {
                        $("#project").html(response.html);
                        $("#project_sel").show();

                    } else if (response.result == "empty") {
                        $("#nomessage").show();
                    }

                }

            })
        } else if (val == "no") {
            $("#addbltn").prop("disabled", false);
            $("#billing_date").show();

            $("#payinghours").show();
            $("#next_date").show();
            $("#remarks").show();
        }


    })



    /* END */





    /* Add Employee Form */


    $("#addempform").submit(function (e) {
        e.preventDefault();

        $("#addempbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#emperror").html("Some error occured. Please try again.");
                $("#emperror").fadeIn().delay('5000').fadeOut('slow');
                $("#addempbtn").prop("disabled", false);

            },
            success: function (response)
            {
                $("#addempbtn").prop("disabled", false);

                if (response.result === "success") {
                    $("#empsuccess").html("Employee added succesfully.");
                    $("#empsuccess").fadeIn();
                    setTimeout(function () {
                        location.reload();
                    }, 1000);


                } else if (response.result === "email") {
                    $("#emperror").html("Employee with this email already exists.");
                    $("#emperror").fadeIn();


                } else if (response.result === "empty") {
                    $("#emperror").html("Please fill in Details.");
                    $("#emperror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#emperror").html("Some error occured. Error: " + response.result);
                    $("#emperror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */

    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $("#noAccess").show();
        console.log("Mobile");
    } else {
        console.log("Desktop");
        $("#noAccess").hide();

    }

    /*Update Employee Form*/


    $("#editempform").submit(function (e) {
        e.preventDefault();

        $("#editempbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#editemperror").html("Some error occured. Please try again.");
                $("#editemperror").fadeIn().delay('5000').fadeOut('slow');
                $("#editempbtn").prop("disabled", false);

            },
            success: function (response)
            {
                $("#editempbtn").prop("disabled", false);

                if (response.result === "success") {
                    $("#editempsuccess").html("Employee updated succesfully.");
                    $("#editempsuccess").fadeIn();
                    setTimeout(function () {
                        location.reload();
                    }, 1000);

                } else if (response.result === "email") {
                    $("#editemperror").html("Employee with this email already exists.");
                    $("#editemperror").fadeIn();


                } else if (response.result === "empty") {
                    $("#editemperror").html("Please fill in Details.");
                    $("#editemperror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#editemperror").html("Some error occured. Error: " + response.result);
                    $("#editemperror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });


    $('#emp-table tbody').on("click", ".empedit", function () {
        // $('#editempform').get(0).reset();
        var id = $(this).data('id');
        var refscrf = $("input[name='_csrfToken']").val();
        $.ajax({
            url: "/bpmt/employee/update",
            data: {"id": id, _csrfToken: refscrf},
            type: 'post', success: function (result)
            {
                if (result) {
                    $('#name').val(result.name)
                    $('#id').val(result.id)
                    $('#phone').val(result.phone)
                    $('#email').val(result.email)
                    $('#empid').val(result.empid)
                    $('#role').val(result.role)
                    $('#team').val(result.team)
                    $('#date').val(result.join_date)
                    $('#status').val(result.status)
                    $('#password').val(result.password)
                }
            }
        });
    });



    /*End*/



    /* Edit role form submission */


    $("#editroleform").submit(function (e) {
        e.preventDefault();

        $("#editrlbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#editrlerror").html("Some error occured. Please try again.");
                $("#editrlerror").fadeIn().delay('5000').fadeOut('slow');
                $("#editrlbtn").prop("disabled", false);
            },
            success: function (response)
            {
                $("#editrlbtn").prop("disabled", false);
                if (response.result === "success") {
                    $("#editrlsuccess").html("Role updated succesfully.");
                    $("#editrlsuccess").fadeIn();
                    setTimeout(function () {
                        location.href = "/bpmt/roles";
                    }, 1000);
                } else if (response.result === "empty") {
                    $("#editrlerror").html("Please fill in Details.");
                    $("#editrlerror").fadeIn().delay('5000').fadeOut('slow');
                } else if (response.result === "name") {
                    $("#editrlerror").html("Role name already exists.");
                    $("#editrlerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#editrlerror").html("Some error occured. Error: " + response.result);
                    $("#editrlerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });



    $('#role_table tbody').on("click", ".editroles", function () {
        //$('#editroleform').get(0).reset();

        var id = $(this).data('id');
        var refscrf = $("input[name='_csrfToken']").val();
        $.ajax({
            url: "/bpmt/roles/update",
            data: {"id": id, _csrfToken: refscrf},
            type: 'post', success: function (result)
            {
                if (result) {
                    $('#rolename').val();
                    $('#rolename').val(result.name);
                    $('#id').val(id);
                    $('#editrole input[type=checkbox]').each(function () {
                        $(this).prop("checked", false);
                        var check_box_value = $(this).val();
                        var check_box = $(this);
                        $.each(result.privilage, function (i, item) {
                            if (check_box_value === item) {
                                $(check_box).prop('checked', 'checked');
                            }
                        });
                    });
                }
            }
        });
    });


    /*End*/

    /* View Privilages */


    $('#role_table tbody').on("click", ".viewroles", function () {
        // $('#viewroleform').get(0).reset();
        var id = $(this).data('id');
        var refscrf = $("input[name='_csrfToken']").val();
        $.ajax({
            url: "/bpmt/roles/view",
            data: {"id": id, _csrfToken: refscrf},
            type: 'post', success: function (result)
            {
                if (result) {
                    var html = '';
                    $.each(result, function (i, item) {
                        html += '<div class="col-sm-6">' + (i + 1) + '.  ' + item + '</div>'
                    });
                    console.log(html);
                    $('#viewrolediv #view_pri').html(html);

                }
            }
        });
    });

    /*End*/
    /* Edit Employee Form */


    $("#editprofileform").submit(function (e) {
        e.preventDefault();

        $("#editbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#emperror").html("Some error occured. Please try again.");
                $("#emperror").fadeIn().delay('5000').fadeOut('slow');
                $("#editbtn").prop("disabled", false);

            },
            success: function (response)
            {
                $("#editbtn").prop("disabled", false);

                if (response.result === "success") {
                    $("#empsuccess").html("Employee updated succesfully.");
                    $("#empsuccess").fadeIn();
                    setTimeout(function () {
                        location.reload();
                    }, 1000);


                } else if (response.result === "email") {
                    $("#emperror").html("Employee with this email already exists.");
                    $("#emperror").fadeIn();


                } else if (response.result === "empty") {
                    $("#emperror").html("Please fill in Details.");
                    $("#emperror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#emperror").html("Some error occured. Error: " + response.result);
                    $("#emperror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */



    /* Edit Term Form */


    $("#edittermform").submit(function (e) {
        e.preventDefault();

        $("#edittermbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#edittermerror").html("Some error occured. Please try again.");
                $("#edittermerror").fadeIn().delay('5000').fadeOut('slow');
                $("#edittermbtn").prop("disabled", false);

            },
            success: function (response)
            {
                $("#edittermbtn").prop("disabled", false);

                if (response.result === "success") {
                    $("#edittermsuccess").html("Term updated succesfully.");
                    $("#edittermsuccess").fadeIn();
                    setTimeout(function () {
                        location.href = "/bpmt/term/"
                    }, 1000);
                } else if (response.result === "empty") {
                    $("#edittermerror").html("Please fill content.");
                    $("#edittermerror").fadeIn().delay('5000').fadeOut('slow');
                } else if (response.result === "name") {
                    $("#edittermerror").html("Name already exists.");
                    $("#edittermerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#edittermerror").html("Some error occured. Error: " + response.result);
                    $("#edittermerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */


    /* Edit Personal Task Form */


    $("#editpersonalform").submit(function (e) {
        e.preventDefault();

        $("#editptbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#editpterror").html("Some error occured. Please try again.");
                $("#editpterror").fadeIn().delay('5000').fadeOut('slow');
                $("#editptbtn").prop("disabled", false);

            },
            success: function (response)
            {
                $("#editptbtn").prop("disabled", false);

                if (response.result === "success") {
                    $("#editptsuccess").html("Task updated succesfully.");
                    $("#editptsuccess").fadeIn();
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else if (response.result === "empty") {
                    $("#editpterror").html("Please fill in Details.");
                    $("#editpterror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#editpterror").html("Some error occured. Error: " + response.result);
                    $("#editpterror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */


    /* Edit Billing Entry Form */


    $("#editbillingform").submit(function (e) {
        e.preventDefault();

        $("#editbtbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#editbterror").html("Some error occured. Please try again.");
                $("#editbterror").fadeIn().delay('5000').fadeOut('slow');
                $("#editbtbtn").prop("disabled", false);

            },
            success: function (response)
            {
                $("#editbtbtn").prop("disabled", false);

                if (response.result === "success") {
                    $("#editbtsuccess").html("Billing Entry updated succesfully.");
                    $("#editbtsuccess").fadeIn();
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else if (response.result === "empty") {
                    $("#editbterror").html("Please fill in Details.");
                    $("#editbterror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#editbterror").html("Some error occured. Error: " + response.result);
                    $("#editbterror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */



    /* Add Term Form */


    $("#addtermform").submit(function (e) {
        e.preventDefault();

        $("#addtermbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#termerror").html("Some error occured. Please try again.");
                $("#termerror").fadeIn().delay('5000').fadeOut('slow');
                $("#addtermbtn").prop("disabled", false);
            },
            success: function (response)
            {
                $("#addtermbtn").prop("disabled", false);
                if (response.result === "success") {
                    $("#termsuccess").html("Term added succesfully.");
                    $("#termsuccess").fadeIn();
                    setTimeout(function () {
                        location.href = "/bpmt/term/";
                    }, 1000);
                } else if (response.result === "empty") {
                    $("#termerror").html("Please fill content.");
                    $("#termerror").fadeIn().delay('5000').fadeOut('slow');
                } else if (response.result === "name") {
                    $("#termerror").html("Name name already exists.");
                    $("#termerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#termerror").html("Some error occured. Error: " + response.result);
                    $("#termerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });



    /*End*/



    /* Edit Client Form */


    $("#editclform").submit(function (e) {
        e.preventDefault();

        $("#editcltn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#editclerror").html("Some error occured. Please try again.");
                $("#editclerror").fadeIn().delay('5000').fadeOut('slow');
                $("#editcltn").prop("disabled", false);

            },
            success: function (response)
            {
                $("#editcltn").prop("disabled", false);

                if (response.result === "success") {
                    $("#editclsuccess").html("Client updated succesfully.");
                    $("#editclsuccess").fadeIn();
                    setTimeout(function () {
                        location.href = "/bpmt/clients/detail/" + response.id;
                    }, 1000);
                } else if (response.result === "email") {
                    $("#editclerror").html("Client with this email already exists.");
                    $("#editclerror").fadeIn();
                } else if (response.result === "empty") {
                    $("#editclerror").html("Please fill in Details.");
                    $("#editclerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#editclerror").html("Some error occured. Error: " + response.result);
                    $("#editclerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */



    /* Add Client Form */


    $("#addclform").submit(function (e) {
        e.preventDefault();
        $("#addcltn").prop("disabled", true);
        $(this).ajaxSubmit({
            error: function () {
                $("#clerror").html("Some error occured. Please try again.");
                $("#clerror").fadeIn().delay('5000').fadeOut('slow');
                $("#addcltn").prop("disabled", false);

            },
            success: function (response) {
                $("#addcltn").prop("disabled", false);

                if (response.result === "success") {

                    $("#clsuccess").html("Client added succesfully.");
                    $("#clsuccess").fadeIn();
                    setTimeout(function () {
                        location.href = "/bpmt/clients/detail/" + response.id;
                    }, 1000);


                } else if (response.result === "email") {
                    $("#clerror").html("Client with this email already exists.");
                    $("#clerror").fadeIn();
                } else if (response.result === "empty") {
                    $("#clerror").html("Please fill in Details.");
                    $("#clerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#clerror").html("Some error occured. Error: " + response.result);
                    $("#clerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */


    /* filter attendance */



    /* End */



    /* Add SEO Stats Form */


    $("#addseoform").submit(function (e) {
        e.preventDefault();
        $("#addpjtn").prop("disabled", true);
        $(this).ajaxSubmit({
            error: function () {
                $("#pjerror").html("Some error occured. Please try again.");
                $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                $("#addpjtn").prop("disabled", false);

            },
            success: function (response) {
                $("#addpjtn").prop("disabled", false);

                if (response.result === "success") {

                    $("#pjsuccess").html("SEO Stats added succesfully.");
                    $("#pjsuccess").fadeIn();
                    // setTimeout(function(){ location.href = "/bpmt/projects/detail/"+response.id ;  }, 1000);
                    setTimeout(function () {
                        location.href = "/bpmt/dashboard/";
                    }, 1000);


                } else if (response.result === "empty") {
                    $("#pjerror").html("Please fill in Details.");
                    $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#pjerror").html("Some error occured. Error: " + response.result);
                    $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */



    /* Edit SEO Stats Form */


    $("#editseoform").submit(function (e) {
        e.preventDefault();
        $("#editpjtn").prop("disabled", true);
        $(this).ajaxSubmit({
            error: function () {
                $("#pjerror").html("Some error occured. Please try again.");
                $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                $("#editpjtn").prop("disabled", false);

            },
            success: function (response) {
                $("#editpjtn").prop("disabled", false);

                if (response.result === "success") {

                    $("#pjsuccess").html("SEO Stats updated succesfully.");
                    $("#pjsuccess").fadeIn();
                    // setTimeout(function(){ location.href = "/bpmt/projects/detail/"+response.id ;  }, 1000);
                    setTimeout(function () {
                        location.href = "/bpmt/dashboard/";
                    }, 1000);


                } else if (response.result === "empty") {
                    $("#pjerror").html("Please fill in Details.");
                    $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#pjerror").html("Some error occured. Error: " + response.result);
                    $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */




    /* Add Team Reviews Form */


    $("#addteamreviewform").submit(function (e) {
        e.preventDefault();

        //alert("HI" + $("#lstAgent2").val());
$("#lstAgent2").val($("#lstAgent").val());

  
        
        if ($("#lstAgent2").val() == "")
        {
            alert("Please select an agent first!");
            return false;
        }




        var x = confirm("Are you sure you want to add the review?");
        if (x)
        {
            $("#addpjtn").prop("disabled", true);
            $(this).ajaxSubmit({
                error: function () {
                    $("#pjerror").html("Some error occured. Please try again.");
                    $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                    $("#addpjtn").prop("disabled", false);

                },
                success: function (response) {
                    $("#addpjtn").prop("disabled", false);

                    if (response.result === "success") {

                        $("#pjsuccess").html("Team Reviews added succesfully.");
                        $("#pjsuccess").fadeIn();
                        // setTimeout(function(){ location.href = "/bpmt/projects/detail/"+response.id ;  }, 1000);
                        setTimeout(function () {
                            location.href = "/bpmt/teamreviews/add/" + response.team;
                        }, 1000);


                    } else if (response.result === "empty") {
                        $("#pjerror").html("Please fill in Details.");
                        $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                    } else {
                        $("#pjerror").html("Some error occured. Error: " + response.result);
                        $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                    }
                }
            });
            return false;
        } else
            return false;




    });

    /* END */































    /* Add Project Form */


    $("#addpjform").submit(function (e) {
        e.preventDefault();
        $("#addpjtn").prop("disabled", true);
        $(this).ajaxSubmit({
            error: function () {
                $("#pjerror").html("Some error occured. Please try again.");
                $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                $("#addpjtn").prop("disabled", false);

            },
            success: function (response) {
                $("#addpjtn").prop("disabled", false);

                if (response.result === "success") {

                    $("#pjsuccess").html("Project added succesfully.");
                    $("#pjsuccess").fadeIn();
                    // setTimeout(function(){ location.href = "/bpmt/projects/detail/"+response.id ;  }, 1000);
                    setTimeout(function () {
                        location.href = "/bpmt/projects/";
                    }, 1000);


                } else if (response.result === "empty") {
                    $("#pjerror").html("Please fill in Details.");
                    $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#pjerror").html("Some error occured. Error: " + response.result);
                    $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */




    /* Edit Project Form */


    $("#editpjform").submit(function (e) {
        e.preventDefault();
        $("#addpjtn").prop("disabled", true);
        $(this).ajaxSubmit({
            error: function () {
                $("#pjerror").html("Some error occured. Please try again.");
                $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                $("#addpjtn").prop("disabled", false);

            },
            success: function (response) {
                $("#addpjtn").prop("disabled", false);

                if (response.result === "success") {

                    $("#pjsuccess").html("Project edited succesfully.");
                    $("#pjsuccess").fadeIn();
                    // setTimeout(function(){ location.href = "/bpmt/projects/detail/"+response.id ;  }, 1000);
                    setTimeout(function () {
                        location.href = "/bpmt/projects/";
                    }, 1000);


                } else if (response.result === "empty") {
                    $("#pjerror").html("Please fill in Details.");
                    $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#pjerror").html("Some error occured. Error: " + response.result);
                    $("#pjerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */


    /* Add Billing Form */


    $("#addblform").submit(function (e) {
        e.preventDefault();

        $("#addbltn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#blerror").html("Some error occured. Please try again.");
                $("#blerror").fadeIn().delay('5000').fadeOut('slow');
                $("#addbltn").prop("disabled", false);
            },
            success: function (response)
            {
                $("#addbltn").prop("disabled", false);
                if (response.result === "success") {
                    $("#blsuccess").html("Billing added succesfully.");
                    $("#blsuccess").fadeIn();
                    setTimeout(function () {
                        location.href = "/bpmt/billing";
                    }, 1000);
                } else if (response.result === "empty") {
                    $("#blerror").html("Please fill in Details.");
                    $("#blerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#blerror").html("Some error occured. Error: " + response.result);
                    $("#blerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });

    /* END */


    /* Add role form submission */


    $("#addroleform").submit(function (e) {
        e.preventDefault();

        $("#addrlbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#rlerror").html("Some error occured. Please try again.");
                $("#rlerror").fadeIn().delay('5000').fadeOut('slow');
                $("#addrlbtn").prop("disabled", false);
            },
            success: function (response)
            {
                $("#addrlbtn").prop("disabled", false);
                if (response.result === "success") {
                    $("#rlsuccess").html("Role added succesfully.");
                    $("#rlsuccess").fadeIn();
                    setTimeout(function () {
                        location.href = "/bpmt/roles";
                    }, 1000);
                } else if (response.result === "empty") {
                    $("#rlerror").html("Please fill in Details.");
                    $("#rlerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#rlerror").html("Some error occured. Error: " + response.result);
                    $("#rlerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });



    /* END */


    /* Add Team form submission */


    $("#addteamform").submit(function (e) {
        e.preventDefault();

        $("#addtmbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#tmerror").html("Some error occured. Please try again.");
                $("#tmerror").fadeIn().delay('5000').fadeOut('slow');
                $("#addtmbtn").prop("disabled", false);
            },
            success: function (response)
            {
                $("#addtmbtn").prop("disabled", false);
                if (response.result === "success") {
                    $("#tmsuccess").html("Team added succesfully.");
                    $("#tmsuccess").fadeIn();
                    setTimeout(function () {
                        location.href = "/bpmt/team";
                    }, 1000);
                } else if (response.result === "empty") {
                    $("#tmerror").html("Please fill in Details.");
                    $("#tmerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#tmerror").html("Some error occured. Error: " + response.result);
                    $("#tmerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });



    /* END */


    /* Update Team form submission */


    $("#editteamform").submit(function (e) {
        e.preventDefault();

        $("#edittmbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#edittmerror").html("Some error occured. Please try again.");
                $("#edittmerror").fadeIn().delay('5000').fadeOut('slow');
                $("#edittmbtn").prop("disabled", false);
            },
            success: function (response)
            {
                $("#edittmbtn").prop("disabled", false);
                if (response.result === "success") {
                    $("#edittmsuccess").html("Team updated succesfully.");
                    $("#edittmsuccess").fadeIn();
                    setTimeout(function () {
                        location.href = "/bpmt/team";
                    }, 1000);
                } else if (response.result === "empty") {
                    $("#edittmerror").html("Please fill in Details.");
                    $("#edittmerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#edittmerror").html("Some error occured. Error: " + response.result);
                    $("#edittmerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });


    $('.edit').click(function () {
        var tid = $(this).data('id');

        var refscrf = $("input[name='_csrfToken']").val();
        $.ajax({
            url: "/bpmt/team/update",
            data: {"tid": tid, _csrfToken: refscrf},
            type: 'post', success: function (result)
            {
                if (result) {
                    $('#team_name').val(result.name)
                    $('#team_tid').val(result.id)
                }
            }
        });
    });



    /* END */





    /* Add personal task form submission */


    $("#addpersonalform").submit(function (e) {
        e.preventDefault();

        $("#addptbtn").prop("disabled", true);

        $(this).ajaxSubmit({
            error: function () {
                $("#ptaskerror").html("Some error occured. Please try again.");
                $("#ptaskerror").fadeIn().delay('5000').fadeOut('slow');
                $("#addptbtn").prop("disabled", false);
            },
            success: function (response)
            {
                $("#addptbtn").prop("disabled", false);
                if (response.result === "success") {
                    $("#ptasksuccess").html("Task added succesfully.");
                    $("#ptasksuccess").fadeIn();
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                } else if (response.result === "empty") {
                    $("#ptaskerror").html("Please fill in Details.");
                    $("#ptaskerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#ptaskerror").html("Some error occured. Error: " + response.result);
                    $("#ptaskerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });


    /* END */

    /* Load Personal Task */
    $('.datatable').dataTable({
        dom: 'Bfrtip',
        buttons: [{
                extend: 'excelHtml5',
                text: 'Export in Excel'

            }]
    });


    $('.datatable_projects').dataTable({
        "paging": false,
        "info": false,

    });



    $('.attendancetable').dataTable({
        dom: 'Bfrtip',
        scrollX: true,
        buttons: [{
                extend: 'excelHtml5',
                text: 'Export in Excel'

            }]
    });

    $('.task_table').dataTable({
        "paging": false,
        "info": false,
        scrollX: true,
        dom: 'Bfrtip',
        buttons: [{
                extend: 'excelHtml5',
                text: 'Export in Excel'

            }],
        'columnDefs': [
            {'sortable': false, 'targets': 0}
        ]
    });

    $('.billinglisttable').dataTable({
        scrollX: true,
        paging: false,
        order: [[1, "asc"]],
        dom: 'Bfrtip',
        buttons: [{
                extend: 'excelHtml5',
                text: 'Export in Excel'

            }]
    });


    $('.btask_table').dataTable({
        scrollX: true,
        dom: 'Bfrtip',
        buttons: [{
                extend: 'excelHtml5',
                text: 'Export in Excel'

            }],

        "footerCallback": function (row, data, start, end, display) {
            var api = this.api(), data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                        i : i;
            };

            // Total over all pages
            total = api
                    .column(7)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

            // Total over this page
            pageTotal = api
                    .column(7, {page: 'current'})
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

            // Update footer
            $(api.column(7).footer()).html(
                    'Total Billed: ' + total.toFixed(2) + ' hrs'
                    );
        }

    });


    /* END */



    /* Client Choose Script */

    $(".existing").change(function () {
        $(".existing").each(function () {
            if ($(this).is(":checked")) {
                if ($(this).val() == "yes") {
                    $("#clientlist").show();
                    $("#reference").val('');
                    $("#client").prop('required', true);
                    $("#reference").prop('required', false);
                    $("#referencebox").hide();
                } else if ($(this).val() == "no") {
                    $("#clientlist").hide();
                    $("#client").val('');
                    $("#client").prop('required', false);
                    $("#reference").prop('required', true);
                    $("#referencebox").show();
                }
            }
        })

    })

    $(".eexisting").change(function () {
        $(".eexisting").each(function () {
            if ($(this).is(":checked")) {
                if ($(this).val() == "yes") {
                    $("#eclientlist").show();
                    $("#ereference").val('');
                    $("#eclient").prop('required', true);
                    $("#ereference").prop('required', false);
                    $("#ereferencebox").hide();
                } else if ($(this).val() == "no") {
                    $("#eclientlist").hide();
                    $("#eclient").val('');
                    $("#eclient").prop('required', false);
                    $("#ereference").prop('required', true);
                    $("#ereferencebox").show();
                }
            }
        })

    })

    /* END */



    /* Share term form submission */


    $("#sharetermform").submit(function (e) {
        e.preventDefault();
        $("#sharetermbtn").prop("disabled", true);
        $(this).ajaxSubmit({
            error: function () {
                $("#termerror").html("Some error occured. Please try again.");
                $("#termerror").fadeIn().delay('5000').fadeOut('slow');
                $("#sharetermbtn").prop("disabled", false);

            },
            success: function (response) {
                $("#sharetermbtn").prop("disabled", false);

                if (response.result === "success") {

                    $("#termsuccess").html("Terms share succesfully.");
                    $("#termsuccess").fadeIn();
                    setTimeout(function () {
                        location.href = "/bpmt/term/";
                    }, 1000);


                } else if (response.result === "failed") {
                    $("#termerror").html("Please fill in Details.");
                    $("#termerror").fadeIn().delay('5000').fadeOut('slow');
                } else if (response.result === "empty") {
                    $("#termerror").html("Kindly select values.");
                    $("#termerror").fadeIn().delay('5000').fadeOut('slow');
                } else if (response.result === "exist") {
                    $("#termerror").html("Already exist.");
                    $("#termerror").fadeIn().delay('5000').fadeOut('slow');
                } else if (response.result === "error") {
                    $("#termerror").html("Error from youe end.");
                    $("#termerror").fadeIn().delay('5000').fadeOut('slow');
                } else {
                    $("#termerror").html("Some error occured. Error: " + response.result);
                    $("#termerror").fadeIn().delay('5000').fadeOut('slow');
                }
            }
        });
        return false;

    });



    $('.shareterm').click(function () {
        var id = $(this).data('id');
        $('#id').val(id);

    });


    /* END */





    /* General Redirect */

    $(".redirectbtn").click(function () {
        var loc = $(this).attr('data-href');
        var cat_fil = $("#categoryfilter").val();
        var date_fil = $("#filterbydate").val();
        var qagent = $("#qagentfilter").val();
        var cond = "";
        if (cat_fil) {
            cond = "cat_id=" + cat_fil;
        }

        if (date_fil) {
            cond += "&completedon=" + date_fil;
        }

        if (qagent) {
            cond += "&quser=" + qagent;
        }


        location.href = loc + "?" + cond;


    });

    /* END */

    $(document).on("keypress", ".numberonly", function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    });

    $(document).on("keypress", ".decimals", function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && charCode != 46 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    });


    $("#eagent").change(function () {
        var val = $("#eagent").val();
        if (val == "") {
            $("#eestimate").prop("required", false);
        } else {
            $("#eestimate").prop("required", true);

        }
    })


    $("#agent").change(function () {
        var val = $("#agent").val();
        if (val == "") {
            $("#estimate").prop("required", false);
        } else {
            $("#estimate").prop("required", true);

        }
    })



    $("#owner").change(function () {
        var val = $("#owner").val();
        if (val == "") {
            $("#duedate").prop("required", false);
        } else {
            $("#duedate").prop("required", true);

        }
    })


    $("#eowner").change(function () {
        var val = $("#eowner").val();
        if (val == "") {
            $("#eduedate").prop("required", false);
        } else {
            $("#eduedate").prop("required", true);

        }
    })


    $("#client").change(function () {
        var val = $(this).val();
        $("#projectlist").hide();
        // alert(val);
        if (val != "") {
            $("#taskproject").html('<option>Fetching Project</option>');
            $("#taskproject").prop('disabled', true);

            var refscrf = $("input[name='_csrfToken']").val();
            $.ajax({
                data: {
                    val: val,
                    _csrfToken: refscrf
                },
                type: "post",
                url: "/bpmt/taskboard/getprojects",
                error: function () {
                    alert("Some error occured. Please re-select");
                    $("#client").val('');
                },
                success: function (response) {
                    if (response.result == "success") {
                        $("#taskproject").prop('disabled', false);

                        if (response.data == null) {
                            $("#projectlist").hide();
                            $("#taskproject").prop('required', false);
                            $("#client").val('');
                            alert('No Project found for the client');
                        } else {

                            $("#projectlist").show();
                            $("#taskproject").prop('required', true);
                            $("#taskproject").html(response.data);
                        }

                    }
                }
            })
        }

    })




    $("#taskproject").change(function () {
        var val = $(this).val();
        $("#commentbox").hide();
        $("#comment").prop('required', false);

        // alert(val);
        if (val != "") {

            var refscrf = $("input[name='_csrfToken']").val();
            $.ajax({
                data: {
                    val: val,
                    _csrfToken: refscrf
                },
                type: "post",
                url: "/bpmt/taskboard/getprojectshours",
                error: function () {
                    alert("Some error occured. Please re-select");
                    $("#taskproject").val('');
                },
                success: function (response) {
                    if (response.result == "success") {

                        if (response.hours == "no") {
                            alert("Warning: This Project has zero/negative balance hours and is not allowed to work anymore. Please contact administrartor.");
                            $("#taskproject").val('');
                        } else if (response.hours <= 0) {
                            alert("Warning: This Project has zero/negative balance hours. Please add comments to continue.");
                            $("#commentbox").show();
                            $("#comment").prop('required', true);
                        }



                    }
                }
            })
        }

    })


    $("#eclient").change(function () {
        var val = $(this).val();
        $("#eprojectlist").hide();
        // alert(val);
        if (val != "") {
            $("#eproject").html('<option>Fetching Project</option>');
            $("#eproject").prop('disabled', true);

            var refscrf = $("input[name='_csrfToken']").val();
            $.ajax({
                data: {
                    val: val,
                    _csrfToken: refscrf
                },
                type: "post",
                url: "/bpmt/taskboard/getprojects",
                error: function () {
                    alert("Some error occured. Please re-select");
                    $("#eclient").val('');
                },
                success: function (response) {
                    if (response.result == "success") {
                        $("#eproject").prop('disabled', false);

                        if (response.data == null) {
                            $("#eprojectlist").hide();
                            $("#eproject").prop('required', false);
                            $("#eclient").val('');
                            alert('No Project found for the client');
                        } else {
                            $("#eprojectlist").show();
                            $("#eproject").prop('required', true);
                            $("#eproject").html(response.data);
                        }

                    }
                }
            })
        }

    })



});


$(".qagentfilter").change(function () {
    var val = $(this).val();
    if (val != "") {
        var cat_val = $(".categoryfilter").val();
        var completed_val = $(".completedfilter").val();

        if (cat_val == "" && completed_val == "") {
            location.href = val;
        } else {
            location.href = "?quser=" + val + "&cat_id=" + cat_val + "&completedon=" + completed_val;
        }
    }
})


$(".agentfilter").change(function () {
    var val = $(this).val();
    if (val != "") {
        var cat_val = $(".categoryfilter").val();
        var completed_val = $(".completedfilter").val();
        var qagent = $(".qagentfilter").val();
        location.href = val + "?quser=" + qagent + "&cat_id=" + cat_val + "&completedon=" + completed_val;

    }
})


$(".categoryfilter").change(function () {
    var val = $(this).val();
    if (val != "") {
        var completed_val = $(".completedfilter").val();
        var billed_val = $(".billedfilter").val();
        var qagent = $(".qagentfilter").val();

        location.href = "?quser=" + qagent + "&cat_id=" + val + "&completedon=" + completed_val;
    }
})


$('.billedfilter').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');
    var val = $(this).val();

    var cat_val = $(".categoryfilter").val();
    if (!cat_val) {
        cat_val = '';
    }
    var completed_val = $(".completedfilter").val();
    if (!completed_val) {
        var completed_val = '';
    }

    location.href = "?billedon=" + val + "&completedon=" + completed_val + "&cat_id=" + cat_val;



});


$('.completedfilter').on('cancel.daterangepicker', function (ev, picker) {
    $(this).val('');

    var val = $(this).val();
    var cat_val = $(".categoryfilter").val();
    if (!cat_val) {
        cat_val = '';
    }
    var billed_val = $(".billedfilter").val();
    if (!billed_val) {
        billed_val = '';
    }


    var quser = $(".qagentfilter").val();
    if (!quser) {
        var quser = '';
    }

    location.href = "?quser=" + quser + "&completedon=" + val + "&cat_id=" + cat_val;

});


//do something, like clearing an input
$('.completedfilter').on('apply.daterangepicker', function (ev, picker) {
    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));


    var val = $(this).val();
    if (val != "") {
        var cat_val = $(".categoryfilter").val();
        if (!cat_val) {
            cat_val = '';
        }
        var billed_val = $(".billedfilter").val();
        if (!billed_val) {
            billed_val = '';
        }
        location.href = "?billedon=" + billed_val + "&completedon=" + val + "&cat_id=" + cat_val;
    }
})

$('.billedfilter').on('apply.daterangepicker', function (ev, picker) {
    //do something, like clearing an input
    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
    var val = $(this).val();
    if (val != "") {
        var cat_val = $(".categoryfilter").val();
        if (!cat_val) {
            cat_val = '';
        }
        var completed_val = $(".completedfilter").val();
        if (!completed_val) {
            var completed_val = '';
        }

        location.href = "?billedon=" + val + "&completedon=" + completed_val + "&cat_id=" + cat_val;
    }

})


/* Update attendance form submission */


$("#updateattform").submit(function (e) {
    e.preventDefault();
    $("#addattbtn").prop("disabled", true);
    $(this).ajaxSubmit({
        error: function () {
            $("#atterror").html("Some error occured. Please try again.");
            $("#atterror").fadeIn().delay('5000').fadeOut('slow');
            $("#addattbtn").prop("disabled", false);

        },
        success: function (response) {
            $("#addattbtn").prop("disabled", false);

            if (response.result === "success") {

                $("#pjsuccess").html("Attendace updated succesfully.");
                $("#pjsuccess").fadeIn();
                setTimeout(function () {
                    location.href = "/bpmt/attendance/";
                }, 1000);


            } else if (response.result === "failed") {
                $("#atterror").html("Please fill in Details.");
                $("#atterror").fadeIn().delay('5000').fadeOut('slow');
            } else {
                $("#atterror").html("Some error occured. Error: " + response.result);
                $("#atterror").fadeIn().delay('5000').fadeOut('slow');
            }
        }
    });
    return false;

});


/* END */



/* Add attendance form submission */


$("#addattform").submit(function (e) {
    e.preventDefault();
    $("#addattbtn").prop("disabled", true);
    $(this).ajaxSubmit({
        error: function () {
            $("#atterror").html("Some error occured. Please try again.");
            $("#atterror").fadeIn().delay('5000').fadeOut('slow');
            $("#addattbtn").prop("disabled", false);

        },
        success: function (response) {
            $("#addattbtn").prop("disabled", false);

            if (response.result === "success") {

                $("#pjsuccess").html("Attendace insert succesfully.");
                $("#pjsuccess").fadeIn();
                setTimeout(function () {
                    location.href = "/bpmt/attendance/";
                }, 1000);


            } else if (response.result === "failed") {
                $("#atterror").html("Please fill in Details.");
                $("#atterror").fadeIn().delay('5000').fadeOut('slow');

            } else if (response.result === "exist") {
                $("#atterror").html("Attendace already exist.");
                $("#atterror").fadeIn().delay('5000').fadeOut('slow');

            } else {
                $("#atterror").html("Some error occured. Error: " + response.result);
                $("#atterror").fadeIn().delay('5000').fadeOut('slow');
            }
        }
    });
    return false;

});


/* END */

/* Time picker */

//     $(document).ready(function(){
//     $('.timepicker').timepicker({
//     timeFormat: 'h:mm p',
//     interval: 60,
//     minTime: '9',
//     maxTime: '7:00pm',
//     startTime: '10:00',
//     dynamic: true,
//     dropdown: false,
//     scrollbar: true
//     });
// }); 

/* End */

$("#selectall_billing").change(function () {
    if ($(this).is(":checked")) {
        $(".bulksend").prop("checked", true);
    } else {
        $(".bulksend").prop("checked", false);
    }
})


$("#selectall_bill").change(function () {
    if ($(this).is(":checked")) {
        $(".bulkbill").prop("checked", true);
    } else {
        $(".bulkbill").prop("checked", false);
    }
})


/*Send number value in controller for search data date wise */


$('.change_record li').on('click', function ()
{
    //var id = $(this).data('id');
    window.location = '/bpmt/dashboard/index/' + $(this).data('id');
});



/*End*/

