$(function () {
    $('.js-sweetalert').on('click', function () {
        var type = $(this).data('type');
        var id =  $(this).data("id") ;
        var url =  $(this).data("url") ;
        var str =  $(this).data("str") ;
        var geturl = $(this).data("get") ;
        if (type === 'basic') {
            showBasicMessage();
        }
        else if (type === 'with-title') {
            showWithTitleMessage();
        }
        else if (type === 'success') {
            showSuccessMessage();
        }
        else if (type === 'confirm') {
            showConfirmMessage(id,url,str);
        }
        else if (type === 'confirmactivate') {
            showConfirmMessage_activated(id,url,str);
        }
        else if (type === 'confirmdeactivate') {
            showConfirmMessage_deactivated(id,url,str);
        }
        
        else if (type === 'cancel') {
            showCancelMessage();
        }
        else if (type === 'with-custom-icon') {
            showWithCustomIconMessage();
        }
        
        else if (type === 'hoursdetail') {
            showhoursdetail(id,url,str);
        }
        
        else if (type === 'editpersonaltask') {
            showeditpersonaltask(id,url,str);
        }
        else if (type === 'editbilling') {
            showeditbilling(id,url,str);
        }
        
        else if (type === 'assignestimated') {
            assignestimated(id,url,str,geturl);
        }
        else if (type === 'assignqualitytask') {
            showTaskAssignQuality(id,url,str,geturl);
        }

        else if (type === 'assigntask') {
            showTaskAssign(id,url,str,geturl);
        }
        
        else if (type === 'movetaskquality') {
            showTaskTransferQuality(id,url,str);
        }

        else if (type === 'movetask') {
            showTaskTransfer(id,url,str);
        }
        else if (type === 'taskqualityrework') {
            taskqualityrework(id,url,str);
        }
        else if (type === 'completetaskquality') {
            completetaskquality(id,url,str);
        }
        else if (type === 'completetaskqualityrework') {
            completetaskqualityrework(id,url,str);
        }
        
        else if (type === 'completetask') {
            showTaskComplete(id,url,str);
        }

        else if (type === 'commenttask') {
            showcommenttask(id,url,str);
        }
        
        else if (type === 'bulksendbillingtask') {
            bulksendbillingtask(id,url,str);
        }
        else if (type === 'sendbillingtask') {
            sendbillingtask(id,url,geturl,str);
        }
        else if (type === 'bulkapprovebillingtask') {
            bulkapprovebillingtask(id,url,str);
        }
        else if (type === 'approvebillingtask') {
            approvebillingtask(id,url,geturl,str);
        }
        else if (type === 'html-message') {
            showHtmlMessage();
        }
        else if (type === 'autoclose-timer') {
            showAutoCloseTimerMessage();
        }
       
        else if (type === 'ajax-loader') {
            showAjaxLoaderMessage();
        }
    });
});

function approvebillingtask(id,url,geturl,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;
    $.ajax({
        data : {
            val : id ,
           _csrfToken : refscrf
        },
        type : "post",
        url : "/bpmt/"+geturl,
        success: function(response){
            if(response.result == "success"){
                var taskdetail = response.task ;
                setTimeout(function(){
                   $("#commenttext").val(taskdetail.comment);
                   $("#spenthour").val(taskdetail.billed);
                }, 1000);
              var htmltext = $("#approvebillingbox").html();
              if(htmltext == ""){
                alert("Oops! Some error occured. Please reload.");
                return false;
            }
        
                $("#approvebillingbox").html('');
                swal({
                    title: "Approve Billing!",
                    text: htmltext,
                    html: true,
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    onOpen: function(){
                        
                    }
                }, function (isConfirm) {
                    if (isConfirm) {
                        var comment =  $("#commenttext").val();
                        var spent =  $("#spenthour").val();
                        if(comment == '' || spent == '' ){
                                if(comment == ""){
                                    $("#completeerror").html("Please write comment.");
                                    $("#completeerror").fadeIn().delay(5000).fadeOut();
                                    swal("Error!", response1, "error");
                                    return false
                                }
                                else if(spent == '' ){
                                    $("#completeerror").html("Please enetr your spent hours.");
                                    $("#completeerror").fadeIn().delay(5000).fadeOut();
                                    swal("Error!", response1, "error");
                                    return false
                                }
                                
                        }
                    
                    $.ajax({
                        data : {
                            val : id ,
                            comment: comment,
                            spent: spent,
                            _csrfToken : refscrf
                        },
                        type : "post",
                        url : "/bpmt/"+url,
                        success: function(response){
                            if(response.result == "success"){
                                swal("Billed!", str+" has been billed.", "success");
                                setTimeout(function(){ location.reload() ;  }, 1000);
                            }
                            else{
                                swal("Error!", response, "error");
                            }
                        }
                    })
                
            }else{
                $("#approvebillingbox").html(htmltext)
            }
                });

            }
            else{
                swal("Error!", response, "error");
            }
        }
    })
 
}



function showTaskAssignQuality(id,url,str,geturl){
    var refscrf = $("input[name='_csrfToken']").val() ;
    var htmltext = $("#assigntaskbox").html();
    if(htmltext == ""){
        alert("Oops! Some error occured. Please reload.");
        return false;
    }
    $("#assigntaskbox").html('');
    swal({
        title: "Assign Task!",
        text: htmltext,
        html: true,
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        onOpen: function(){
            
        }
    }, function (isConfirm) {
        if (isConfirm) {
            var assigned =  $("#assagent").val();
            
                if(assigned == '' ){
                    $("#assignerror").html("Please choose an agent.");
                    $("#assignerror").fadeIn().delay(5000).fadeOut();
                    swal("Error!", response1, "error");
                    return false
             }
                    $.ajax({
                        data : {
                            val : id ,
                            assigned: assigned,
                            _csrfToken : refscrf
                        },
                        type : "post",
                        url : "/bpmt/"+url,
                        success: function(response){
                            if(response.result == "success"){
                                swal("Assigned!", str+" has been assigned.", "success");
                                setTimeout(function(){ location.reload() ;  }, 1000);
                            }
                            else{
                                swal("Error!", response, "error");
                            }
                        }
                    })
                              
    
}else{
    $("#assigntaskbox").html(htmltext)
}

}
);
}


function showTaskAssign(id,url,str,geturl){
    var refscrf = $("input[name='_csrfToken']").val() ;
    $("#acommentbox").hide();
    $("#acomment").prop('required' , false );
    $.ajax({
        data : {
            val : id ,
           _csrfToken : refscrf
        },
        type : "post",
        url : "/bpmt/"+geturl,
        success: function(response){
            if(response.result == "success"){
                var taskdetail = response.task ;
                setTimeout(function(){
                   $("#estimatehour").val(taskdetail.estimate);
                   if(response.hours == "no"){
                    swal("Error!", "Can't assign task for the project due to negative balance. Contact administrator.", "error");
                    return false;
                    
                   }else if(response.hours < 0){
                    // alert(response.hours);
                    $("#acommentbox").show();
                    $("#acomment").prop('required' , true );
                }
                }, 1000);
    var htmltext = $("#assigntaskbox").html();
    if(htmltext == ""){
        alert("Oops! Some error occured. Please reload.");
        return false;
    }
    $("#assigntaskbox").html('');
    // alert(response.hours);
   
    swal({
        title: "Assign Task!",
        text: htmltext,
        html: true,
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
        onOpen: function(){
            
        }
    }, function (isConfirm) {
        if (isConfirm) {
            var assigned =  $("#assagent").val();
            var estimatehour =  $("#estimatehour").val();
            var comment = $("#acomment").val();
            if(assigned == '' || estimatehour == '' || estimatehour == '' ){
                    if(assigned == '' ){
                        $("#assignerror").html("Please choose an agent.");
                        $("#assignerror").fadeIn().delay(5000).fadeOut();
                        swal("Error!", response1, "error");
                        return false
                    }
                    else if(estimatehour == '' || estimatehour == 0 ){
                        $("#assignerror").html("Please enter estimate time greater that 0.");
                        $("#assignerror").fadeIn().delay(5000).fadeOut();
                        swal("Error!", response1, "error");
                        return false
                    }

            }
        
        $.ajax({
            data : {
                val : id ,
                assigned: assigned,
                estimate: estimatehour,
                comment:comment,
                _csrfToken : refscrf
            },
            type : "post",
            url : "/bpmt/"+url,
            success: function(response){
                if(response.result == "success"){
                    swal("Assigned!", str+" has been assigned.", "success");
                    setTimeout(function(){ location.reload() ;  }, 1000);
                }
                else{
                    swal("Error!", response, "error");
                }
            }
        })
    
}else{
    $("#assigntaskbox").html(htmltext)
}
    });

}
        }
    })
}



function bulkapprovebillingtask(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;
    var arr = [];
    $(".bulkbill").each(function(){
        if($(this).is(':checked') ){
            arr.push($(this).val());
        }
    })

    if(!arr.length){
        alert("Please select at least one task");
        return false;
    }

           swal({
                    title: "Approve Billing!!",
                    text: "Are you sure you want to bulk bill these tasks ?",
                    html: true,
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    
                }, function (isConfirm) {
                    if (isConfirm) {
                     
                    $.ajax({
                        data : {
                            val : arr ,
                            _csrfToken : refscrf
                        },
                        type : "post",
                        url : "/bpmt/"+url,
                        success: function(response){
                            if(response.result == "success"){
                                swal("Billed!", str+" has been billed.", "success");
                                setTimeout(function(){ location.reload() ;  }, 1000);
                            }
                            else{
                                swal("Error!", response.result, "error");
                            }
                        }
                    })
                }
                
       
                });

}


function bulksendbillingtask(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;
    var arr = [];
    var billtime = [] ;
    $(".bulksend").each(function(){
        if($(this).is(':checked') ){
            arr.push($(this).val());
            billtime.push($(this).attr('data-time'));
        }
    })

    if(!arr.length){
        alert("Please select at least one task");
        return false;
    }

           swal({
                    title: "Send to Billing!",
                    text: "Are you sure you want to bulk send these tasks ?",
                    html: true,
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    
                }, function (isConfirm) {
                    if (isConfirm) {
                     
                    $.ajax({
                        data : {
                            val : arr ,
                            billtime : billtime,
                            _csrfToken : refscrf
                        },
                        type : "post",
                        url : "/bpmt/"+url,
                        success: function(response){
                            if(response.result == "success"){
                                swal("Sent!", str+" has been sent for billing.", "success");
                                setTimeout(function(){ location.reload() ;  }, 1000);
                            }
                            else{
                                swal("Error!", response.result, "error");
                            }
                        }
                    })
                }
                
       
                });

}



function sendbillingtask(id,url,geturl,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;
    $.ajax({
        data : {
            val : id ,
           _csrfToken : refscrf
        },
        type : "post",
        url : "/bpmt/"+geturl,
        success: function(response){
            if(response.result == "success"){
                var taskdetail = response.task ;
                setTimeout(function(){
                   $("#commenttext").val(taskdetail.comment);
                   $("#spenthour").val(taskdetail.spent);
                }, 1000);
              var htmltext = $("#sendbillingbox").html();
              if(htmltext == ""){
                alert("Oops! Some error occured. Please reload.");
                return false;
            }
        
                $("#sendbillingbox").html('');
                swal({
                    title: "Send to Billing!",
                    text: htmltext,
                    html: true,
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    onOpen: function(){
                        
                    }
                }, function (isConfirm) {
                    if (isConfirm) {
                        var comment =  $("#commenttext").val();
                        var spent =  $("#spenthour").val();
                        if(comment == '' || spent == '' ){
                                if(comment == ""){
                                    $("#completeerror").html("Please write comment.");
                                    $("#completeerror").fadeIn().delay(5000).fadeOut();
                                    swal("Error!", response.result, "error");
                                    return false
                                }
                                else if(spent == '' ){
                                    $("#completeerror").html("Please enetr your spent hours.");
                                    $("#completeerror").fadeIn().delay(5000).fadeOut();
                                    swal("Error!", response.result, "error");
                                    return false
                                }
                                
                        }
                    
                    $.ajax({
                        data : {
                            val : id ,
                            comment: comment,
                            spent: spent,
                            _csrfToken : refscrf
                        },
                        type : "post",
                        url : "/bpmt/"+url,
                        success: function(response){
                            if(response.result == "success"){
                                swal("Sent!", str+" has been sent for billing.", "success");
                                setTimeout(function(){ location.reload() ;  }, 1000);
                            }
                            else{
                                swal("Error!", response.result, "error");
                            }
                        }
                    })
                
            }else{
                $("#sendbillingbox").html(htmltext)
            }
                });

            }
            else{
                swal("Error!", response.result, "error");
            }
        }
    })
 
}

function showeditpersonaltask(id,url,str){
    var refscrf = $("input[name='_csrfToken']").val() ;
    $("#ecommentbox").hide();
    $("#ecomment").prop("required" , false );
    $.ajax({
        data : {
            val : id ,
           _csrfToken : refscrf
        },
        type : "post",
        url : "/bpmt/"+url,
        success: function(response){
            if(response.result == "success"){
                var task = response.task ;
                $("#ecategory").val(task.category); 
                $("#etitle").val(task.tittle); 
                $("#edescription").html(task.description); 
                if(task.duedate != ""){
                    $("#eduedate").datepicker({
                        autoclose: true
                      });
                    $("#eduedate").datepicker('setDate', task.duedate); 
                }
                $("#eagent").val(task.assigned); 
                $("#eestimate").val(task.estimate);
                $("#eowner").val(task.owner)
                if(response.hours < 0){
                    $("#ecommentbox").show();
                    $("#ecomment").prop("required" , true );
                }
                if(task.owner == ""){
                    $("#eduedate").prop("required" , false );
                  }
                  else{
                    $("#eduedate").prop("required" , true );
                
                  }
                  if(task.assigned == ""){
                    $("#eduedate").prop("required" , false );
                  }
                  else{
                    $("#eduedate").prop("required" , true );
                
                  }
 
                $(".eexisting").each(function() {
                    if($(this).val() == task.reftype){
                        $(this).prop('checked',true)
                    }

                }); 
                if(task.reftype == "yes"){
                    $("#ereferencebox").hide();
                    $("#eclient").val(task.client); 
                    $.ajax({
                        data : {
                          val : task.client,
                          _csrfToken : refscrf
                        },
                        type: "post",
                        url: "/bpmt/taskboard/getprojects",
                        error: function(){
                          alert("Some error occured. Please re-select");
                          $("#eclient").val('');
                        },
                        success: function(response){
                            if(response.result == "success"){
                              $("#eproject").prop('disabled' , false );
                  
                              if(response.data == null){
                                $("#eprojectlist").hide();
                                $("#eproject").prop('required' , false );
                                $("#eclient").val('');
                                alert('No Project found for the client');
                              }
                              else{
                                $("#eprojectlist").show();
                                $("#eproject").prop('required' , true );
                                $("#eproject").html(response.data);
                                $("#eproject").val(task.project);
                              }
                  
                            }
                        }
                      })
                    $("#eclientlist").show();

                }
                else{
                    $("#ereference").val(task.reference);
                    $("#ereferencebox").show();
                    $("#eclientlist").hide();
                }

                $("#vista").val(id) 
                $('#edittask').modal('show');

            }
            else{
                swal("Error!", response.result, "error");
            }
        }
    })
}
function showeditbilling(id,url,str){
    var refscrf = $("input[name='_csrfToken']").val() ;
    $.ajax({
        data : {
            val : id ,
           _csrfToken : refscrf
        },
        type : "post",
        url : "/bpmt/"+url,
        success: function(response){
            if(response.result == "success"){
                var billing = response.billing ;
                $("#bproject").val(billing.pid); 
                $("#bpaid").val(billing.hours); 
                $("#remark").val(billing.remark); 
                if(billing.next_payment != ""){
                    $("#next_payment").datepicker({
                        autoclose: true
                      });
                    $("#next_payment").datepicker('setDate', billing.next_payment); 
                }
                if(billing.billing_date != ""){
                    $("#billing_date").datepicker({
                        autoclose: true
                      });
                    $("#billing_date").datepicker('setDate', billing.billing_date); 
                }
                 

                $("#vista").val(id) 
                $('#editbilling').modal('show');

            }
            else{
                swal("Error!", response.result, "error");
            }
        }
    })
}





function assignestimated(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;
    var htmltext = $("#assignestimated").html();
    if(htmltext == ""){
                    alert("Oops! Some error occured. Please reload.");
                    return false;
                }
            
                $("#assignestimated").html('');
                swal({
                    title: "Assign estimate for rework!",
                    text: htmltext,
                    html: true,
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    onOpen: function(){
                        
                    }
                }, function (isConfirm) {
                    if (isConfirm) {
                        var estimatehour =  $("#reworkestimatehour").val();
                        var repeatval = '' ;
                        if(estimatehour == ''){
                                    swal("Error!", "Please add estimate hour.", "error");
                                    return false
                        }
                    
                    $.ajax({
                        data : {
                            val : id ,
                            estimate: estimatehour,
                           _csrfToken : refscrf
                        },
                        type : "post",
                        url : "/bpmt/"+url,
                        success: function(response){
                            if(response.result == "success"){
                                swal("Assigned!", str+" has been assigned with estimate.", "success");
                                setTimeout(function(){ location.reload() ;  }, 1000);
                            }
                            else{
                                swal("Error!", response.result, "error");
                            }
                        }
                    })
                
            }else{
                $("#assignestimated").html(htmltext)
            }
                });


}



function completetaskqualityrework(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;
    var htmltext = $("#completetaskquality").html();
    if(htmltext == ""){
                    alert("Oops! Some error occured. Please reload.");
                    return false;
                }
            
                $("#completetaskquality").html('');
                swal({
                    title: "Send for retesting!",
                    text: htmltext,
                    html: true,
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    onOpen: function(){
                        
                    }
                }, function (isConfirm) {
                    if (isConfirm) {
                        var comment =  $("#qcommenttext").val();
                        var spent =  $("#qspenthour").val();
                        if(comment == ''){
                                    $("#reworkerror").html("Please write comment.");
                                    $("#reworkerror").fadeIn().delay(5000).fadeOut();
                                    swal("Error!", response1, "error");
                                    return false
                        }
                    
                    $.ajax({
                        data : {
                            val : id ,
                            comment: comment,
                            spent: spent,
                           _csrfToken : refscrf
                        },
                        type : "post",
                        url : "/bpmt/"+url,
                        success: function(response){
                            if(response.result == "success"){
                                swal("Sent!", str+" has been sent for rewtest.", "success");
                                setTimeout(function(){ location.reload() ;  }, 1000);
                            }
                            else{
                                swal("Error!", response.result, "error");
                            }
                        }
                    })
                
            }else{
                $("#reowrkbox").html(htmltext)
            }
                });


}

function taskqualityrework(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;
    var htmltext = $("#reowrkbox").html();
    if(htmltext == ""){
                    alert("Oops! Some error occured. Please reload.");
                    return false;
                }
            
                $("#reowrkbox").html('');
                swal({
                    title: "Send for rework!",
                    text: htmltext,
                    html: true,
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    onOpen: function(){
                        
                    }
                }, function (isConfirm) {
                    if (isConfirm) {
                        var comment =  $("#recommenttext").val();
                        var repeatval = '' ;
                        if(comment == ''){
                                    $("#reworkerror").html("Please write comment.");
                                    $("#reworkerror").fadeIn().delay(5000).fadeOut();
                                    swal("Error!", response1, "error");
                                    return false
                        }
                     var bugs = $("#reworkbug").val();
                    
                    $.ajax({
                        data : {
                            val : id ,
                            comment: comment,
                            bugs: bugs,
                           _csrfToken : refscrf
                        },
                        type : "post",
                        url : "/bpmt/"+url,
                        success: function(response){
                            if(response.result == "success"){
                                swal("Sent!", str+" has been sent for rework.", "success");
                                setTimeout(function(){ location.reload() ;  }, 1000);
                            }
                            else{
                                swal("Error!", response.result, "error");
                            }
                        }
                    })
                
            }else{
                $("#reowrkbox").html(htmltext)
            }
                });


}



function showcommenttask(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;
    $("#commentchatbox").html('');
    $.ajax({
        data : {
            val : id ,
           _csrfToken : refscrf
        },
        type : "post",
        url : "/bpmt/"+url,
        success: function(response){
            if(response.result == "success"){
                var comments = response.comment ;               
                var htmltext = comments;
                if(htmltext == ""){
                    alert("Oops! Some error occured. Please reload.");
                    return false;
                }
            
                swal({
                    title: "Comments on Task!",
                    text: htmltext,
                    html: true,
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    onOpen: function(){
                        
                    }
                }, function (isConfirm) {
                    if (isConfirm) {
                     
                       
                      
                    
                    // $.ajax({
                    //     data : {
                    //         val : id ,
                    //         comment: comment,
                    //        _csrfToken : refscrf
                    //     },
                    //     type : "post",
                    //     url : "/bpmt/"+url,
                    //     success: function(response){
                    //         if(response.result == "success"){
                               
                    //         }
                    //         else{
                               
                    //         }
                    //     }
                    // })
                
            }else{
                $("#commentchatbox").html(htmltext)
            }
                });

            }
            else{
                swal("Error!", response.result, "error");
            }
        }
    })

}



function showTaskComplete(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;
    $("#commentbox").hide();
    $("#frequencybox").hide();
    $("#negativetask").val(0);

    $.ajax({
        data : {
            val : id ,
           _csrfToken : refscrf
        },
        type : "post",
        url : "/bpmt/taskboard/gettask",
        success: function(response){
            if(response.result == "success"){
                var taskdetail = response.task ;
                setTimeout(function(){
                    if(taskdetail.negative == "1"){
                        // $("#commentbox").show();
                        $("#negativetask").val(1);
                    }
                    if(taskdetail.ongoing == "true"){
                        if(taskdetail.negative == "1"){
                            $("#commentbox").show();
                        }
                        $("#repeattask").prop('checked' , true );
                        $("#frequencybox").show();
                        $("#frequency").val(taskdetail.frequency);
                        if(taskdetail.frequency == "1"){
                            $("#repeatdate").datepicker({
                                autoclose  : true,
                                startDate : new Date()

                             });
                             $("#repeatdate").datepicker('setDate', taskdetail.frequencyvalue);

                            $("#repeatdate").addClass("required");
                            $("#datebox").show();
    
                        }
                        else if(taskdetail.frequency == "3"){
                            $("#repeatday").val(taskdetail.frequencyvalue);
                            $("#repeatday").addClass("required");
                            $("#daybox").show();
    
                        }
                        else if(taskdetail.frequency == "4"){
                            $("#repeatmonth").val(taskdetail.frequencyvalue);
                            $("#repeatmonth").addClass("required");
                            $("#monthbox").show();
    
                        }
    
                    }
                    if(taskdetail.category != 4){
                        $("#repeatdate").removeClass("required");
                        $("#repeatday").removeClass("required");
                        $("#repeatmonth").removeClass("required");
                        $("#daybox").hide();
                        $("#frequencybox").hide();
                        $("#datebox").hide();
                        $("#monthbox").hide();
                        $("#frequency").val('');
                        $("#repeattask").prop('checked' , false );
                        $("#repeattaskbox").hide();

                  
                    }
                }, 1000);
              


                var htmltext = $("#completebox").html();
                if(htmltext == ""){
                    alert("Oops! Some error occured. Please reload.");
                    return false;
                }
            
                $("#completebox").html('');
                swal({
                    title: "Mark as Complete!",
                    text: htmltext,
                    html: true,
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    onOpen: function(){
                        
                    }
                }, function (isConfirm) {
                    if (isConfirm) {
                        var comment =  $("#commenttext").val();
                        var spent =  $("#spenthour").val();
                        var durationrepeated =  $("#durationrepeated").val();
                        var repeattask =  $("#repeattask").prop('checked');
                        var qualitytask =  $("#qualitytask").prop('checked');
                        // var qualitytask =  true;
                        var repcomment =  $("#commentneg").val();
                        // alert(repcomment);
                        // alert($("#negativetask").val());
                        var repeatval = '' ;
                        if(comment == '' || spent == '' || spent == 0 || repeattask == true  ){
                                var repeattype = $("#frequency").val();
                                if(comment == ""){
                                    $("#completeerror").html("Please write comment.");
                                    $("#completeerror").fadeIn().delay(5000).fadeOut();
                                    swal("Error!", response1, "error");
                                    return false
                                }
                                else if(spent == '' || spent == 0){
                                    $("#completeerror").html("Please enetr your spent hours.");
                                    $("#completeerror").fadeIn().delay(5000).fadeOut();
                                    swal("Error!", response1, "error");
                                    return false
                                }
                                else if(repeattask == true && repeattype != ""){
                                    var flag = 1 ;
                                    $(".hideform").each(function(){
                                        if($(this).hasClass('required') && $(this).val() == '' ){
                                               flag = 0;
                                        }
                                        else if($(this).hasClass('required') && $(this).val() != '' ){
                                            repeatval = $(this).val() ;
                                        }
            
                                       
                                    })
                                    if(flag == 0){
                                        
                                        $("#completeerror").html("Please enter a value");
                                        $("#completeerror").fadeIn().delay(5000).fadeOut();
                                        swal("Error!", response1, "error");
                                        return false;
                                    }
                                }
                                else if(repeattask == true && repcomment == "" && $("#negativetask").val() == 1  ){
                                    $("#completeerror").html("Please add a comment to repeat the negative task.");
                                    $("#completeerror").fadeIn().delay(5000).fadeOut();
                                    swal("Error!", response1, "error");
                                    return false;
                                }
                                else if(repeattask == true && repeattype == ""){
                                    $("#completeerror").html("Please choose a repeat frequency type.");
                                    $("#completeerror").fadeIn().delay(5000).fadeOut();
                                    swal("Error!", response1, "error");
                                    return false;
                                }
                        }
                    
                    $.ajax({
                        data : {
                            val : id ,
                            comment: comment,
                            repcomment: repcomment,
                            spent: spent,
                            repeattask: repeattask,
                            repeattype: repeattype,
                            repeatval: repeatval,
                            qualitytask: qualitytask,
                            durationrepeated: durationrepeated,
                           _csrfToken : refscrf
                        },
                        type : "post",
                        url : "/bpmt/"+url,
                        success: function(response){
                            if(response.result == "success"){
                                swal("Completed!", str+" has been completed.", "success");
                                setTimeout(function(){ location.reload() ;  }, 1000);
                            }
                            else{
                                swal("Error!", response.result, "error");
                            }
                        }
                    })
                
            }else{
                $("#completebox").html(htmltext)
            }
                });

            }
            else{
                swal("Error!", response.result, "error");
            }
        }
    })

}


function repeatCheck(){
    if($("#repeattask").prop("checked")){
        $("#frequencybox").show();
        $("#durationrepeatedbox").show();
        if($("#negativetask").val() == 1){
            $("#commentbox").show();
        }
        $("#repeattask").addClass("required");

    }
    else{
        $("#commentbox").hide();
        $("#frequencybox").hide();
        $(".hideme").hide();
        $("#repeattask").removeClass("required");
        $(".hideform").removeClass("required");

    }
}

function frequencyChnage(){
    var val = $("#frequency").val();
    $(".hideme").hide();
    $(".hideform").removeClass("required");
    if(val == 1){
        $("#datebox").show();
        $(".datepicker").datepicker({
            autoclose: true,
            startDate : new Date()

        })
        $("#repeatdate").addClass("required");

    }
    else if(val == 3){
        $("#daybox").show();
        $("#repeatday").addClass("required");
        
    }
    else if(val == 4){
        $("#monthbox").show();
        $("#repeatmonth").addClass("required");
        
    }
}



function completetaskquality(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;

    swal({
        title: "Mark task status as complete?",
        text: "You will not be able to undo this action!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007bff",
        confirmButtonText: "Yes!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.ajax({
            data : {
                val : id ,
               _csrfToken : refscrf
            },
            type : "post",
            url : "/bpmt/"+url,
            success: function(response){
                if(response.result == "success"){
                    swal("Done!", str+" has been marked as completed.", "success");
                    setTimeout(function(){ location.reload() ;  }, 1000);
                }
                else{
                    swal("Error!", response.result, "error");
                }
            }
        })
    });
}


function showTaskTransferQuality(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;

    swal({
        title: "Change task status to ongoing?",
        text: "You will not be able to undo this action!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007bff",
        confirmButtonText: "Yes!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.ajax({
            data : {
                val : id ,
               _csrfToken : refscrf
            },
            type : "post",
            url : "/bpmt/"+url,
            success: function(response){
                if(response.result == "success"){
                    swal("Done!", str+" has been marked as ongoing.", "success");
                    setTimeout(function(){ location.reload() ;  }, 1000);
                }
                else{
                    swal("Error!", response.result, "error");
                }
            }
        })
    });
}

function showTaskTransfer(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;

    swal({
        title: "Change task status to ongoing?",
        text: "You will not be able to undo this action!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007bff",
        confirmButtonText: "Yes!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.ajax({
            data : {
                val : id ,
               _csrfToken : refscrf
            },
            type : "post",
            url : "/bpmt/"+url,
            success: function(response){
                if(response.result == "success"){
                    swal("Done!", str+" has been marked as ongoing.", "success");
                    setTimeout(function(){ location.reload() ;  }, 1000);
                }
                else{
                    swal("Error!", response.result, "error");
                }
            }
        })
    });
}


function showConfirmMessage(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;

    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this data!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007bff",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.ajax({
            data : {
                val : id ,
               _csrfToken : refscrf,
               str : str
            },
            type : "post",
            url : "/bpmt/"+url,
            success: function(response){
                if(response.result == "success"){
                    swal("Deleted!", str+" has been deleted.", "success");
                    setTimeout(function(){ location.reload() ;  }, 1000);
                }
                else{
                    swal("Error!", response.result, "error");
                }
            }
        })
    });
}


function showConfirmMessage_deactivated(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;

    swal({
        title: "Are you sure?",
        text: "Please confirm you want to deactivate this "+str+". This will be reported.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007bff",
        confirmButtonText: "Yes, deactivate it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.ajax({
            data : {
                val : id ,
               _csrfToken : refscrf,
               str : str

            },
            type : "post",
            url : "/bpmt/"+url,
            success: function(response){
                if(response.result == "success"){
                    swal("Deactivated!", str+" has been deactivated.", "success");
                    setTimeout(function(){ location.reload() ;  }, 1000);
                }
                else{
                    swal("Error!", response.result, "error");
                }
            }
        })
    });
}


function showConfirmMessage_activated(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;

    swal({
        title: "Are you sure?",
        text: "Please confirm you want to activate this "+str+". This will be reported.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007bff",
        confirmButtonText: "Yes, activate it!",
        closeOnConfirm: false,
        showLoaderOnConfirm: true
    }, function () {
        $.ajax({
            data : {
                val : id ,
               _csrfToken : refscrf,
               str : str

            },
            type : "post",
            url : "/bpmt/"+url,
            success: function(response){
                if(response.result == "success"){
                    swal("Activate!", str+" has been activate.", "success");
                    setTimeout(function(){ location.reload() ;  }, 1000);
                }
                else{
                    swal("Error!", response.result, "error");
                }
            }
        })
    });
}

//These codes takes from http://t4t5.github.io/sweetalert/
function showBasicMessage() {
    swal("Here's a message!");
}

function showWithTitleMessage() {
    swal("Here's a message!", "It's pretty, isn't it?");
}

function showSuccessMessage() {
    swal("Good job!", "You clicked the button!", "success");
}

function showCancelMessage() {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#007bff",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            swal("Deleted!", "Your imaginary file has been deleted.", "success");
        } else {
            swal("Cancelled", "Your imaginary file is safe :)", "error");
        }
    });
}

function showWithCustomIconMessage() {
    swal({
        title: "Sweet!",
        text: "Here's a custom image.",
        imageUrl: "../assets/images/sm/avatar2.jpg"
    });
}

function showhoursdetail(id,url,str) {
    var refscrf = $("input[name='_csrfToken']").val() ;
    $("#hourloader"+id).css('display' , 'inline-block' );
    $.ajax({
        data : {
            val : id,
           _csrfToken : refscrf
        },
        type : "post",
        url : "/bpmt/"+url,
        success: function(response){
            if(response.result == "success"){
                var html = "<h3><b>Client:</b> "+str+"</h3>"+response.data ;
                swal({
                    title: "Project-wise hour break up",
                    text: html,
                    html: true
                });
                $("#hourloader"+id).css('display' , 'none' );

            }
            else{
                swal("Error!", response.result, "error");
            }
        }
    })
   
}

function showAutoCloseTimerMessage() {
    swal({
        title: "Auto close alert!",
        text: "I will close in 2 seconds.",
        timer: 2000,
        showConfirmButton: false
    });
}

function showPromptMessage() {
    swal({
        title: "An input!",
        text: "Write something interesting:",
        type: "input",
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Write something"
    }, function (inputValue) {
        if (inputValue === false) return false;
        if (inputValue === "") {
            swal.showInputError("You need to write something!"); return false
        }
        swal("Nice!", "You wrote: " + inputValue, "success");
    });
}

function showAjaxLoaderMessage() {
    swal({
        title: "Ajax request example",
        text: "Submit to run ajax request",
        type: "info",
        showCancelButton: true,
        closeOnConfirm: false,
        showLoaderOnConfirm: true,
    }, function () {
        setTimeout(function () {
            swal("Ajax request finished!");
        }, 2000);
    });
}