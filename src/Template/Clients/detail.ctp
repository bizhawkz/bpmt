<div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card client-detail">
                        <div class="body">
                            <div class="profile-image">
                                <img src="/bpmt/img/<?=($client->picture == "" ? "avatar_big.png" : $client->picture)?>" alt="<?=ucfirst($client->fname)?></strong> <?=ucfirst($client->lname)?>">
                            </div>
                            <div class="details">
                                <h4 class="m-t-0 m-b-0"><strong><?=ucfirst($client->fname)?></strong> <?=ucfirst($client->lname)?></h4>
                                <p><?=$client->address?>, <?=$client->c['name']?></p>
                                <p><b><?=$client->active?></b> Active Projects | <b><?=$client->inactive?></b> Inactive Projects
                                <br>Balance hours: <b><?=$client->balance?></b></p>
                                <div class="m-t-15">
                                    <button class="btn btn-success redirectbtn" data-href="/bpmt/billing/detail/<?=md5($client->id)?>?ref=c&v=<?=md5($client->id)?>"  >Manage Billing</button>
                                    <button class="btn btn-primary redirectbtn" data-href="/bpmt/projects/add"  >+ Add Project</button>
                                    <button class="btn btn-success redirectbtn" data-href="/bpmt/clients/edit/<?=md5($client->id)?>"  >Edit Client Information</button>
                                    <button class="btn btn-primary redirectbtn pull-right" data-href="<?=$red?>"  >Go Back</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
            <?php
            foreach($projects as $project){
                ?>
 <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
                    <div class="card">
                        <div class="header">
                            <h2><?=$project['name']?><small>Created on <?=date('jS F, Y', $project->created)?></small></h2>                        
                            <ul class="header-dropdown">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu dropdown-menu-right animated bounceIn">
                                        <li><a href="/bpmt/projects/detail/<?=md5($project['id'])?>">Go to Project</a></li>
                                     </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <h6 class="m-b-15">Info about <?=$project['name']?> <span class="badge badge-success float-right">New</span></h6>                                
                            <p><?=(trim($project['description']) == "" ? "Not Available" :  $project['description']) ?></p>
                            <ul class="list-unstyled team-info m-t-20">
                                <li class="m-r-15"><small class="text-muted">Team</small></li>
                                <?php
                                $team = $project['team'] ;
                                foreach($team as $member){
                                    ?>
                                <li><img src="/bpmt/img/<?=($member['picture'] == "" ? "avatar.jpg" : $member['picture'])?>" title="<?=$member['name']?>" alt="<?=$member['name']?>"></li>
                                <?php
                                }
                                ?>

                            </ul>
                            <div class="progress progress-xs progress-transparent custom-color-blue" style="margin-bottom:10px">
                            <div class="progress-bar" data-transitiongoal="<?=$project['progress']?>" aria-valuenow="<?=$project['progress']?>" style="width: <?=$project['progress']?>%;"></div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h6>Hours <small>Spent/Total</small>: <?=$project['spent']?>/<?=$project['hours']?> hrs</h6>
                                </div>
                                 
                            </div>
                            <div class="progress progress-xs progress-transparent custom-color-blue" style="margin-bottom:10px">
                            <div class="progress-bar" data-transitiongoal="<?=$project['balance_status']?>" aria-valuenow="<?=$project['balance_status']?>" style="width: <?=$project['balance_status']?>%;"></div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <h6>Hours <small>Balance</small>: <?=$project['balance']?> hrs</h6>
                                </div>
                                 
                            </div>
                        </div>
                    </div>
                </div>

                <?php
            }
            ?>
               
 
            </div>
      