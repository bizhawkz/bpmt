

            <div class="row clearfix">
                <div class="col-12">
                    <div class="card">
                        <div class="body">
                        <?php	echo $this->Form->create(false , ['url' => ['action' => 'addcl'] , 'id' => "addclform" , 'method' => "post"  ]); ?>
            <div class="error" id="clerror"></div>
                    <div class="success" id="clsuccess"></div>
                            <div class="row clearfix">
          
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="fname" required placeholder="First Name *">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="lname" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" required placeholder="Email ID *">
                                    </div>
                                </div>    
                                 
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" pattern="[0-9+-()]*"  name="mobile" placeholder="Mobile No">
                                    </div>
                                </div>
                                
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Address" name="address">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                    <select  class="form-control" name="type" >
                                        <option value="">Choose Type</option>
                                        <option value="1">Project based</option>
                                        <option value="2">Hour based</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <select  class="form-control" name="country" placeholder="Country">
                                        <option value="">Choose Country</option>
                                         
                                            <?php
                                            foreach($countries  as $country){
                                                ?>
                                                <option value="<?=$country['id']?>"><?=$country['name']?></option>

                                                <?php
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>
                                                 
                            </div>
                            <button type="submit" class="btn btn-primary" id="addcltn">Add</button>
                   <?php echo $this->Form->end(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    