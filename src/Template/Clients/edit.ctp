

            <div class="row clearfix">
                <div class="col-12">
                    <div class="card">
                        <div class="body">
                        <?php	echo $this->Form->create(false , ['url' => ['action' => 'editcl'] , 'id' => "editclform" , 'method' => "post"  ]); ?>
            <div class="error" id="editclerror"></div>
                    <div class="success" id="editclsuccess"></div>
                            <div class="row clearfix">
          
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="fname" required value="<?=$client->fname?>" placeholder="First Name *">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="lname" value="<?=$client->lname?>" placeholder="Last Name">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <input type="email" class="form-control" name="email" required value="<?=$client->email?>" placeholder="Email ID *">
                                    </div>
                                </div>    
                                 
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" pattern="[0-9+-()]*" value="<?=$client->mobile?>"  name="mobile" placeholder="Mobile No">
                                    </div>
                                </div>
                                
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Address" value="<?=$client->address?>" name="address">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                    <select  class="form-control" name="type" >
                                        <option value="">Choose Type</option>
                                        <option value="1" <?=($client->type == 1 ? "selected" : "" )?>>Project based</option>
                                        <option value="2" <?=($client->type == 2 ? "selected" : "" )?>>Hour based</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <select  class="form-control" name="country"   placeholder="Country">
                                        <option value="">Choose Country</option>
                                         
                                            <?php
                                            foreach($countries  as $country){
                                                ?>
                                                <option <?=($client->country == $country['id'] ? "selected" : "" )?> value="<?=$country['id']?>"><?=$country['name']?></option>

                                                <?php
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>
                                                 
                            </div>
                            <input type="hidden" name="cid" value="<?=md5($client->id)?>" >
                            <button type="submit" class="btn btn-success" id="editcltn">Save</button>
                            <button type="button" class="btn btn-primary recirectbtn" data-href="/bpmt/clients/" >Go Back</button>
                   <?php echo $this->Form->end(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    