

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body project_report">
                            <div class="table-responsive">
                                <table class="table table-hover js-basic-example dataTable table-custom m-b-0">
                                    <thead>
                                        <tr>                                            
                                            <th>Name</th>
                                            <th>Location</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($cl_details as $client ){
                                        ?>
                                        <tr>
                                                <td class="project-title">
                                                <h6><?=ucfirst($client['fname'])?> <?=ucfirst($client['lname'])?></h6>
                                                <small>Created <?=date('jS F, Y' , $client['created'])?></small>
                                            </td>
                                            <td><?=$client['c']['name']?></td>
                                            <td><?=($client['status'] == 1 ? "Active"  : "Inactive" )?></td>
                                            <td class="project-actions">
                                                <a href="clients/detail/<?=md5($client['id'])?>" data-toggle="tooltip" data-title="View/Manage Client" class="btn btn-sm btn-outline-primary"><i class="icon-settings"></i></a>
                                                <?php
                                                if(in_array(11 , $user_privilage )){ ?>

                                                 <a href="javascript:void(0);" data-id="<?=md5($client['id'])?>" class="btn btn-sm btn-outline-danger js-sweetalert" data-toggle="tooltip" data-title="Delete Client"  data-str="<?=$client['c']['name']?>" data-url="clients/delete"  data-type="confirm"><i class="icon-trash"></i></a>
                                                    <?php
                                                 if($client['status'] == 1){
                                                     ?>
                                                 <a href="javascript:void(0);" data-id="<?=md5($client['id'])?>" class="btn btn-sm btn-outline-danger js-sweetalert" data-toggle="tooltip" data-title="Deactivate Client"  data-str="<?=$client['c']['name']?>" data-url="clients/deactivate"  data-type="confirmdeactivate"><i class="icon-arrow-down"></i></a>

                                                     <?php
                                                 }else{
                                                     ?>
                                                    <a href="javascript:void(0);" data-id="<?=md5($client['id'])?>" class="btn btn-sm btn-outline-danger js-sweetalert" data-toggle="tooltip" data-title="Activate Client"  data-str="<?=$client['c']['name']?>" data-url="clients/activate"  data-type="confirmactivate"><i class="icon-arrow-up"></i></a>
                                                <?php
                                                 }
                                                }
                                                 ?>
                                            </td>
                                        </tr>                                    
                                        <?php
                                    }
                                    ?>

                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>


    <?php	
echo $this->Form->create(false , ['url' => ['action' => ''] , 'id' => "" , 'method' => "post"  ]); 
echo $this->Form->end(); ?>