<?php 
	echo $this->Html->script('ckeditor/ckeditor', array('inline' => false));
?>
<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="card">
			<div class="header">
                <h2>Edit Term</h2>
            </div>
			<div class="body">
				<?php	echo $this->Form->create(false , ['url' => ['action' => 'editterm'] , 'id' => "edittermform" , 'method' => "post"  ]); ?>

                <div class="row clearfix">
                    <div class="error" id="edittermerror"></div>
                    <div class="success" id="edittermsuccess"></div>
                    <div class="col-md-12">
                        <input type="hidden"  value="<?= $term_detail[0]['id']?>" name="id">
                        <label id="fileHelp" class="form-text text-muted">Name</label>
                        <div class="form-group">                                    
                            <input type="text" class="form-control" value="<?= $term_detail[0]['name']?>" required name="name" placeholder="Name">
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <label id="fileHelp" class="form-text text-muted">Content</label>
                        <div class="form-group">                                    
                            <textarea name="editor" class="ckeditor" id="editor"><?=$term_detail[0]['content']?></textarea>
                            
                        </div>
                    </div>
                    <div style="clear:both;height: 10px;width: 100%;" ></div>
                    
              
                    <div class="button_row" >
                    <hr>
                    <button type="submit" class="btn btn-primary" id="edittermbtn">Updated</button>
                    <a href="/bpmt/term/" class="btn btn-secondary" style="margin-right: 10px;" >CLOSE</a>
                    </div>
                    
                   <?php echo $this->Form->end(); ?>

			</div>
		</div>
	</div>
</div>
