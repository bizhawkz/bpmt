<div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Term List</h2>
                            <ul class="header-dropdown">
                                <li><a href="/bpmt/term/add" title="Add" class="btn btn-sm btn-success">Add New</a></li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover js-basic-example dataTable table-custom table-striped m-b-0 c_list" id="role_table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>
                                                <label class="fancy-checkbox">
                                                    <input class="select-all" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>Name</th>
                                            <th>Content</th>
                                            <th>Created on</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($term_details as $term){
                                        ?>
                                        <tr>
                                            <td class="width45">
                                            <label class="fancy-checkbox">
                                                    <input class="checkbox-tick" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <?=$term['name']?>
                                            </td>
                                            <td>
                                                <span><?=$term['content']?></span>
                                            </td>
                                            <td><?=date('jS F Y', $term['created'])?></td>
                                            <td>
                                                <a href="/bpmt/term/edit/<?=$term['id']?>" title="Add" id="<?= $term['id']; ?>" class="btn btn-sm btn-outline-secondary" title="Edit"><i class="fa fa-edit"></i></a>
                                                
                                                <button type="button" data-id="<?=$term['id']?>" data-url="term/delete" class="btn btn-sm btn-outline-danger js-sweetalert " title="Delete" data-str="delete" data-type="confirm"><i class="fa fa-trash-o"></i></button>

                                                <button type="button" data-id="<?= $term['id']; ?>" data-toggle="modal" data-target="#shareterm" class="btn btn-sm btn-outline-secondary shareterm" title="Share"><i class="fa fa-share"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


<!-- Share Terms -->

<div class="modal animated zoomIn" id="shareterm" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="title" id="defaultModalLabel">Share Terms</h6>
            </div>
            <div class="modal-body">
            <?php   echo $this->Form->create(false , ['url' => ['action' => 'shareterm'] , 'id' => "sharetermform" , 'method' => "post"  ]); ?>

                <div class="row clearfix">
                    <div class="error" id="termerror"></div>
                    <div class="success" id="termsuccess"></div>
                    <input type="hidden" id="id" name="id" >
                    <div class="col-md-12">
                        <h6>Selection by :  
                        </h6>
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                <a class="nav-item wise nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Team Wise</a>
                                <a class="nav-item wise nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Role Wise</a>
                                <a class="nav-item wise nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">User Wise</a>
                            </div>
                        </nav>
                    </div>
                    <div class="tab-content" id="nav-tabContent">
                        <div class="col-md-12 tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="form-group">                                    
                                <select  class="form-control" multiple  name="team[]">
                                <option value="" >Choose Team</option>
                                <?php
                                foreach($team_details as $team){
                                    echo '<option value="'.$team['id'].'" >'.$team['name'].'</option>' ;
                                }
                                ?>
                                </select>
                            </div>   
                        </div>
                        <div class="col-md-12 tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="form-group">                                    
                                <select  class="form-control" multiple  name="role[]">
                                <option value="" >Choose Role/Position</option>
                                <?php
                                foreach($role_details as $role){
                                    echo '<option value="'.$role['id'].'" >'.$role['name'].'</option>' ;
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                            <div class="form-group">                                    
                                <select  class="form-control" multiple  name="user[]">
                                <option value="" >Choose User</option>
                                <?php
                                foreach($user_details as $user){
                                    echo '<option value="'.$user['id'].'" >'.$user['name'].'</option>' ;
                                }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div style="clear:both;height: 10px;width: 100%;" ></div>
                    
                    <div class="button_row" >
                    <hr>
                    <button type="submit" class="btn btn-primary" id="sharetermbtn">Share</button>
                    <button type="button" class="btn btn-secondary" style="margin-right: 10px;" data-dismiss="modal">CLOSE</button>
                    </div>
                    
                   <?php echo $this->Form->end(); ?>
                   
                </div>
            </div>
             
        </div>
    </div>
</div>

<!-- End -->            