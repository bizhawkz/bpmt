<div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">                        
                        <div class="body">
                            <h3 class="heading"><?=$client_name?>'s Billing History  <a class="btn btn-primary pull-right" href="<?=$red?>"  style="color:#fff" >Go Back</a></h3>
                            <h6><span style="color:#007bff" >Net Balance hour: <?=$total_hours?> |</span> <span style="color:#65a047" >Next Payment Due: <?=$next_payment?> |</span> <span style="color:#252f15"> Last Payment Date: <?=$last_payment?> </span></h6>
                            <div class="table-responsive">
                                <table class="table table-hover js-basic-example dataTable table-custom">
                                    <thead>
                                        <tr>
                                            <th>Payment Date</th>
                                            <th>Next Payment Due</th>
                                            <th>Hours</th>
                                            <th>Billing For Project</th>                                            
                                            <th>Remarks</th>
                                            <th>Billed By</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($clients as $client){
                                        ?>
                                        <tr>
                                        <td><?=date('jS M, Y' ,$client['billing_date'])?></td>
                                        <td><?=($client['next_payment'] == NULL ? "NA" : date('jS M, Y' ,$client['next_payment']))?></td>
                                        <td><?=$client['hours']?></td>
                                        <td><?=($client['pid'] == 0 ? "NA" : $client['p']['name'])?></a></td>
                                        <td><?=($client['remark'] == NULL ? "NA" : '<i class="fa fa-info-circle" data-toggle="tooltip" data-title="'.$client['remark'].'">')?></td>
                                        <td><?=$client['uname']?> </td>
                                        <td> 
                                        <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Edit" data-type="editbilling" data-id="<?=md5($client['id'])?>" data-url="billing/getbilling" data-str="Billing" ><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Delete" data-type="confirm" data-id="<?=md5($client['id'])?>" data-url="billing/delete" data-str="Billing" ><i class="fa fa-trash" ></i> </a>
                                        </td>
                                        </tr>

                                        <?php
                                    }
                                    ?>
                                       

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>

            <?php	echo $this->Form->create(false , ['url' => [] , 'method' => "post"  ]); ?>
            <?php echo $this->Form->end(); ?>


            
<!-- Edit Billing -->

<div class="modal fade" id="editbilling" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h6 class="title" id="defaultModalLabel">Edit Billing</h6>
</div>
<div class="modal-body">
<?php	echo $this->Form->create(false , ['url' => ['action' => 'editbilling'] , 'id' => "editbillingform" , 'method' => "post"  ]); ?>
<div class="row clearfix">
   
    <div class="col-12">
    <small>Project</small>
        <select name="project" id="bproject" class="form-control show-tick m-b-10">
            <option value="" selected>No Project Selected</option>
            <?php
                    foreach($project_details as $project){
                        ?>
            <option value="<?=$project['id']?>"><?=$project['name']?></option>
            <?php
                    }
            ?>
               </select>
    </div>
    <div class="col-12">
    <small>Paid Hours</small>
    <div class="input-group" >
    <input type="text"  id="bpaid" name="paid"  pattern="[0-9.-]*" class="form-control" placeholder="Paid hours">
    </div>
    </div>
    <div class="col-12">
        <small>Current Payment Date</small>
        <div class="input-group" >
            <input type="text" name="billing_date" id="billing_date" class="form-control commondatepicker" >
        </div>
    </div>           
    <div class="col-12">
        <small>Next Payment Date</small>
        <div class="input-group" >
            <input type="text" name="next_payment" id="next_payment" class="form-control commondatepicker" >
        </div>
    </div>      
    <div class="col-12">
    <small>Remarks</small>
    <div class="input-group" >
    <input type="text"  id="remark" name="remark"  class="form-control" placeholder="Remarks">
    </div>
    </div>     


         
</div>
</div>


<div class="modal-footer">
<input type="hidden" name="vista" id="vista" >

<button type="submit" id="editbtbtn" class="btn btn-primary">Save</button>
<button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
</div>
<?php echo $this->Form->end(); ?>

</div>
</div>
</div>

