<div class="row clearfix">

                <div class="col-lg-12">
                    <div class="card">                        
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover billinglisttable table-custom">
                                    <thead>
                                        <tr>
                                            <th>Client ID</th>
                                            <th>Client Name</th>
                                            <th>Project Name</th>
                                            <th>Paid Hours</th>
                                            <th>Balance Hours</th>
                                            <th>Negative Limit</th>
                                            <th>Last Billed</th>
                                            <th>Last Payment</th>
                                            <th>Next Payment</th>
                                            <th>Remarks</th>
                                            <th><?=$currentmonth?></th>
                                            <th><?=$previousmonth1?></th>
                                            <th><?=$previousmonth2?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($clients as $client){
                                        if($client['type'] == "1" ){
                                            $pj_hours = $this->Client->findhours(md5($client['pid'])) ;
                                            $pj_hours_show = $this->Client->showhours(md5($client['pid'])) ;    
                                        }
                                        else{
                                            $pj_hours = $client['hours'];
                                            $pj_hours_show = $client['balance'];
                                        }
    

                                        if($client['ptype'] == 1){
                                            $ptype = "Prepaid" ; 
                                        }elseif($client['ptype'] == 2){
                                            $ptype = "Postpaid" ;
                                            
                                        }else{
                                            $ptype = "NA" ;
                                        }  
                                        $class = "" ;
                                        $color = "" ;
                                        if($client['status'] == 1 && $pj_hours < 0 ){
                                          $class = "class='red'" ;
                                        }
                                        elseif($client['status'] == 2 || $client['pstatus'] == 2 ){
                                            $class = 'class="grey"' ;
                                        }
                                        ?>
                                        <tr <?=$class?> <?=$color?>>
                                            <td><a href="/bpmt/billing/detail/<?=$client['ccid']?>"><?=$client['cid']?></a></td>
                                            <td><a href="/bpmt/clients/detail/<?=$client['ccid']?>"><?=$client['fname']?> <?=$client['lname']?></a></td>
                                            <td><a href="/bpmt/<?=($client['pname'] == "" ? "billing" : "projects" )?>/detail/<?=$client['ccid']?>"><?=($client['pname'] == "" ? "General Tasks"  : $client['pname'])?></a> </td>
                                            <td><a href="/bpmt/billing/detail/<?=$client['ccid']?>"><?=$client['hours']?></a></td>
                                            <td><a href="/bpmt/billing/detail/<?=$client['ccid']?>"><?=$pj_hours_show?> (<?=$ptype?>)</a></td>
                                            <td><a href="/bpmt/billing/detail/<?=$client['ccid']?>"><?=$client['nlimit']?></a></td>
                                           
                                            <td><a href="/bpmt/billing/detail/<?=$client['ccid']?>"><?=$client['last_billed']?></a></td>  
                                            
                                            <td><a href="/bpmt/billing/detail/<?=$client['ccid']?>"><?=$client['billing_date']?></a></td>

                                            <td><a href="/bpmt/billing/detail/<?=$client['ccid']?>"><?=$client['next_payment']?></a></td>
                                            <td><i class="fa fa-info-circle" data-toggle="tooltip" data-title="<?=($client['remark'] == "" ? "NA" : $client['remark'] )?>" ></i> </td>
                                            <td><?=$client['currentmonth']?></td>
                                            <td><?=$client['previousmonth1']?></td>
                                            <td><?=$client['previousmonth2']?></td>
                                            
                                        </tr>

                                        <?php
                                    }
                                    ?>
                                       

                                    </tbody>
                                </table>
                                <div class="navbut">
                                <a class="btn btn-primary" href="?offset=<?=($pageoffset > 0 ? $pageoffset - 10 : 0)?>"  >< Prev</a>
                                <?php
                                  for($i = 0 ;$i < $pagetotal ; $i++   ){
                                      ?>
                                <a class="btn btn-primary pagebtn <?=($i*10 == $pageoffset ? 'active' : '')?>" href="?offset=<?=$i*10?>" ><?=$i+1?></a>

                                  <?php
                                  }
                                  ?>
                                <a class="btn btn-primary" href="?offset=<?=$pageoffset + 10?>" >Next ></a>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>

<?php	echo $this->Form->create(false , ['url' => ['action' => ''] , 'id' => "" , 'method' => "post"  ]); ?>
<?php echo $this->Form->end(); ?>
          