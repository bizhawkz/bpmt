

            <div class="row clearfix">

<div class="col-lg-3 col-md-12">
    <div class="card redirectbtn taskbutton active" data-href="/bpmt/billing/tasks/<?=$user_sel_id?>">
        <div class="header">
            <h2>Not Billed Tasks<small><?=count($retrieve_task)?> Task(s)</small></h2>

        </div>
    </div>    
    </div>   
    <div class="col-lg-3 col-md-12">
    <div class="card planned_task redirectbtn taskbutton" data-href="/bpmt/billing/billed/<?=$user_sel_id?>">
        <div class="header">
            <h2>Billed Tasks<small><?=$count_billed?> Task(s)</small></h2>
            
        </div>
    </div>    
    </div>   
        
    <div class="col-md-12">
    <div class="belowmsg"><?=$below_message?></div>

    <div class="row">
    <div class="col-md-3"> 
                            <small class="form-text text-muted">Category</small>
                            <div class="form-group">
                            <select name="categoryfilter" class="categoryfilter form-control pull-left">
                        <option value="" >Select Category</option>
                        <option value="all" <?=($user_cat_id == 'all' ? "selected" : "" )?> >All Category</option>
                            <?php
                              foreach($cat_details as $category){
                                echo '<option value="'.md5($category['id']).'"   '.($user_cat_id == md5($category['id']) ? "selected" : "" ).'  >'.$category['name'].'</option>' ; 
                              }
                            ?>
                        </select>
                        </div>
                        </div>
                        <div class="col-md-3"> 
                        <small class="form-text text-muted">Agent</small>
                        <div class="form-group">
                      <select name="agentuser" class="agentfilter form-control pull-left">
                        <option value="" >Select Agent</option>
 
                        <option value="alltask" <?=($user_sel_id == 'alltask' ? "selected" : "" )?> >All   task</option>
                       
                          <?php
                                   foreach($emp_details as $employee){
                                   echo '<option value="'.md5($employee['id']).'"   '.($user_sel_id == md5($employee['id']) ? "selected" : "" ).'  >'.$employee['name'].'</option>' ; 
                                   }
                           ?>
                       </select>
                       </div>
                       </div>
                       <div class="col-md-3"> 
                        <small class="form-text text-muted">Completed On</small>
                        <div class="form-group">
                           <input type="text" name="completed" value="<?=$completedon?>" class="completedfilter form-control daterange">
                       </div>
                       </div>
</div>
        <a class="btn btn-primary addbtn-primary js-sweetalert" href="javascript:void(0);" data-type="bulkapprovebillingtask" data-id="" data-url="taskboard/bulkapprovebillingtask"  data-str="Task">Bulk Bill <i class="fa fa-arrow-right" data-toggle="tooltip" title="Bulk Bill task"></i></a>
     
    </div>
    <div class="col-md-12">
    <div class="card">
        <div class="body">
            <table class="task_table" >
            <thead>
            <th><input type="checkbox" value="" id="selectall_bill" ></th>
            <th>Task ID</th>
            <th>Title</th> 
            <th>Client</th> 
            <th>Project</th> 
            <th>Category</th> 
            <th>Completed by </th> 
            <th>Comment</th>
            <th>Completed On</th>
            <th>E.H.</th>
            <th>S.H.</th>
            <th>B.H.</th>
            <th>Actions</th>
            </thead>
            <tbody>
                <?php
                foreach($retrieve_task as $task){
                ?>
                <tr>
                <td><input type="checkbox" value="<?=$task['id']?>" class="bulkbill" ></td>
                <td>TSK00<?=$task['id']?></td>
                <td><?=$task['tittle']?></td>
                <td><?=$task['cl']['fname']?> <?=$task['cl']['lname']?></td>
                <td><?=$task['p']['name']?></td>
                <td><?=$task['c']['name']?></td>
                <td><?=$task['a']['name']?></td>
                <td><?=$task['comment']?></td>
                <td><?=date('jS M,y', $task['completedtime'])?></td>
                <td><?=$task['estimate']?></td>
                <td><?=$task['spent']?></td>
                <td><?=$task['billed']?></td>
                <td>  
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" title="Bill task" data-type="approvebillingtask" data-id="<?=md5($task['id'])?>" data-url="taskboard/approvebillingtask" data-get="taskboard/gettask" data-str="Task"  ><i class="fa fa-check-circle" ></i> </a>  
                 <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" title="Comments on task" data-type="commenttask" data-id="<?=md5($task['id'])?>" data-url="taskboard/commenttask" data-str="Task"  ><i class="fa fa-comment" ></i> </a>  
               
                </td>

                </tr>
                              
                        <?php } ?> 
                </tbody> 
                </table>                               
            </div>
        </div>
    </div>
</div>




<?php	echo $this->Form->create(false , ['url' => ['action' => ''] , 'id' => "" , 'method' => "post"  ]); ?>

<?php echo $this->Form->end(); ?>


<!-- EXTRA HTML --->

<div id="approvebillingbox"  class="hideme">
        <div id='completeerror' class='error'></div>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Available Comment</small>
        <textarea class='form-control' name='comment'  id='commenttext'></textarea>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Spent hours (Please change if spent hours is not equal to billable hours)</small>
        <input style='display:block' type='text'  class='form-control decimals' id='spenthour' >

    </div>

