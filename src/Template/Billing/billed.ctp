

            <div class="row clearfix">

<div class="col-lg-3 col-md-12">
    <div class="card redirectbtn taskbutton" data-href="/bpmt/billing/tasks/<?=$user_sel_id?>">
        <div class="header">
            <h2>Not Billed Tasks<small><?=$count_billing?> Task(s)</small></h2>

        </div>
    </div>    
    </div>   
    <div class="col-lg-3 col-md-12">
    <div class="card planned_task redirectbtn taskbutton active" data-href="/bpmt/billing/billed/<?=$user_sel_id?>">
        <div class="header">
            <h2>Billed Tasks<small><?=count($retrieve_task)?> Task(s)</small></h2>
            
        </div>
    </div>    
    </div>   
        
    <div class="col-md-12">
    <div class="belowmsg"><?=$below_message?></div>
    
    <div class="row">
    <div class="col-md-3"> 
                            <small class="form-text text-muted">Category</small>
                            <div class="form-group">
                            <select name="categoryfilter" class="categoryfilter form-control pull-left">
                        <option value="" >Select Category</option>
                        <option value="all" <?=($user_cat_id == 'all' ? "selected" : "" )?> >All Category</option>
                            <?php
                              foreach($cat_details as $category){
                                echo '<option value="'.md5($category['id']).'"   '.($user_cat_id == md5($category['id']) ? "selected" : "" ).'  >'.$category['name'].'</option>' ; 
                              }
                            ?>
                        </select>
                        </div>
                        </div>
                        <div class="col-md-3"> 
                        <small class="form-text text-muted">Agent</small>
                        <div class="form-group">
                      <select name="agentuser" class="agentfilter form-control pull-left">
                        <option value="" >Select Agent</option>
 
                        <option value="alltask" <?=($user_sel_id == 'alltask' ? "selected" : "" )?> >All   task</option>
                       
                          <?php
                                   foreach($emp_details as $employee){
                                   echo '<option value="'.md5($employee['id']).'"   '.($user_sel_id == md5($employee['id']) ? "selected" : "" ).'  >'.$employee['name'].'</option>' ; 
                                   }
                           ?>
                       </select>
                       </div>
                       </div>
                       <div class="col-md-3"> 
                        <small class="form-text text-muted">Completed On</small>
                        <div class="form-group">
                           <input type="text" name="completed" value="<?=$completedon?>" class="completedfilter form-control daterange">
                       </div>
                       </div>
                       <div class="col-md-3"> 
                        <small class="form-text text-muted">Billed On</small>
                        <div class="form-group">
                           <input type="text" name="completed" value="<?=$billedon?>" class="billedfilter form-control daterange">
                       </div>
                       </div>
</div>
    </div>
    <div class="col-md-12">
    <div class="card">
        <div class="body">
            <table class="btask_table" >
            <thead>
            <th>Task ID</th>
            <th>Client</th>
            <th>Project</th>
            <th>Description</th>
            <th>Agent</th> 
            <th>Team</th>
            <th>Completed On</th>
            <th>Billed For</th>
            <th>Billed On</th>
            <th>S.T.</th>
            <th>B.H.</th>
            <th>T.N.U</th>
            <th>Reason</th>
            <th>Thread</th>
            <th>Contract</th>
            </thead>
            <tbody>
                <?php
                $total_billed = 0 ;
                $total_spent = 0 ;
                foreach($retrieve_task as $task){
                    $total_billed += $task['billed'] ; 
                    $total_spent += $task['spent'] ; 
                ?>
                <tr>
                <td>TSK00<?=$task['id']?></td>
                <td><?=$task['cl']['fname']?> <?=$task['cl']['lname']?></td>
                <td><?=$task['p']['name']?></td>
                <td><?=$task['comment']?></td>
                <td><?=$task['a']['name']?></td>           
                <td><?=$task['c']['name']?></td>
                <td><?=date('jS M,Y', $task['completedtime'])?></td>
                <td><?=date('jS M,Y', $task['billedtime'])?></td>
                <td><?=date('jS M,Y', $task['billedon'])?></td>
                <td><?=$task['spent']?></td>
                <td><?=$task['billed']?></td>
                <td><?=($task['spent'] ? $task['spent'] : 0) - ($task['billed'] ? $task['billed'] : 0) ?></td>
                <td>NA</td> 
                <td>NA</td>
                <td>NA</td>
                </tr>
                              
                        <?php } ?> 
                </tbody> 
                </table>      
                <div class="pull-left" style="padding: 10px 0px">
                    <b>Total Spent Hours: <?=$total_spent?></b><br>
                    <b>Total Billed Hours: <?=$total_billed?></b>
                </dov>                         
            </div>
        </div>
    </div>
</div>




<?php	echo $this->Form->create(false , ['url' => ['action' => ''] , 'id' => "" , 'method' => "post"  ]); ?>

<?php echo $this->Form->end(); ?>


<!-- EXTRA HTML --->

<div id="approvebillingbox"  class="hideme">
        <div id='completeerror' class='error'></div>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Available Comment</small>
        <textarea class='form-control' name='comment'  id='commenttext'></textarea>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Spent hours (Please change if spent hours is not equal to billable hours)</small>
        <input style='display:block' type='number' min="0.25" value=1 class='form-control decimals' id='spenthour' >

    </div>


    

