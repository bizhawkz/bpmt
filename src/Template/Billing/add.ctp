

            <div class="row clearfix">
                <div class="col-12">
                    <div class="card">
                        <div class="body">
                         
                        <?php	echo $this->Form->create(false , ['url' => ['action' => 'addbl'] , 'id' => "addblform" , 'method' => "post"  ]); ?>
            <div class="error" id="blerror"></div>
                    <div class="success" id="blsuccess"></div>
                            <div class="row clearfix">
                            <div class="col-md-4 col-sm-12">
                                    <div class="form-group">
                                <label style="margin-right: 10px;">Choose Client  </label>
                                        <select  class="form-control" id="clientbl" name="client" placeholder="client">
                                        <option value="">Choose Client</option>
                                         
                                            <?php
                                            foreach($clients  as $client){
                                                ?>
                                                <option value="<?=$client['id']?>"><?=$client['fname']?> <?=$client['lname']?></option>

                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 hideme" id="proassign">
                                <div class="form-group">
                                <label style="margin-right: 10px;">Do you want to assign hours to a project?  </label>
                                            <input type="radio" name="assignpro" class="assignpro" value="yes"  > Yes
                                            <input type="radio" name="assignpro" class="assignpro" value="no"  > No
                                </div>
                                </div>
                                <div class="col-md-12 col-sm-12" >
                                <div id="nomessage" >No Projects Found for this client.</div>
                                </div>
                                <div class="col-md-4 col-sm-12 hideme hideme2" id="project_sel">
                                    <div class="form-group">
                                    <label>Choose Project</label>
                                        <select  class="form-control" name="project" id="project" placeholder="Project">
                                         
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 hideme hideme2 hideme3" >
                                    <div class="form-group">
                                    <label>Payment Type</label>
                                        <select  class="form-control" name="payment_type">
                                        <option value="">Choose Payment Type</option>
                                        <option value="1">Partial</option>
                                        <option value="2">Full</option>
                                         
                                          
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="col-md-4 col-sm-12 hideme hideme2 hideme3">
                                    <div class="form-group" >
                                        <label>Total Hours</label>
                                        <input type="text" class="form-control" name="total" id="total" value="0" disabled placeholder="Total Hours">
                                    </div>
                                </div>    
                                 
                                <div class="col-md-4 col-sm-12 hideme hideme2 hideme3" id="payinghours">
                                    <div class="form-group"  >
                                    <label>Currently Paying Hours</label>
                                        <input type="text" class="form-control"  required name="paying"  placeholder="Currently Paying Hours">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 hideme hideme2 hideme3">
                                    <div class="form-group">
                                    <label>Paid Hours</label>
                                        <input type="text" class="form-control " id="paid" disabled  name="paid" value="0" placeholder="Paid hours">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 hideme hideme2 hideme3">
                                    <div class="form-group">
                                    <label>Pending Hours</label>
                                        <input type="text" class="form-control" disabled id="pending"  name="pending" value="0" placeholder="Pending hours">
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 hideme hideme2 hideme3" id="next_date">
                                    <div class="form-group">
                                    <label>Next Payment Date</label>
                                      
                                        <input type="text" class="form-control datepicker"   name="next_date" placeholder="Next Payment Date">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 hideme hideme2 hideme3" id="remarks">
                                    <div class="form-group">
                                    <label>Remarks</label>
                                      
                                        <input type="text" class="form-control hideme3"   name="remark" placeholder="Any Remarks?">
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12 hideme hideme2 hideme3" id="billing_date">
                                    <div class="form-group">
                                    <label>Billing Date</label>
                                        <input type="text" class="form-control hideme3 commondatepicker"   name="billing_date" placeholder="Billing Date">
                                    </div>
                                </div>


                                 
                                                 
                            </div>
                            <button type="submit" class="btn btn-primary" id="addbltn">Add</button>
                   <?php echo $this->Form->end(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    