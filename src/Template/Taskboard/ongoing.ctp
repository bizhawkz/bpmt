
    <div class="col-md-12">
    <div class="card">
        <div class="body">
            <table class="task_table" >
            <thead>
            <th>Task</th>
            <th>Assigned to</th> 
            <th>Assigned on</th> 
            <th>E.T.</th> 
            <th>Client</th>
            <th>Project</th>
            <th>Due Date</th>
            <th>Actions</th>
            </thead>
            <tbody>
                <?php
                $today = strtotime('today');

                foreach($retrieve_task as $task){
                    // $pj_hours = $this->Client->findhours($task['project']) ;
                    // $pj_hours_show = $this->Client->showhours($task['project']) ;
                    $pj_hours= 0 ;
                    $pj_hours_show = 0 ;
                ?>
                <tr class="<?=($pj_hours < 0 ? $this->Client->updatepositive($task['id']) : $this->Client->updatenegative($task['id']) )?>">
                <td>
                <b>ID:</b> TSK00<?=$task['id']?><br>
                <b>Title:</b> <?=$task['tittle']?><br>
                <b>Description:</b> <?=$task['description']?><br>
                <b>Category:</b> <?=$task['c']['name']?><br>
                <b>Added by:</b> <?=$task['cr']['name']?><br>
                <b>Manage by:</b> <?=$task['o']['name']?><br>
                <b>Added on:</b> <?=date('jS F,Y', $task['created'])?>
                </td> 
                <td><?=$task['a']['name']?></td>
                <td><?=date('jS M,y', $task['assignedtime'])?></td>
                <td><?=($task['rework'] == 1 ? $task['rework_estimate']."(Rework)" : $task['estimate'])?></td>

                <td><?=($task['reftype'] == "yes" ?   $task['cl']['fname'].' '.$task['cl']['lname'] : $task['reference'] )?></td>
                <td><?=$task['p']['name']?></td>
                 <td><?=date('jS M,y', $task['duedate'])?></td>
                <td>  
                <?php
                if(md5($user_details[0]['id']) == $task['creator'] || md5($user_details[0]['id']) == $task['owner'] ){
                ?>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Delete" data-type="confirm" data-id="<?=md5($task['id'])?>" data-url="taskboard/delete" data-str="Task" ><i class="fa fa-trash" ></i> 
                </a>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Edit" data-type="editpersonaltask" data-id="<?=md5($task['id'])?>" data-url="taskboard/gettask" data-str="Task" ><i class="fa fa-pencil"></i>   </a>
                <?php
                }
                ?>

                <?php
                if($task['rework'] == 1){
                ?>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Complete" data-type="completetaskqualityrework" data-id="<?=md5($task['id'])?>" data-url="taskboard/completetaskqualityrework" data-str="Task"  ><i class="fa fa-check-circle" ></i> </a>
                <?php
                }else{
                ?>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Complete" data-type="completetask" data-id="<?=md5($task['id'])?>" data-url="taskboard/completetask" data-str="Task"  ><i class="fa fa-check-circle" ></i> </a>
                <?php
                } 
                ?>
  
              <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Comments on task" data-type="commenttask" data-id="<?=md5($task['id'])?>" data-url="taskboard/getcommenttask" data-str="Task"  ><i class="fa fa-comment" ></i> </a>  
                
<?php
               if($task['task_status_new_rework'] == 2 ){
                    ?>
                    <small class="billed">Rework</small>
                 <?php   
                }  
?>              



</td>

                </tr>
                              
                        <?php } ?> 
                </tbody> 
                </table>
<?php
 $paginator = $this->Paginator;
 echo "<div class = 'paging'>";
 //for the first page link // the parameter’First’ is the labeled, same with other pagination link
 echo $paginator->first('First');
 echo " ";
 //if there was previous records, prev link will be displayed
 if($paginator->hasPrev()){
 echo $paginator->prev('<<');
 }
 //modulus => 3 specifies how many page number will be displayed
 echo $paginator->numbers(array('modulus' =>3)); 
 if($paginator->hasNext()){ //there are records, next link will be displayed
 echo $paginator->next('>>');
 }
 echo $paginator->last('Last'); //for the last page link

echo $paginator->counter([
    'format' => 'Page {{page}} of {{pages}}'
]);

 echo "</div)";
 ?> 
                               
            </div>
        </div>
    </div>
</div>




<?php	echo $this->Form->create(false , ['url' => ['action' => ''] , 'id' => "" , 'method' => "post"  ]); ?>

<?php echo $this->Form->end(); ?>


<!-- EXTRA HTML --->

<div id="completetaskquality"  class="hideme">
        <div id='completetaskqualityerror' class='error'></div>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Add comment</small>
        <textarea class='form-control' name='comment'  id='qcommenttext'></textarea>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>How long did it take? (Your Spent hours)</small>
        <input style='display:block' type='number' min="0.25" value=1 class='form-control decimals' id='qspenthour' >
        
       


    </div>



<div id="completebox"  class="hideme">
        <input  type="hidden" id="negativetask" value="0" >
        <div id='completeerror' class='error'></div>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Add comment</small>
        <textarea class='form-control' name='comment'  id='commenttext'></textarea>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>How long did it take? (Your Spent hours)</small>
        <input style='display:block' type='number' min="0.25" value=1 class='form-control decimals' id='spenthour' >
        <label style='text-align:left;float: left;margin: 4px 0px;width:50%'>
        <input class='form-control'  type='checkbox' value='yes' style='display: inline-block;height: 15px;float: left;width: auto;margin: 5px;' name='quality' id='qualitytask' >Send for quality?</label>
        <label style='text-align:left;float: right;margin: 4px 0px;width:50%' id="repeattaskbox" >
        <input class='form-control'  type='checkbox' value='yes' onchange="repeatCheck()" style='display: inline-block;height: 15px;float: left;width: auto;margin: 5px;' name='repet' id='repeattask' >Repeat this Task?</label>
        <div style="display:none" id="commentbox">
        <small style='text-align:left;float: left;margin: 4px 0px;width:100%'>Additional comment to repeat since the task is in negative</small>
        <textarea class="form-control" id="commentneg"></textarea>
                
        </div>
        <div style="display:none" id="durationrepeatedbox">
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Estimate For Repeated Task?</small>
        <input style='display:block' type='number' min="0.25" value=1 class='form-control decimals' name='durationrepeated' id='durationrepeated' >

        </div>
        <div style="display:none" id="frequencybox">
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Frequency</small>
            <select class='form-control' onchange='frequencyChnage()' name='frequency' id='frequency'  >
                <option value=''>Choose one</option>
                <option value="1">One time</option>
                <option value='2'>Daily</option>
                <option value='3'>Weekly</option>
                <option value='4'>Monthly</option>
                </select>
        </div>
        <div class="hideme" id="datebox">
            <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Pick a date</small>
                <input style='display:block' type='text' class='form-control datepicker hideform' id='repeatdate' >
        </div>
        <div class="hideme" id="daybox">
         <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Which day of the week?</small>
            <select class='form-control hideform' name='repeatday' id='repeatday'  >
                <option value=''>Choose one</option>
                <option value='1'>Monday</option>
                <option value='2'>Tuesday</option>
                <option value='3'>Wednesday</option>
                <option value='4'>Thursday</option>
                <option value='5'>Friday</option>
                <option value='6'>Saturday</option>
                <option value='7'>Sunday</option>
            </select>
        </div>
        <div class="hideme" id="monthbox">        
         <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Which day of the month?</small>
            <input type='text' value="1" style='display:block' class='form-control hideform numberonly' name='repeatmonth' id='repeatmonth' patter='[0-9]*' >
        </div>
       


    </div>

    <div id="commentchatbox" class="hideme">

</div>

<!-- Edit Task -->
<div class="modal fade" id="edittask" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h6 class="title" id="defaultModalLabel">Edit Task</h6>
</div>
<div class="modal-body">
<?php	echo $this->Form->create(false , ['url' => ['action' => 'editpersonal'] , 'id' => "editpersonalform" , 'method' => "post"  ]); ?>
<div class="row clearfix">

   
    <div class="col-12">
        <small>Task Category</small>
        <div class="form-group">                                    
        <select name="category" id="ecategory" class="form-control show-tick m-b-10">
            <option value="">Select Category</option>
            <?php
                foreach($cat_details as $category){
                    ?>
            <option value="<?=$category['id']?>"><?=$category['name']?></option>
            <?php
                }
            ?>
            </select>
        </div>
    </div>
    <div class="col-12">
    <small>Task Title</small>
        <div class="form-group">                                   
            <input type="text"  name="title" id="etitle" class="form-control" placeholder="Task title">
        </div>
    </div>
    <div class="col-12">
    <small>Task Description</small>
        <div class="form-group">                                    
            <textarea name="description" id="edescription" class="form-control" placeholder="Description"></textarea>
        </div>
    </div>
    <div class="col-12">
    <div class="form-group">
            <label>Is it for a existing client?</label><br>
            <label><input type="radio" value="yes" name="reftype" class="eexisting" > Yes</label>                                   
            <label><input type="radio" value="no" name="reftype" class="eexisting" > No</label>
        </div>
        <div class="form-group" id="eclientlist">                                   
    <small>Client</small>
        <select name="client" id="eclient" class="form-control show-tick m-b-10">
            <option value="">Select Client</option>
            <?php
                    foreach($cli_details as $client){
                        ?>
            <option value="<?=md5($client['id'])?>"><?=$client['fname']?> <?=$client['lname']?></option>
            <?php
                    }
            ?>
               </select>
               <div class="form-group hideme" id="eprojectlist"> 
               
               <small>Project</small>
                     <select name="project" id="eproject" class="form-control show-tick m-b-10">
                         <option value="">Select Project</option>
                     </select>
                     </div>

        </div>
        <div class="form-group hideme" id="ereferencebox">                                   
    <small>Reference (e.g Client name)</small>
            <input type="text"  id="ereference" name="reference" class="form-control" placeholder="Reference">
        </div>
    </div>
    <div class="col-12 hideme" id="ecommentbox">
                    <small>Add Comment to continue adding task in negative hours </small>
                        <textarea name="comment" id="ecomment" class="form-control show-tick m-b-10"></textarea>
                    </div>
    <div class="col-12">
    <small>Task Manager </small>
        <select name="owner" id="eowner" required class="form-control show-tick m-b-10">
            <option value="">Select User</option>
            <?php
                    foreach($emp_details as $employee){
                        ?>
            <option value="<?=md5($employee['id'])?>"><?=$employee['name']?></option>
            <?php
                    }
            ?>
               </select>
    </div>
    <div class="col-12">
    <small>Assigned to </small>
        <select name="agent" id="eagent" class="form-control show-tick m-b-10">
            <option value="">Select User</option>
            <?php
                    foreach($emp_details as $employee){
                        ?>
            <option value="<?=md5($employee['id'])?>"><?=$employee['name']?></option>
            <?php
                    }
            ?>
               </select>
    </div>
    <div class="col-12">
    <small>Time Estimate (in hrs)</small>
    <div class="input-group" >
    <input type="text"  id="eestimate" required name="estimate" pattern="[0-9.]*" class="form-control" placeholder="Estimate in hours">
    </div>
    </div>
    <div class="col-12">
        <small>Due Date</small>
        <div class="input-group" >
            <input type="text" name="duedate" id="eduedate" class="form-control commondatepicker" >
        </div>
    </div>        
    <div class="error" id="ptaskerror"></div>
    <div class="success" id="ptasksuccess"></div>
            
</div>
</div>
<div class="modal-footer">
<input type="hidden" name="vista" id="vista" >

<button type="submit" id="editptbtn" class="btn btn-primary">Save</button>
<button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
</div>
<?php echo $this->Form->end(); ?>

</div>
</div>
</div>
    <style>
    .sweet-alert{
        margin-top:5px!important;
        top: 0!important;
    }
    </style>