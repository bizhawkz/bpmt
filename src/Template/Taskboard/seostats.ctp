

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                        <?php	echo $this->Form->create(false , ['url' => ['action' => 'addpj'] , 'id' => "addpjform" , 'method' => "post"  ]); ?>
        
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                <label>Project Name*</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" required placeholder="Project Name *">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <div class="form-group">
                                    <label>Client Name*</label>
                                        <select name="client" required class="form-control show-tick">
                                            <option>Select Client Name</option>
                                            <?php
                                            foreach($cli_details as $client){
                                                 ?>
                                            <option value="<?=md5($client['id'])?>"><?=$client['fname']?> <?=$client['lname']?></option>

                                                 <?php
                                             }
                                             ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                <label>Start Date*</label>
                                    <div class="form-group">
                                        <input type="text" name="sdate" required   class="form-control commondatepicker" placeholder="Start date *">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <div class="form-group">
                                    <label>End Date</label>
                                        <input name="edate" type="text"    class="form-control datepicker" placeholder="End date">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <div class="form-group">
                                    <label>Project Estimated Hours*</label>
                                    <input name="hours" type="text" required  class="form-control" placeholder="Hours">
                                    </div>
                                </div>  
                                                
                            </div>
                            <div class="row clearfix">
                            <div class="col-md-3 col-sm-12">
                                    <label>Rate in Dollar</label>
                                    <div class="form-group">
                                        <input name="rate" type="number"  class="form-control" placeholder="Rate">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <label>Billing Type*</label>
                                    <div class="form-group">
                                        <select name="type" required class="form-control show-tick">
                                            <option value="">Select Type</option>
                                            <option value="1">Hourly</option>
                                            <option value="2">Fixed</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <label>Payment Type*</label>
                                    <div class="form-group">                                        
                                        <select name="ptype" required class="form-control show-tick">
                                        <option value="">Select Type</option>
                                            <option  value="1">Prepaid</option>
                                            <option  value="2">Postpaid</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <label>Negative Hours Limit*</label>
                                    <div class="form-group">             
                                    <input name="nlimit" type="text" pattern="[0-9.-]*" class="form-control"  placeholder="Negative Hours Limit">

                                    </div>
                                </div>
                             
                              
                            </div>

                            <div class="row clearfix">
                            <div class="col-md-3 col-sm-12">
                                    <label>Project Lead*</label>
                                    <div class="form-group">                                        
                                        <select name="lead" required class="form-control show-tick">
                                            <option>Select</option>
                                             <?php
                                             foreach($emp_details as $employee){
                                                 ?>
                                            <option value="<?=md5($employee['id'])?>"><?=$employee['name']?></option>

                                                 <?php
                                             }
                                             ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <label>Team*</label>
                                    <div class="form-group">                                        
                                        <select name="team" required class="form-control" >
                                        <option>Select</option>
                                            <?php
                                            foreach($team_details as $team){
                                                 ?>
                                            <option value="<?=md5($team['id'])?>"><?=$team['name']?></option>

                                                 <?php
                                             }
                                             ?>
                                        </select>
                                    </div>
                                </div>       
                                <div class="col-md-3 col-sm-12">
                                    <label>Allow Negative Tasks</label>
                                    <div class="form-group">                                        
                                       <input type="checkbox" name="nallowed" value="1" >
                                    </div>
                                </div>                   
                                <div class="col-sm-12">
                                <label>Describe about the project</label>
                                    <textarea name="description"  class="form-control"></textarea>
                                </div>
                                <div class="col-sm-12">

                                <div class="error" id="pjerror"></div>
                    <div class="success" id="pjsuccess"></div>                                    <div class="mt-4">
                                        <button type="submit" id="addpjtn" class="btn btn-primary">Create</button>
                   <?php echo $this->Form->end(); ?>
                                        
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>