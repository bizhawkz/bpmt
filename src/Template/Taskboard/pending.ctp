

    <div class="col-md-12">
    <div class="card">
        <div class="body">
            <table class="task_table" >
            <thead>
            <th>Task</th>
            <th>Client</th> 
            <th>Project</th>
            <th>Due Date</th>
            <th>Actions</th>
            </thead>
            <tbody>
                <?php
                $today = strtotime('today');

                foreach($retrieve_task as $task){
                    // $pj_hours = $this->Client->findhours($task['project']) ;
                    // $pj_hours_show = $this->Client->showhours($task['project']) ;
                    $pj_hours= 0 ;
                    $pj_hours_show = 0 ;
                ?>
                <tr class="<?=( $pj_hours < 0 ? "red" : $this->Client->updatenegative($task['id']) )?>">
                <td>
                <b>ID:</b> TSK00<?=$task['id']?><br>
                <b>Title:</b> <?=$task['tittle']?><br>
                <b>Description:</b> <?=$task['description']?><br>
                <b>Category:</b> <?=$task['c']['name']?><br>
                <b>Added by:</b><?=$task['cr']['name']?><br>
                <b>Manage by:</b> <?=$task['o']['name']?><br>
                <b>Added on:</b> <?=date('jS M,y', $task['created'])?>
                </td> 
                <td> <?=($task['reftype'] == "yes" ?   $task['cl']['fname'].' '.$task['cl']['lname'] : $task['reference'] )?></td>
                <td><?=$task['p']['name']?></td>
                <td><?=date('jS M,y', $task['duedate'])?></td>
                <td>  
                <?php
                if(md5($user_details[0]['id']) == $task['creator'] || md5($user_details[0]['id']) == $task['owner'] ){
                ?>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Delete" data-type="confirm" data-id="<?=md5($task['id'])?>" data-url="taskboard/delete" data-str="Task" ><i class="fa fa-trash" ></i> 
                </a>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip"  title="Edit" data-type="editpersonaltask" data-id="<?=md5($task['id'])?>" data-url="taskboard/gettask" data-str="Task" ><i class="fa fa-pencil"></i>   </a>
                <?php
                }
                ?>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Assign Task" data-type="assigntask" data-id="<?=md5($task['id'])?>" data-url="taskboard/assigntask" data-get="taskboard/gettask" data-str="Task"  ><i class="fa fa-tag" ></i> </a>  
               
                </td>

                </tr>
                              
                        <?php } ?> 
                </tbody> 
                </table> 

<?php
 $paginator = $this->Paginator;
 echo "<div class = 'paging'>";
 //for the first page link // the parameter’First’ is the labeled, same with other pagination link
 echo $paginator->first('First');
 echo " ";
 //if there was previous records, prev link will be displayed
 if($paginator->hasPrev()){
 echo $paginator->prev('<<');
 }
 //modulus => 3 specifies how many page number will be displayed
 echo $paginator->numbers(array('modulus' =>3)); 
 if($paginator->hasNext()){ //there are records, next link will be displayed
 echo $paginator->next('>>');
 }
 echo $paginator->last('Last'); //for the last page link

echo $paginator->counter([
    'format' => 'Page {{page}} of {{pages}}'
]);

 echo "</div)";
 ?> 




                              
            </div>
        </div>
    </div>
</div>







<!-- EXTRA HTML --->


<!-- Add New Task -->
<div class="modal fade" id="addtask" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h6 class="title" id="defaultModalLabel">Add New Task</h6>
</div>
<div class="modal-body">
<?php	echo $this->Form->create(false , ['url' => ['action' => 'addpersonal'] , 'id' => "addpersonalform" , 'method' => "post"  ]); ?>
<div class="row clearfix">

    <div class="col-12">
        <small>Task Category</small>
        <div class="form-group">                                    
        <select name="category" required class="form-control show-tick m-b-10">
            <option value="">Select Category</option>
            <?php
                foreach($cat_details as $category){
                    ?>
            <option value="<?=$category['id']?>"><?=$category['name']?></option>
            <?php
                }
            ?>
            </select>
        </div>
    </div>
    <div class="col-12">
    <small>Task Title</small>
        <div class="form-group">                                   
            <input type="text" required  name="title" class="form-control" placeholder="Task title">
        </div>
    </div>
    <div class="col-12">
    <small>Task Description</small>
        <div class="form-group">                                    
            <textarea name="description" required  class="form-control" placeholder="Description"></textarea>
        </div>
    </div>
    <div class="col-12">
    <div class="form-group">
            <label>Is it for a existing client?</label><br>
            <label><input type="radio" value="yes" name="reftype" class="existing" checked> Yes</label>                                   
            <label><input type="radio" value="no" name="reftype" class="existing" > No</label>
        </div>
        <div class="form-group" id="clientlist">                                   
    <small>Client</small>
        <select name="client" id="client" class="form-control show-tick m-b-10">
            <option value="">Select Client</option>
            <?php
                    foreach($cli_details as $client){
                        ?>
            <option value="<?=md5($client['id'])?>"><?=$client['fname']?> <?=$client['lname']?></option>
            <?php
                    }
            ?>
               </select>
               <div class="form-group hideme" id="projectlist"> 
                            <small>Project</small>
                        <select name="project" id="taskproject" class="form-control show-tick m-b-10">
                            <option value="">Select Project</option>
                        </select>
                        
                        </div>
        </div>
        <div class="form-group hideme" id="referencebox">                                   
    <small>Reference (e.g Client name)</small>
            <input type="text"  id="reference" name="reference" class="form-control" placeholder="Reference">
        </div>
    </div>
    <div class="col-12 hideme" id="commentbox">
                    <small>Add Comment to continue adding task in negative hours </small>
                        <textarea name="comment" id="comment" class="form-control show-tick m-b-10"></textarea>
                    </div>
    <div class="col-12">
    <small>Task Manager </small>
        <select name="owner"  class="form-control show-tick m-b-10">
            <option value="">Select User</option>
            <?php
                    foreach($emp_details as $employee){
                        ?>
            <option value="<?=md5($employee['id'])?>"><?=$employee['name']?></option>
            <?php
                    }
            ?>
               </select>
    </div>
    <div class="col-12">
    <small>Assigned to </small>
        <select name="agent" id="agent" class="form-control show-tick m-b-10">
            <option value="">Select User</option>
            <?php
                    foreach($emp_details as $employee){
                        ?>
            <option value="<?=md5($employee['id'])?>"><?=$employee['name']?></option>
            <?php
                    }
            ?>
               </select>
    </div>
    <div class="col-12">
    <small>Time Estimate (in hrs)</small>
    <div class="input-group" >
    <input type="text"  id="estimate" name="estimate" pattern="[0-9.]*" class="form-control" placeholder="Estimate in hours">
    </div>
    </div>
    <div class="col-12">
        <small>Due Date</small>
        <div class="input-group" >
            <input type="text" name="duedate" class="form-control datepicker" name="start">
        </div>
    </div>           
    <div class="error" id="ptaskerror"></div>
                    <div class="success" id="ptasksuccess"></div>         
</div>
</div>
<div class="modal-footer">
<button type="submit" id="addptbtn" class="btn btn-primary">Add</button>
<button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
</div>
<?php echo $this->Form->end(); ?>

</div>
</div>
</div>



<!-- Edit Task -->
<div class="modal fade" id="edittask" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h6 class="title" id="defaultModalLabel">Edit Task</h6>
</div>
<div class="modal-body">
<?php	echo $this->Form->create(false , ['url' => ['action' => 'editpersonal'] , 'id' => "editpersonalform" , 'method' => "post"  ]); ?>
<div class="row clearfix">

   
    <div class="col-12">
        <small>Task Category</small>
        <div class="form-group">                                    
        <select name="category" id="ecategory" class="form-control show-tick m-b-10">
            <option value="">Select Category</option>
            <?php
                foreach($cat_details as $category){
                    ?>
            <option value="<?=$category['id']?>"><?=$category['name']?></option>
            <?php
                }
            ?>
            </select>
        </div>
    </div>
    <div class="col-12">
    <small>Task Title</small>
        <div class="form-group">                                   
            <input type="text"  name="title" id="etitle" class="form-control" placeholder="Task title">
        </div>
    </div>
    <div class="col-12">
    <small>Task Description</small>
        <div class="form-group">                                    
            <textarea name="description" id="edescription" class="form-control" placeholder="Description"></textarea>
        </div>
    </div>
    <div class="col-12">
    <div class="form-group">
            <label>Is it for a existing client?</label><br>
            <label><input type="radio" value="yes" name="reftype" class="eexisting" > Yes</label>                                   
            <label><input type="radio" value="no" name="reftype" class="eexisting" > No</label>
        </div>
        <div class="form-group" id="eclientlist">                                   
    <small>Client</small>
        <select name="client" id="eclient" class="form-control show-tick m-b-10">
            <option value="">Select Client</option>
            <?php
                    foreach($cli_details as $client){
                        ?>
            <option value="<?=md5($client['id'])?>"><?=$client['fname']?> <?=$client['lname']?></option>
            <?php
                    }
            ?>
               </select>
               <div class="form-group hideme" id="eprojectlist"> 
                            <small>Project</small>
                        <select name="project" id="eproject" class="form-control show-tick m-b-10">
                            <option value="">Select Project</option>
                        </select>
                        
                        </div>
        </div>
        <div class="form-group hideme" id="ereferencebox">                                   
    <small>Reference (e.g Client name)</small>
            <input type="text"  id="ereference" name="reference" class="form-control" placeholder="Reference">
        </div>
    </div>
    <div class="col-12 hideme" id="ecommentbox">
                    <small>Add Comment to continue adding task in negative hours </small>
                        <textarea name="comment" id="ecomment" class="form-control show-tick m-b-10"></textarea>
                    </div>
    <div class="col-12">
    <small>Task Manager </small>
        <select name="owner" id="eowner" required class="form-control show-tick m-b-10">
            <option value="">Select User</option>
            <?php
                    foreach($emp_details as $employee){
                        ?>
            <option value="<?=md5($employee['id'])?>"><?=$employee['name']?></option>
            <?php
                    }
            ?>
               </select>
    </div>
    <div class="col-12">
    <small>Assigned to </small>
        <select name="agent" id="eagent" class="form-control show-tick m-b-10">
            <option value="">Select User</option>
            <?php
                    foreach($emp_details as $employee){
                        ?>
            <option value="<?=md5($employee['id'])?>"><?=$employee['name']?></option>
            <?php
                    }
            ?>
               </select>
    </div>
    <div class="col-12">
    <small>Time Estimate (in hrs)</small>
    <div class="input-group" >
    <input type="text"  id="eestimate" name="estimate" required pattern="[0-9.]*" class="form-control" placeholder="Estimate in hours">
    </div>
    </div>
    <div class="col-12">
        <small>Due Date</small>
        <div class="input-group" >
            <input type="text" name="duedate" id="eduedate" class="form-control commondatepicker" >
        </div>
    </div>    
    <div class="error" id="ptaskerror"></div>
                    <div class="success" id="ptasksuccess"></div>                
</div>
</div>
<div class="modal-footer">
<input type="hidden" name="vista" id="vista" >

<button type="submit" id="editptbtn" class="btn btn-primary">Save</button>
<button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
</div>
<?php echo $this->Form->end(); ?>

</div>
</div>
</div>


<div id="assigntaskbox"  class="hideme">
        <div id='assignerror' class='error'></div>
        <div class="col-12">
    <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Assigned to </small>
        <select name="agent" id="assagent" class="form-control show-tick m-b-10">
            <option value="">Select User</option>
            <?php
                    foreach($emp_details as $employee){
                        ?>
            <option value="<?=md5($employee['id'])?>"><?=$employee['name']?></option>
            <?php
                    }
            ?>
               </select>
               <div class="hideme" id="acommentbox">
                    <small style='text-align:left;float: left;margin: 10px 0px 4px;'>This project has negative hours. Add Comment to continue.</small>
                        <textarea name="comment" id="acomment" class="form-control show-tick m-b-10"></textarea>
                    </div>
    <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Estimated hours </small>
        <input style='display:block' type='number' min="0.25" value=1 class='form-control decimals' id='estimatehour' >

    </div>

    </div>