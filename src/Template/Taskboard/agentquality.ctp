
    <div class="col-md-12">
    <?php if(in_array('35' , $user_privilage)){ ?> 
    <a class="btn btn-primary addbtn-primary js-sweetalert" href="javascript:void(0);" data-type="bulksendbillingtask" data-id="" data-url="taskboard/bulksendbillingtask"  data-str="Task">Bulk send <i class="fa fa-arrow-right" data-toggle="tooltip" title="Bulk Send task"></i></a>
    <?php } ?>
    
    <div class="card">
        <div class="body">
            <table class="task_table" >
            <thead>
            <th><?php if(in_array('35' , $user_privilage)){ ?> <input type="checkbox" value="" id="selectall_billing" > <?php } ?></th>
            <th>Task</th>
            <th>Q.A</th> 
            <th>S.T</th> 
            <th>Comment</th> 
            <th>Completed by</th> 
            <th>Client</th>
            <th>Project</th>
            <th>Completed On</th>
            <th>Actions</th>
            </thead>
            <tbody>
                <?php
                $today = strtotime('today');

                foreach($retrieve_task as $task){
                    // $pj_hours = $this->Client->findhours($task['project']) ;
                    // $pj_hours_show = $this->Client->showhours($task['project']) ;
                    $pj_hours= 0 ;
                    $pj_hours_show = 0 ;
                ?>
                <tr  class="<?=($pj_hours < 0 ? "red" : $this->Client->updatenegative($task['id']) )?>">
                <td>
                <?php if(in_array('35' , $user_privilage)){ ?> <input type="checkbox" value="<?=$task['id']?>" data-time="<?=$task['spent']?>" class="bulksend" ><?php } ?> 
                </td>
                <td>
                <b>ID:</b> TSK00<?=$task['id']?><br>
                <b>Title:</b> <?=$task['tittle']?><br>
                <!-- <b>Description:</b> <?=$task['description']?><br> -->
                <b>Category:</b> <?=$task['c']['name']?><br>
                <!-- <b>Due date:</b> <?=date('jS M, y', $task['duedate'])?><br> -->
                  <!-- <b>Added by:</b> <?=$task['cr']['name']?><br>                           
                <b>Manage by:</b> <?=$task['o']['name']?><br>                         
                <b>Added on:</b> <?=date('jS M, y', $task['created'])?><br>
                <b>Assigned on:</b> <?=date('jS M, y', $task['assignedtime'])?><br> -->
                </td>
                <td><?=($task['qagent'] == NULL ? "NA" : $task['q']['name'])?></td>
                <td><?=$task['spent']?></td>                
                <td><?=$task['comment']?></td>                
                <td><?=$task['a']['name']?></td>
                <td><?=($task['reftype'] == "yes" ?   $task['cl']['fname'].' '.$task['cl']['lname'] : $task['reference'] )?></td>
                <td><?=$task['p']['name']?></td>
                <td><?=date('jS M, y', $task['completedtime'])?></td>
                <td>  
                <?php
                if(md5($user_details[0]['id']) == $task['creator'] || md5($user_details[0]['id']) == $task['owner'] ){
                ?>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Delete" data-type="confirm" data-id="<?=md5($task['id'])?>" data-url="taskboard/delete" data-str="Task" ><i class="fa fa-trash" ></i></a>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Edit" data-type="editpersonaltask" data-id="<?=md5($task['id'])?>" data-url="taskboard/gettask" data-str="Task" ><i class="fa fa-pencil"></i></a>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" data-toggle="tooltip" title="Complete task" data-type="completetaskquality" data-id="<?=md5($task['id'])?>" data-url="taskboard/completetaskquality" data-get="taskboard/gettask" data-str="Task"  ><i class="fa fa-arrow-right" ></i> </a>  
                <?php
                }
                ?>
                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Comments on task" data-type="commenttask" data-id="<?=md5($task['id'])?>" data-url="taskboard/commenttask" data-str="Task"  ><i class="fa fa-comment" ></i> </a>  
               
                </td>

                </tr>
                              
                        <?php } ?> 
                </tbody> 
                </table> 
            
<?php
 $paginator = $this->Paginator;
 echo "<div class = 'paging'>";
 //for the first page link // the parameter’First’ is the labeled, same with other pagination link
 echo $paginator->first('First');
 echo " ";
 //if there was previous records, prev link will be displayed
 if($paginator->hasPrev()){
 echo $paginator->prev('<<');
 }
 //modulus => 3 specifies how many page number will be displayed
 echo $paginator->numbers(array('modulus' =>3)); 
 if($paginator->hasNext()){ //there are records, next link will be displayed
 echo $paginator->next('>>');
 }
 echo $paginator->last('Last'); //for the last page link

echo $paginator->counter([
    'format' => 'Page {{page}} of {{pages}}'
]);

 echo "</div)";
 ?> 

            
            
            
            
            </div>
        </div>
    </div>
</div>




<?php	echo $this->Form->create(false , ['url' => ['action' => ''] , 'id' => "" , 'method' => "post"  ]); ?>

<?php echo $this->Form->end(); ?>


<!-- EXTRA HTML --->

<div id="sendbillingbox"  class="hideme">
        <div id='completeerror' class='error'></div>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Available Comment</small>
        <textarea class='form-control' name='comment'  id='commenttext'></textarea>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Spent hours (Please change if spent hours is not equal to billable hours)</small>
        <input style='display:block' type='number' min="0" value=1 class='form-control decimals' id='spenthour' >

    </div>



<!-- Edit Task -->
<div class="modal fade" id="edittask" tabindex="-1" role="dialog">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<h6 class="title" id="defaultModalLabel">Edit Task</h6>
</div>
<div class="modal-body">
<?php	echo $this->Form->create(false , ['url' => ['action' => 'editpersonal'] , 'id' => "editpersonalform" , 'method' => "post"  ]); ?>
<div class="row clearfix">
   
    <div class="col-12">
        <small>Task Category</small>
        <div class="form-group">                                    
        <select name="category" id="ecategory" class="form-control show-tick m-b-10">
            <option value="">Select Category</option>
            <?php
                foreach($cat_details as $category){
                    ?>
            <option value="<?=$category['id']?>"><?=$category['name']?></option>
            <?php
                }
            ?>
            </select>
        </div>
    </div>
    <div class="col-12">
    <small>Task Title</small>
        <div class="form-group">                                   
            <input type="text"  name="title" id="etitle" class="form-control" placeholder="Task title">
        </div>
    </div>
    <div class="col-12">
    <small>Task Description</small>
        <div class="form-group">                                    
            <textarea name="description" id="edescription" class="form-control" placeholder="Description"></textarea>
        </div>
    </div>
    <div class="col-12">
    <div class="form-group">
            <label>Is it for a existing client?</label><br>
            <label><input type="radio" value="yes" name="reftype" class="eexisting" > Yes</label>                                   
            <label><input type="radio" value="no" name="reftype" class="eexisting" > No</label>
        </div>
        <div class="form-group" id="eclientlist">                                   
    <small>Client</small>
        <select name="client" id="eclient" class="form-control show-tick m-b-10">
            <option value="">Select Client</option>
            <?php
                    foreach($cli_details as $client){
                        ?>
            <option value="<?=md5($client['id'])?>"><?=$client['fname']?> <?=$client['lname']?></option>
            <?php
                    }
            ?>
               </select>
               <div class="form-group hideme" id="eprojectlist"> 
                            <small>Project</small>
                        <select name="project" id="eproject" class="form-control show-tick m-b-10">
                            <option value="">Select Project</option>
                        </select>
                        
                        </div>
        </div>
        <div class="form-group hideme" id="ereferencebox">                                   
    <small>Reference (e.g Client name)</small>
            <input type="text"  id="ereference" name="reference" class="form-control" placeholder="Reference">
        </div>
    </div>
    <div class="col-12 hideme" id="commentbox">
                    <small>Add Comment to continue adding task in negative hours </small>
                        <textarea name="comment" id="comment" class="form-control show-tick m-b-10"></textarea>
                    </div>
    <div class="col-12">
    <small>Task Manager </small>
        <select name="owner" id="eowner" required class="form-control show-tick m-b-10">
            <option value="">Select User</option>
            <?php
                    foreach($emp_details as $employee){
                        ?>
            <option value="<?=md5($employee['id'])?>"><?=$employee['name']?></option>
            <?php
                    }
            ?>
               </select>
    </div>
    <div class="col-12">
    <small>Assigned to </small>
        <select name="agent" id="eagent" class="form-control show-tick m-b-10">
            <option value="">Select User</option>
            <?php
                    foreach($emp_details as $employee){
                        ?>
            <option value="<?=md5($employee['id'])?>"><?=$employee['name']?></option>
            <?php
                    }
            ?>
               </select>
    </div>
    <div class="col-12">
    <small>Time Estimate (in hrs)</small>
    <div class="input-group" >
    <input type="text"  id="eestimate" required  name="estimate"  pattern="[0-9.]*" class="form-control" placeholder="Estimate in hours">
    </div>
    </div>
    <div class="col-12">
        <small>Due Date</small>
        <div class="input-group" >
            <input type="text" name="duedate" id="eduedate" class="form-control commondatepicker" >
        </div>
    </div>         
    <div class="error" id="ptaskerror"></div>
    <div class="success" id="ptasksuccess"></div>
           
</div>
</div>
<div class="modal-footer">
<input type="hidden" name="vista" id="vista" >

<button type="submit" id="editptbtn" class="btn btn-primary">Save</button>
<button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
</div>
<?php echo $this->Form->end(); ?>

</div>
</div>
</div>
    <style>
    .sweet-alert{
        margin-top:5px!important;
        top: 0!important;
    }
    </style>