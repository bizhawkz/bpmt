
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Team List</h2>
                            <ul class="header-dropdown">
                                <li><a href="javascript:void(0);" class="btn btn-info" data-toggle="modal" data-target="#addteam">Add New</a></li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover js-basic-example dataTable table-custom table-striped m-b-0 c_list">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>
                                                <label class="fancy-checkbox">
                                                    <input class="select-all" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>Name</th>
                                            <th>Created on</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($team_details as $team)
                                    {
                                        ?>
                                        <tr>
                                            <td class="width45">
                                            <label class="fancy-checkbox">
                                                    <input class="checkbox-tick" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>
                                            <h6 class="mb-0"><?=$team['name']?></h6>
                                            </td>
                                            <td><?=date('jS F Y', $team['created'])?></td>
                                            <td>
                                                <button type="button" data-id="<?= $team['id']; ?>" data-toggle="modal" data-target="#editteam" class="btn btn-sm btn-outline-secondary edit" title="Edit"><i class="fa fa-edit"></i></button>
                                                
                                                <button type="button" data-id="<?= $team['id']; ?>" data-url="team/delete" data-str="delete"  class="btn btn-sm btn-outline-danger js-sweetalert" title="Delete" data-type="confirm"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>



    <!------------------ Extra HTML --------------------->

    
<div class="modal animated zoomIn" id="addteam" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="title" id="defaultModalLabel">Add Team</h6>
            </div>
            <div class="modal-body">
            <?php	echo $this->Form->create(false , ['url' => ['action' => 'addteam'] , 'id' => "addteamform" , 'method' => "post"  ]); ?>

                <div class="row clearfix">
                    <div class="error" id="tmerror"></div>
                    <div class="success" id="tmsuccess"></div>
                    <div class="col-md-12">
                        <div class="form-group">                                    
                            <input type="text" class="form-control" required name="name" placeholder="Name">
                        </div>
                    </div>
                   
                    
                    <div class="button_row" >
                    <hr>
                    <button type="submit" class="btn btn-primary" id="addtmbtn">Add</button>
                    <button type="button" class="btn btn-secondary" style="margin-right: 10px;" data-dismiss="modal">CLOSE</button>
                    </div>
                    
                   <?php echo $this->Form->end(); ?>
                   
                </div>
            </div>
             
        </div>
    </div>
</div>


    <!------------------ Update Team Name --------------------->

    
<div class="modal animated zoomIn" id="editteam" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="title" id="defaultModalLabel">Update Team</h6>
            </div>
            <div class="modal-body">
            <?php   echo $this->Form->create(false , ['url' => ['action' => 'editteam'] , 'id' => "editteamform" , 'method' => "post"  ]); ?>

                <div class="row clearfix">
                    <div class="error" id="edittmerror"></div>
                    <div class="success" id="edittmsuccess"></div>
                    <div class="col-md-12">
                        <div class="form-group">                                    
                            <input type="text" id="team_name" class="form-control"  required name="name" placeholder="Name">
                            <input type="hidden" id="team_tid" class="form-control" name="tid" >
                        </div>
                    </div>
                   
                    
                    <div class="button_row" >
                    <hr>
                    <button type="submit" class="btn btn-primary" id="edittmbtn">Update</button>
                    <button type="button" class="btn btn-secondary" style="margin-right: 10px;" data-dismiss="modal">CLOSE</button>
                    </div>
                    
                   <?php echo $this->Form->end(); ?>
                   
                </div>
            </div>
             
        </div>
    </div>
</div>
