
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Roles List</h2>
                            <ul class="header-dropdown">
                                <li><a href="javascript:void(0);" class="btn btn-info" data-toggle="modal" data-target="#addrole">Add New</a></li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover js-basic-example dataTable table-custom table-striped m-b-0 c_list" id="role_table">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>
                                                <label class="fancy-checkbox">
                                                    <input class="select-all" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>Name</th>
                                            <th>Privilages</th>
                                            <th>Created on</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($role_details as $role){
                                        ?>
                                        <tr>
                                            <td class="width45">
                                            <label class="fancy-checkbox">
                                                    <input class="checkbox-tick" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </td>
                                            <td>
                                                <h6 class="mb-0"><?=$role['name']?></h6>
                                            </td>
                                            <td>
                                                <a data-toggle="modal" style="color:black;" class="viewroles" data-id="<?= $role['id']; ?>" href="#viewrole"><?= count(explode(',',$role['privilage'])); ?> Privilages</a>
                                            </td>

                                            <td><?=date('jS F Y', $role['added_date'])?></td>
                                            <td>
                                                <button type="button" data-id="<?= $role['id']; ?>" class="btn btn-sm btn-outline-secondary editroles" data-toggle="modal"  data-target="#editrole" title="Edit"><i class="fa fa-edit"></i></button>
                                                <button type="button" data-url="roles/delete" data-id="<?=$role['id']?>" data-str="delete" class="btn btn-sm btn-outline-danger js-sweetalert" title="Delete" data-type="confirm"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>



    <!------------------ Extra HTML --------------------->

    
<div class="modal animated zoomIn" id="addrole" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="title" id="defaultModalLabel">Add Role</h6>
            </div>
            <div class="modal-body">
            <?php	echo $this->Form->create(false , ['url' => ['action' => 'addrole'] , 'id' => "addroleform" , 'method' => "post"  ]); ?>

                <div class="row clearfix">
                    <div class="error" id="rlerror"></div>
                    <div class="success" id="rlsuccess"></div>
                    <div class="col-md-12">
                        <div class="form-group">                                    
                            <input type="text" class="form-control" required name="name" placeholder="Name">
                        </div>
                    </div>
                   
                    <div class="col-md-12">
                        <div class="form-group"> 
                        <h6 class="subhead">Employee Management Privilages</h6>
                            <?php
                            foreach($emprole_details as $privilage ){
                                echo '<label class="checkbox-inline col-md-4"><input type="checkbox" class="inline-checkbox" value="'.$privilage['id'].'"  name="privilage[]" >'.$privilage['name'].'</label>' ;
                            }                                   
                            ?>
                        <h6 class="subhead">Client Management Privilages</h6>
                            <?php
                            foreach($clientrole_details as $privilage ){
                                echo '<label class="checkbox-inline col-md-4"><input type="checkbox" class="inline-checkbox" value="'.$privilage['id'].'"  name="privilage[]" >'.$privilage['name'].'</label>' ;
                            }                                   
                            ?>
                        <h6 class="subhead">Project Management Privilages</h6>
                            <?php
                            foreach($projectrole_details as $privilage ){
                                echo '<label class="checkbox-inline col-md-4"><input type="checkbox" class="inline-checkbox" value="'.$privilage['id'].'"  name="privilage[]" >'.$privilage['name'].'</label>' ;
                            }                                   
                            ?>
                        <h6 class="subhead">Billing Management Privilages</h6>
                            <?php
                            foreach($billrole_details as $privilage ){
                                echo '<label class="checkbox-inline col-md-4"><input type="checkbox" class="inline-checkbox" value="'.$privilage['id'].'"  name="privilage[]" >'.$privilage['name'].'</label>' ;
                            }                                   
                            ?>
                        <h6 class="subhead">Task Management Privilages</h6>
                        
                            <?php
                            foreach($taskrole_details as $privilage ){
                                echo '<label class="checkbox-inline col-md-4"><input type="checkbox" class="inline-checkbox" value="'.$privilage['id'].'"  name="privilage[]" >'.$privilage['name'].'</label>' ;
                            }                                   
                            ?>
                        </div>
                    </div>
                    
                    <div class="button_row" >
                    <hr>
                    <button type="submit" class="btn btn-primary" id="addrlbtn">Add</button>
                    <button type="button" class="btn btn-secondary" style="margin-right: 10px;" data-dismiss="modal">CLOSE</button>
                    </div>
                    
                   <?php echo $this->Form->end(); ?>
                   
                </div>
            </div>
             
        </div>
    </div>
</div>




<!------------------ Roles View --------------------->

    
<div class="modal animated zoomIn" id="viewrole" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="title" id="defaultModalLabel">Privilages List</h6>
            </div>
            <div class="modal-body">

                <div class="row clearfix">
                    <div class="col-md-12" id="viewrolediv">
                        <div class="container-fluid"> 
                           <div class="row" id="view_pri">
                           </div>
                        </div>
                    </div>                   
                </div>
            </div>
             
        </div>
    </div>
</div>


    <!------------------ Edit Role --------------------->

    
<div class="modal animated zoomIn" id="editrole" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="title" id="defaultModalLabel">Update Role</h6>
            </div>
            <div class="modal-body">
            <?php   echo $this->Form->create(false , ['url' => ['action' => 'editrole'] , 'id' => "editroleform" , 'method' => "post"  ]); ?>

                <div class="row clearfix">
                    <div class="error" id="editrlerror"></div>
                    <div class="success" id="editrlsuccess"></div>
                    <div class="col-md-12">
                        <div class="form-group">                                    
                            <input type="text" class="form-control" id="rolename" required name="name" placeholder="Name">
                            <input type="hidden" id="id"  name="id" >
                        </div>
                    </div>
                   
                    <div class="col-md-12">
                        <div class="form-group"> 
                        <h6 class="subhead">Employee Management Privilages</h6>
                            <?php
                            foreach($emprole_details as $privilage ){
                                echo '<label class="checkbox-inline col-md-4"><input type="checkbox" class="inline-checkbox" id="privilage" value="'.$privilage['id'].'"  name="privilage[]" >'.$privilage['name'].'</label>' ;
                            }                                   
                            ?>
                        <h6 class="subhead">Client Management Privilages</h6>
                            <?php
                            foreach($clientrole_details as $privilage ){
                                echo '<label class="checkbox-inline col-md-4"><input type="checkbox" class="inline-checkbox" id="privilage" value="'.$privilage['id'].'"  name="privilage[]" >'.$privilage['name'].'</label>' ;
                            }                                   
                            ?>
                        <h6 class="subhead">Project Management Privilages</h6>
                            <?php
                            foreach($projectrole_details as $privilage ){
                                echo '<label class="checkbox-inline col-md-4"><input type="checkbox" class="inline-checkbox" id="privilage" value="'.$privilage['id'].'"  name="privilage[]" >'.$privilage['name'].'</label>' ;
                            }                                   
                            ?>
                        <h6 class="subhead">Billing Management Privilages</h6>
                            <?php
                            foreach($billrole_details as $privilage ){
                                echo '<label class="checkbox-inline col-md-4"><input type="checkbox" class="inline-checkbox" id="privilage" value="'.$privilage['id'].'"  name="privilage[]" >'.$privilage['name'].'</label>' ;
                            }                                   
                            ?>
                        <h6 class="subhead">Task Management Privilages</h6>
                        
                            <?php
                            foreach($taskrole_details as $privilage ){
                                echo '<label class="checkbox-inline col-md-4"><input type="checkbox" class="inline-checkbox" id="privilage" value="'.$privilage['id'].'"  name="privilage[]" >'.$privilage['name'].'</label>' ;
                            }                                   
                            ?>
                        </div>
                    </div>
                    
                    <div class="button_row" >
                    <hr>
                    <button type="submit" class="btn btn-primary" id="editrlbtn">Update</button>
                    <button type="button" class="btn btn-secondary" style="margin-right: 10px;" data-dismiss="modal">CLOSE</button>
                    </div>
                    
                   <?php echo $this->Form->end(); ?>
                   
                </div>
            </div>
             
        </div>
    </div>
</div>