
<!doctype html>
<html lang="en">

<head>
<title>:: BPMT :: Login</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="BPMT Login Page">
<meta name="author" content="Bizhawkz">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="/bpmt/css/bootstrap.min.css">

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<!-- MAIN CSS -->
<link rel="stylesheet" href="/bpmt/css/main.css">
<link rel="stylesheet" href="/bpmt/css/color_skins.css">
<style>
div#error {
    display: none;
    background: red;
    color: #fff;
    padding: 10px;
    margin-bottom: 10px;
    text-align:center;
}
.fa-spinner{
    display: none;
}
</style>
</head>

<body class="theme-orange">
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle auth-main">
				<div class="auth-box">
                    <div class="top">
                        <img src="/bpmt/img/logo.svg" alt="BPMT">
                    </div>
					<div class="card">
                        <div class="header">
                            <p class="lead">Login to your account</p>
                        </div>
                        <div class="body">
                        <div id="error" ></div>
        <?php	echo $this->Form->create(false , ['url' => ['action' => 'logincheck'] , 'id' => "loginform"  ]); ?>
 
                                <div class="form-group">
                                    <label for="signin-email" class="control-label sr-only">Email</label>
                                    <input type="email" class="form-control" id="email" name="email" value="" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="signin-password" class="control-label sr-only">Password</label>
                                    <input type="password" class="form-control" id="password" name="password" value="" placeholder="Password">
                                </div>
                                <div class="form-group clearfix">
                                    <label class="fancy-checkbox element-left">
                                        <input type="checkbox" name="remember">
                                        <span>Remember me</span>
                                    </label>								
                                </div>
                                <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-spinner fa-spin"></i> LOGIN</button>
                                <div class="bottom" style="text-align:center;margin:10px auto">
                                    <span class="helper-text m-b-10"><i class="fa fa-lock"></i> <a href="page-forgot-password.html">Forgot password?</a></span>
                              
                              
                                </div>
    <?php echo $this->Form->end(); ?>
                          
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
<script>

/* Login form submission */

$("#loginform").submit(function(e){
    $(".fa-spinner").css('display' , 'inline-block' );
    e.preventDefault();
   $(this).ajaxSubmit({
    success: function(response){
        
      if(response.result === "success" ){ 
                location.href = "dashboard" ; 
                
            }
      else if(response.result === "email" ){
    $(".fa-spinner").css('display' , 'none' );

          $("#error").html("Invalid Login Details.") ;
          $("#error").fadeIn().delay('5000').fadeOut('slow');
        
  }
  else if(response.result === "captcha" ){
    $(".fa-spinner").css('display' , 'none' );
       
          $("#error").html("Please check the reCaptcha.") ;
          $("#error").fadeIn().delay('5000').fadeOut('slow');
        
  }
  else{
    $(".fa-spinner").css('display' , 'none' );

    $("#error").html("Some error occured. Error: "+response.result) ;
    $("#error").fadeIn().delay('5000').fadeOut('slow');
  }
    }	
  });			
  return false;
  
  });
  
  /* END */
</script>
</body>
</html>
