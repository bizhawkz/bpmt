<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

// echo $value ;

?>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<h2>Pending billing list</h2>

<table>
  <tr>
    <th>Client</th>
    <th>Last Payment</th>
    <th>Expected Payment</th>
    <th>Last Remark</th>
  </tr>
  <?php
  foreach($value as $val){
      ?>
  <tr>
    <td><?=$val['client']?></td>
    <td><?=date('jS F,Y' , $val[0]['billing_date'])?></td>
    <td><?=date('jS F,Y' , $val[0]['next_payment'])?></td>
    <td><?=date('jS F,Y' , $val[0]['remark'])?></td>
      </tr>

      <?php
  }
  ?>

</table>