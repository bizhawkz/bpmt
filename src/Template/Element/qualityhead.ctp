<div class="row clearfix">


<div class="col-lg-2 col-md-12" data-title="Click here to view your raw tasks (not assigned niether managed)" data-toggle="tooltip">
    <div class="card redirectbtn taskbutton <?=($actionName ==  "index" ? "active" : "" )?>" data-href="/bpmt/quality<?=($user_sel_id == "" ? "" : '/index/'.$user_sel_id )?>">
        <div class="header">
            <h2>New<small><?=$count_unassigned?> Task(s)</small></h2>

        </div>

    </div>    
    </div>   

    <div class="col-lg-2 col-md-12" data-title="Click here to view tasks managed by you(not assigned)" data-toggle="tooltip">
    <div class="card planned_task redirectbtn taskbutton <?=($actionName ==  "assigned" ? "active" : "" )?>" data-href="/bpmt/quality/assigned<?=($user_sel_id == "" ? "" : '/'.$user_sel_id )?>">
        <div class="header">
            <h2>Assigned<small><?=$count_assigned?> Task(s)</small></h2>
        </div>
    </div>    
    </div>

    
    <div class="col-lg-2 col-md-12" data-title="Click here to view assigned tasks" data-toggle="tooltip">
    <div class="card assigned_task redirectbtn taskbutton <?=($actionName ==  "ongoing" ? "active" : "" )?>" data-href="/bpmt/quality/ongoing<?=($user_sel_id == "" ? "" : '/'.$user_sel_id )?>">
        <div class="header">
            <h2>Ongoing<small><?=$count_progress?> Task(s)</small></h2>
              
        </div>
        
    </div>    
    </div>  
    
    <div class="col-lg-2 col-md-12" data-title="Click here to view rework tasks,i.e task that you have started" data-toggle="tooltip">
    <div class="card red redirectbtn taskbutton <?=($actionName ==  "rework" ? "active" : "" )?>" data-href="/bpmt/quality/rework<?=($user_sel_id == "" ? "" : '/'.$user_sel_id )?>" >
        <div class="header">
            <h2 class="red" style="color:#fff">Rework<small style="color:#fff"><?=$count_rework?> Task(s)</small></h2>
        </div>
        
    </div>    
    </div>   
    
<div class="col-lg-2 col-md-12" data-title="Click here to view your tasks undergoing quality" data-toggle="tooltip">
    <div class="card redirectbtn completetask taskbutton <?=($actionName ==  "completed" ? "active" : "" )?>" data-href="/bpmt/quality<?=($user_sel_id == "" ? "" : '/completed/'.$user_sel_id )?>">
        <div class="header">
            <h2>Completed<small><?=$count_completed?> Task(s)</small></h2>

        </div>
         
    </div>    
    </div>   
           
  
    <div class="col-md-12">
                        <div class="row">

                         
                        <div class="col-md-3"> 
                            <small class="form-text text-muted">Category</small>
                            <div class="form-group">
                            <select name="categoryfilter" id="categoryfilter" class="categoryfilter  form-control pull-left">
                        <option value="" >Select Category</option>
  <option value="all" <?=($user_cat_id == 'all' ? "selected" : "" )?> >All Category</option>
                            <?php
                              foreach($cat_details as $category){
                                echo '<option value="'.md5($category['id']).'"   '.($user_cat_id == md5($category['id']) ? "selected" : "" ).'  >'.$category['name'].'</option>' ; 
                              }
                            ?>
                        </select>
                        </div>
                        </div>
                        <div class="col-md-3"> 
                        <small class="form-text text-muted">Agent</small>
                        <div class="form-group">
                      <select name="agentuser" class="agentfilter form-control pull-left">
                        <option value="" >Select Agent</option>

                        <?php
                        if(in_array('21' , $user_privilage )){
                            ?>
                        <option value="alltask" <?=($user_sel_id == 'alltask' ? "selected" : "" )?> >All   task</option>
                       <?php
                        }
                        ?>
                      
                       
                          <?php
                                   foreach($emp_details as $employee){
                                   echo '<option value="'.md5($employee['id']).'"   '.($user_sel_id == md5($employee['id']) ? "selected" : "" ).'  >'.$employee['name'].'</option>' ; 
                                   }
                           ?>
                       </select>
                       </div>
                       </div>
                       <?php
                            if($actionName != "index"){
                        ?>
                        <div class="col-md-3"> 
                        <small class="form-text text-muted">Quality Agent</small>
                        <div class="form-group">
                      <select name="qagentuser" id="qagentfilter" class="qagentfilter form-control pull-left">
                        <option value="" >Select Agent</option>

                        <?php
                        if(in_array('21' , $user_privilage )){
                            ?>
                        <option value="alltask" <?=($qa_sel_id == 'alltask' ? "selected" : "" )?> >All   task</option>
                       <?php
                        }
                        ?>
                      
                       
                          <?php
                                   foreach($qa_details as $qaemployee){
                                   echo '<option value="'.md5($qaemployee['id']).'"   '.($qa_sel_id == md5($qaemployee['id']) ? "selected" : "" ).'  >'.$qaemployee['name'].'</option>' ; 
                                   }
                           ?>
                       </select>
                       </div>
                       </div>
                       <?php
                            }
                       ?>

                       <!-- <div class="col-md-2"> 
                        <small class="form-text text-muted">Completed On</small>
                        <div class="form-group">
                           <input type="text" name="completed" value="<?=$completedon?>" class="completedfilter form-control daterange">
                       </div>
                       </div> -->
                       <!-- <div class="col-md-2"> 
                        <small class="form-text text-muted">Billed On</small>
                        <div class="form-group">
                           <input type="text" name="billed" value="<?=$billedon?>" class="billedfilter form-control daterange">
                       </div>
                       </div>  
                        
                           <div class="col-md-3"> 
                        <small class="form-text text-muted">Filter by Date</small>
                        <div class="form-group">
                           <input type="text" name="completed" id="filterbydate" value="<?=$completedon?>" class="completedfilter form-control daterange">
                       </div>
                       </div>
                        </div>
                        <div style="clear:both;height:5px"></div>
                       
                    </div> -->