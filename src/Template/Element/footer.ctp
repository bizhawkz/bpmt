  
</div>



<!-- Default Size -->
<div class="modal fade" id="view_info" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="title" id="defaultModalLabel">User Details</h6>
            </div>
            <div class="modal-body">
                <div class="body top_counter">
                    <div class="icon">
                        <img src="/bpmt/img/avatar2.jpg" class="rounded-circle" alt="">
                    </div>
                    <div class="content m-t-5">
                        <div>Team Leader</div>
                        <h6>Aiden Chavez</h6>
                    </div>
                </div>
                <hr>                
                <small class="text-muted">Address: </small>
                <p>795 Folsom Ave, Suite 600 San Francisco, 94107</p>
                <small class="text-muted">Email address: </small>
                <p>michael@gmail.com</p>
                <small class="text-muted">Mobile: </small>
                <p>+ 202-555-2828</p>
                <small class="text-muted">Birth Date: </small>
                <p class="m-b-0">October 22th, 1990</p>                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>

<div id="noAccess">
This site can only be accessed on Desktop.
</div>


<!-- ?t=<?=strtotime('now')?> -->
<!-- Javascript -->
<script src="/bpmt/js/libscripts.bundle.js"></script>

<script src="/bpmt/js/index.js"></script>
<script src="/bpmt/js/vendorscripts.bundle.js"></script>
<script src="/bpmt/js/mainscripts.bundle.js"></script>

<script src="/bpmt/js/jvectormap.bundle.js"></script>  
<script src="/bpmt/js/morrisscripts.bundle.js"></script> 
<script src="/bpmt/js/knob.bundle.js"></script>  


<script src="/bpmt/js/bootstrap-datepicker.min.js"></script> 
<script src="/bpmt/js/bootstrap-multiselect.js"></script>
<script src="/bpmt/js/dropify.min.js"></script>

<!-- <script src="/bpmt/js/summernote.js"></script> -->
<script src="/bpmt/js/dropify.js"></script>
<script src="/bpmt/js/sweetalert.min.js"></script>  
   <script src="/bpmt/js/dialogs.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>

<script src="/bpmt/js/datatablescripts.bundle.js"></script>
 
<script src="/bpmt/js/jquery-datatable.js"></script>

<script src="https://cdn.datatables.net/responsive/2.2.1/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>

<script src="/bpmt/js/mdtimepicker.js" type="text/javascript"></script>
    <link href="/bpmt/css/mdtimepicker.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script src="/bpmt/js/custom.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>

  
</body>
</html>
