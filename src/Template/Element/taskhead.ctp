<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="bh_chart hidden-xs down-row">
                            <div class="float-left m-r-15">
                                <small>Management Spent</small>
                                <h6 class="mb-0 mt-1"><i class="icon-clock"></i> <?=($count_spent_management == "" ? 0 : $count_spent_management)?></h6>
                            </div>
                            <!-- <span class="bh_visitors float-right">2,5,1,8,3,6,7,5</span> -->
                        </div>
                        <div class="bh_chart hidden-sm down-row">
                            <div class="float-left m-r-15">
                                <small>Client Meeting</small>
                                <h6 class="mb-0 mt-1"><i class="icon-clock"></i> <?=($count_spent_cmeeting == "" ? 0 : $count_spent_cmeeting)?></h6>
                            </div>
                            <!-- <span class="bh_visits">10,8,9,3,5,8,5</span> -->
                        </div>
                        <div class="bh_chart hidden-sm down-row">
                            <div class="float-left m-r-15">
                                <small>Office Meeting</small>
                                <h6 class="mb-0 mt-1"><i class="icon-clock"></i> <?=($count_spent_omeeting == "" ? 0 : $count_spent_omeeting)?></h6>
                            </div>
                            <!-- <span class="bh_chats float-right">1,8,5,6,2,4,3,2</span> -->
                        </div>
                        <div class="bh_chart hidden-sm down-row">
                            <div class="float-left m-r-15">
                                <small>Research</small>
                                <h6 class="mb-0 mt-1"><i class="icon-clock"></i> <?=($count_spent_research == "" ? 0 : $count_spent_research)?></h6>
                            </div>
                            <!-- <span class="bh_chats float-right">1,8,5,6,2,4,3,2</span> -->
                        </div>
                        <div class="bh_chart hidden-sm down-row">
                            <div class="float-left m-r-15">
                                <small>Non-Productive</small>
                                <h6 class="mb-0 mt-1"><i class="icon-clock"></i> <?=($count_spent_np == "" ? 0 : $count_spent_np)?></h6>
                            </div>
                            <!-- <span class="bh_chats float-right">1,8,5,6,2,4,3,2</span> -->
                        </div>
                        <!-- <div class="topmsg"><?=$top_message?></div> -->

                    </div>
<?php
if(in_array('22' , $user_privilage )){
?>
<div class="col-lg-<?=(in_array('22' , $user_privilage) ? "3" : "2")?> col-md-12" data-title="Click here to view your raw tasks (not assigned niether managed)" data-toggle="tooltip">
    <div class="card redirectbtn taskbutton <?=($actionName ==  "index" ? "active" : "" )?>" data-href="/bpmt/taskboard<?=($user_sel_id == "" ? "" : '/index/'.$user_sel_id )?>">
        <div class="header">
            <h2>New<small><?=$count_unassigned?> Task(s)</small></h2>

        </div>
        <div class="below_hours">
            <?=$unassigned_hours?> hr(s)
        </div>
    </div>    
    </div>   

    <div class="col-lg-<?=(in_array('22' , $user_privilage) ? "3" : "2")?> col-md-12" data-title="Click here to view tasks managed by you(not assigned)" data-toggle="tooltip">
    <div class="card planned_task redirectbtn taskbutton <?=($actionName ==  "pending" ? "active" : "" )?>" data-href="/bpmt/taskboard/pending<?=($user_sel_id == "" ? "" : '/'.$user_sel_id )?>">
        <div class="header">
            <h2>Pending<small><?=$count_pending?> Task(s)</small></h2>
            
        </div>
        <div class="below_hours">
            <?=$pending_hours?> hr(s)
        </div>
    </div>    
    </div>
<?php }
    ?>
    
    <div class="col-lg-<?=(in_array('22' , $user_privilage) ? "3" : "2")?> col-md-12" data-title="Click here to view assigned tasks" data-toggle="tooltip">
    <div class="card assigned_task redirectbtn taskbutton <?=($actionName ==  "assigned" ? "active" : "" )?>" data-href="/bpmt/taskboard/assigned<?=($user_sel_id == "" ? "" : '/'.$user_sel_id )?>">
        <div class="header">
            <h2>Assigned New <span style="font-size:small !important; "><?=$count_assigned?> Task(s) </span>  <?php if(isset($count_rework2)) {?> Assigned Rework <span style="font-size:small !important; "><?=$count_rework2?> Task(s)</span> <?php } ?> 
</h2>
              
        </div>
        <div class="below_hours">
            <?=$assigned_hours?> hr(s) / <?=$rework2_hours?> hr(s)
        </div>
    </div>    
    </div>  
    
    <div class="col-lg-<?=(in_array('22' , $user_privilage) ? "3" : "2")?> col-md-12" data-title="Click here to view ongoing tasks,i.e task that you have started" data-toggle="tooltip">
    <div class="card ongoing_task redirectbtn taskbutton <?=($actionName ==  "ongoing" ? "active" : "" )?>" data-href="/bpmt/taskboard/ongoing<?=($user_sel_id == "" ? "" : '/'.$user_sel_id )?>" >
        <div class="header">
            <h2>Ongoing<small><?=$count_progress?> Task(s)</small></h2>
        </div>
        <div class="below_hours">
            <?=$progress_hours?> hr(s)
        </div>
    </div>    
    </div>   
    
<div class="col-lg-<?=(in_array('22' , $user_privilage) ? "3" : "2")?> col-md-12" data-title="Click here to view your tasks undergoing quality" data-toggle="tooltip">
    <div class="card redirectbtn qualitytask taskbutton <?=($actionName ==  "agentquality" ? "active" : "" )?>" data-href="/bpmt/taskboard<?=($user_sel_id == "" ? "" : '/agentquality/'.$user_sel_id )?>">
        <div class="header">
            <h2>Under Quality<small><?=$count_quality?> Task(s)</small></h2>

        </div>
        <div class="below_hours">
            <?=$quality_hours?> hr(s)
        </div>
    </div>    
    </div>   
     
<div class="col-lg-<?=(in_array('22' , $user_privilage) ? "3" : "2")?> col-md-12" data-title="Click here to view your rework tasks sent by quality" data-toggle="tooltip">
    <div class="card reworktask redirectbtn taskbutton <?=($actionName ==  "rework" ? "active" : "" )?>" data-href="/bpmt/taskboard<?=($user_sel_id == "" ? "" : '/rework/'.$user_sel_id )?>">
        <div class="header">
            <h2>Rework<small><?=$count_rework?> Task(s)</small></h2>

        </div>
        <div class="below_hours">
            <?=$rework2_hours?> hr(s)
        </div>
    </div>    
    </div>   
    <div class="col-lg-<?=(in_array('22' , $user_privilage) ? "3" : "2")?> col-md-12" data-title="Click here to view completed tasks which are yet to be billed" data-toggle="tooltip">
    <div class="card completed_task redirectbtn taskbutton <?=($actionName ==  "completed" ? "active" : "" )?>" data-href="/bpmt/taskboard/completed<?=($user_sel_id == "" ? "" : '/'.$user_sel_id )?>">
        <div class="header">
            <h2>Completed<small><?=$count_completed?> Task(s)</small></h2>
        </div>
        <div class="below_hours">
            <?=$completed_hours?> hr(s)
        </div>
    </div>    
    </div>  
    <div class="col-lg-<?=(in_array('22' , $user_privilage) ? "3" : "2")?> col-md-12" data-title="Click here to view billed tasks" data-toggle="tooltip">
    <div class="card billed_task redirectbtn taskbutton <?=($actionName ==  "billed" ? "active" : "" )?>" data-href="/bpmt/taskboard/billed<?=($user_sel_id == "" ? "" : '/'.$user_sel_id )?>">
        <div class="header">
            <h2>Billed <?=($completedon == "" ? "Today" : "" )?><small><?=$count_billed?> Task(s)</small></h2>
                 

        </div>
        <div class="below_hours">
            <?=$billed_hours_day?> hr(s)
        </div>
    </div>    
    </div>    
    <div class="col-md-12">
    <div class="belowmsg"><?=$below_message?></div>

                        <div class="row">

                        <?php
                        if(in_array('26' , $user_privilage )){
                            ?>
                        <div class="col-md-3"> 
                            <small class="form-text text-muted">Category</small>
                            <div class="form-group">
                            <select name="categoryfilter" id="categoryfilter" class="categoryfilter  form-control pull-left">
                        <option value="" >Select Category</option>
  <option value="all" <?=($user_cat_id == 'all' ? "selected" : "" )?> >All Category</option>
                            <?php
                              foreach($cat_details as $category){
                                echo '<option value="'.md5($category['id']).'"   '.($user_cat_id == md5($category['id']) ? "selected" : "" ).'  >'.$category['name'].'</option>' ; 
                              }
                            ?>
                        </select>
                        </div>
                        </div>
                        <div class="col-md-3"> 
                        <small class="form-text text-muted">Agent</small>
                        <div class="form-group">
                      <select name="agentuser" class="agentfilter form-control pull-left">
                        <option value="" >Select Agent</option>

                        <?php
                        if(in_array('21' , $user_privilage )){
                            ?>
                        <option value="alltask" <?=($user_sel_id == 'alltask' ? "selected" : "" )?> >All   task</option>
                       <?php
                        }
                        ?>
                        <option value="all" <?=($user_sel_id == 'all' ? "selected" : "" )?> >All managed task</option>
                       
                          <?php
                                   foreach($emp_details as $employee){
                                   echo '<option value="'.md5($employee['id']).'"   '.($user_sel_id == md5($employee['id']) ? "selected" : "" ).'  >'.$employee['name'].'</option>' ; 
                                   }
                           ?>
                       </select>
                       </div>
                       </div>
                    
                       <!-- <div class="col-md-2"> 
                        <small class="form-text text-muted">Completed On</small>
                        <div class="form-group">
                           <input type="text" name="completed" value="<?=$completedon?>" class="completedfilter form-control daterange">
                       </div>
                       </div> -->
                       <!-- <div class="col-md-2"> 
                        <small class="form-text text-muted">Billed On</small>
                        <div class="form-group">
                           <input type="text" name="billed" value="<?=$billedon?>" class="billedfilter form-control daterange">
                       </div>
                       </div>  -->
                        <?php
                        }
                        ?>
                           <div class="col-md-3"> 
                        <small class="form-text text-muted">Filter by Date</small>
                        <div class="form-group">
                           <input type="text" name="completed" id="filterbydate" value="<?=$completedon?>" class="completedfilter form-control daterange">
                       </div>
                       </div>
                        </div>
                        <div style="clear:both;height:5px"></div>
                        <div class="pull-right">
                       
                        <a class="btn btn-warning red addbtn-primary" data-title="Click to view tasks with negative balance" data-toggle="tooltip" href="/bpmt/taskboard/negative/<?=$user_sel_id?><?=($user_cat_id == "" ? "" : "?cat_id=".$user_cat_id )?>" >Negative task (<?=$count_negative?>) </a>
                       <a class="btn btn-warning addbtn-primary" data-title="Click to view scheduled tasks" data-toggle="tooltip" href="/bpmt/taskboard/scheduled/<?=$user_sel_id?><?=($user_cat_id == "" ? "" : "?cat_id=".$user_cat_id )?>" >Scheduled task (<?=$scheduled_num?>) </a>
                        <?php 
                        if(in_array('22' , $user_privilage )){
                        ?>
                        <a class="btn btn-primary addbtn-primary" href="javascript:void(0);" data-toggle="modal" data-target="#addtask">Add task <i class="icon-plus" data-toggle="tooltip" data-title="Click here to add task"></i></a>
                        <?php
                        }
                        ?>
                        </div>
                    </div>