
    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12">
                    <div class="card">
                        <div class="body project_report">
                            <div class="table-responsive">
                                <table class="table table-hover datatable_projects table-custom m-b-0">
                                    <thead>
                                        <tr>                                            
                                            <th>Project</th>
                                            <th>Client</th>
                                            <th>Deadline</th>
                                            <th>Balance</th>
                                            <th>Negative Limit</th>
                                            <th>Hours Usage</th>
                                            <th>Lead</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($pj_details as $project ){
                                        ?>
                                        <tr>
                                            <td class="project-title">
                                                <h6><?=$project['name']?></h6>
                                                <small>Created <?=date('jS F, Y' , $project['created'])?></small>
                                            </td>
                                            <td><?=$project['c']['fname']?></td>
                                            <td><?=($project['end'] == "" || $project['end'] == NULL ? "NA" : date('jS F, Y' , $project['end']))?></td>  
                                            <td><?=$project['balance']?> (<?=($project['ptype'] == 1 ? "Prepaid" : "Postpaid" )?>)</td>
                                            <td><?=$project['nlimit']?></td>
                                           
                                            <td>
                                                <div class="progress progress-xs">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?=$project['progress']?>%;"></div>                                                
                                                </div>
                                                <small>Hours Usage: <?=$project['progress']?>%</small>
                                            </td>
                                            <td><?=$project['e']['name']?></td>
                                            
                                            <td><span class="badge <?=($project['status'] == 1 ? "badge-success" : "badge-default")?>"><?=($project['status'] == 1 ? "Active" : "Inactive")?></span></td>
                                            <td class="project-actions">
                                                <a href="/bpmt/projects/edit/<?=md5($project['id'])?>" class="btn btn-sm btn-outline-primary"><i class="icon-pencil"></i></a>
                                                <?php
                                                if(in_array(15 , $user_privilage )){ 
                                                    ?>

                                              
                                                 <a href="javascript:void" class="btn btn-sm btn-outline-danger js-sweetalert" data-id="<?=md5($project['id'])?>" data-toggle="tooltip" data-title="Delete Project" data-type="confirm"  data-url="projects/delete" data-str="<?=$project['name']?>"><i class="icon-trash"></i></a>
                                                 <?php
                                                 if($project['status'] == 1){
                                                     ?>
                                                 <a href="javascript:void(0);" data-id="<?=md5($project['id'])?>" class="btn btn-sm btn-outline-danger js-sweetalert" data-toggle="tooltip" data-title="Deactivate Project"  data-str="<?=$project['name']?>" data-url="projects/deactivate"  data-type="confirmdeactivate"><i class="icon-arrow-down"></i></a>

                                                     <?php
                                                 }else{
                                                     ?>
                                                    <a href="javascript:void(0);" data-id="<?=md5($project['id'])?>" class="btn btn-sm btn-outline-success js-sweetalert" data-toggle="tooltip" data-title="Activate Project"  data-str="<?=$project['name']?>" data-url="projects/activate"  data-type="confirmactivate"><i class="icon-arrow-up"></i></a>
                                                <?php
                                                 }
                                                }
                                                 ?>
                                        
                                            </td>
                                        </tr>                                    
                                        <?php
                                    }
                                    ?>

                                        
                                    </tbody>
                                </table>
<?php


 $paginator = $this->Paginator;
 echo "<div class = 'paging'>";
 //for the first page link // the parameter’First’ is the labeled, same with other pagination link
 echo $paginator->first('First');
 echo " ";
 //if there was previous records, prev link will be displayed
 if($paginator->hasPrev()){
 echo $paginator->prev('<<');
 }
 //modulus => 3 specifies how many page number will be displayed
 echo $paginator->numbers(array('modulus' =>3)); 
 if($paginator->hasNext()){ //there are records, next link will be displayed
 echo $paginator->next('>>');
 }
 echo $paginator->last('Last'); //for the last page link

echo $paginator->counter([
    'format' => 'Page {{page}} of {{pages}}'
]);
echo "</div)";



 


 ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

<?php	
echo $this->Form->create(false , ['url' => ['action' => ''] , 'id' => "" , 'method' => "post"  ]); 
echo $this->Form->end(); ?>
    
