

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                        <?php	
                        // print_r($project);
                         echo $this->Form->create(false , ['url' => ['action' => 'editpj'] , 'id' => "editpjform" , 'method' => "post"  ]); ?>
        
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                <label>Project Name*</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="name" value="<?=$project->name?>" required placeholder="Project Name *">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                <label>Client*</label>
                                    <div class="form-group">
                                        <select  name="client" required class="form-control">
                                        <option value="">Select Client Name</option>

                                            <?php
                                            foreach($cli_details as $client){
                                                 ?>
                                            <option value="<?=md5($client['id'])?>"  <?=($project->cid == md5($client['id']) ? 'selected' : '' )?> ><?=$client['fname']?> <?=$client['lname']?></option>

                                                 <?php
                                             }
                                             ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                <label>Start Date*</label>
                                    <div class="form-group">
                                        <input type="text" name="sdate" required data-provide="datepicker" data-date-autoclose="true" class="form-control" value="<?=date('m/d/Y' , $project->start)?>" placeholder="Start date *">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                <label>End Date</label>
                                    <div class="form-group">
                                        <input name="edate" type="text"  data-provide="datepicker" data-date-autoclose="true" class="form-control" value="<?=($project->end == "" ? "" : date('m/d/Y' , $project->end))?>" placeholder="End date *">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                <label>Project Estimated Hours*</label>
                                    <div class="form-group">
                                    <input name="hours" type="text" required value="<?=$project->hours?>" class="form-control" placeholder="Hours">
                                    </div>
                                </div>                                
                            </div>
                            <div class="row clearfix">
                                <div class="col-md-3 col-sm-12">
                                    <label>Rate in Dollar</label>
                                    <div class="form-group">
                                        <input name="rate" type="number"  class="form-control" value="<?=$project->rate?>" placeholder="Rate *">
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <label>Billing Type*</label>
                                    <div class="form-group">
                                        <select name="type" required class="form-control show-tick">
                                            <option value="">Select Type</option>
                                            <option <?=($project->type == 1 ? "selected" : "" )?> value="1">Hourly</option>
                                            <option  <?=($project->type == 2 ? "selected" : "" )?> value="2">Fixed</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <label>Payment Type*</label>
                                    <div class="form-group">                                        
                                        <select name="ptype" required class="form-control show-tick">
                                        <option value="">Select Type</option>
                                            <option <?=($project->ptype == 1 ? "selected" : "" )?> value="1">Prepaid</option>
                                            <option  <?=($project->ptype == 2 ? "selected" : "" )?> value="2">Postpaid</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <label>Negative Hours Limit*</label>
                                    <div class="form-group">             
                                    <input name="nlimit" type="text" pattern="[0-9.-]*" class="form-control" value="<?=$project->nlimit?>" placeholder="Negative Hours Limit">

                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                            <div class="col-md-3 col-sm-12">
                                    <label>Project Lead*</label>
                                    <div class="form-group">                                        
                                        <select name="lead" required class="form-control show-tick">
                                            <option>Select</option>
                                             <?php
                                             foreach($emp_details as $employee){
                                                 ?>
                                            <option value="<?=md5($employee['id'])?>" <?=($project->lead == md5($employee['id']) ? "selected" : "" )?>><?=$employee['name']?></option>

                                                 <?php
                                             }
                                             ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <label>Team*</label>
                                    <div class="form-group">             
                                    <select name="team" required class="form-control" >
                                        <option>Select</option>                           
                                    <?php
                                            foreach($team_details as $team){
                                                 ?>
                                            <option value="<?=md5($team['id'])?>" <?=(md5($team['id']) == $project->team ? "selected" : "" )?>  ><?=$team['name']?></option>

                                                 <?php
                                             }
                                             ?>
                                             </select>
                                    </div> 
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <label>Allow Negative Tasks</label>
                                    <div class="form-group">                                        
                                       <input type="checkbox" name="nallowed" <?=($project->nallowed == 1 ? "checked" : "" )?> value="1" >
                                    </div>
                                </div>                     
                                <div class="col-sm-12">
                                    <textarea name="description" placeholder="Enter description here" class="form-control"><?=$project->description?></textarea>
                                </div>
                                <div class="col-sm-12">
                                <input type="hidden" name="pid" value="<?=md5($project->id)?>" >
                                <input type="hidden" name="oclient" value="<?=$project->cid?>" >

                                <div class="error" id="pjerror"></div>
                    <div class="success" id="pjsuccess"></div>                                    <div class="mt-4">
                                        <button type="submit" id="addpjtn" class="btn btn-primary">Save</button>
                   <?php echo $this->Form->end(); ?>
                                        
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>