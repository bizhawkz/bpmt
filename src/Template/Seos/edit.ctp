

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="body">
                        <?php	echo $this->Form->create(false , ['url' => ['action' => 'editpj'] , 'id' => "editseoform" , 'method' => "post"  ]); ?>
        
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                <h3>SEO Stats </h3>
                                </div>
                                                   
                                <div class="col-sm-12">
                                <label>Keywords</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="keywords"  placeholder="Keywords " value="<?php if(isset($retrieve_seo_stats[0]['keywords'])){echo $retrieve_seo_stats[0]['keywords'];} ?>">
                                    </div>
                                </div>
                                                   
                                <div class="col-sm-12">
                                <label>Visits </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="visits"  placeholder="Visits " value="<?php if(isset($retrieve_seo_stats[0]['visits'])){echo $retrieve_seo_stats[0]['visits'];} ?>">
                                    </div>
                                </div>
                                                   
                                <div class="col-sm-12">
                                <label>BackLinks </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="backlinks"  placeholder="BackLinks " value="<?php if(isset($retrieve_seo_stats[0]['backlinks'])){echo $retrieve_seo_stats[0]['backlinks'];} ?>">
                                    </div>
                                </div>
                                                   
                                <div class="col-sm-12">
                                <label>Optins </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="options"  placeholder="Optins " value="<?php if(isset($retrieve_seo_stats[0]['options'])){echo $retrieve_seo_stats[0]['options'];} ?>">
                                    </div>
                                </div>
                                       

                                <div class="col-sm-12">
                                <h3>SMO Stats </h3>
                                </div>

                                
                                <div class="col-sm-12">
                                <label>Followers Insta</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="smo_followers_insta"  placeholder="SMO Followers Insta" value="<?php if(isset($retrieve_seo_stats[0]['smo_followers_insta'])){echo $retrieve_seo_stats[0]['smo_followers_insta'];} ?>">
                                    </div>
                                </div>
                                                   
                                <div class="col-sm-12">
                                <label>Twitter</label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="smo_twittr"  placeholder="Twitter" value="<?php if(isset($retrieve_seo_stats[0]['smo_twittr'])){echo $retrieve_seo_stats[0]['smo_twittr'];} ?>">
                                    </div>
                                </div>
                                                   
                                <div class="col-sm-12">
                                <label>Linkedin </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="smo_linkedin"  placeholder="Linkedin" value="<?php if(isset($retrieve_seo_stats[0]['smo_linkedin'])){echo $retrieve_seo_stats[0]['smo_linkedin'];} ?>">
                                    </div>
                                </div>
                                                   
                                <div class="col-sm-12">
                                <label>SMO Optins </label>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="smo_optins"  placeholder="SMO Optins" value="<?php if(isset($retrieve_seo_stats[0]['smo_optins'])){echo $retrieve_seo_stats[0]['smo_optins'];} ?>">
                                    </div>
                                </div>
                                                   
                                <div class="col-sm-12">

                                <div class="error" id="pjerror"></div>
                    <div class="success" id="pjsuccess"></div>   
                    <div class="mt-4">
                        <input type="hidden" name="id" id="id" value="<?php if(isset($retrieve_seo_stats[0]['id'])){echo $retrieve_seo_stats[0]['id'];} ?>" >
                        <input type="hidden" name="userid" id="userid" value="<?php if(isset($retrieve_seo_stats[0]['userid'])){echo $retrieve_seo_stats[0]['userid'];} ?>" >

                                        <button type="submit" id="editpjtn" class="btn btn-primary">Update</button>
                   <?php echo $this->Form->end(); ?>
                                        
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>