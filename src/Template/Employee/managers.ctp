
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Manager List</h2>
                            <ul class="header-dropdown">
                                <li><a href="javascript:void(0);" class="btn btn-info" data-toggle="modal" data-target="#addemp">Add New</a></li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover js-basic-example dataTable table-custom table-striped m-b-0 c_list">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th>
                                                <label class="fancy-checkbox">
                                                    <input class="select-all" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </th>
                                            <th>Name</th>
                                            <th>Team</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($emp_details as $employee){
                                        ?>
                                        <tr>
                                            <td class="width45">
                                            <label class="fancy-checkbox">
                                                    <input class="checkbox-tick" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                                <?php
                                                if($employee['picture'] == "" ){
                                                    $img = "avatar.jpg" ;
                                                }
                                                else{
                                                    $img = $employee['picture'] ;

                                                }
                                                    ?>

 
                                                <img src="/bpmt/img/<?=$img?>" class="rounded-circle avatar" alt="">
                                            </td>
                                            <td>
                                                <h6 class="mb-0"><?=$employee['name']?></h6>
                                                <span><?=$employee['email']?></span>
                                            </td>
                                                <td><?=$employee['team']?></td>
                                            <td>
                                                <a href="/bpmt/taskboard/index/<?=md5($employee['id'])?>" class="btn btn-sm btn-outline-secondary" title="View"><i class="fa fa-list"></i></a> 
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                        
                                       
                                       
                                       
                                       
                                       
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!--- Extra Scripts here ---->

    


<div class="modal animated zoomIn" id="addemp" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="title" id="defaultModalLabel">Add Employee</h6>
            </div>
            <div class="modal-body">
            <?php	echo $this->Form->create(false , ['url' => ['action' => 'addemp'] , 'id' => "addempform" , 'method' => "post"  ]); ?>

                <div class="row clearfix">
                    <div class="error" id="emperror"></div>
                    <div class="success" id="empsuccess"></div>
                    <div class="col-md-6">
                        <div class="form-group">                               
                            <small id="fileHelp" class="form-text text-muted">Name</small>
                            <input type="text" class="form-control" required name="name" placeholder="Name">
                        </div>
                    </div>
                    <div class="col-md-6"> 
                    <small id="fileHelp" class="form-text text-muted">Email ID</small>
                        <div class="form-group">                                    
                            <input type="text" class="form-control" required name="email" placeholder="Email ID">
                        </div>
                    </div>
                    <div class="col-4">
                    <small id="fileHelp" class="form-text text-muted">Phone Number</small>

                        <div class="form-group">                                    
                            <input type="text" class="form-control" required name="phone" placeholder="Phone Number">
                        </div>
                    </div>    
                    <div class="col-md-4">
                    <small id="fileHelp" class="form-text text-muted">Employee ID</small>
                        <div class="form-group">                                   
                            <input type="text" class="form-control" required name="empid" placeholder="Employee ID">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <small id="fileHelp" class="form-text text-muted">Join Date</small>
                        <div class="form-group">                                    
                            <input type="text" class="form-control datepicker" required name="join_date" placeholder="Join Date">
                        </div>
                    </div>
                     <div class="col-md-12">
                    <small id="fileHelp" class="form-text text-muted">Password</small>

                        <div class="form-group">                                    
                            <input type="text" class="form-control" required name="password" placeholder="Password">
                        </div>
                    </div>
                    <div style="clear:both;height: 10px;width: 100%;" ></div>
                    <div class="col-md-6">
                    <small id="fileHelp"  class="form-text text-muted">Role/Position</small>
                        <div class="form-group">                                    
                            <select  class="form-control" placeholder="" required name="role">
                            <option value="" >Choose Role/Position</option>
                            <?php
                            foreach($role_details as $role){
                                echo '<option value="'.$role['id'].'" >'.$role['name'].'</option>' ;
                            }
                            ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                    <small id="fileHelp" class="form-text text-muted">Team</small>
                        <div class="form-group">                                    
                            <select  class="form-control" placeholder="" required name="team">
                            <option value="" >Choose Team</option>
                            <?php
                            foreach($team_details as $team){
                                echo '<option value="'.$team['id'].'" >'.$team['name'].'</option>' ;
                            }
                            ?>
                            </select>
                        </div>
                    </div>
              
                    <div class="button_row" >
                    <hr>
                    <button type="submit" class="btn btn-primary" id="addempbtn">Add</button>
                    <button type="button" class="btn btn-secondary" style="margin-right: 10px;" data-dismiss="modal">CLOSE</button>
                    </div>
                    
                   <?php echo $this->Form->end(); ?>
                   
                </div>
            </div>
             
        </div>
    </div>
</div>
<!-- Default Size -->