                            <div class="card">
                                <div class="body">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12">
                                        <?php	echo $this->Form->create(false , ['url' => ['action' => 'editprofile'] , 'id' => "editprofileform" , 'enctype' => "multipart/form-data"  , 'method' => "post"  ]); ?>

                                            <h6>Account Data</h6>
                                            <div class="error" id="emperror"></div>
                                          <div class="success" id="empsuccess"></div>
                                            <div class="form-group">                                                
                                                <input type="text" class="form-control" name="name" value="<?=$user_details[0]['name']?>" placeholder="Username">
                                            </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" value="<?=$user_details[0]['email']?>" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="phone" value="<?=$user_details[0]['phone']?>" placeholder="Phone Number">
                                            </div>
                                            <div class="form-group">                                                
                                                <input type="file" class="form-control" name="picture" value="" >
                                                <input type="hidden" class="form-control" name="apicture" value="<?=$user_details[0]['picture']?>" >
                                            </div>

                                        </div>

                                        <div class="col-lg-12 col-md-12">
                                            <h6>Change Password</h6>
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="opassword" placeholder="Current Password">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="password" placeholder="New Password">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control" name="cpassword" placeholder="Confirm New Password">
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" id="editbtn" class="btn btn-primary">Update</button> &nbsp;&nbsp;
<?php echo $this->Form->end(); ?>
                                    
                                </div>
                            </div>