<?php 

		if(!empty($user_attendance[0]['day']))
		{
			$showmonth = date('F', $user_attendance[0]['day']);
			$user = md5($user_attendance[0]['userid']); 
		}
		else
		{	
			$showmonth = "";
			$user = $usersid;	
		}
 ?>

<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="header">
				<h2><?= $currentmonth ?> attendance history 
				<a href="/bpmt/attendance" class="btn btn-primary pull-right">Back</a>
			
			</h2>
                	<div class="py-1" style="width: 50%;">
 
                <?php	
                	$base_url = array('controller' => 'attendance', 'action' => 'search');
                       echo $this->Form->create("Filteratt",array('url' => $base_url , 'method' => "post"  ));
                    ?>
                    <input type="hidden" value="<?=$user?>"  name="user">
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <select class="form-control" id="month" name="month" class="month">
									      <option value="" >Select Month</option>
									      <option <?=($month == 1 ? "selected" : "")?> value="1">January</option>
									      <option <?=($month == 2 ? "selected" : "")?> value="2">February</option>
									      <option <?=($month == 3 ? "selected" : "")?> value="3">March</option>
									      <option <?=($month == 4 ? "selected" : "")?> value="4">April</option>
									      <option <?=($month == 5 ? "selected" : "")?> value="5">May</option>
									      <option <?=($month == 6 ? "selected" : "")?> value="6">June</option>
									      <option <?=($month == 7 ? "selected" : "")?> value="7">July</option>
									      <option <?=($month == 8 ? "selected" : "")?> value="8">August</option>
									      <option <?=($month == 9 ? "selected" : "")?> value="9">September</option>
									      <option <?=($month == 10 ? "selected" : "")?> value="10">October</option>
									      <option <?=($month == 11 ? "selected" : "")?> value="11">November</option>
									      <option <?=($month == 12 ? "selected" : "")?> value="12">December</option>
									    </select>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                	<?php
                                		echo '<select class="form-control" name="year"';
										 $starting_year = '2019';
										 $ending_year = date('Y');
										 $current_year = date('Y');
										 
										 for($starting_year; $starting_year <= $current_year; $starting_year++) 
										 {
										    echo '<option value="'.$starting_year.'"';
										    if( $starting_year ==  $current_year ) 
										    {
										        echo ' selected="selected"';
										    }
										     echo ' >'.$starting_year.'</option>';
										 }               
										 echo '<select>';
                                	?>
                                </div>                            
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?php echo $this->Form->input('Search',array('type'=>'submit', 'class'=>'btn btn-secondary', 'label'=>false));?>
                                </div>                            
                            </div>
                            <div class="row clearfix">
                                <div class="col-sm-6">
                                    <div class="error" id="atterror">
                                    </div>
                                    <div class="success" id="attsuccess">
                                    </div>
                                    <div class="mt-4">
                                    
                  <?php echo $this->Form->end();?>

                                    </div>
                                </div>
                            </div>
                        </div>
                	
                </div>	
            </div>
            <div class="body">
            	<table class="table datatable">
					<thead class="thead-dark">
					<tr>
						<th>Sr. No.</th>
						<th>Name</th>
						<th>Date</th>
						<th>Login time</th>
						<th>Logout time</th>
						<th>Hours Spent</th>
						<th>Hours Billed</th>
						<!-- <th>Action</th> -->
					</tr>
					<thead class="thead-dark">
					<tbody>
					
					<?php 
					$sn=1;
					foreach($user_attendance as $value)
					{ 
                        $spent_detail = $this->Employee->getspent($value['userid'] , $value['day'] ) ;
                        $billed_detail = $this->Employee->getbilled($value['userid'] , $value['day'] ) ;

					  	?>
							<tr>
								<td><?= $sn; ?></td>
								<td><?= $value['u']['name'] ?></td>
								<td><?= date('jS M, y', $value['day']); ?></td>
								<td><?= date("h:i A", $value['login']);?></td>
								<td><?= ($value['logout'] == "" ? "NA" : date("h:i A", $value['logout']))?></td>
								
								<td><?= ($spent_detail == "" ? 0 : $spent_detail) ?></td>
								<td><?= ($billed_detail == "" ? 0 : $billed_detail) ?></td>
								<!-- <td>
									<a href="/bpmt/attendance/update/<?=md5($value['id'])?>" title="Edit" class="btn btn-sm btn-outline-secondary"><i class="fa fa-edit"></i></a>
								</td> -->
							</tr>
							
					  	<?php
					  	$sn++;
					   
					} 
					?>
					<tbody>
				</table>
            </div>
        </div>
    </div>
</div>            
