<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12">
		<div class="card">
			<div class="header">
                <h2>Attendance List</h2>
                <ul class="header-dropdown">
                    <li><a href="javascript:void(0);" class="btn btn-info" data-toggle="modal" data-target="#addatt">Add New</a></li>
                </ul>
            </div>
            <div class="py-1" style="width: 50%;">
 
 <?php	
 
        echo $this->Form->create("Filteratt",array('url' => '' , 'method' => "get"  ));
     ?>

         <div class="row clearfix">
             <div class="col-sm-6">
                 <div class="form-group">
                     <select class="form-control" id="month" name="month" class="month">
                           <option value="" >Select Month</option>
                           <option <?=($month == 1 ? "selected" : "")?> value="1">January</option>
                           <option <?=($month == 2 ? "selected" : "")?> value="2">February</option>
                           <option <?=($month == 3 ? "selected" : "")?> value="3">March</option>
                           <option <?=($month == 4 ? "selected" : "")?> value="4">April</option>
                           <option <?=($month == 5 ? "selected" : "")?> value="5">May</option>
                           <option <?=($month == 6 ? "selected" : "")?> value="6">June</option>
                           <option <?=($month == 7 ? "selected" : "")?> value="7">July</option>
                           <option <?=($month == 8 ? "selected" : "")?> value="8">August</option>
                           <option <?=($month == 9 ? "selected" : "")?> value="9">September</option>
                           <option <?=($month == 10 ? "selected" : "")?> value="10">October</option>
                           <option <?=($month == 11 ? "selected" : "")?> value="11">November</option>
                           <option <?=($month == 12 ? "selected" : "")?> value="12">December</option>
                         </select>
                 </div>
             </div>
             <div class="col-sm-4">
                 <div class="form-group">
                     <?php
                         echo '<select class="form-control" name="year"';
                          $starting_year = '2019';
                          $ending_year = date('Y');
                          $current_year = date('Y');
                          
                          for($starting_year; $starting_year <= $current_year; $starting_year++) 
                          {
                             echo '<option value="'.$starting_year.'"';
                             if( $starting_year ==  $current_year ) 
                             {
                                 echo ' selected="selected"';
                             }
                              echo ' >'.$starting_year.'</option>';
                          }               
                          echo '<select>';
                     ?>
                 </div>                            
             </div>
             <div class="col-sm-2">
                 <div class="form-group">
                     <?php echo $this->Form->input('Search',array('type'=>'submit', 'class'=>'btn btn-secondary', 'label'=>false));?>
                 </div>                            
             </div>
             <div class="row clearfix">
                 <div class="col-sm-6">
                     <div class="error" id="atterror">
                     </div>
                     <div class="success" id="attsuccess">
                     </div>
                     <div class="mt-4">
                     
   <?php echo $this->Form->end();?>

                     </div>
                 </div>
             </div>
         </div>
     
 </div>	
			<div class="body">
				<table class="table attendancetable">
					<thead class="thead-dark">
					<tr>
						<th rowspan="2">#</th>
						<th rowspan="2">Name</th>
						<?php
                        $currentday = date("d");

                        if($month != date('m')){
                            $currentday=cal_days_in_month(CAL_GREGORIAN,$month,$year);
                        }
                        // $currentday = $currentday + 1;
                        for($i=$currentday ; $i >= 1 ; $i--){
                            
?>
                            <th colspan="4" style="border-right: 1px solid;"><?=date("jS F, Y", strtotime($i."-".$month."-".$year))?></th>
                            <?php
                        }
                        ?>
                        </tr>
                        <tr>
                        <?php
                        for($i=$currentday ; $i >= 1 ; $i--){
    						?>
                            <th>Login</th>
                            <th>Logout</th>
                            <th>Spent</th>
                            <th>Billed</th>
                            <?php
                        }
                        ?>
                        </tr>
					<thead class="thead-dark">
					<tbody>
					
					<?php 
					$sn=1;
					foreach($users_details as $value)
					{ 
                        $today_date = strtotime('today midnight');                            
                        // $attendance_detail = $this->Employee->getattendance($value['id'] , $today_date ) ;
                    // $pj_hours = $this->Client->findhours($task['project']) ;

                          ?>
							<tr>
								<td><?= $sn; ?></td>
								<td><a href="/bpmt/attendance/view/<?=md5($value['id'])?>/<?=date('n')?>/<?=date('Y')?>"><?= $value['name'] ?></a></td>
                                <?php
                        // $currentday = date("m");
                        for($i=$currentday ; $i >= 1 ; $i--){
                            $get_date = strtotime('-'.($currentday-$i).'days') ;
                            $get_date =  strtotime("today"  , $get_date);
                           
                        if($month != date('m')){

                            //  strtotime($i."-".$month."-".$year) ;
                            // $get_date = strtotime('-'.($currentday-$i).'days') ;
                            $get_date =  strtotime($i."-".$month."-".$year);

                        }
                            // echo  date("jS F, Y h:i s A" , $get_date);
                            $attendance_detail = $this->Employee->getattendance($value['id'] , $get_date ) ;
                            $spent_detail = $this->Employee->getspent($value['id'] , $get_date ) ;
                            $billed_detail = $this->Employee->getbilled($value['id'] , $get_date ) ;
 
                            $bcolor = "inherit" ;
                            $tcolor = "inherit" ;
                            $lbcolor = "inherit" ;
                            $ltcolor = "inherit" ;
                            $sbcolor = "inherit" ;
                            $stcolor = "inherit" ;
                            if(isset($attendance_detail[0]['login'])){
                                $hours =  date("h", $attendance_detail[0]['login']) ;
                                $minute =  date("i", $attendance_detail[0]['login']) ;
                                if($hours>9 || ($hours == 9 && $minute > 5)){
                                    $lbcolor = "red" ;
                                    $ltcolor = "white" ;
                                }
                            }
                            if($spent_detail != "" && $spent_detail < 7){
                                $sbcolor = "red" ;
                                $stcolor = "white" ;
                            }
                            ?>
                            <td style="color:<?=$ltcolor?>;background-color:<?=$lbcolor?>" ><?=(!isset($attendance_detail[0]['login'])  ? "NA" : date("h:i A", $attendance_detail[0]['login']))?></td>
								<td style="color:<?=$tcolor?>;background-color:<?=$bcolor?>"><?=(!isset($attendance_detail[0]['logout'])  ? "NA" : date("h:i A", $attendance_detail[0]['logout']))?></td>
                                <td style="color:<?=$stcolor?>;background-color:<?=$sbcolor?>"><?= ($spent_detail == "" ? 0 : $spent_detail) ?></td>
                                <td style="color:<?=$tcolor?>;background-color:<?=$bcolor?>"><?= ($billed_detail == "" ? 0 : $billed_detail) ?></td>
                            <?php
                        }
                        ?>
							
								<!-- <td>
                                    <!-- <?php
                                    if(isset($attendance_detail[0]['login'])){
                                        ?>
									<a href="/bpmt/attendance/update/<?=md5($attendance_detail[0]['id'])?>" title="Edit" class="btn btn-sm btn-outline-secondary"><i class="fa fa-edit"></i></a>								
                                        <?php
                                    }
                                    ?> 
                                    <a href="/bpmt/attendance/view/<?=md5($value['id'])?>/<?=date('n')?>/<?=date('Y')?>" title="View" class="btn btn-sm btn-outline-primary"><i class="fa fa-eye"></i></a>
								</td> -->
							</tr>
							
					  	<?php
                          $sn++;                         
                        
					} 
					?>
					<tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- Add User Attendance -->


<div class="modal animated zoomIn" id="addatt" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="title" id="defaultModalLabel">Add Attendance</h6>
            </div>
            <div class="modal-body">
            <?php	echo $this->Form->create(false , ['url' => ['action' => 'addatt'] , 'id' => "addattform" , 'method' => "post"  ]); ?>

                <div class="row clearfix">
                    <div class="error" id="atterror"></div>
                    <div class="success" id="attsuccess"></div>
                    <div class="col-md-12">
                        <div class="form-group">                               
                            <small id="fileHelp" class="form-text text-muted">Name</small>
                            <select  name="name" required class="form-control">
                                <option value="">Select Employee Name</option>
                                    <?php
                                    	foreach($users_details as $user){
                                         ?>
                                    		<option value="<?=$user['id']?>" ><?=$user['name']?></option>
                                         <?php
                                     }
                                     ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <small id="fileHelp" class="form-text text-muted">Date</small>
                        <div class="form-group">                                    
                            <input type="text" class="form-control backdatepicker" required name="date" placeholder="Date">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <small id="fileHelp" class="form-text text-muted">Login Time</small>
                        <div class="form-group">                                    
                            <input type="text" class="form-control timepicker" required name="logintime" id="logintime" placeholder="Login time">
                        </div>
                    </div>
                     <div class="col-md-12">
                        <small id="fileHelp" class="form-text text-muted">Logout Time</small>
                        <div class="form-group">                                    
                            <input type="text" class="form-control timepicker"  name="logouttime" id="logouttime" placeholder="Logout time">
                        </div>
                    </div>
                    <div style="clear:both;height: 10px;width: 100%;" ></div>
                    
              
                    <div class="button_row" >
                    <hr>
                    <button type="submit" class="btn btn-primary" id="addattbtn">Add</button>
                    <button type="button" class="btn btn-secondary" style="margin-right: 10px;" data-dismiss="modal">CLOSE</button>
                    </div>
                    
                   <?php echo $this->Form->end(); ?>
                   
                </div>
            </div>
             
        </div>
    </div>
</div>
<!-- Default Size -->

 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
<script>

</script>

<script>
	$(document).ready(function(){
    $('.timepicker').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '9',
    maxTime: '7:00pm',
    defaultTime: '9',
    startTime: '10:00',
    dynamic: true,
    dropdown: false,
    scrollbar: false
	});
});

	
</script>
