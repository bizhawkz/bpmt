
<?php 
    
    $emp_aid = $users_attendance[0]['id'];
    $emp_id = $users_attendance[0]['userid'];
    $emp_login_date = date('jS M, y', $users_attendance[0]['day']);
    $emp_login = date('H:i', $users_attendance[0]['login']);
    $emp_logout = date('H:i', $users_attendance[0]['logout']);
    $emp_name = $users_attendance[0]['u']['name'];
    
?>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="card">
            <div class="header">
                <h2>Update Attendance</h2>
            </div>
            <div class="body">
                <?php	
                    echo $this->Form->create(false , ['url' => ['action' => 'updateatt'] , 'id' => "updateattform" , 'method' => "post"  ]); ?>
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <label>Name*</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly  value="<?=$emp_name?>" required placeholder="Employee Name *">
                                </div>
                            </div>
                            <div class="col-sm-12">
                            <label>Date*</label>
                                <div class="form-group">
                                    <input type="text" class="form-control" readonly value="<?=$emp_login_date?>" required placeholder="Employee Login Date *">
                                </div>                            </div>
                            <div class="col-sm-12">
                            <label>Login Time*</label>
                                <div class="form-group">
                                    <input type="text" class="form-control timepicker"  name="logintime" value="<?=$emp_login?>" required placeholder="Employee Login Time *">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <label>Logout Time*</label>
                                <div class="form-group">
                                    <input type="text" class="form-control timepicker"  name="logouttime" value="<?=$emp_logout?>" required placeholder="Employee Logout Time *">
                                </div>
                                </div>
                                                               
                            </div>
                           
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <input type="hidden" name="aid" value="<?=md5($emp_aid)?>" >
                                    <div class="error" id="atterror">
                                    </div>
                                    <div class="success" id="attsuccess">
                                    </div>
                                    <div class="mt-4">
                                        <button type="submit" id="addattbtn" class="btn btn-primary">Save</button>
                   <?php echo $this->Form->end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</div>    


