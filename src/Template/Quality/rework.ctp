    <div class="col-md-12">
    <div class="card">
        <div class="body">
        <table class="task_table" >
                            <thead>
                            <th>Task</th>
                            <th>QA</th>
                            <th>Original Comment</th>
                            <th>QA Comment</th>
                            <th>Client</th>
                            <th>Project</th>
                            <th>Completed by</th>
                            <th>Completed on</th>
                            <th>Actions</th>
                            </thead>
                            <tbody>
                                <?php
                                    $today = strtotime('today');

                                foreach($retrieve_task as $task){
                                    // $pj_hours = $this->Client->findhours($task['project']) ;
                                    // $pj_hours_show = $this->Client->showhours($task['project']) ;
                                    $pj_hours = 0 ;
                                    $pj_hours_show = 0 ;                
                                ?>
                                <tr class="<?=($pj_hours < 0 ? "red" : $this->Client->updatenegative($task['id']) )?>">
                                <td>
                                <b>ID:</b> TSK00<?=$task['id']?><br>
                                <b>Title:</b> <?=$task['tittle']?><br>
                                <b>Description:</b> <?=$task['description']?><br>
                                <b>Category:</b> <?=$task['c']['name']?><br>
                                <b>Added by:</b> <?=$task['cr']['name']?><br>
                                <b>Added on:</b> <?=date('jS M,y', $task['created'])?>
                                </td> 
                                <td><?=($task['q']['name'] )?></td>
                                <td><?=($task['comment'] )?></td>
                                <td><?=($task['qrework_comment'] )?></td>
                                <td><?=($task['reftype'] == "yes" ?   $task['cl']['fname'].' '.$task['cl']['lname'] : $task['reference'] )?></td>
                                <td><?=($task['p']['name'] == "" ? "NA" : $task['p']['name'])?></td>
                                <td><?=($task['a']['name'] == "" ? "NA" : $task['a']['name'])?></td>
                                <td><?=($task['completedtime'] == "" ? "NA" : date('jS M,y', $task['completedtime']))?></td>
                                <td>  
                                <?php
                                if(md5($user_details[0]['id']) == $task['creator'] || md5($user_details[0]['id']) == $task['owner']){
                                ?>
                                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Delete" data-type="confirm" data-id="<?=md5($task['id'])?>" data-url="taskboard/delete" data-str="Task" ><i class="fa fa-trash" ></i> 
                                </a>
                                <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" title="Edit" data-type="editpersonaltask" data-id="<?=md5($task['id'])?>" data-url="taskboard/gettask" data-str="Task" ><i class="fa fa-pencil"></i>   </a>
                                <?php
                                }
                                ?>
                             <a href="javascript:void(0)" class="action-icon pull-right js-sweetalert" data-toggle="tooltip" data-toggle="tooltip" title="Complete task" data-type="completetaskquality" data-id="<?=md5($task['id'])?>" data-url="taskboard/completetaskquality" data-get="taskboard/gettask" data-str="Task"  ><i class="fa fa-arrow-right" ></i> </a>  
         
                               
                               
                                </td>

                                </tr>
                                              
                                        <?php } ?> 
                                </tbody> 
                                </table> 
            </div>
        </div>
    </div>
</div>

 

<?php	echo $this->Form->create(false , ['url' => ['action' => ''] , 'id' => "" , 'method' => "post"  ]); ?>

<?php echo $this->Form->end(); ?>


<!-- EXTRA HTML --->

<div id="reowrkbox"  class="hideme">
        <div id='completeerror' class='error'></div>
        <small style='text-align:left;float: left;margin: 10px 0px 4px;'>Add comment</small>
        <textarea class='form-control' name='comment'  id='recommenttext'></textarea>

    </div>

