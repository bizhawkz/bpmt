<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class DashboardController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
         
    
    
               
           public function getWeeks($month,$year){
	$month = intval($month);				//force month to single integer if '0x'
	$suff = array('st','nd','rd','th','th','th'); 		//week suffixes
	$end = date('t',mktime(0,0,0,$month,1,$year)); 		//last date day of month: 28 - 31
  	$start = date('w',mktime(0,0,0,$month,1,$year)); 	//1st day of month: 0 - 6 (Sun - Sat)
	$last = 7 - $start; 					//get last day date (Sat) of first week
	$noweeks = ceil((($end - ($last + 1))/7) + 1);		//total no. weeks in month
	//$output = "";						//initialize string		
	$output = array();						//initialize string		
	$monthlabel = str_pad($month, 2, '0', STR_PAD_LEFT);
        
        $i = 0;
        
	for($x=1;$x<$noweeks+1;$x++){	
		if($x == 1){
			$startdate = "$year-$monthlabel-01";
			$day = $last - 6;
		}else{
			$day = $last + 1 + (($x-2)*7);
			$day = str_pad($day, 2, '0', STR_PAD_LEFT);
			$startdate = "$year-$monthlabel-$day";
		}
		if($x == $noweeks){
			$enddate = "$year-$monthlabel-$end";
		}else{
			$dayend = $day + 6;
			$dayend = str_pad($dayend, 2, '0', STR_PAD_LEFT);
			$enddate = "$year-$monthlabel-$dayend";
		}
		//$output .= "{$x}{$suff[$x-1]} week -> Start date=$startdate End date=$enddate <br />";	
		$output[$i]['week'] = "{$x}{$suff[$x-1]} week";
                $output[$i]['startdate']= strtotime($startdate);	
                $output[$i]['enddate']= strtotime($enddate);
                
                $i++;
	}
        
        //echo "<pre>";print_r($output); echo "</pre>"; die("hi");
	return $output;
}
    
    
    public function index($number=null,$month1=null,$year1=null)
            {
                $user_table = TableRegistry::get('users');
                $role_table = TableRegistry::get('roles');
                $team_table = TableRegistry::get('team');
                $task_table = TableRegistry::get('task');
                    $teamreviews_table = TableRegistry::get('teamreviews');
                
                
                
                $task_cat_table = TableRegistry::get('taskcategory');
                $emp_attendance_table = TableRegistry::get('emp_attendance');
                $team_performance_table = array();
                if($number == 1)
                {
                    $first = strtotime("-30 day midnight");
                    $last = strtotime("today midnight");
                    $pertext = "Last 30 Days " ;
                }
                elseif($number == 7)
                {
                    $first = strtotime("-7 day midnight");
                    $last = strtotime("today midnight");
                    $pertext = "Last 7 Days " ;

                }
                elseif($number == "")
                {
                    $first = strtotime("-7 day midnight");
                    $last = strtotime("today midnight");
                    $pertext = "Last 7 Days " ;

                }

                $currentfirst = strtotime("first day of this month midnight");
                $currentlast = strtotime("first day of next month midnight");

                $startday1 = strtotime("19 march 2020");
                $endday1 = strtotime("21 march 2020");

                $startday2 = strtotime("-6 day midnight");
                $endday2 = strtotime("-5 day midnight");

                $startday3 = strtotime("-5 day midnight");
                $endday3 = strtotime("-4 day midnight");

                $startday4 = strtotime("-4 day midnight");
                $endday4 = strtotime("-3 day midnight");

                $startday5 = strtotime("-3 day midnight");
                $endday5 = strtotime("-2 day midnight");

                $startday6 = strtotime("-2 day midnight");
                $endday6 = strtotime("-1 day midnight");

                $startday7 = strtotime("-1 day midnight");
                $endday7 = strtotime("today midnight");

                $uid=$this->request->session()->read('users_id');


                $user_details = $user_table->find()->where([ 'id' => $uid])->toArray();
                
               // print_r($user_details); die("aaa");
                
                //$month = date('m');
                //$year = date("Y"); 
                
                
               // $month = "9";
               // $year = "2020"; 
                
                if ($month1) 
                {
                    $month = $month1;
                }
                else
                {
                    $month = date('m');
                }

                if ($year1) 
                {
                    $year = $year1;
                }
                else
                {
                    $year = date("Y"); 
                }

                
                $ary_weeks = array();
                
                $ary_weeks =  $this->getWeeks($month,$year);
                
                //echo "<pre>";print_r($ary_weeks); echo "</pre>"; die("hi2");
                
                $ary_weeks_data = array();
                
                for($i=0; $i<count($ary_weeks);$i++)
{
                    $week_first_date = $ary_weeks[$i]['startdate'];
                    $week_last_date = $ary_weeks[$i]['enddate'];
                  //  $week_last_date = $ary_weeks[$i]['startdate'] +  86400;

                $ary_weeks_data[$i]['week_attendance'] = $emp_attendance_table->find()->distinct('userid')->select([ 'full' => 'sum(CASE WHEN Status = 1 THEN 1 END)' , 'haif' => 'sum(CASE WHEN Status = 2 THEN 0.5 END)' , 'day' ])->where([ 'day >=' => $week_first_date, 'day <=' => $week_last_date , 'userid' => $uid ])->toArray();
                $ary_weeks_data[$i]['week_spent'] = $task_table->find()->distinct('assigned')->select([ 'spent' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" , 'completedtime >=' => $week_first_date, 'completedtime <=' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                $ary_weeks_data[$i]['week_rework'] = $task_table->find()->distinct('assigned')->select([ 'rework' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" ,'task_status_new_rework >=' => '2', 'completedtime >=' => $week_first_date,'completedtime <=' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                $ary_weeks_data[$i]['week_review'] = $teamreviews_table->find()->where([ 'process_adherence IS NOT NULL' ,'week_start_date >=' => $week_first_date,'week_end_date <=' => $week_last_date , 'userid' => $uid ])->order(['created' => 'DESC' ])->limit(1)->toArray();
                $ary_weeks_data[$i]['week_review_admin'] = $teamreviews_table->find()->where([ 'admin_process_adherence IS NOT NULL' ,'week_start_date >=' => $week_first_date,'week_end_date <=' => $week_last_date , 'userid' => $uid ])->order(['created' => 'DESC' ])->limit(1)->toArray();
                

                //$ary_weeks_data[$i]['week_billed'] = $task_table->find()->distinct('assigned')->select([ 'billed' => 'SUM(billed)' , 'billedon' ])->where(['billed IS NOT' => "null" , 'billedon >=' => $week_first_date, 'billedon <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                $ary_weeks_data[$i]['week_billed'] = $task_table->find()->distinct('assigned')->select([ 'billed' => 'SUM(billed)' , 'billedon' ])->where(['billed IS NOT' => "null" , 'billedtime >=' => $week_first_date, 'billedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                    
                
                $ary_weeks_data[$i]['week_attendance_day_wise'] = $emp_attendance_table->find()->select([ 'u.name' , 'emp_attendance.userid' , 'emp_attendance.login' , 'emp_attendance.logout' , 'emp_attendance.day' , 'emp_attendance.id' ])->join([
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  emp_attendance.userid' 
                        ]
                    ])->where(['(emp_attendance.userid)' => $uid , 'emp_attendance.day >=' => $week_first_date, 'emp_attendance.day <=' => $week_last_date   ])->order(['emp_attendance.day' => 'ASC' ])->toArray();
                
                //echo "<pre>";print_r($ary_weeks_data[$i]); echo "</pre>"; 
                
            /*    
                $log = $emp_attendance_table->find()->select([ 'u.name' , 'emp_attendance.userid' , 'emp_attendance.login' , 'emp_attendance.logout' , 'emp_attendance.day' , 'emp_attendance.id' ])->join([
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  emp_attendance.userid' 
                        ]
                    ])->where(['(emp_attendance.userid)' => $uid , 'emp_attendance.day >=' => $week_first_date, 'emp_attendance.day <=' => $week_last_date   ])->order(['emp_attendance.day' => 'ASC' ]);       
debug($log);
*/
                
  /*
                $log = $task_table->find()->distinct('assigned')->select([ 'spent' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" , 'completedtime >=' => $week_first_date, 'completedtime <=' => $week_last_date , 'assigned' => md5($uid) ]);       
debug($log);

*/
                
//die("hi2");
                }
                
                
                //echo "<pre>";print_r($ary_weeks_data); echo "</pre>"; die("hi2");

                
                
                $month_attendance = $emp_attendance_table->find()->distinct('userid')->select([ 'full' => 'sum(CASE WHEN Status = 1 THEN 1 END)' , 'haif' => 'sum(CASE WHEN Status = 2 THEN 0.5 END)' , 'day' ])->where([ 'day >=' => $currentfirst, 'day <' => $currentlast , 'userid' => $uid ])->toArray();

                $month_spent = $task_table->find()->distinct('assigned')->select([ 'spent' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" , 'completedtime >=' => $currentfirst, 'completedtime <' => $currentlast , 'assigned' => md5($uid) ])->toArray();
                $month_rework = $task_table->find()->distinct('assigned')->select([ 'rework' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" ,'task_status_new_rework >=' => '2', 'completedtime >=' => $currentfirst,'completedtime <' => $currentlast , 'assigned' => md5($uid) ])->toArray();
                //$count_assigned_rework = $task_table->find()->select(['rework_estimate' => 'SUM(task.estimate)' ])->where([$assigned_cond , $cat_cond,   ['OR' => [['task.status' => '1']]], 'task.task_status_new_rework' => '2' ,'task.assigned !=' => '' , 'OR' => $user_cond ])->first();

                
                $month_billed = $task_table->find()->distinct('assigned')->select([ 'billed' => 'SUM(billed)' , 'billedon' ])->where(['billed IS NOT' => "null" , 'billedon >=' => $currentfirst, 'billedon <' => $currentlast , 'assigned' => md5($uid) ])->toArray();

                $month_bug = $task_table->find()->distinct('assigned')->select([ 'bug' => 'SUM(bug)' , 'completedtime' ])->where([ 'completedtime >=' => $currentfirst, 'completedtime <' => $currentlast , 'assigned' => md5($uid) ])->toArray();

                /* for graph */

                if($number == 7)
                {   
                    $days = 7;
                }
                elseif($number == 1)
                {
                    $days = 30;     
                }
                elseif($number == "")
                {   
                    $days = 7;
                }

                if($days==30)
                {
                    $grapharray = array();
                    
                    for($day=0; $day <= 30; $day++)
                    {   
                        $d = $day;
                        $d++;
                       
                        $firstday = strtotime(- $d." day midnight"); 
                        $lastday = strtotime(- $day. "day midnight");

                      $task_graph = $task_table->find()->distinct('t.name')->select([  'billed' => 'SUM(billed)', 't.name' ,'task.billedon' , 'task.id' , 'u.id' ])->join([
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(u.id) =  task.assigned' 
                        ],
                        't' => [
                            'table' => 'team',
                            'type' => 'LEFT',
                            'conditions' =>  't.id =  u.team' 
                        ]
                    ])->where([ 't.name IS NOT' => "Admin" , 'u.status' => '1' , 't.name <>' => "Quality" , 'task.billedon >=' => $firstday, 'task.billedon <' => 
                    $lastday  ])->toArray();

                    array_push($grapharray, $task_graph);    
                    }
                }
                if($days==7)
                {
                    $grapharray = array();
                    
                    for($day=0; $day <= 7; $day++)
                    {  
                       $d = $day;
                       $d++;
                       $firstday  = strtotime(- $d." day midnight"); 
                       $lastday = strtotime(- $day. "day midnight");
                       

                      $task_graph = $task_table->find()->distinct('t.name')->select([  'billed' => 'SUM(billed)', 't.name' ,'task.billedon' , 'task.id' , 'u.id' ])->join([
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(u.id) =  task.assigned' 
                        ],
                        't' => [
                            'table' => 'team',
                            'type' => 'LEFT',
                            'conditions' =>  't.id =  u.team' 
                        ]
                    ])->where(['billed IS NOT' => "null" , 't.name IS NOT' => "Admin" , 'u.status' => '1' , 't.name <>' => "Quality" , 'task.billedon >=' => $firstday, 'task.billedon <' => 
                    $lastday  ])->toArray();

                    array_push($grapharray, $task_graph);    
                    }
                }

                $emp_performance = $task_table->find()->distinct('u.id')->select([  't.name' , 'task.id' , 'task.assigned' , 'task.billedon' , 'u.id' ])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(u.id) =  task.assigned' 
                    ],
                    't' => [
                        'table' => 'team',
                        'type' => 'LEFT',
                        'conditions' =>  't.id =  u.team' 
                    ]
                ])->where(['billed IS NOT' => "null" , 'u.status' => '1' , 't.name IS NOT' => "Admin" , 'task.billedon >=' => $first, 'task.billedon <' => $last  ])->toArray();


                 $team_performance_table = $task_table->find()->distinct('t.name')->select([ 'billed' => 'SUM(task.billed)' , 't.name' , 'task.id' , 'task.assigned' , 'task.billedon' , 'u.id' ])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(u.id) =  task.assigned' 
                    ],
                    't' => [
                        'table' => 'team',
                        'type' => 'LEFT',
                        'conditions' =>  't.id =  u.team' 
                    ]
                ])->where(['billed IS NOT' => "null"  , 't.name IS NOT' => "Quality" , 'u.status' => '1' ,  'task.billedon >=' => $first , 't.name <>' => "Admin" , 'task.billedon <' => $last  ])->toArray();
                

              
                $top_task_table = $task_table->find()->distinct('task.assigned')->select([ 'billed' => 'SUM(task.billed)', 'r.name' ,'t.name' ,'u.picture' , 'task.id' , 'task.billedon' , 'u.id' , 'u.name' ])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(u.id) =  task.assigned' 
                    ],
                    'r' => [
                        'table' => 'roles',
                        'type' => 'LEFT',
                        'conditions' =>  'r.id =  u.role' 
                    ],
                    't' => [
                        'table' => 'team',
                        'type' => 'LEFT',
                        'conditions' =>  't.id =  u.team' 
                    ]
                ])->where(['billed IS NOT' => "null" , 'u.status' => '1' ,  'task.billedon >=' => $first, 'task.billedon <' => $last  ])->order(['billed' => 'DESC'])->limit(6)->toArray(); 

                    

                $worst_task_table = $task_table->find()->distinct('task.assigned')->select([ 'billed' => 'SUM(task.billed)', 'r.name', 't.name', 'task.id','u.picture' , 'task.billedon' , 'u.id' , 'u.name'  ])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(u.id) =  task.assigned' 
                    ],
                    'r' => [
                        'table' => 'roles',
                        'type' => 'LEFT',
                        'conditions' =>  'r.id =  u.role' 
                    ],
                    't' => [
                        'table' => 'team',
                        'type' => 'LEFT',
                        'conditions' =>  't.id =  u.team' 
                    ]
                ])->where(['billed IS NOT' => "null" , 'u.status' => '1' , 'task.billedon >=' => $first, 'task.billedon <' => $last  ])->order(['billed' => 'ASC'])->limit(6)->toArray(); 
              
/*
                $day5 = date('w');
                $week_first_date_seo = strtotime('-'.$day.' days');
                $week_last_date_seo = strtotime('+'.(6-$day).' days');
*/                
                
                /*
                $week_first_date_seo = strtotime('-7 days');
                $week_last_date_seo = strtotime('now');
                */
                $week_first_date_seo = strtotime('last sunday');
                $week_last_date_seo = strtotime('saturday');
                
  /*              
$week_start = date('m-d-Y', strtotime('-'.$day.' days'));
$week_end = date('m-d-Y', strtotime('+'.(6-$day).' days'));
*/
                
                
//echo $week_first_date_seo;
//echo $week_last_date_seo;
//die;



                $this->set("week_first_date_seo", $week_first_date_seo);
                $this->set("week_last_date_seo", $week_last_date_seo);
                
                $this->set("pertext", $pertext);
                $this->set("team_bill", $emp_performance);
                $this->set("month_attendances", $month_attendance);
                $this->set("month_spents", $month_spent);
                
                $this->set("month_rework", $month_rework);
                $this->set("ary_weeks", $ary_weeks);
                $this->set("ary_weeks_data", $ary_weeks_data);
                
                $this->set("month_billeds", $month_billed);
                $this->set("month_bugs", $month_bug); 
                $this->set("top_users", $top_task_table); 
                $this->set("worst_users", $worst_task_table); 
                $this->set("team_performances",$team_performance_table);

                $this->set("graph_data", $grapharray);
                $this->set("user_details", $user_details);
                $this->set("month2", $month); 


                $this->viewBuilder()->setLayout('user');
            }

             public function search()
            {
                
                $month = $this->request->data('month');
                $year = $this->request->data('year');
                
                if ($month !='' && $month != 0) 
                {
                    $month = $this->request->data('month');
                }
                else
                {
                    $month = date('m');
                }

                if ($year !='' && $year != 0) 
                {
                    $year = $this->request->data('year');
                }
                else
                {
                    $year = date("Y");
                }

                
                $number = 7;
                
                return $this->redirect('/dashboard/index/'.$number.'/'.$month.'/'.$year);

            }


}

  

