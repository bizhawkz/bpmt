<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;


use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie');
        date_default_timezone_set("Asia/Kolkata");

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');


         $cook_var = $this->Cookie->read('atoken') ;
         $uid = $this->Cookie->read('id') ;
        
         $controllerName = $this->request->getParam('controller');
   
         $actionName = $this->request->getParam('action');


       
         if(($cook_var == "" || $cook_var ==  null || $uid == "" || $uid == null ) &&  $controllerName != "Login"  ) {
            setcookie('atoken', '', time()-1000 , '/bpmt/'  );
            setcookie('id', '', time()-1000  , '/bpmt/' );
        
            return $this->redirect('/login');      
  
        }	
         elseif( $cook_var != "" && $uid != "" ){
           
            if( $controllerName  == "Login" || $controllerName == "Pages"   ){
                return $this->redirect('/dashboard');
            }
            $users_table = TableRegistry::get('users');
            $privilage_table = TableRegistry::get('privilages');
            $category_table = TableRegistry::get('taskcategory');
            $task_table = TableRegistry::get('task');
            $termuser_table = TableRegistry::get('termuser');
            $terms_table = TableRegistry::get('terms');

            $empid=$this->request->session()->read('users_id');
            
            $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;

           /* $retrieve_termuser = $termuser_table->find()->select(['id'])->where(['userid' => $empid ,'status'=> '0' ])->toArray() ;*/

            $retrieve_termuser = $termuser_table->find()->distinct('t.name')->select(['termuser.id','t.id' , 't.name','t.content' ])->join([  
                    't' => [
                        'table' => 'terms',
                        'type' => 'LEFT',
                        'conditions' =>  't.id =  termuser.termid' 
                    ]
                ])->where(['termuser.userid' => $empid ,'termuser.status'=> '0' ])->first() ;

            $country_table = TableRegistry::get('countries');
            $encpw = $this->Cookie->read('atoken') ;
            $sesscode = $this->Cookie->read('sesscode') ;
            $retrieve_users = $users_table->find()->select(['id' ,'name' , 'phone'  , 'password'  , 'email' ,'role' , 'privilages' , 'sesscode' ,'terms' ,  'picture','team' ])->where(['md5(id)' => $uid ]) ; 
            $user_details = $retrieve_users->toArray() ;  
            
            $this->request->session()->write('users_id', $user_details[0]['id']);

            if( !empty($user_details[0])  &&  md5($user_details[0]['password']) == $encpw &&  $user_details[0]['sesscode'] == $sesscode )
            {
 
                if(!empty($retrieve_termuser)   && $controllerName != "Terms" )
                {   
                    return $this->redirect('/terms');

                }
                elseif(empty($retrieve_termuser)  && $controllerName == "Terms" )
                {
                    return $this->redirect('/dashboard');
                }
                else{
                    $this->set("user_details", $user_details); 
                    if($controllerName == "Dashboard"){
                        $mainpage = "Project Dashboard" ;
                        $subpage = $mainpage ;
                    } 
                    else{
                        $mainpage = $controllerName ;
                        if($actionName == "index"){
                            $subpage = $controllerName." List"  ;

                        }
                        else{
                            
                            $subpage = $controllerName." / ".ucfirst($actionName)." ".$controllerName  ;

                        }

                    }
                    $retrieve_countries = $country_table->find()->select(['name'   , 'id' ])->toArray() ; 
                    $sidebar_prefix = "" ;
                    $user_privilage = explode(',',$user_details[0]['privilages']) ;
                    $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                    $count_emp = $users_table->find()->select(['id'   ])->where([ 'status' => '1' ])->count() ;     
                    if(in_array('21' , $user_privilage )){
                        $spec_cond = 'alltask' ;
                    }
                    else if (in_array('26' , $user_privilage )){
                        $spec_cond = 'all' ;        
                    }
                    else{
                        $spec_cond = md5($user_details[0]['id']) ;
                    }
                    $userid = $this->Cookie->read('id');
                    if($spec_cond == "all"){
                        $user_cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;                      
                    }
                    elseif($spec_cond == 'alltask' ){
                        $user_cond = [];                       
                    }
                    else{                  
                        $user_cond = ['task.assigned' =>  $userid]  ;
                    }
                    // $user_cond = [['task.assigned' =>  $uid], ['task.owner' =>  $uid] ,['task.creator' =>  $uid] ] ;
                    $timelimitupper = strtotime('midnight first day of next month');
                    $timelimitlower = strtotime('midnight first day of this month') ;
                    $completedon_cond =  [ 'task.completedtime >=' =>  $timelimitlower  , 'task.completedtime <=' =>  $timelimitupper   ];
                     
                    $count_bug = $task_table->find()->select(['bug' => 'SUM(task.bug)' ])->where([   "OR" => $user_cond ])->first();

                    $employee_priv_get = $privilage_table->find()->select(['id' ])->where([ 'category' => 'Employee Management' ])->toArray() ;
                    $client_priv_get = $privilage_table->find()->select(['id' ])->where([ 'category' => 'Client Management' ])->toArray() ;
                    $project_priv_get = $privilage_table->find()->select(['id' ])->where([ 'category' => 'Project Management' ])->toArray() ;
                    $billing_priv_get = $privilage_table->find()->select(['id' ])->where([ 'category' => 'Billing Management' ])->toArray() ;
                    $task_priv_get = $privilage_table->find()->select(['id' ])->where([ 'category' => 'Task Management' ])->toArray() ;
                    foreach($employee_priv_get as $epmpriv){
                        $employee_priv[] = $epmpriv['id'];
                    }
                    foreach($client_priv_get as $clipriv){
                        $client_priv[] = $clipriv['id'];
                    }

                    foreach($project_priv_get as $prjpriv){
                        $project_priv[] = $prjpriv['id'];
                    }

                    foreach($billing_priv_get as $billpriv){
                        $billing_priv[] = $billpriv['id'];
                    }

                    foreach($task_priv_get as $taskpriv){
                        $task_priv[] = $taskpriv['id'];
                    }

                    // print_r($employee_priv);
                    // print_r($user_privilage);
                    // $task_table = TableRegistry::get('task');
                    // $timelimit = strtotime('tomorrow');
                    // $user_cat_id = $this->request->query('cat_id') ;
                    // $cat_cond = '' ;
    
                    // if($user_cat_id != ""  && $user_cat_id != "all" ){
                    //             $cat_cond =  [ 'md5(category)' =>  $user_cat_id ];
                    // }
    
               
                    // $this->set("user_cat_id", $user_cat_id);  
                    $this->set("employee_priv", $employee_priv);  
                    $this->set("user_privilage", $user_privilage);      
                    $this->set("client_priv", $client_priv);      
                    $this->set("project_priv", $project_priv);      
                    $this->set("billing_priv", $billing_priv);      
                    $this->set("task_priv", $task_priv);      
                    $this->set("cat_details", $retrieve_cat);
                    
                    $this->set("count_bug", $count_bug);      
                    $this->set("emp_details", $retrieve_emp);      
                    $this->set("coountemp", $count_emp);  
                    $this->set("mainpage", $mainpage);  
                    $this->set("countries", $retrieve_countries);  
                    $this->set("controllerName", $controllerName);  
                    $this->set("actionName", $actionName);  
                    $this->set("subpage", $subpage);  
                    $this->set("sidebar_prefix", $sidebar_prefix);  
                }
               

            }
            else{
                setcookie('atoken', '', time()-1000   , '/bpmt/' );
                setcookie('id', '', time()-1000  , '/bpmt/' );
            
    
                return $this->redirect('/login');  
            }
          

        }
    }

    public function json($data){
        $this->response->type('json');
        $this->response->body(json_encode($data));
        return $this->response;
    }
}
