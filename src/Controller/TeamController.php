<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class TeamController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
            public function index()
            {
                $this->viewBuilder()->setLayout('user');
                $team_table = TableRegistry::get('team'); 

                $retrieve_teams = $team_table->find()->select(['id' , 'name' ,'created'  ])->where([ 'status' => '1'  ])->toArray() ;

               
                $this->set("team_details", $retrieve_teams); 

            }

            public function update()
            {   
                if($this->request->is('post')){

                $tid = $this->request->data['tid'];
                $team_table = TableRegistry::get('team'); 
                
                $update_teams = $team_table->find()->select(['name' , 'id'])->where(['id' => $tid])->toArray() ; 
                    $data = ['name'=>$update_teams[0]['name'],'id'=>$update_teams[0]['id']];
                   return $this->json($data);

                }
                
            }

            public function add(){
                $this->viewBuilder()->setLayout('user');
            }
            public function detail(){
                $this->viewBuilder()->setLayout('user');
            }

            public function addteam(){
                if ($this->request->is('ajax') && $this->request->is('post') ){

                    $team_table = TableRegistry::get('team');
                    $activ_table = TableRegistry::get('activity');
                     
                        $team = $team_table->newEntity();
                        $team->name =  $this->request->data('name')  ;
                        $team->status =  1 ;
                        $team->created = strtotime('now');
                        $team->modified = strtotime('now');
                        if($saved = $team_table->save($team) ){
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Team Created"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($saved->id)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success'  ];
    
                            }
                            else{
                        $res = [ 'result' => 'activity not saved'  ];
    
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'team not saved'  ];
                        }
 

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }

                return $this->json($res);

            }

            public function editteam()
            {   
                if ($this->request->is('post')){
                    $team_table = TableRegistry::get('team');
                    $activ_table = TableRegistry::get('activity');
                    
                    $name = $this->request->data('name') ;   
                    $tid = $this->request->data('tid') ;
                    $now = strtotime('now');

                    if( $team_table->query()->update()->set([ 'name' => $name  , 'modified' => $now  ])->where([ 'id' => $tid  ])->execute())
                    {
                        $activity = $activ_table->newEntity();
                        $activity->action =  "Team Updated"  ;
                        $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
    
                        $activity->value = md5($tid);
                        $activity->origin = $this->Cookie->read('id')   ;
                        $activity->created = strtotime('now');
                        
                        if($saved = $activ_table->save($activity) )
                        {
                            $res = [ 'result' => 'success'  ];

                        }
                        else
                        {
                            $res = [ 'result' => 'activity not saved'  ];

                        }    
                    }
                    else
                    {
                        $res = [ 'result' => 'team not saved'  ];
                    }                   
                }
                else
                {
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }
            

            public function delete()
            {
                $rid = $this->request->data('val') ;
                $team_table = TableRegistry::get('team');
                $activ_table = TableRegistry::get('activity');
                
                $teamid = $team_table->find()->select(['id'])->where(['id'=> $rid ])->first();
                if($teamid)
                {   
                    $team = $team_table->get($rid);
                    $teamdel = $team_table->delete($team);
                    
                    if($teamdel)
                    {
                        $activity = $activ_table->newEntity();
                        $activity->action =  "Team Deleted"  ;
                        $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
                        $activity->value = $rid    ;
                        $activity->origin = $this->Cookie->read('id')   ;
                        $activity->created = strtotime('now');

                        if($saved = $activ_table->save($activity) )
                        {
                            $res = [ 'result' => 'success'  ];
                        }
                        else
                        {
                            $res = [ 'result' => 'failed'  ];
                        }
                    }
                    else
                    {
                        $res = [ 'result' => 'not delete'  ];
                    }    
                }
                else
                {
                    $res = [ 'result' => 'error'  ];
                }

                return $this->json($res);
            }


            }
