<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class LoginController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
            public function index()
            {
                                
                $this->viewBuilder()->setLayout('login');
            }

            public function logincheck(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $users_table = TableRegistry::get('users');
                    $activ_table = TableRegistry::get('activity');
                    $attendance_table = TableRegistry::get('emp_attendance');
                    if($this->request->data('email') != "" && $this->request->data('password') != ""  ){
                         
                    // if($this->request->data('g-recaptcha-response') != "" ){
                        // $Url = "https://www.google.com/recaptcha/api/siteverify";
                        // $SecretKey = "6Ld2xJEUAAAAAD387va-0GhqXg46IK1m5e_5R8NR";
                        // $data = array('secret' => $SecretKey, 'response' => $this->request->data('g-recaptcha-response'));
                        // $ch = curl_init($Url);
                        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);  
                        // curl_setopt($ch, CURLOPT_POST, true);
                        // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
                        // $verifyResponse = curl_exec($ch);
                        // curl_close($ch);
                        // $responseData = json_decode($verifyResponse);
                        // if($responseData->success == 1 ) {
                            $retrieve_users = $users_table->find()->select(['id' , 'password' ])->where(['email' => $this->request->data('email') , 'password' =>   $this->request->data('password') , 'status' => '1' ]) ;
                            $user_details = $retrieve_users->toArray() ; 
                            if(empty($user_details[0])){
                                $res = [ 'result' => 'email' ];
                            }
                            else{
                                $rem = $this->request->data('remember') ;
                                if($rem == "on"){
                                    $this->Cookie->write('id',  md5($user_details[0]['id']) ,   time()+1000000000000 );
                                    $this->Cookie->write('atoken',  md5($user_details[0]['password']) ,  time()+1000000000000000 );
                                }
                                else{
                                    $this->Cookie->write('id',  md5($user_details[0]['id'])  ,   time()+1000000);
                                $this->Cookie->write('atoken',  md5($user_details[0]['password'])  ,   time()+1000000);
                                }
                                
                                $sess_code = uniqid() ; 
                                $this->Cookie->write('sesscode',$sess_code );
                                $update_user = $users_table->query()->update()->set(['sesscode' => $sess_code ])->where(['id' => $user_details[0]['id'] ])->execute()  ;

                                 /* Insert record of attendance */
                                $current_date = strtotime('today midnight');
                                // echo "<br>";
                               $samedaylogin = $attendance_table->find()->select(['id', 'login'])->where(['userid' =>  $user_details[0]['id'] , 'day' => strtotime('today midnight') ]);
                                 $numbers = $samedaylogin->count();
                               
                                if($numbers == 0)
                                {   
                                    $hours =  date("h", strtotime('now'));
                                    $minute =  date("i", strtotime('now'));
                                    
                                    if($hours < 9 || ($hours == 9 && $minute < 5))
                                    {
                                        $attendance = $attendance_table->newEntity();
                                        $attendance->userid =  $user_details[0]['id'] ;
                                        $attendance->day =  $current_date;
                                        $attendance->login = strtotime('now');
                                        $attendance->added = strtotime('now');
                                        $attendance->status = 1;
                                        $saved = $attendance_table->save($attendance) ;
                                    }
                                    elseif($hours>9 || ($hours == 9 && $minute > 5))
                                    {
                                        $attendance = $attendance_table->newEntity();
                                        $attendance->userid =  $user_details[0]['id'] ;
                                        $attendance->day =  $current_date;
                                        $attendance->login = strtotime('now');
                                        $attendance->added = strtotime('now');
                                        $attendance->status = '2';
                                        $saved = $attendance_table->save($attendance) ;
                                    }
                                }
                               
                                /* end of insert attendance details  */                                


                                $activity = $activ_table->newEntity();
                                $activity->action =  "Login"  ;
                                $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                                $activity->value = $this->Cookie->read('id')   ;
                                $activity->created = strtotime('now');
                                $saved = $activ_table->save($activity) ;
                                $res = [ 'result' => 'success'  ];

                               
                           
                            }
                        // }
                        // else{
                        //     $res = [ 'result' => 'captcha'  ];
                        // }

                    
                    // }
                    // else{
                    //     $res = [ 'result' => 'captcha'  ];
                    // }
    
                    }
                    else{
                        $res = [ 'result' => 'empty'  ];
                    }
                   
                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];
                }
                return $this->json($res);
            }


            
    public function sendBillingNotification(){

        $task_table = TableRegistry::get('task');
        $client_table = TableRegistry::get('client');                
        $billing_table = TableRegistry::get('billing');
        $project_table = TableRegistry::get('project');


         $now = strtotime('now');
       
        $get_client = $client_table->find()->select([ 'id' , 'fname' , 'lname' , 'type'  ])->where([ 'status' => 1 ])->order(['fname' => 'asc'])->toArray();    
        $i = 0 ;    
        $html = '
        <html>
        <head>
        
        <style>
        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
        }
        
        td, th {
          border: 1px solid #dddddd;
          text-align: left;
          padding: 8px;
        }
        
        tr:nth-child(even) {
          background-color: #dddddd;
        }
        </style>
        </head>
        <body>
        
        <h2>Pending billing list</h2>
        
        <table>
          <tr>
            <th>Client</th>
            <th>Project</th>
            <th>Current Balance</th>
            <th>Last Payment</th>
            <th>Due Payment</th>
            <th>Last Remark</th>
          </tr>
          ' ;
        foreach($get_client as $client){
            $retrieve_project = $project_table->find()->select(['name' ,'id'   ])->where([ 'cid' => md5($client['id'])  , 'status' => 1 ])->toArray() ;
            if($client['type'] == 1 ){
        
        foreach($retrieve_project as $project){
            
            $retrieve_billing = $billing_table->find()->select(['billing_date', 'next_payment' ,  'remark'  ])->where([ 'pid' => $project['id']   , 'deleted' => 0 ])->order(['billing_date' => 'desc' ])->limit(1)->toArray() ;
            if(!empty($retrieve_billing[0]))
            {

                if($retrieve_billing[0]['next_payment'] < $now  && $retrieve_billing[0]['next_payment'] != "" ){
                    $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)'  ])->where([ 'project' => md5($project['id']) ,  'status' => 4 ])->first();    
 
                    if($count_billed->billed == ""){
                        $billed = 0 ;
                    }
                        else{
                        $billed = $count_billed->billed ;
                    }
        
                                
                    $count_hours = $billing_table->find()->select(['hours' => 'SUM(hours)'  ])->where([ 'pid' => $project['id'] ,  'deleted' => 0 ])->first(); 
                    if($count_hours->hours == ""){
                        $balance = 0 ;
                    }
                    else{
                        $balance = $count_hours->hours ;
                    }
                    $result[$i]['client'] = $client['fname']." ".$client['lname'] ;
     
                    $result[$i]['detail'] = $retrieve_billing ;
                    $html .= '<tr>
                    <td>'.$client['fname']." ".$client['lname'].'</td>
                    <td>'.$project['name'].'</td>
                    <td>'.(round($balance,2) - round($billed,2)).'</td>
                    <td>'.date('jS M, Y' , $retrieve_billing[0]['billing_date']).'</td>
                    <td>'.date('jS M, Y' , $retrieve_billing[0]['next_payment']).'</td>
                    <td>'.$retrieve_billing[0]['remark'].'</td>
                      </tr>' ;                
                        $i++;
        
                }

            }

        }  
    }
    elseif($client['type'] == 2 ){

        $retrieve_billing = $billing_table->find()->select(['billing_date', 'next_payment' ,  'remark'  ])->where([ 'cid' => $client['id']   , 'deleted' => 0 ])->order(['billing_date' => 'desc' ])->limit(1)->toArray() ;
        if($retrieve_billing[0]['next_payment'] < $now  && $retrieve_billing[0]['next_payment'] != "" ){
            $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)'  ])->where([ 'client' => md5($client['id']) ,  'status' => 4 ])->first();    
            if($count_billed->billed == ""){
                $billed = 0 ;
            }
            else{
                $billed = $count_billed->billed ;
            }
            $count_hours = $billing_table->find()->select(['hours' => 'SUM(hours)'  ])->where([ 'cid' => $client['id'] ,  'deleted' => 0 ])->first();  
            if($count_hours->hours == ""){
                $balance = 0 ;
            }
            else{
                $balance = $count_hours->hours ;
            }
            $result[$i]['client'] = $client['fname']." ".$client['lname'] ;

            $result[$i]['detail'] = $retrieve_billing ;
            $html .= '<tr>
            <td>'.$client['fname']." ".$client['lname'].'</td>
            <td>NA</td>
            <td>'.(round($balance,2) - round($billed,2)) .'</td>

            <td>'.date('jS m,Y' , $retrieve_billing[0]['billing_date']).'</td>
            <td>'.date('jS m,Y' , $retrieve_billing[0]['next_payment']).'</td>
            <td>'.$retrieve_billing[0]['remark'].'</td>
              </tr>' ;                
                $i++;

        }

}

        }
        $html .=  '</table>' ;
        // echo $html ;
        // die;
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        
        // More headers
        $headers .= 'From: No-reply <info@theworkroomtools.com>' . "\r\n";
        $headers .= 'Cc: jyoti@outsourcingservicesusa.com' . "\r\n";
        $headers .= 'Bcc: yasir@outsourcingservicesusa.com' . "\r\n";
        $to = "info@bizhawkz.com" ; 
        // $to = "yasirkhancse@gmail.com" ; 
        $subject = "Billing Notification" ; 
        mail($to , $subject , $html , $headers );
  
      
        $res = [ 'result' => $result  ];

        return $this->json($res);

        
    }

            }
