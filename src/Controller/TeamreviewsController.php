<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class TeamreviewsController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    
    
public function getWeeks($month,$year){
	$month = intval($month);				//force month to single integer if '0x'
	$suff = array('st','nd','rd','th','th','th'); 		//week suffixes
	$end = date('t',mktime(0,0,0,$month,1,$year)); 		//last date day of month: 28 - 31
  	$start = date('w',mktime(0,0,0,$month,1,$year)); 	//1st day of month: 0 - 6 (Sun - Sat)
	$last = 7 - $start; 					//get last day date (Sat) of first week
	$noweeks = ceil((($end - ($last + 1))/7) + 1);		//total no. weeks in month
	//$output = "";						//initialize string		
	$output = array();						//initialize string		
	$monthlabel = str_pad($month, 2, '0', STR_PAD_LEFT);
        
        $i = 0;
        
	for($x=1;$x<$noweeks+1;$x++){	
		if($x == 1){
			$startdate = "$year-$monthlabel-01";
			$day = $last - 6;
		}else{
			$day = $last + 1 + (($x-2)*7);
			$day = str_pad($day, 2, '0', STR_PAD_LEFT);
			$startdate = "$year-$monthlabel-$day";
		}
		if($x == $noweeks){
			$enddate = "$year-$monthlabel-$end";
		}else{
			$dayend = $day + 6;
			$dayend = str_pad($dayend, 2, '0', STR_PAD_LEFT);
			$enddate = "$year-$monthlabel-$dayend";
		}
		//$output .= "{$x}{$suff[$x-1]} week -> Start date=$startdate End date=$enddate <br />";	
		$output[$i]['week'] = "{$x}{$suff[$x-1]} week";
                $output[$i]['startdate']= strtotime($startdate);	
                $output[$i]['enddate']= strtotime($enddate);
                
                $i++;
	}
        
        //echo "<pre>";print_r($output); echo "</pre>"; die("hi");
	return $output;
}    
    
    
    
    
    
    
            public function index(){
                $project_table = TableRegistry::get('project');
                $billing_table = TableRegistry::get('billing');
                $task_table = TableRegistry::get('task');
                
                $this->loadComponent('Flash'); // Include the FlashComponent
  $this->loadComponent('Paginator'); // it will load Paginator

/*                
                 $retrieve_proejct = $project_table->find()->select(['project.id' , 'project.nlimit' , 'c.fname' , 'c.lname' , 'e.name' , 'e.picture' , 'project.name' , 'project.end' , 'project.status' , 'project.ptype' ,'project.created' , 'project.hours' ])->join([
                    'c' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(c.id) =  project.cid' 
                    ],
                    'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(e.id) =  project.lead' 
                    ]
                 ])->where([ 'project.status !=' => '0' , 'c.status !=' => '0' ])->toArray() ;

*/                 
                 
      $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '15'
  ];           
                 $retrieve_proejct = $this->paginate($project_table->find()->select(['project.id' , 'project.nlimit' , 'c.fname' , 'c.lname' , 'e.name' , 'e.picture' , 'project.name' , 'project.end' , 'project.status' , 'project.ptype' ,'project.created' , 'project.hours' ])->join([
                    'c' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(c.id) =  project.cid' 
                    ],
                    'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(e.id) =  project.lead' 
                    ]
                 ])->where([ 'project.status !=' => '0' , 'c.status !=' => '0' ])->order(['project.name' => 'asc']))->toArray() ;


                 foreach($retrieve_proejct as $project){
                    $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'status' => 4 , 'project' => md5($project['id']) ])->first();
                    $project['spent'] = $count_billed->billed ;
                    if($project['spent'] == 0 || $project['hours'] == 0){
                        $project['progress'] = 0 ;
                    }
                    else{
                        $project['progress'] = round(($project['spent']/$project['hours'])*100 , 2 ) ;

                    }


                    $query = $billing_table->find();                     
                    $retrieve_billing_total = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['pid' => $project['id'] , 'deleted' => 0 ])->first() ;
                    $project['balance'] = $retrieve_billing_total->hours - $project['spent']  ;

                 }

                $this->set("pj_details", $retrieve_proejct);  
                $this->viewBuilder()->setLayout('user');


            }
            
            
            public function add($team = null,$month1=null,$year1=null,$agent=null,$fetched_team=null){
              
                if($agent != 999999)
                {
                
                
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $team_table = TableRegistry::get('team');
                
                                    $teamreviews_table = TableRegistry::get('teamreviews');

                
                                $emp_attendance_table = TableRegistry::get('emp_attendance');
                $task_table = TableRegistry::get('task');

                     $retrieve_team = $team_table->find()->select(['id' , 'name' ])->where([ 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;           
                
                
                     $this->set("retrieve_team", $retrieve_team); 
                
                
                
                
                if ($month1) 
                {
                    $month = $month1;
                }
                else
                {
                    $month = date('m');
                }

                if ($year1) 
                {
                    $year = $year1;
                }
                else
                {
                    $year = date("Y"); 
                }
                
                
                                $ary_weeks_current_month = array();
                                $month_current = date('m');
                    $year_current = date("Y"); 
                $ary_weeks_current_month =  $this->getWeeks($month_current,$year_current);

                
                                $ary_weeks_previous_month = array();
                    $month_previous = date('m', strtotime('first day of last month'));
                    $year_previous = date('y', strtotime('first day of last month'));
                $ary_weeks_previous_month =  $this->getWeeks($month_previous,$year_previous);
                    
                    
                    
                
                
                $uid=$agent;
                
                if($uid)
                {
                                
$user_details[0] = $users_table->find()->where([ 'id' => $uid])->toArray();
                
               // print_r($user_details); die("aaa");
                
                //$month = date('m');
                //$year = date("Y"); 
                
                
               // $month = "9";
               // $year = "2020"; 
                
                

                                $currentfirst = strtotime("first day of this month midnight");
                $currentlast = strtotime("first day of next month midnight");

                $ary_weeks = array();
                
                $ary_weeks =  $this->getWeeks($month,$year);
                
                //echo "<pre>";print_r($ary_weeks); echo "</pre>"; die("hi2");
                
                $ary_weeks_data = array();
                
                for($i=0; $i<count($ary_weeks);$i++)
{
                    $week_first_date = $ary_weeks[$i]['startdate'];
                    $week_last_date = $ary_weeks[$i]['enddate'];

                $ary_weeks_data[0][$i]['week_attendance'] = $emp_attendance_table->find()->distinct('userid')->select([ 'full' => 'sum(CASE WHEN Status = 1 THEN 1 END)' , 'haif' => 'sum(CASE WHEN Status = 2 THEN 0.5 END)' , 'day' ])->where([ 'day >=' => $week_first_date, 'day <' => $week_last_date , 'userid' => $uid ])->toArray();
                $ary_weeks_data[0][$i]['week_spent'] = $task_table->find()->distinct('assigned')->select([ 'spent' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" , 'completedtime >=' => $week_first_date, 'completedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                $ary_weeks_data[0][$i]['week_rework'] = $task_table->find()->distinct('assigned')->select([ 'rework' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" ,'task_status_new_rework >=' => '2', 'completedtime >=' => $week_first_date,'completedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                $ary_weeks_data[0][$i]['week_review'] = $teamreviews_table->find()->where([ 'process_adherence IS NOT NULL' ,'week_start_date >=' => $week_first_date,'week_end_date <=' => $week_last_date , 'userid' => $uid ])->order(['created' => 'DESC' ])->limit(1)->toArray();
                $ary_weeks_data[0][$i]['week_review_admin'] = $teamreviews_table->find()->where([ 'admin_process_adherence IS NOT NULL' ,'week_start_date >=' => $week_first_date,'week_end_date <=' => $week_last_date , 'userid' => $uid ])->order(['created' => 'DESC' ])->limit(1)->toArray();
                

                //$ary_weeks_data[0][$i]['week_billed'] = $task_table->find()->distinct('assigned')->select([ 'billed' => 'SUM(billed)' , 'billedon' ])->where(['billed IS NOT' => "null" , 'billedon >=' => $week_first_date, 'billedon <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                $ary_weeks_data[0][$i]['week_billed'] = $task_table->find()->distinct('assigned')->select([ 'billed' => 'SUM(billed)' , 'billedon' ])->where(['billed IS NOT' => "null" , 'billedtime >=' => $week_first_date, 'billedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                    
                
                $ary_weeks_data[0][$i]['week_attendance_day_wise'] = $emp_attendance_table->find()->select([ 'u.name' , 'emp_attendance.userid' , 'emp_attendance.login' , 'emp_attendance.logout' , 'emp_attendance.day' , 'emp_attendance.id' ])->join([
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  emp_attendance.userid' 
                        ]
                    ])->where(['(emp_attendance.userid)' => $uid , 'emp_attendance.day >=' => $week_first_date, 'emp_attendance.day <=' => $week_last_date   ])->order(['emp_attendance.day' => 'ASC' ])->toArray();
                
                //echo "<pre>";print_r($ary_weeks_data[0][$i]); echo "</pre>"; 
                
            /*    
                $log = $emp_attendance_table->find()->select([ 'u.name' , 'emp_attendance.userid' , 'emp_attendance.login' , 'emp_attendance.logout' , 'emp_attendance.day' , 'emp_attendance.id' ])->join([
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  emp_attendance.userid' 
                        ]
                    ])->where(['(emp_attendance.userid)' => $uid , 'emp_attendance.day >=' => $week_first_date, 'emp_attendance.day <=' => $week_last_date   ])->order(['emp_attendance.day' => 'ASC' ]);       
debug($log);
*/
                
//die("hi2");
                }
                
                
                
                                $month_rework = $task_table->find()->distinct('assigned')->select([ 'rework' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" ,'task_status_new_rework >=' => '2', 'completedtime >=' => $currentfirst,'completedtime <' => $currentlast , 'assigned' => md5($uid) ])->toArray();
                //$count_assigned_rework = $task_table->find()->select(['rework_estimate' => 'SUM(task.estimate)' ])->where([$assigned_cond , $cat_cond,   ['OR' => [['task.status' => '1']]], 'task.task_status_new_rework' => '2' ,'task.assigned !=' => '' , 'OR' => $user_cond ])->first();

                                
                                /*
                  $week_first_date_seo = strtotime('-6 days');
                $week_last_date_seo = strtotime('now');
*/
                                
                                 $week_first_date_seo = strtotime('last sunday');
                $week_last_date_seo = strtotime('saturday');
                
                
//echo $week_first_date_seo;
//echo $week_last_date_seo;
//die;



                $this->set("week_first_date_seo", $week_first_date_seo);
                $this->set("week_last_date_seo", $week_last_date_seo);
               
                
                
                $this->set("month_rework", $month_rework);
                $this->set("ary_weeks", $ary_weeks);
                $this->set("ary_weeks_data", $ary_weeks_data);
                
                
                                $this->set("user_details2", $user_details);
                                $this->set("agent", $uid);


                }
                //echo "<pre>";print_r($ary_weeks_data); echo "</pre>"; die("hi2");

                
                                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                if($team == 6)
                {
                $retrieve_emp = $users_table->find()->select(['id' , 'name', 'team','role' ])->where(['team !='=>$team] )->order(['team'=>'asc','role'=>'asc'])->toArray() ;
                    
                }
                else
                {
                $retrieve_emp = $users_table->find()->select(['id' , 'name', 'team','role' ])->where(['role >' => '2', 'team'=>$team] )->order(['name'=>'asc'])->toArray() ;
                }                

//$retrieve_team = $team_table->find()->select(['id' , 'name' ])->where([ 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;

               // $retrieve_client = $client_table->find()->select(['id' , 'fname' , 'lname'])->where([ 'status' => '1'])->order(['fname' => 'asc' ])->toArray() ;

//                $this->set("userid", $userid);  
//                $this->set("team_details", $retrieve_team);  
//                $this->set("cli_details", $retrieve_client);
                
                /*
                $login = $this->Account->find('first', array(
                        'conditions' => array(
                            'Account.email' => $username,
                            'Account.password' => $password
                        )
                    ));
                 
                 */
                
//                $log = $users_table->find()->select(['id' , 'name' ])->where(['role >' => '2', 'team'=>$team] )->order(['name'=>'asc']);
//debug($log);
//                 echo date('Y-m-d',strtotime('last sunday -7 days')); 
//                 echo date('Y-m-d',strtotime('last saturday'));
//                 die;
                 
//                 $week_first_date = strtotime('last sunday -7 days');
//                 $week_last_date = strtotime('last saturday');
        

               
                /*
                if(date('D') == 'Sun') 
                { 
                $week_first_date = strtotime('last sunday');
                $week_last_date = strtotime('last saturday');

} 
else
{
             $week_first_date = strtotime('last sunday -7 days');
                 $week_last_date = strtotime('last saturday');
}
   
*/
$canSubmitWeeklyReview = "1";
//Check Whether Today is Thu, Fri, Sat
//If it is so then disable Weekly Review Submit Button
if(date('D') == 'Thu' || date('D') == 'Fri'|| date('D') == 'Sat') 
{ 
//$canSubmitWeeklyReview = "2";
} 





                /*
                 
                //08/16/2020 @ 12:00am (UTC)
                 $week_first_date = '1597536000';
                 
                 //08/22/2020 @ 12:00am (UTC)
                 $week_last_date = '1598054400';
                 
                 
                 */
                
/*
                 $ary_weeks = array();
                  $ary_weeks_data = array();
                
                 //echo "<pre>"; print_r($retrieve_emp); die;
                
                
                                for($i=0; $i<count($retrieve_emp);$i++)
{
                                    $uid = $retrieve_emp[$i]['id'];
                                    
                                    
//                    $week_first_date = $ary_weeks[$i]['startdate'];
//                    $week_last_date = $ary_weeks[$i]['enddate'];

//                $ary_weeks_data[0][$i]['week_attendance'] = $emp_attendance_table->find()->distinct('userid')->select([ 'full' => 'sum(CASE WHEN Status = 1 THEN 1 END)' , 'haif' => 'sum(CASE WHEN Status = 2 THEN 0.5 END)' , 'day' ])->where([ 'day >=' => $week_first_date, 'day <' => $week_last_date , 'userid' => $uid ])->toArray();
//                $ary_weeks_data[0][$i]['week_spent'] = $task_table->find()->distinct('assigned')->select([ 'spent' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" , 'completedtime >=' => $week_first_date, 'completedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
//                $ary_weeks_data[0][$i]['week_rework'] = $task_table->find()->distinct('assigned')->select([ 'rework' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" ,'task_status_new_rework >=' => '2', 'completedtime >=' => $week_first_date,'completedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
$ary_weeks_data[0][$i]['week_attendance_day_wise'] = $emp_attendance_table->find()->select([ 'u.name' , 'emp_attendance.userid' , 'emp_attendance.login' , 'emp_attendance.logout' , 'emp_attendance.day' , 'emp_attendance.id' ])->join([
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  emp_attendance.userid' 
                        ]
                    ])->where(['(emp_attendance.userid)' => $uid , 'emp_attendance.day >=' => $week_first_date, 'emp_attendance.day <=' => $week_last_date   ])->order(['emp_attendance.day' => 'ASC' ])->toArray();
                                
          
                $ary_weeks_data[0][$i]['week_review'] = $teamreviews_table->find()->where([ 'week_start_date >=' => $week_first_date,'week_end_date <=' => $week_last_date , 'userid' => $uid ])->order(['created' => 'DESC' ])->limit(1)->toArray();

                                   
}
           
 */



                 //echo "<pre>"; print_r($ary_weeks_data); die;


//$this->set("month2", $month); 
                $this->set("month2", $month); 

$this->set("retrieve_emp", $retrieve_emp);  
//$this->set("ary_weeks_data", $ary_weeks_data);  
$this->set("team", $team);  
$this->set("canSubmitWeeklyReview", $canSubmitWeeklyReview);  
$this->set("ary_weeks_current_month", $ary_weeks_current_month);  
$this->set("ary_weeks_previous_month", $ary_weeks_previous_month);  


                $this->viewBuilder()->setLayout('user');
                }
                else if($fetched_team != 888888)
                {
                
                
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $team_table = TableRegistry::get('team');
                
                                    $teamreviews_table = TableRegistry::get('teamreviews');

                
                                $emp_attendance_table = TableRegistry::get('emp_attendance');
                $task_table = TableRegistry::get('task');

                     $retrieve_team = $team_table->find()->select(['id' , 'name' ])->where([ 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;           
                
                
                     $this->set("retrieve_team", $retrieve_team); 
                
                $retrieved_employees = $users_table->find()->select(['id' , 'name', 'team','role' ])->where(['team'=>$fetched_team] )->order(['name'=>'asc'])->toArray() ;
                
                
$ary_weeks = array();                
 $ary_weeks_data = array();                
 $user_details = array();                
                
 //echo count($retrieved_employees); die;
                
                for($d=0; $d<count($retrieved_employees); $d++ )
                {
                
                if ($month1) 
                {
                    $month = $month1;
                }
                else
                {
                    $month = date('m');
                }

                if ($year1) 
                {
                    $year = $year1;
                }
                else
                {
                    $year = date("Y"); 
                }
                
                
                                $ary_weeks_current_month = array();
                                $month_current = date('m');
                    $year_current = date("Y"); 
                $ary_weeks_current_month =  $this->getWeeks($month_current,$year_current);

                
                                $ary_weeks_previous_month = array();
                    $month_previous = date('m', strtotime('first day of last month'));
                    $year_previous = date('y', strtotime('first day of last month'));
                $ary_weeks_previous_month =  $this->getWeeks($month_previous,$year_previous);
                    
                    
                    
                
                
                //$uid=$agent;
                $uid=$retrieved_employees[$d]['id'];
                
                //echo "<pre>";print_r($retrieved_employees);die;
                
                //echo $uid;
                //echo $d; die;
                
                if($uid)
                {
                    
                    //echo $d;die;
                      $user_details[$d] = $users_table->find()->where([ 'id' => $uid])->toArray();
          
//$udet = $users_table->find()->where([ 'id' => $uid])->toArray();
  //        $user_details[$d] = $udet;
          
               //echo "<pre>"; print_r($user_details); die("aaa");
                
                //$month = date('m');
                //$year = date("Y"); 
                
                
               // $month = "9";
               // $year = "2020"; 
                
                

                                $currentfirst = strtotime("first day of this month midnight");
                $currentlast = strtotime("first day of next month midnight");

              //  $ary_weeks = array();
                
                $ary_weeks =  $this->getWeeks($month,$year);
                
                //echo "<pre>";print_r($ary_weeks); echo "</pre>"; die("hi2");
                
             //   $ary_weeks_data = array();
                
                for($i=0; $i<count($ary_weeks);$i++)
{
                    $week_first_date = $ary_weeks[$i]['startdate'];
                    $week_last_date = $ary_weeks[$i]['enddate'];

                $ary_weeks_data[$d][$i]['week_attendance'] = $emp_attendance_table->find()->distinct('userid')->select([ 'full' => 'sum(CASE WHEN Status = 1 THEN 1 END)' , 'haif' => 'sum(CASE WHEN Status = 2 THEN 0.5 END)' , 'day' ])->where([ 'day >=' => $week_first_date, 'day <' => $week_last_date , 'userid' => $uid ])->toArray();
                $ary_weeks_data[$d][$i]['week_spent'] = $task_table->find()->distinct('assigned')->select([ 'spent' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" , 'completedtime >=' => $week_first_date, 'completedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                $ary_weeks_data[$d][$i]['week_rework'] = $task_table->find()->distinct('assigned')->select([ 'rework' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" ,'task_status_new_rework >=' => '2', 'completedtime >=' => $week_first_date,'completedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                $ary_weeks_data[$d][$i]['week_review'] = $teamreviews_table->find()->where([ 'process_adherence IS NOT NULL' ,'week_start_date >=' => $week_first_date,'week_end_date <=' => $week_last_date , 'userid' => $uid ])->order(['created' => 'DESC' ])->limit(1)->toArray();
                $ary_weeks_data[$d][$i]['week_review_admin'] = $teamreviews_table->find()->where([ 'admin_process_adherence IS NOT NULL' ,'week_start_date >=' => $week_first_date,'week_end_date <=' => $week_last_date , 'userid' => $uid ])->order(['created' => 'DESC' ])->limit(1)->toArray();
                

                //$ary_weeks_data[$d][$i]['week_billed'] = $task_table->find()->distinct('assigned')->select([ 'billed' => 'SUM(billed)' , 'billedon' ])->where(['billed IS NOT' => "null" , 'billedon >=' => $week_first_date, 'billedon <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                $ary_weeks_data[$d][$i]['week_billed'] = $task_table->find()->distinct('assigned')->select([ 'billed' => 'SUM(billed)' , 'billedon' ])->where(['billed IS NOT' => "null" , 'billedtime >=' => $week_first_date, 'billedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
                    
                
                $ary_weeks_data[$d][$i]['week_attendance_day_wise'] = $emp_attendance_table->find()->select([ 'u.name' , 'emp_attendance.userid' , 'emp_attendance.login' , 'emp_attendance.logout' , 'emp_attendance.day' , 'emp_attendance.id' ])->join([
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  emp_attendance.userid' 
                        ]
                    ])->where(['(emp_attendance.userid)' => $uid , 'emp_attendance.day >=' => $week_first_date, 'emp_attendance.day <=' => $week_last_date   ])->order(['emp_attendance.day' => 'ASC' ])->toArray();
                
             //   echo "<pre>";print_r($ary_weeks_data); echo "</pre>"; die;
                
            /*    
                $log = $emp_attendance_table->find()->select([ 'u.name' , 'emp_attendance.userid' , 'emp_attendance.login' , 'emp_attendance.logout' , 'emp_attendance.day' , 'emp_attendance.id' ])->join([
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  emp_attendance.userid' 
                        ]
                    ])->where(['(emp_attendance.userid)' => $uid , 'emp_attendance.day >=' => $week_first_date, 'emp_attendance.day <=' => $week_last_date   ])->order(['emp_attendance.day' => 'ASC' ]);       
debug($log);
*/
                
//die("hi2");
                }
                
                
                
                                $month_rework[$d] = $task_table->find()->distinct('assigned')->select([ 'rework' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" ,'task_status_new_rework >=' => '2', 'completedtime >=' => $currentfirst,'completedtime <' => $currentlast , 'assigned' => md5($uid) ])->toArray();
                //$count_assigned_rework = $task_table->find()->select(['rework_estimate' => 'SUM(task.estimate)' ])->where([$assigned_cond , $cat_cond,   ['OR' => [['task.status' => '1']]], 'task.task_status_new_rework' => '2' ,'task.assigned !=' => '' , 'OR' => $user_cond ])->first();

                  $week_first_date_seo = strtotime('-6 days');
                $week_last_date_seo = strtotime('now');

                
                
//echo $week_first_date_seo;
//echo $week_last_date_seo;
//die;

 }
                }
                
// echo "<pre>";print_r($ary_weeks_data); echo "</pre>"; die;
 //echo "<pre>";print_r($user_details); echo "</pre>"; die;
 
 
                $this->set("week_first_date_seo", $week_first_date_seo);
                $this->set("week_last_date_seo", $week_last_date_seo);
               
                
                
                $this->set("month_rework", $month_rework);
                $this->set("ary_weeks", $ary_weeks);
                $this->set("ary_weeks_data", $ary_weeks_data);
                
                
                                $this->set("user_details2", $user_details);
                                $this->set("agent", "");
                                $this->set("fetched_team", $fetched_team);


               
                //echo "<pre>";print_r($ary_weeks_data); echo "</pre>"; die("hi2");

                
                                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                if($team == 6)
                {
                $retrieve_emp = $users_table->find()->select(['id' , 'name', 'team','role' ])->where(['team !='=>$team] )->order(['team'=>'asc','role'=>'asc'])->toArray() ;
                    
                }
                else
                {
                $retrieve_emp = $users_table->find()->select(['id' , 'name', 'team','role' ])->where(['role >' => '2', 'team'=>$team] )->order(['name'=>'asc'])->toArray() ;
                }                

//$retrieve_team = $team_table->find()->select(['id' , 'name' ])->where([ 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;

               // $retrieve_client = $client_table->find()->select(['id' , 'fname' , 'lname'])->where([ 'status' => '1'])->order(['fname' => 'asc' ])->toArray() ;

//                $this->set("userid", $userid);  
//                $this->set("team_details", $retrieve_team);  
//                $this->set("cli_details", $retrieve_client);
                
                /*
                $login = $this->Account->find('first', array(
                        'conditions' => array(
                            'Account.email' => $username,
                            'Account.password' => $password
                        )
                    ));
                 
                 */
                
//                $log = $users_table->find()->select(['id' , 'name' ])->where(['role >' => '2', 'team'=>$team] )->order(['name'=>'asc']);
//debug($log);
//                 echo date('Y-m-d',strtotime('last sunday -7 days')); 
//                 echo date('Y-m-d',strtotime('last saturday'));
//                 die;
                 
//                 $week_first_date = strtotime('last sunday -7 days');
//                 $week_last_date = strtotime('last saturday');
        

               
                /*
                if(date('D') == 'Sun') 
                { 
                $week_first_date = strtotime('last sunday');
                $week_last_date = strtotime('last saturday');

} 
else
{
             $week_first_date = strtotime('last sunday -7 days');
                 $week_last_date = strtotime('last saturday');
}
   
*/
$canSubmitWeeklyReview = "1";
//Check Whether Today is Thu, Fri, Sat
//If it is so then disable Weekly Review Submit Button
if(date('D') == 'Thu' || date('D') == 'Fri'|| date('D') == 'Sat') 
{ 
//$canSubmitWeeklyReview = "2";
} 





                /*
                 
                //08/16/2020 @ 12:00am (UTC)
                 $week_first_date = '1597536000';
                 
                 //08/22/2020 @ 12:00am (UTC)
                 $week_last_date = '1598054400';
                 
                 
                 */
                
/*
                 $ary_weeks = array();
                  $ary_weeks_data = array();
                
                 //echo "<pre>"; print_r($retrieve_emp); die;
                
                
                                for($i=0; $i<count($retrieve_emp);$i++)
{
                                    $uid = $retrieve_emp[$i]['id'];
                                    
                                    
//                    $week_first_date = $ary_weeks[$i]['startdate'];
//                    $week_last_date = $ary_weeks[$i]['enddate'];

//                $ary_weeks_data[$i]['week_attendance'] = $emp_attendance_table->find()->distinct('userid')->select([ 'full' => 'sum(CASE WHEN Status = 1 THEN 1 END)' , 'haif' => 'sum(CASE WHEN Status = 2 THEN 0.5 END)' , 'day' ])->where([ 'day >=' => $week_first_date, 'day <' => $week_last_date , 'userid' => $uid ])->toArray();
//                $ary_weeks_data[$i]['week_spent'] = $task_table->find()->distinct('assigned')->select([ 'spent' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" , 'completedtime >=' => $week_first_date, 'completedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
//                $ary_weeks_data[$i]['week_rework'] = $task_table->find()->distinct('assigned')->select([ 'rework' => 'SUM(spent)' , 'completedtime' ])->where(['spent IS NOT' => "null" ,'task_status_new_rework >=' => '2', 'completedtime >=' => $week_first_date,'completedtime <' => $week_last_date , 'assigned' => md5($uid) ])->toArray();
$ary_weeks_data[$i]['week_attendance_day_wise'] = $emp_attendance_table->find()->select([ 'u.name' , 'emp_attendance.userid' , 'emp_attendance.login' , 'emp_attendance.logout' , 'emp_attendance.day' , 'emp_attendance.id' ])->join([
                        'u' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  emp_attendance.userid' 
                        ]
                    ])->where(['(emp_attendance.userid)' => $uid , 'emp_attendance.day >=' => $week_first_date, 'emp_attendance.day <=' => $week_last_date   ])->order(['emp_attendance.day' => 'ASC' ])->toArray();
                                
          
                $ary_weeks_data[$i]['week_review'] = $teamreviews_table->find()->where([ 'week_start_date >=' => $week_first_date,'week_end_date <=' => $week_last_date , 'userid' => $uid ])->order(['created' => 'DESC' ])->limit(1)->toArray();

                                   
}
           
 */



                 //echo "<pre>"; print_r($ary_weeks_data); die;


//$this->set("month2", $month); 
                $this->set("month2", $month); 

$this->set("retrieve_emp", $retrieve_emp);  
//$this->set("ary_weeks_data", $ary_weeks_data);  
$this->set("team", $team);  
$this->set("canSubmitWeeklyReview", $canSubmitWeeklyReview);  
$this->set("ary_weeks_current_month", $ary_weeks_current_month);  
$this->set("ary_weeks_previous_month", $ary_weeks_previous_month);  


                $this->viewBuilder()->setLayout('user');
                }
            }

            public function addpj(){
                if ($this->request->is('ajax') && $this->request->is('post') ){


                    $teamreviews_table = TableRegistry::get('teamreviews');
                    $activ_table = TableRegistry::get('activity');
                    
                   // $week_first_date = strtotime('last sunday -7 days');
                 //$week_last_date = strtotime('last saturday');

/*                    
                    if(date('D') == 'Sun') 
                { 
                $week_first_date = strtotime('last sunday');
                $week_last_date = strtotime('last saturday');

} 
else
{
             $week_first_date = strtotime('last sunday -7 days');
                 $week_last_date = strtotime('last saturday');
}
*/
$ary_week_for_reviews = array();
                  $ary_week_for_reviews =   explode(",",$this->request->data('week_for_reviews'));
               $week_first_date  = $ary_week_for_reviews[0];   
               $week_last_date  = $ary_week_for_reviews[1]; 
               
//               echo $week_first_date."<br>";
//               echo $week_last_date."<br>";
//               die;
               
                    
                    
                        $teamreviews = $teamreviews_table->newEntity();
                        $teamreviews->userid =  $this->request->data('agent')  ;
                        $teamreviews->process_adherence =  $this->request->data('process_adherence')  ;
                        $teamreviews->weekly_review =  $this->request->data('weekly_review')  ;
                        $teamreviews->star_rating =  $this->request->data('rating')  ;
                        $teamreviews->admin_process_adherence =  $this->request->data('admin_process_adherence')  ;
                        $teamreviews->admin_weekly_review =  $this->request->data('admin_weekly_review')  ;
                        $teamreviews->admin_star_rating =  $this->request->data('admin_star_rating')  ;
                        $teamreviews->delivery_projects =  $this->request->data('delivery_projects')  ;
                        $teamreviews->week_start_date = $week_first_date ;
                        $teamreviews->week_end_date =  $week_last_date  ;
                        $teamreviews->created = strtotime('now');
                        $teamreviews->modified = strtotime('now');
                        
                       $team =  $this->request->data('team')  ;

                        if($saved = $teamreviews_table->save($teamreviews) ){
                            $pjid = $saved->id; 
                            // $members = $this->request->data('members');
                            // foreach($members as $member){
                            //    $project_t = $project_team->newEntity();
                            //    $project_t->pid =  $pjid  ;
                            //    $project_t->uid =  $member  ;
                            //    $project_t->created =  strtotime($this->request->data('edate'))  ;
                            //    $project_team->save($project_t) ;
                            // }
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Team Review Created"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($pjid)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success' , 'team' => $team  ];
    
                            }
                            else{
                        $res = [ 'result' => 'activity not saved'  ];
    
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'Team Review not saved'  ];
                        }
                    
                   

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            
            
            
public function search()
            {
                
                $month = $this->request->data('month');
                $year = $this->request->data('year');
                
                
                $agent = $this->request->data('agent');
                
                
                if(isset($agent) && $agent != "")
                {
                }
                else
                {
                $agent = 999999;
                    }


                $team = $this->request->data('team');
                
                
                $fetched_team = $this->request->data('lstTeam');

                if(isset($fetched_team) && $fetched_team != "")
                {
                }
                else
                {
                $fetched_team = 888888;
                    }




                
                if ($month !='' && $month != 0) 
                {
                    $month = $this->request->data('month');
                }
                else
                {
                    $month = date('m');
                }

                if ($year !='' && $year != 0) 
                {
                    $year = $this->request->data('year');
                }
                else
                {
                    $year = date("Y");
                }

                
                //$number = 7;
                
                return $this->redirect('/teamreviews/add/'.$team.'/'.$month.'/'.$year.'/'.$agent.'/'.$fetched_team);

            }            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            public function edit($pid){
                $project_table = TableRegistry::get('project');
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $team_table = TableRegistry::get('team');
                $retrieve_project = $project_table->find()->where([ 'status !=' => '0' , 'md5(id)' => $pid  ])->first() ;
                $retrieve_emp = $users_table->find()->select(['id' , 'name' ])->where([ 'FIND_IN_SET(\'25\',privilages)'  , 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;
                $retrieve_team = $team_table->find()->select(['id' , 'name' ])->where([ 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;

                $retrieve_client = $client_table->find()->select(['id' , 'fname' , 'lname'])->where([ 'status' => '1'])->order(['fname' => 'asc' ])->toArray() ;
                $this->set("emp_details", $retrieve_emp);  
                $this->set("team_details", $retrieve_team);  
                $this->set("cli_details", $retrieve_client);  

                $this->set("project", $retrieve_project);  
               $this->viewBuilder()->setLayout('user');
            }


            public function detail($id){
                $project_table = TableRegistry::get('project');
                $project_t_table = TableRegistry::get('project_team');
                $retrieve_proejct = $project_table->find()->select(['project.id' , 'c.fname' , 'c.lname' , 'c.picture' , 'c.id' , 'e.name' , 'e.picture' ,  'e.email'  ,'project.hours', 'project.name' , 'project.start', 'project.end' , 'project.status' , 'project.created' ])->join([
                    'c' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  project.cid' 
                    ],
                    'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'e.id =  project.lead' 
                    ]
                    
                 ])->where([ 'md5(project.id)' => $id  ])->first() ;

                 $retrieve_proejct_team = $project_t_table->find()->select([   'e.name' ,  'e.email' , 'e.picture'   ])->join([
                     'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'e.id =  project_team.uid' 
                    ]
                    
                 ])->where([ 'md5(project_team.pid)' => $id  ])->toArray() ;
                 if(!isset($retrieve_proejct->id)){
                    return $this->redirect('/projects');      

                 }
                 $this->set("retrieve_project", $retrieve_proejct);  
                 $this->set("project_team", $retrieve_proejct_team);  

                $this->viewBuilder()->setLayout('user');
            }

            
            public function editpj(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $task_table = TableRegistry::get('task');
                $client_table = TableRegistry::get('client');
                $billing_table = TableRegistry::get('billing');
                $name = $this->request->data('name') ;
                $start = strtotime($this->request->data('sdate')) ;
                $end = NULL ;
                if($this->request->data('edate') != "")
                {
                    $end =  strtotime($this->request->data('edate')) ;
                }

                $type = $this->request->data('type') ;
                $hours = $this->request->data('hours') ;
                $client = $this->request->data('client') ;
                $oclient = $this->request->data('oclient') ;
                $rate = $this->request->data('rate') ;
                $lead = $this->request->data('lead') ;
                $team = $this->request->data('team') ;
                $ptype =  $this->request->data('ptype')  ;
                $nlimit = $this->request->data('nlimit') ;
                
                $nallowed = $this->request->data('nallowed') ;
                if($nallowed != 1){
                    $nallowed = 0 ;
                 }
                $description = $this->request->data('description') ;
                $pid = $this->request->data('pid') ;
                $now = strtotime('now') ;
                $retrieve_project = $project_table->find()->select(['id'  ])->where([ 'md5(id) !=' => $pid , 'status !=' => '0' ])->count() ;
                $retrieve_oclient = $client_table->find()->select(['id'])->where([ 'md5(id)' => $oclient  ])->first() ;
                $retrieve_client = $client_table->find()->select(['id'])->where([ 'md5(id)' => $client  ])->first() ;
                
                if($retrieve_project= $project_table->query()->update()->set([ 'name' => $name , 'start' => $start  , 'end' => $end , 'type' => $type , 'hours' => $hours  , 'cid' => $client ,'nlimit' => $nlimit , 'ptype' => $ptype 
                , 'rate' => $rate , 'lead' => $lead , 'team' => $team ,'nallowed' => $nallowed , 'description' => $description ,'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                $task_table->query()->update()->set([ 'client' => $client , 'modified' => $now ])->where([ 'client' => $oclient  ])->execute();
                $billing_table->query()->update()->set([ 'cid' => $retrieve_client->id  ])->where([ 'cid' => $retrieve_oclient->id , 'md5(pid)' => $pid  ])->execute();
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Project Edited"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = $pid    ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success' , 'id' => $pid   ];
    
                            }
                }
          
                return $this->json($res);
 
                
            }

            public function delete(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $pid = $this->request->data('val') ;
                $now = strtotime('now') ;
                $str = $this->request->data('str') ;

                if($retrieve_client = $project_table->query()->update()->set([ 'status' => 0 , 'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                    "CC: jyoti@outsourcingservicesusa.com";
                    mail("info@outsourcingservicesusa.com" , "Project Deleted" , "<b>".ucfirst($str)."</b> has been deleted" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Project Deleted"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $pid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) )
                    {
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not deleted'];
                }


                return $this->json($res);

            }

            public function deactivate(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $pid = $this->request->data('val') ;
                $str = $this->request->data('str') ;
                $now = strtotime('now') ;

                if($retrieve_client = $project_table->query()->update()->set([ 'status' => 2 , 'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                                "CC: jyoti@outsourcingservicesusa.com";
                    mail("info@outsourcingservicesusa.com" , "Project Deactivated" , "<b>".ucfirst($str)."</b> has been deactivated" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Project Deactivated"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $pid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) ){
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not deactivated'];
                }


                return $this->json($res);

            }

            
            public function activate(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $str = $this->request->data('str') ;

                $pid = $this->request->data('val') ;
                $now = strtotime('now') ;

                if($retrieve_client = $project_table->query()->update()->set([ 'status' => 1 , 'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                    "CC: jyoti@outsourcingservicesusa.com";
                     mail("info@outsourcingservicesusa.com" , "Project Activated" , "<b>".ucfirst($str)."</b> has been activated" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Project activated"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $pid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) ){
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not activated'];
                }


                return $this->json($res);

            }

            }
