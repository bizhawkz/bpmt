<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
use Cake\Controller\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\View\ClientHelper;
use Cake\ORM\TableRegistry;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class TaskboardController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
        public function initialize()
        {
        parent::initialize();

            // if(isset($this->params['url']['arg']))
                $passedArgs = $this->request->getParam('pass');
                $this->spent_hours = 0 ;
                $this->billed_hours = 0 ;
                $this->unassigned_hours = 0 ;
                $this->assigned_hours = 0 ;
                $this->rework2_hours = 0 ;
                $this->pending_hours = 0 ;
                $this->progress_hours = 0 ;
                $this->completed_hours = 0 ;
               // $this->loadHelper('Client');
                $this->loadComponent('Flash'); // Include the FlashComponent
  $this->loadComponent('Paginator'); // it will load Paginator
                
                if(isset($passedArgs[0])){

                     $userid = $passedArgs[0]; 
                     $timelimit = strtotime('tomorrow');
                     
                    if($userid == null){
                        if($this->Cookie->read('id') == ""){
                            return $this->redirect('/login');      
                        }
                        $userid = $this->Cookie->read('id');
                        $user_view_id = $this->Cookie->read('id');
                        
                    }
                    $user_cat_id = $this->request->query('cat_id') ;
                    $cat_cond = "" ;
                    if($user_cat_id != "" && $user_cat_id != "all" ){
                                $cat_cond =  ['md5(task.category)' =>  $user_cat_id ];
                    }
    
                        
                   
                        if($userid == "all"){
                            $userid = $this->Cookie->read('id');
    
                            $user_cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                            $user_cond_unass = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                            ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                            ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'creator !=' =>  $userid]
                            ];
                        }
                        elseif($userid == 'alltask' ){
                            $user_cond = [];
                            $user_cond_unass = [    ['task.owner is' =>  NULL],
                            ['task.owner' =>  ''  ],
                            ['task.assigned' =>  '' ]
                            ];
                          
                        }
                        else{
                        //  $userid = $this->Cookie->read('id');
                        $user_cond_unass = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                            $user_cond = ['task.assigned' =>  $userid]  ;
                        }
                         $timelimitupper = strtotime('midnight first day of next month');
                         $bend_date = strtotime('midnight first day of next month');
                         
                        $timelimitlower = strtotime('midnight first day of this month') ;
                        $bstart_date = strtotime('midnight first day of this month');
                        $task_table = TableRegistry::get('task');
                        $btimelimitlower = strtotime('today');
                        $btimelimitupper = $btimelimitlower + 86400 ;
                        $completedon = $this->request->query('completedon') ;
                        $date_show_upper = strtotime('today') ;
                
                        $completedon_cond = '' ;
                        $assigned_cond = '' ;
                        $billed_cond = '' ;
                      
                        if($completedon != ""  ){
                            $date_array = explode('-',$completedon);
                            $timelimitlower = strtotime(trim($date_array[0])); 
                            $timelimitupper = strtotime(trim($date_array[1])) + 86400 ; 
                            $btimelimitlower =  $timelimitlower ;
                            $btimelimitupper = $timelimitupper ;
                            $date_show_upper =  $timelimitupper - 86400 ;
                        }
                        $completedon_cond =  [  'task.completedtime <=' =>  $timelimitupper   ];
                        $completedon_cond_all =  [ 'task.completedtime >=' =>  $timelimitlower  , 'task.completedtime <=' =>  $timelimitupper   ];
                        $assigned_cond =  [ 'task.duedate <' =>  $timelimit  ,'task.assignedtime <=' =>  $timelimitupper   ];
                        
                        $billed_cond =  [ 'task.billedon >=' =>  $btimelimitlower ,'task.billedon <=' =>  $btimelimitupper   ];
                        $billed_cond_gross =  [ 'task.billedon >=' =>  $timelimitlower ,'task.billedon <=' =>  $timelimitupper   ];
                        
                

                        

                        $count_spent = $task_table->find()->select(['spent' => 'SUM(task.spent)' ])->where([   $completedon_cond_all   , "OR" => $user_cond , $cat_cond ])->first();

                        $count_spent_management = $task_table->find()->select(['spent' => 'SUM(task.spent)' ])->where([   $completedon_cond_all   , "OR" => $user_cond , 'task.category' =>  2  ])->first();

                        $count_spent_cmeeting = $task_table->find()->select(['spent' => 'SUM(task.spent)' ])->where([   $completedon_cond_all   , "OR" => $user_cond , 'task.category' =>  9  ])->first();

                        $count_spent_omeeting = $task_table->find()->select(['spent' => 'SUM(task.spent)' ])->where([   $completedon_cond_all   , "OR" => $user_cond , 'task.category' =>  10  ])->first();

                        $count_spent_research = $task_table->find()->select(['spent' => 'SUM(task.spent)' ])->where([   $completedon_cond_all   , "OR" => $user_cond , 'task.category' =>  3  ])->first();


                        $count_negative_hours = $task_table->find()->select(['spent' => 'SUM(task.spent)' ])->where([   $completedon_cond_all   , "OR" => $user_cond , $cat_cond , 'task.negative' => '1' ])->first();                        
                        $last_negative_hours = $task_table->find()->select(['task.completedtime' ])->where([ $completedon_cond_all   , "OR" => $user_cond , $cat_cond , 'task.negative' => '1' ])->order(['task.completedtime' => 'desc'])->limit(1)->first();                        
                       
                        $count_billed = $task_table->find()->select(['billed' => 'SUM(task.billed)' ])->where([ 'task.status' => 4 ,  $billed_cond_gross   , "OR" => $user_cond , $cat_cond ])->first();
                       

                        $last_billed = $task_table->find()->select(['billedon' ])->where([ 'task.status' => 4 ,  $billed_cond_gross   , "OR" => $user_cond , $cat_cond ])->order(['billedon' => 'desc'])->limit(1)->first();
                  
                        $count_unassigned = $task_table->find()->select(['estimate' => 'SUM(task.estimate)' ])->where(['task.duedate <' => $timelimit ,   $cat_cond, 'task.status' => '1', 'OR' => $user_cond_unass ])->first();
                  
                       
                        $count_pending = $task_table->find()->select(['estimate' => 'SUM(task.estimate)'   ])->where([  'task.duedate <' => $timelimit , $cat_cond , 'task.status' => '1' , 'task.assigned' => '' , 'task.owner !=' =>  '' , 'OR' =>  $user_cond ])->orWhere([ 'task.status' => '1', $cat_cond , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' => $user_cond ])->first() ;
                      
                        $count_assigned = $task_table->find()->select(['estimate' => 'SUM(task.estimate)' ])->where([$assigned_cond , $cat_cond, 'task.status' => '1', 'task.rework' => 0 , 'task.assigned !=' => '' , 'OR' => $user_cond ])->first();

              
                        $count_assigned_rework = $task_table->find()->select(['rework_estimate' => 'SUM(task.estimate)' ])->where([$assigned_cond , $cat_cond,   ['OR' => [['task.status' => '1']]], 'task.task_status_new_rework' => '2' ,'task.assigned !=' => '' , 'OR' => $user_cond ])->first();
                        //$count_assigned_rework = debug($task_table->find()->select(['estimate' => 'SUM(task.estimate)' ])->where([$assigned_cond , $cat_cond, 'task.status' => '6'   ,'task.assigned !=' => '' , 'OR' => $user_cond ]));
                        //$count_assigned_rework = debug($task_table->find()->select(['estimate' => 'SUM(task.estimate)' ])->where([$assigned_cond , $cat_cond, 'task.status' => '6'  ,'task.rework' => 0 ,'task.assigned !=' => '' , 'OR' => $user_cond ]));
                        //dd($count_assigned_rework->sql());
                        
                        //echo $count_assigned_rework;
                        //die;
                        //debug($count_assigned_rework);
                       // echo "count_assigned_rework".$count_assigned_rework; die("aaa");

                        $count_progress = $task_table->find()->select(['estimate' => 'SUM(task.estimate)'])->where([ 'task.status' => '2' , $cat_cond  , $assigned_cond ,'task.rework' => 0, 'OR' => $user_cond ])->first() ;

                        $count_negative = $task_table->find()->where([ 'task.negative' => '1' , $cat_cond  , 'OR' => $user_cond ])->count() ;


                        $count_progress_rework = $task_table->find()->select(['rework_estimate' => 'SUM(task.rework_estimate)' ])->where([$assigned_cond , $cat_cond, 'task.status' => '2'  ,'task.rework' => 1 ,'task.assigned !=' => '' , 'OR' => $user_cond ])->first();


                        $count_completed = $task_table->find()->select(['spent' => 'SUM(task.spent)'])->where([ 'task.status' => '3'  , $cat_cond , $completedon_cond ,'OR' => $user_cond  ])->first() ;

                        $count_completed_rework = $task_table->find()->select(['rework_spent' => 'SUM(task.rework_spent)'])->where([ 'task.status' => '3'  , $cat_cond , $completedon_cond ,'OR' => $user_cond  ])->first() ;
                        
                        $count_quality = $task_table->find()->select(['spent' => 'SUM(task.spent)'])->where([ 'task.status' => '5' , $cat_cond , $completedon_cond ,'OR' => $user_cond  ])->orWhere([ 'task.status' => '6'  , 'task.rework' => 0 , $cat_cond , $completedon_cond ,'OR' => $user_cond  ])->first() ;
        
                        $count_billed_day = $task_table->find()->select(['billed' => 'SUM(task.billed)' ])->where([ 'task.status' => 4 ,  $billed_cond   , "OR" => $user_cond , $cat_cond ])->first();
                  
                        //  print_r($count_assigned);
                        

                    
                    $top_message = "*Above hour stats are from ".date('m/d/Y',$timelimitlower).' to '.date('m/d/Y',$date_show_upper  ) ;
                    $this->set("top_message", $top_message); 
                    
                    if($userid == "alltask"){
                        $user_name = "All ";
                    }
                    elseif($userid == "all"){
                        $user_name = "All managed task ";
                    } 
                    else{
                        $users_table = TableRegistry::get('users');
                        $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $userid ])->toArray() ;
                        $user_name =  $user_view[0]['name']."'s " ;
        
                    }

                    $below_message = "Showing task of ".$user_name." from ".date('m/d/Y',$timelimitlower).' to '.date('m/d/Y',$date_show_upper ) ;
                    $this->set("below_message", $below_message); 
                    $this->set("count_negative", $count_negative); 
                    $this->set("count_spent_management", round($count_spent_management->spent,2)); 
                    $this->set("count_spent_cmeeting", round($count_spent_cmeeting->spent,2)); 
                    $this->set("count_spent_omeeting", round($count_spent_omeeting->spent,2)); 
                    $this->set("count_spent_research", round($count_spent_research->spent,2)); 
                    $this->set("negative_hours", round($count_negative_hours->spent,2)); 
                    $this->set("date_show_upper", date('m/d',$date_show_upper ));
                    if(isset($last_billed->billedon)){
                        $this->set("last_billed", date('m/d',$last_billed->billedon ));
                    }
                    else{
                        $this->set("last_billed", "NA");
                    }

                    if(isset($last_negative_hours->completedtime)){
                        $this->set("last_negative", date('m/d',$last_negative_hours->completedtime ));
                    }
                    else{
                        $this->set("last_negative", "NA");
                    }

                    
                    
                    
                    $this->spent_hours = round(($count_spent->spent == "" ? 0 : $count_spent->spent),2) ;
                    $this->billed_hours = round(($count_billed->billed == "" ? 0 : $count_billed->billed),2) ;
                    $count_spent_np = $this->spent_hours - $this->billed_hours  ; 
                    $this->set("count_spent_np", round($count_spent_np,2));

                    $this->billed_hours_day = round(($count_billed_day->billed == "" ? 0 : $count_billed_day->billed),2) ;
                    $this->unassigned_hours = round(($count_unassigned->estimate == "" ? 0 : $count_unassigned->estimate ),2);
                    $this->assigned_hours = round(($count_assigned->estimate == "" ? 0 : $count_assigned->estimate),2) ;
                    $this->rework2_hours = round(($count_assigned_rework->rework_estimate == "" ? 0 : $count_assigned_rework->rework_estimate),2)  ;
                    $this->pending_hours = round(($count_pending->estimate == "" ? 0 : $count_pending->estimate),2) ;
                    $this->progress_hours = round(($count_progress->estimate == "" ? 0 : $count_progress->estimate),2) + round(($count_progress_rework->rework_estimate == "" ? 0 : $count_progress_rework->rework_estimate),2) ;
                    $this->completed_hours = round(($count_completed->spent == "" ? 0 : $count_completed->spent),2)+ round(($count_completed_rework->rework_spent == "" ? 0 : $count_completed_rework->rework_spent),2) ;
                    $this->quality_hours = round(($count_quality->spent == "" ? 0 : $count_quality->spent),2) ;
                    $this->completedon_cond = $completedon_cond ;
                    $this->billed_cond = $billed_cond ;
                    $this->assigned_cond = $assigned_cond ;
                    $this->completedon =  $completedon  ;
                    
                    //echo $this->rework2_hours; die;
 }
               

        }
            public function index($userid = null){
                
                
                                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    
                }


                //  $actionName = $this->request['url'];
                //  print_r($actionName);

               
                $user_sel_id = $userid ;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = "" ;
                if($user_cat_id != "" && $user_cat_id != "all" ){
                            $cat_cond =  ['md5(task.category)' =>  $user_cat_id ];
                }

              
                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                          $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }
                }

                

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;
                $timelimit = strtotime('tomorrow');
                $retrieve_unassigned = $this->paginate($task_table->find()->select(['task.id' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.description' ,'task.client', 'task.project','task.duedate' , 'task.created' , 'task.creator' ,'task.owner' , 'task.billing' ,'p.name', 'o.name', 'cr.name' , 'cl.fname', 'cl.lname' ,  'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',  
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([
                    'task.duedate <' => $timelimit ,
                     $cat_cond, 
                    'task.status' => '1',
                     'OR' => 
                                $cond1
                                    ]))->toArray() ;
                
                
                $retrieve_unassigned2 = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.description' ,'task.client', 'task.project','task.duedate' , 'task.created' , 'task.creator' ,'task.owner' , 'task.billing' ,'p.name', 'o.name', 'cr.name' , 'cl.fname', 'cl.lname' ,  'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',  
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([
                    'task.duedate <' => $timelimit ,
                     $cat_cond, 
                    'task.status' => '1',
                     'OR' => 
                                $cond1
                                    ])->toArray() ;                
                
                
                $retrieve_rework2 = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ ['OR' => [['task.status' => '1'], ['task.status' => '4']]],'task.task_status_new_rework' => '2', $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ])->toArray() ;                
                
                
        
                
             
                $today = strtotime('today');

                $count_pending = $task_table->find()->select(['task.id'   ])->where([  'task.duedate <' => $timelimit , $cat_cond ,
                    'task.status' => '1' , 'task.assigned' => '' , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->orWhere([ 'task.status' => '1' , 'task.assigned is' => NULL ,$cat_cond, 'task.owner !=' =>  '' , 'OR' => $cond2 ])->count() ;

                $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1' , $cat_cond , $this->assigned_cond ,'task.assigned !=' => ''   , 'OR' => $cond ])->count() ;

                $count_quality = $task_table->find()->select(['task.id'])->where([ 'task.status' => '5' ,$this->completedon_cond, $cat_cond , 'OR' => $cond ])->orWhere([ 'task.status' => '6' ,$this->completedon_cond, 'task.rework' => 0 , $cat_cond , 'OR' => $cond  ])->count() ;

                $count_rework = $task_table->find()->select(['task.id'])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ])->count() ;


                $count_progress = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '2' , $cat_cond  , $this->assigned_cond , 'OR' => $cond ])->count() ;


                $count_completed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3'  , $cat_cond , $this->completedon_cond ,'OR' => $cond  ])->count() ;

                $count_billed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '4'   , $cat_cond  , $this->billed_cond , 'OR' => $cond  ])->count() ;

                if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }
                
                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond ])->count() ;
                $this->set("scheduled_num", $scheduled_num);  
                $this->set("user_view_id", $user_view_id);
                $this->set("user_sel_id", $user_sel_id);
                $this->set("user_cat_id", $user_cat_id);
                
                
                $this->set("completedon", $this->completedon);  
                $this->set("assigned_hours", $this->assigned_hours);  
                $this->set("rework2_hours", $this->rework2_hours);  
                $this->set("unassigned_hours", $this->unassigned_hours);  
                $this->set("pending_hours", $this->pending_hours);  
                $this->set("progress_hours", $this->progress_hours);  
                $this->set("completed_hours", $this->completed_hours);  
                $this->set("quality_hours", $this->quality_hours);  
                
                $this->set("billed_hours", $this->billed_hours); 
                $this->set("billed_hours_day", $this->billed_hours_day); 
                $this->set("count_quality", $count_quality);  
                $this->set("count_rework", $count_rework);  
                $this->set("count_rework2", count($retrieve_rework2));  

                $this->set("spent_hours", $this->spent_hours);  
                $this->set("sidebar_prefix", $user_name);  
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrieve_unassigned);  
                $this->set("count_unassigned", count($retrieve_unassigned2));  
                $this->set("count_pending", $count_pending);  
                $this->set("count_assigned", $count_assigned);  
                $this->set("count_progress", $count_progress);  
                $this->set("count_completed", $count_completed);  
                $this->set("count_billed", $count_billed);  
                $this->viewBuilder()->setLayout('usertask');

            }

      /*      
            public function seostats($userid = null){
                
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $team_table = TableRegistry::get('team');
                
                $retrieve_emp = $users_table->find()->select(['id' , 'name' ])->where([ 'FIND_IN_SET(\'25\',privilages)'  , 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;
                $retrieve_team = $team_table->find()->select(['id' , 'name' ])->where([ 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;

                $retrieve_client = $client_table->find()->select(['id' , 'fname' , 'lname'])->where([ 'status' => '1'])->order(['fname' => 'asc' ])->toArray() ;

                $this->set("emp_details", $retrieve_emp);  
                $this->set("team_details", $retrieve_team);  
                $this->set("cli_details", $retrieve_client);  


                $this->viewBuilder()->setLayout('user');
               
            }
            
            
             public function addpj(){
                if ($this->request->is('ajax') && $this->request->is('post') ){

                    $project_table = TableRegistry::get('project');
                    $activ_table = TableRegistry::get('activity');
                    $project_team = TableRegistry::get('project_team');
                        $project = $project_table->newEntity();
                        $project->name =  $this->request->data('name')  ;
                        $project->cid =  $this->request->data('client')  ;
                        $project->start = strtotime($this->request->data('sdate')) ;
                        if($this->request->data('edate') != ""){
                            $project->end =  strtotime($this->request->data('edate'))  ;
                        }
                        $project->hours =  $this->request->data('hours')  ;
                        $project->type =  $this->request->data('type')  ;
                        $project->rate =  $this->request->data('rate') ;
                        $project->lead =  $this->request->data('lead')  ;
                        $project->nlimit =  $this->request->data('nlimit')  ;
                        $project->nallowed = $this->request->data('nallowed') ;
                        if($project->nallowed != 1){
                            $project->nallowed = 0 ;
                         }
                        $project->ptype =  $this->request->data('ptype')  ;
                        $project->team =  $this->request->data('team')  ;
                        $project->description =  $this->request->data('description')  ;
                        $project->status =  1  ;
                        
                        $project->created = strtotime('now');
                        $project->modified = strtotime('now');
                        if($saved = $project_table->save($project) ){
                            $pjid = $saved->id; 
                            // $members = $this->request->data('members');
                            // foreach($members as $member){
                            //    $project_t = $project_team->newEntity();
                            //    $project_t->pid =  $pjid  ;
                            //    $project_t->uid =  $member  ;
                            //    $project_t->created =  strtotime($this->request->data('edate'))  ;
                            //    $project_team->save($project_t) ;
                            // }
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Project Created"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($pjid)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success' , 'id' => md5($pjid)  ];
    
                            }
                            else{
                        $res = [ 'result' => 'activity not saved'  ];
    
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'project not saved'  ];
                        }
                    
                   

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }
            
            
*/
            public function pending($userid = null){
                
                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];
                
                
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid ;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }
                $user_sel_id = $userid;
               
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;

                if($user_cat_id != "" && $user_cat_id != "all"   ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }

                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                        $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                        $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }
                }

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;

                $count_unassigned = $task_table->find()->select(['task.id' ])->where([ 
                    'task.duedate <' => $timelimit ,
                    $cat_cond,
                    'task.status' => '1',
                    'OR' => 
                               $cond1
                                             
                ])->count() ;
                //  dd($retrieve_unassigned);

                $today = strtotime('today');
                
                
                $retrieve_pending = $this->paginate($task_table->find()->select(['task.id'  , 'task.tittle' , 'task.reftype' , 'task.reference' , 'task.description' ,'task.client', 'task.duedate' , 'task.project', 'task.creator' , 'task.owner' , 'task.created' , 'task.billing', 'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '1' , $cat_cond , 'task.duedate <' => $timelimit ,'task.assigned' =>  '' ,'task.owner !=' =>  '' , 'OR' => $cond2 ])->orWhere([ 'task.status' => '1' , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ]))->toArray() ;
                 
                
                
                
                $retrieve_pending2 = $task_table->find()->select(['task.id'  , 'task.tittle' , 'task.reftype' , 'task.reference' , 'task.description' ,'task.client', 'task.duedate' , 'task.project', 'task.creator' , 'task.owner' , 'task.created' , 'task.billing', 'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '1' , $cat_cond , 'task.duedate <' => $timelimit ,'task.assigned' =>  '' ,'task.owner !=' =>  '' , 'OR' => $cond2 ])->orWhere([ 'task.status' => '1' , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->toArray() ;
                 
                
                
               $retrieve_rework2 = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([  ['OR' => [['task.status' => '1'], ['task.status' => '4']]],'task.task_status_new_rework' => '2', $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ])->toArray() ;                
                
                
       
                
                
                $count_quality = $task_table->find()->select(['task.id'])->where([ 'task.status' => '5', $this->completedon_cond , $cat_cond , 'OR' => $cond ])->orWhere([ 'task.status' => '6' , $this->completedon_cond ,'task.rework' => 0 , $cat_cond , 'OR' => $cond  ])->count() ;
                 $count_rework = $task_table->find()->select(['task.id'])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ])->count() ;

                $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1' , $cat_cond, $this->assigned_cond , 'task.assigned !=' => '' , 'OR' => $cond ])->count() ;

                $count_progress = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '2'  , $cat_cond , $this->assigned_cond , 'OR' => $cond ])->count() ;


                $count_completed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3' , $cat_cond  , $this->completedon_cond , 'OR' => $cond  ])->count() ;
                $count_billed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '4'  , $cat_cond , $this->billed_cond , 'OR' => $cond ])->count() ;
                $this->set("user_view_id", $user_view_id);
                
                if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }
                
                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond ])->count() ;
                $this->set("scheduled_num", $scheduled_num);  
                $this->set("sidebar_prefix", $user_name);
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("user_sel_id", $user_sel_id);
                $this->set("user_cat_id", $user_cat_id);
                $this->set("assigned_hours", $this->assigned_hours);  
                $this->set("completedon", $this->completedon);  
                $this->set("billed_hours_day", $this->billed_hours_day); 
                $this->set("count_quality", $count_quality);  
                $this->set("quality_hours", $this->quality_hours);  
                $this->set("count_rework", $count_rework);  

                                $this->set("rework2_hours", $this->rework2_hours);  
                $this->set("count_rework2", count($retrieve_rework2));  

                $this->set("unassigned_hours", $this->unassigned_hours);  
                $this->set("pending_hours", $this->pending_hours);  
                $this->set("progress_hours", $this->progress_hours);  
                $this->set("completed_hours", $this->completed_hours);  
                $this->set("billed_hours", $this->billed_hours);  
                $this->set("spent_hours", $this->spent_hours);  
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrieve_pending);  
                $this->set("count_pending", count($retrieve_pending2));  
                $this->set("count_unassigned", $count_unassigned);  
                $this->set("count_assigned", $count_assigned);  
                $this->set("count_progress", $count_progress);  
                $this->set("count_completed", $count_completed);  
                $this->set("count_billed", $count_billed);  
                $this->viewBuilder()->setLayout('usertask');

            }

            public function assigned($userid = null){
                
                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid ;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');

                }
                
                $user_sel_id = $userid;
               
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;

                if($user_cat_id != "" && $user_cat_id != "all"  ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }


                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{      
                if($userid == 'alltask' ){
                    $cond = [];
                    $cond1 = [    ['task.owner is' =>  NULL],
                    ['task.owner' =>  ''  ],
                    ['task.assigned' =>  '' ]
                    ];
                    $cond2 = [];
                    $cond3 = [];
                }
                else{
                    $cond = [['task.assigned' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
            }
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;

                $count_unassigned = $task_table->find()->select(['task.id' ])->where([
                     'task.duedate <=' => $timelimit ,
                    $cat_cond ,
                    'task.status' => '1',
                    'OR' => $cond1 
                ])->count() ;
                //  dd($retrieve_unassigned);

                $today = strtotime('today');
                $retrieve_assigned = $this->paginate($task_table->find()->select(['task.id', 'task.task_status_new_rework'  ,'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([['OR' => [['task.status' => '1'], ['task.task_status_new_rework' => '2' ,'task.status' => '1' ]]], $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ]))->toArray() ;

      
                
                
           /*     
                       $retrieve_assigned = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ ['OR' => [['task.status' => '1'], ['task.status' => '6']]], $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ]);

    dd($retrieve_assigned);
     */           
                
/*               
                $query = $articles->find()
    ->where([
        'author_id' => 3,
        'OR' => [['task.status' => '1'], ['task.status' => '6']],
    ]);
*/                    
                
                
                
    $retrieve_assigned2 = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '1' , $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ])->toArray() ;                
                
                
                
                
                
  $retrieve_rework2 = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([  ['OR' => [['task.status' => '1'], ['task.status' => '4']]],'task.task_status_new_rework' => '2', $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ])->toArray() ;                
                
                
                  
                
                
                
                
                
                
                
                
                
                
                
                
                
                 $count_pending = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1' , $cat_cond , 'task.duedate <' => $timelimit ,  'task.assigned' => '' , 'OR' =>  $cond2  ])->orWhere([ 'task.status' => '1' , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->count() ;

                 $count_quality = $task_table->find()->select(['task.id'])->where([ 'task.status' => '5', $this->completedon_cond , $cat_cond , 'OR' => $cond ])->orWhere([ 'task.status' => '6' , $this->completedon_cond , 'task.rework' => 0 , $cat_cond , 'OR' => $cond  ])->count() ;
                 $count_rework = $task_table->find()->select(['task.id'])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ])->count() ;

                $count_progress = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '2' , $cat_cond , $this->assigned_cond , 'OR' => $cond ])->count() ;


                $count_completed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3'  , $cat_cond , $this->completedon_cond ,'OR' => $cond  ])->count() ;
                $count_billed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '4'   ,$cat_cond, $this->billed_cond , 'OR' => $cond  ])->count() ;
                if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }
                
                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond ])->count() ;
                
                $this->set("assigned_hours", $this->assigned_hours);  
                $this->set("completedon", $this->completedon);  
                $this->set("billed_hours_day", $this->billed_hours_day); 
                $this->set("count_quality", $count_quality);  
                $this->set("quality_hours", $this->quality_hours);  
                $this->set("count_rework", $count_rework);  
                
                                $this->set("rework2_hours", $this->rework2_hours);  

                $this->set("unassigned_hours", $this->unassigned_hours);  
                $this->set("pending_hours", $this->pending_hours);  
                $this->set("progress_hours", $this->progress_hours);  
                $this->set("completed_hours", $this->completed_hours);  
                $this->set("billed_hours", $this->billed_hours);  
                $this->set("spent_hours", $this->spent_hours);  
                $this->set("scheduled_num", $scheduled_num);  
                $this->set("sidebar_prefix", $user_name);
                $this->set("user_view_id", $user_view_id);
                $this->set("user_sel_id", $user_sel_id);
                $this->set("user_cat_id", $user_cat_id);
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrieve_assigned);  
                $this->set("count_assigned", count($retrieve_assigned2));  
                $this->set("count_rework2", count($retrieve_rework2));  
                $this->set("count_unassigned", $count_unassigned);  
                $this->set("count_pending", $count_pending);  
                $this->set("count_progress", $count_progress);  
                $this->set("count_completed", $count_completed);  
                $this->set("count_billed", $count_billed);  
                $this->viewBuilder()->setLayout('usertask');

            }

            
            public function ongoing($userid = null){
                
                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];
                
                
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid ;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');

                }

                $user_sel_id = $userid;
                
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;

                if($user_cat_id != "" && $user_cat_id != "all"  ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }


                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [] ;
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                          $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_quality = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' , 'FIND_IN_SET(\'37\' , privilages )' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;

                $count_unassigned = $task_table->find()->select(['task.id' ])->where([ 
                    'task.duedate <=' => $timelimit ,
                    $cat_cond ,
                    'task.status' => '1',
                    'OR' => 
                               $cond1
                           
                 ])->count() ;
                //  dd($retrieve_unassigned);
                $today = strtotime('today');


                $count_pending = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '1' , $cat_cond , 'task.duedate <' => $timelimit ,  'task.assigned' => '' ,'task.owner !=' =>  '' , 'OR' => [ $cond2 ] ])->orWhere([ 'task.status' => '1'  ,$cat_cond, 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->count() ;
                $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1' ,$cat_cond ,  $this->assigned_cond , 'task.assigned !=' => '' ,  'OR' => $cond ])->count() ;

                $retrive_progress = $this->paginate($task_table->find()->select(['task.id' , 'task.task_status_new_rework'  ,'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype','task.client', 'task.reference' , 'task.project', 'task.description' , 'task.rework' , 'task.rework_estimate' , 'task.duedate' , 'task.creator','task.owner' , 'task.created'  , 'task.billing', 'p.name' , 'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '2' , $this->assigned_cond , $cat_cond ,'OR' => $cond ]))->toArray() ;
                  
                
                
                
                $retrive_progress2 = $task_table->find()->select(['task.id' , 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype','task.client', 'task.reference' , 'task.project', 'task.description' , 'task.rework' , 'task.rework_estimate' , 'task.duedate' , 'task.creator','task.owner' , 'task.created'  , 'task.billing', 'p.name' , 'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '2' , $this->assigned_cond , $cat_cond ,'OR' => $cond ])->toArray() ;
                  
                
                
                
                
     $retrieve_rework2 = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([  ['OR' => [['task.status' => '1'], ['task.status' => '4']]],'task.task_status_new_rework' => '2', $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ])->toArray() ;                
                
                
                 
                
                
                $count_quality = $task_table->find()->select(['task.id'])->where([ 'task.status' => '5', $this->completedon_cond , $cat_cond , 'OR' => $cond ])->orWhere([ 'task.status' => '6' ,$this->completedon_cond , 'task.rework' => 0 , $cat_cond , 'OR' => $cond  ])->count() ;

                  $count_rework = $task_table->find()->select(['task.id'])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ])->count() ;

                $count_completed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3' , $cat_cond , $this->completedon_cond , 'OR' => $cond  ])->count() ;
                $count_billed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '4' , $cat_cond  , $this->billed_cond , 'OR' => $cond  ])->count() ;
                if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }

                
                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond ])->count() ;
                $this->set("scheduled_num", $scheduled_num);  
                $this->set("sidebar_prefix", $user_name);
                $this->set("user_view_id", $user_view_id);
                $this->set("user_sel_id", $user_sel_id);
                $this->set("user_cat_id", $user_cat_id);
                $this->set("assigned_hours", $this->assigned_hours);  
                $this->set("completedon", $this->completedon);  
                $this->set("billed_hours_day", $this->billed_hours_day); 
                $this->set("count_quality", $count_quality);  
                $this->set("quality_hours", $this->quality_hours);  
                $this->set("count_rework", $count_rework);  

                $this->set("rework2_hours", $this->rework2_hours);  
$this->set("count_rework2", count($retrieve_rework2)); 
                
                $this->set("unassigned_hours", $this->unassigned_hours);  
                $this->set("pending_hours", $this->pending_hours);  
                $this->set("progress_hours", $this->progress_hours);  
                $this->set("completed_hours", $this->completed_hours);  
                $this->set("billed_hours", $this->billed_hours);  
                $this->set("spent_hours", $this->spent_hours);  
                $this->set("emp_details", $retrieve_emp);  
                $this->set("qa_details", $retrieve_quality);  
                
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrive_progress);  
                $this->set("count_progress", count($retrive_progress2));  
                $this->set("count_unassigned", $count_unassigned);  
                $this->set("count_pending", $count_pending);  
                $this->set("count_assigned", $count_assigned);  
                $this->set("count_completed", $count_completed);  
                $this->set("count_billed", $count_billed);  
                $this->viewBuilder()->setLayout('usertask');

            }


            
            public function completed($userid = null){
                
                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];
                
                
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }

                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;
                
                if($user_cat_id != ""  && $user_cat_id != "all" ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }


                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                          $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->toArray() ;

                $count_unassigned = $task_table->find()->select(['task.id' ])->where([
                    'task.duedate <=' => $timelimit ,
                    'task.status' => '1',
                    $cat_cond ,
                    'OR' => 
                               $cond1
                           
                 ])->count() ;
                //  dd($retrieve_unassigned);

                $today = strtotime('today');
                $count_pending = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '1' , $cat_cond , 'task.duedate <' => $timelimit , 'task.assigned' => '' ,'task.owner !=' =>  '' , 'OR' => $cond2  ])->orWhere([ 'task.status' => '1' , $cat_cond , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->count() ;
                $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1', $cat_cond , $this->assigned_cond , 'task.assigned !=' =>  '' ,  'OR' => $cond ])->count() ;

                $count_progress = $task_table->find()->select(['task.id'])->where([ 'task.status' => '2' ,$cat_cond , $this->assigned_cond , 'OR' => $cond ])->count() ;


                $retrive_completed = $this->paginate($task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' , 'task.project',  'task.comment' ,'task.rework' ,'task.rework_spent' , 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' , 'task.negative' , 'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '3' , $this->completedon_cond , $cat_cond , 'OR' => $cond  ]))->toArray() ;
                  
                
                

                $retrive_completed2 = $task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' , 'task.project',  'task.comment' ,'task.rework' ,'task.rework_spent' , 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' , 'task.negative' , 'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '3' , $this->completedon_cond , $cat_cond , 'OR' => $cond  ])->toArray() ;
                  
                
                
      $retrieve_rework2 = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([  ['OR' => [['task.status' => '1'], ['task.status' => '4']]],'task.task_status_new_rework' => '2', $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ])->toArray() ;                
                
                
                   
                
                
                
                $count_billed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '4' ,  $this->billed_cond ,'OR' => $cond  ])->count() ;
                  $count_quality = $task_table->find()->select(['task.id'])->where([ 'task.status' => '5' ,$this->completedon_cond , $cat_cond , 'OR' => $cond ])->orWhere([ 'task.status' => '6' , 'task.rework' => 0 , $cat_cond ,$this->completedon_cond, 'OR' => $cond  ])->count() ;
                  $count_rework = $task_table->find()->select(['task.id'])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ])->count() ;

                  if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }

                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond ])->count() ;
                $this->set("scheduled_num", $scheduled_num);  
                  $this->set("sidebar_prefix", $user_name);
                  $this->set("user_view_id", $user_view_id);
                  $this->set("user_sel_id", $user_sel_id);
                  $this->set("user_cat_id", $user_cat_id);
                  $this->set("assigned_hours", $this->assigned_hours);  
                  $this->set("completedon", $this->completedon);  
                  $this->set("billed_hours_day", $this->billed_hours_day); 
                  $this->set("count_quality", $count_quality);  
                  $this->set("quality_hours", $this->quality_hours);  
                  $this->set("count_rework", $count_rework);  

                $this->set("rework2_hours", $this->rework2_hours);  
$this->set("count_rework2", count($retrieve_rework2));                   
                
                  $this->set("unassigned_hours", $this->unassigned_hours);  
                  $this->set("pending_hours", $this->pending_hours);  
                  $this->set("progress_hours", $this->progress_hours);  
                  $this->set("completed_hours", $this->completed_hours);  
                  $this->set("billed_hours", $this->billed_hours);  
                  $this->set("spent_hours", $this->spent_hours);  
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrive_completed);  
                $this->set("count_completed", count($retrive_completed2));  
                $this->set("count_unassigned", $count_unassigned);  
                $this->set("count_pending", $count_pending);  
                $this->set("count_assigned", $count_assigned);  
                $this->set("count_progress", $count_progress);  
                $this->set("count_billed", $count_billed);  
                $this->viewBuilder()->setLayout('usertask');

            }


            public function negative($userid = null){
                
                                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];

                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }

                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;
                
                if($user_cat_id != ""  && $user_cat_id != "all" ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }


                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                          $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->toArray() ;

                $count_unassigned = $task_table->find()->select(['task.id' ])->where([
                    'task.duedate <=' => $timelimit ,
                    'task.status' => '1',
                    $cat_cond ,
                    'OR' => 
                               $cond1
                           
                 ])->count() ;
                //  dd($retrieve_unassigned);

                $today = strtotime('today');
                $count_pending = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '1' , $cat_cond , 'task.duedate <' => $timelimit , 'task.assigned' => '' ,'task.owner !=' =>  '' , 'OR' => $cond2  ])->orWhere([ 'task.status' => '1' , $cat_cond , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->count() ;
                $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1', $cat_cond , $this->assigned_cond , 'task.assigned !=' =>  '' ,  'OR' => $cond ])->count() ;

                $count_progress = $task_table->find()->select(['task.id'])->where([ 'task.status' => '2' ,$cat_cond , $this->assigned_cond , 'OR' => $cond ])->count() ;


                $retrive_negative = $this->paginate($task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' , 'task.project',  'task.comment' ,'task.rework' ,'task.rework_spent' , 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' ,  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.negative' => '1' ,  $cat_cond , 'OR' => $cond  ]))->toArray() ;
                  $count_completed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3' , $this->completedon_cond , $cat_cond , 'OR' => $cond  ])->count() ;
                  $count_billed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '4' ,  $this->billed_cond ,'OR' => $cond  ])->count() ;
                  $count_quality = $task_table->find()->select(['task.id'])->where([ 'task.status' => '5' , $cat_cond ,$this->completedon_cond, 'OR' => $cond ])->orWhere([ 'task.status' => '6' , $this->completedon_cond, 'task.rework' => 0 , $cat_cond , 'OR' => $cond  ])->count() ;
                  $count_rework = $task_table->find()->select(['task.id'])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ])->count() ;

                  if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }

                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond ])->count() ;
                $this->set("scheduled_num", $scheduled_num);  
                  $this->set("sidebar_prefix", $user_name);
                  $this->set("user_view_id", $user_view_id);
                  $this->set("user_sel_id", $user_sel_id);
                  $this->set("user_cat_id", $user_cat_id);
                  $this->set("assigned_hours", $this->assigned_hours);  
                  $this->set("completedon", $this->completedon);  
                  $this->set("billed_hours_day", $this->billed_hours_day); 
                  $this->set("count_quality", $count_quality);  
                  $this->set("quality_hours", $this->quality_hours);  
                  $this->set("count_rework", $count_rework);  

                $this->set("rework2_hours", $this->rework2_hours);  
                  
                  $this->set("unassigned_hours", $this->unassigned_hours);  
                  $this->set("pending_hours", $this->pending_hours);  
                  $this->set("progress_hours", $this->progress_hours);  
                  $this->set("completed_hours", $this->completed_hours);  
                  $this->set("billed_hours", $this->billed_hours);  
                  $this->set("spent_hours", $this->spent_hours);  
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrive_negative);  
                $this->set("count_completed", $count_completed);  
                $this->set("count_unassigned", $count_unassigned);  
                $this->set("count_pending", $count_pending);  
                $this->set("count_assigned", $count_assigned);  
                $this->set("count_progress", $count_progress);  
                $this->set("count_billed", $count_billed);  
                $this->viewBuilder()->setLayout('user');


            }


            public function agentquality($userid = null){
                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];
                
                
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }

                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;
                
                if($user_cat_id != ""  && $user_cat_id != "all" ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }


                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                          $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->toArray() ;

                $count_unassigned = $task_table->find()->select(['task.id' ])->where([
                    'task.duedate <=' => $timelimit ,
                    'task.status' => '1',
                    $cat_cond ,
                    'OR' => 
                               $cond1
                           
                 ])->count() ;
                //  dd($retrieve_unassigned);

                $today = strtotime('today');
                $count_pending = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '1' , $cat_cond , 'task.duedate <' => 
                
                $timelimit , 'task.assigned' => '' ,'task.owner !=' =>  '' , 'OR' => $cond2  ])->orWhere([ 'task.status' => '1' , $cat_cond , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->count() ;
                
                $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1', $cat_cond , $this->assigned_cond , 'task.assigned !=' =>  '' ,  'OR' => $cond ])->count() ;
                

                $count_progress = $task_table->find()->select(['task.id'])->where([ 'task.status' => '2' ,$cat_cond , $this->assigned_cond , 'OR' => $cond ])->count() ;
                $count_completed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3' , $this->completedon_cond , $cat_cond , 'OR' => $cond   ])->count() ;


                $retrive_quality = $this->paginate($task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' , 'task.project','task.qagent', 'q.name',  'task.comment' , 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' ,  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '5' , $cat_cond , 'OR' => $cond , $this->completedon_cond  ])->orWhere([ 'task.status' => '6'  , 'task.rework' => 0 , $cat_cond, $this->completedon_cond , 'OR' => $cond  ]))->toArray() ;

                
                
                
                $retrive_quality2 = $task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' , 'task.project','task.qagent', 'q.name',  'task.comment' , 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' ,  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '5' , $cat_cond , 'OR' => $cond , $this->completedon_cond  ])->orWhere([ 'task.status' => '6'  , 'task.rework' => 0 , $cat_cond, $this->completedon_cond , 'OR' => $cond  ])->toArray() ;
                
                
    $retrieve_rework2 = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([  ['OR' => [['task.status' => '1'], ['task.status' => '4']]],'task.task_status_new_rework' => '2', $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ])->toArray() ;                
                
                
                  
                
                
                $count_billed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '4' ,  $this->billed_cond ,'OR' => $cond  ])->count() ;
                  $count_rework = $task_table->find()->select(['task.id'])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ])->count() ;

                  if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }

                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond ])->count() ;
                $this->set("scheduled_num", $scheduled_num);  
                  $this->set("sidebar_prefix", $user_name);
                  $this->set("user_view_id", $user_view_id);
                  $this->set("user_sel_id", $user_sel_id);
                  $this->set("user_cat_id", $user_cat_id);
                  $this->set("assigned_hours", $this->assigned_hours);  
                  $this->set("completedon", $this->completedon);  
                  $this->set("billed_hours_day", $this->billed_hours_day); 
                  $this->set("quality_hours", $this->quality_hours);  
                  $this->set("count_rework", $count_rework);  

                $this->set("rework2_hours", $this->rework2_hours);  
$this->set("count_rework2", count($retrieve_rework2));                    
                
                  $this->set("unassigned_hours", $this->unassigned_hours);  
                  $this->set("pending_hours", $this->pending_hours);  
                  $this->set("progress_hours", $this->progress_hours);  
                  $this->set("completed_hours", $this->completed_hours);  
                  $this->set("billed_hours", $this->billed_hours);  
                  $this->set("spent_hours", $this->spent_hours);  
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrive_quality);  
                $this->set("count_quality", count($retrive_quality2));  
                $this->set("count_completed", $count_completed);  
                $this->set("count_unassigned", $count_unassigned);  
                $this->set("count_pending", $count_pending);  
                $this->set("count_assigned", $count_assigned);  
                $this->set("count_progress", $count_progress);  
                $this->set("count_billed", $count_billed);  
                $this->viewBuilder()->setLayout('usertask');

            }


            

            public function rework($userid = null){
                
                
                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];
                
                
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }

                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;
                
                if($user_cat_id != ""  && $user_cat_id != "all" ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }


                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                          $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->toArray() ;

                $count_unassigned = $task_table->find()->select(['task.id' ])->where([
                    'task.duedate <=' => $timelimit ,
                    'task.status' => '1',
                    $cat_cond ,
                    'OR' => 
                               $cond1
                           
                 ])->count() ;
                //  dd($retrieve_unassigned);

                $today = strtotime('today');
                $count_pending = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '1' , $cat_cond , 'task.duedate <' => $timelimit , 'task.assigned' => '' ,'task.owner !=' =>  '' , 'OR' => $cond2  ])->orWhere([ 'task.status' => '1' , $cat_cond , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->count() ;
                $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1', $cat_cond , $this->assigned_cond , 'task.assigned !=' =>  '' ,  'OR' => $cond ])->count() ;

                $count_progress = $task_table->find()->select(['task.id'])->where([ 'task.status' => '2' ,$cat_cond , $this->assigned_cond , 'OR' => $cond ])->count() ;
                $count_completed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3' , $this->completedon_cond , $cat_cond , 'OR' => $cond   ])->count() ;

                $count_quality = $task_table->find()->select(['task.id'])->where([ 'task.status' => '5' ,$this->completedon_cond, $cat_cond , 'OR' => $cond ])->orWhere([ 'task.status' => '6' ,$this->completedon_cond, 'task.rework' => 0 , $cat_cond , 'OR' => $cond  ])->count() ;
/*
                $retrive_rework = $this->paginate($task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' , 'task.project','task.qagent', 'q.name', 'task.comment' , 'task.qrework_comment' , 'task.arework_comment', 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' ,  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ]))->toArray() ;
                  
                
          
                $retrive_rework2 = $task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' , 'task.project','task.qagent', 'q.name', 'task.comment' , 'task.qrework_comment' , 'task.arework_comment', 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' ,  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ])->toArray() ;
*/                  
                
                
                $retrive_rework = $this->paginate($task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' , 'task.project','task.qagent', 'q.name', 'task.comment' , 'task.qrework_comment' , 'task.arework_comment', 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' ,  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ ['OR' => [['task.status' => '1'], ['task.status' => '4']]],'task.task_status_new_rework' => '2', $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ]))->toArray() ;                
                  
                
          
                $retrive_rework2 = $task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' , 'task.project','task.qagent', 'q.name', 'task.comment' , 'task.qrework_comment' , 'task.arework_comment', 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' ,  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ ['OR' => [['task.status' => '1'], ['task.status' => '4']]],'task.task_status_new_rework' => '2', $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ])->toArray() ;                
                  
                
                
                /*
                $retrive_rework3 = $task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' , 'task.project','task.qagent', 'q.name', 'task.comment' , 'task.qrework_comment' , 'task.arework_comment', 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' ,  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ])->toArray() ;
                  
               */
                
                
                
            $retrive_rework3 = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([  ['OR' => [['task.status' => '1'], ['task.status' => '4']]],'task.task_status_new_rework' => '2', $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ])->toArray() ;                
                
                
            
                
                
                
                
                
                
                
                
                
                
                
                
                $count_billed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '4' ,  $this->billed_cond ,'OR' => $cond  ])->count() ;

                  if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }

                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond ])->count() ;
                $this->set("scheduled_num", $scheduled_num);  
                  $this->set("sidebar_prefix", $user_name);
                  $this->set("user_view_id", $user_view_id);
                  $this->set("user_sel_id", $user_sel_id);
                  $this->set("user_cat_id", $user_cat_id);
                  $this->set("assigned_hours", $this->assigned_hours);  
                  $this->set("completedon", $this->completedon);  
                  $this->set("billed_hours_day", $this->billed_hours_day); 
                  $this->set("quality_hours", $this->quality_hours);  

                $this->set("rework2_hours", $this->rework2_hours);  
                $this->set("count_rework2", count($retrive_rework3));  
                  
                  $this->set("unassigned_hours", $this->unassigned_hours);  
                  $this->set("pending_hours", $this->pending_hours);  
                  $this->set("progress_hours", $this->progress_hours);  
                  $this->set("completed_hours", $this->completed_hours);  
                  $this->set("billed_hours", $this->billed_hours);  
                  $this->set("spent_hours", $this->spent_hours);  
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrive_rework);  
                $this->set("count_quality", $count_quality);  
                $this->set("count_rework", count($retrive_rework2));  
                $this->set("count_completed", $count_completed);  
                $this->set("count_unassigned", $count_unassigned);  
                $this->set("count_pending", $count_pending);  
                $this->set("count_assigned", $count_assigned);  
                $this->set("count_progress", $count_progress);  
                $this->set("count_billed", $count_billed);  
                $this->viewBuilder()->setLayout('usertask');

            }

            
            
            public function billed($userid = null){
                
                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];
                
                
                
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }
                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;

                if($user_cat_id != "" && $user_cat_id != "all"  ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }


                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];  
                          $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');

                 $task_table = TableRegistry::get('task');
              
                 $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $count_unassigned = $task_table->find()->select(['task.id' ])->where([ 
                    'task.duedate <=' => $timelimit ,
                    'task.status' => '1',
                    $cat_cond,
                    'OR' => 
                               $cond1
                           
                ])->count() ;
            
                $today = strtotime('today');
                $count_pending = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '1',$cat_cond , 'task.duedate <' => $timelimit , 'task.assigned' => '' , 'task.owner !=' =>  '' , 'OR' => $cond2 ])->orWhere([ 'task.status' => '1' , $cat_cond , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->count() ;
                $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1' ,$cat_cond , $this->assigned_cond , 'task.assigned !=' =>  '' , 'OR' => $cond])->count() ;

                $count_progress = $task_table->find()->select(['task.id'])->where([ 'task.status' => '2' , $cat_cond , 'task.duedate <' => $timelimit ,'OR' => $cond ])->count() ;
                $count_quality = $task_table->find()->select(['task.id'])->where([ 'task.status' => '5' , $this->completedon_cond, $cat_cond , 'OR' => $cond ])->orWhere([ 'task.status' => '6' ,$this->completedon_cond, 'task.rework' => 0 , $cat_cond , 'OR' => $cond  ])->count() ;
                $count_rework = $task_table->find()->select(['task.id'])->where([ 'task.status' => '6' , 'task.rework' => 1 , $cat_cond , 'OR' => $cond  ])->count() ;
                $retrive_billed = $this->paginate($task_table->find()->select(['task.id' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.description' , 'task.duedate' , 'task.client' , 'task.creator', 'task.project', 'task.created' , 'task.completedtime'  ,'task.owner' ,'task.billedtime' ,'task.assignedtime' ,'task.billed','task.spent' , 'task.billing' , 'p.name' , 'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '4' , $cat_cond , $this->billed_cond , 'OR' => $cond  ]))->toArray() ;
                
                 
                
                
                $retrive_billed2 = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.description' , 'task.duedate' , 'task.client' , 'task.creator', 'task.project', 'task.created' , 'task.completedtime'  ,'task.owner' ,'task.billedtime' ,'task.assignedtime' ,'task.billed','task.spent' , 'task.billing' , 'p.name' , 'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '4' , $cat_cond , $this->billed_cond , 'OR' => $cond  ])->toArray() ;
                
                 
                
   $retrieve_rework2 = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' , 'task.project', 'task.description' ,'task.rework' , 'task.rework_estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([  ['OR' => [['task.status' => '1'], ['task.status' => '4']]],'task.task_status_new_rework' => '2', $cat_cond , $this->assigned_cond ,'task.assigned !=' =>  '' ,  'OR' => $cond ])->toArray() ;                
                
                
                     
                
                
                
                $count_completed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3', $this->completedon_cond , $cat_cond   , 'OR' => $cond  ])->count() ;
                  if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }

                
                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond ])->count() ;
                $this->set("scheduled_num", $scheduled_num);  
                  $this->set("sidebar_prefix", $user_name);
                  $this->set("user_view_id", $user_view_id);
                  $this->set("user_sel_id", $user_sel_id);
                  $this->set("user_cat_id", $user_cat_id);
                  $this->set("assigned_hours", $this->assigned_hours);  
                  $this->set("completedon", $this->completedon);  
                  $this->set("billed_hours_day", $this->billed_hours_day); 
                  $this->set("count_quality", $count_quality);  
                  $this->set("quality_hours", $this->quality_hours);  
                  
                $this->set("rework2_hours", $this->rework2_hours);  
                $this->set("count_rework2", count($retrieve_rework2));  
                  
                
                  $this->set("unassigned_hours", $this->unassigned_hours);  
                  $this->set("pending_hours", $this->pending_hours);  
                  $this->set("progress_hours", $this->progress_hours);  
                  $this->set("completed_hours", $this->completed_hours);  
                  $this->set("billed_hours", $this->billed_hours);  
                  $this->set("spent_hours", $this->spent_hours);  
                        $count_billed = count($retrive_billed2);
                 $this->set("retrieve_task", $retrive_billed2);  
                 $this->set("cli_details", $retrieve_client);  

                $this->set("count_billed", $count_billed);  
                $this->set("count_rework", $count_rework);  
                $this->set("count_unassigned", $count_unassigned);  
                $this->set("count_pending", $count_pending);  
                $this->set("count_assigned", $count_assigned);  
                $this->set("count_progress", $count_progress);
                $this->set("count_completed", $count_completed);  
                $this->viewBuilder()->setLayout('usertask');

            }

            public function employee($emp_id) {

                $users_table = TableRegistry::get('users');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_user = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' , 'md5(id)' => $emp_id ])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->toArray() ;
                $retrieve_pending = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reference' , 'task.description' , 'task.duedate' , 'u.name' , 'c.name'  ])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(u.id) =  task.assigned' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '1'   ,'OR' => [['task.assigned' =>  $emp_id], ['task.owner' =>  $emp_id]] ])->toArray() ;
                $retrieve_progress = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reference' , 'task.description' , 'task.duedate' , 'u.name' , 'c.name' ])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(u.id) =  task.assigned' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '2'  , 'OR' => [['task.assigned' =>  $emp_id], ['task.owner' =>  $emp_id]] ])->toArray() ;
                $retrieve_completed = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reference' , 'task.description' , 'task.duedate' , 'u.name' , 'c.name'])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(u.id) =  task.assigned' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '3' ,'OR' => [['task.assigned' =>  $emp_id], ['task.owner' =>  $emp_id]] ])->toArray() ;
                 $sidebar_prefix = $retrieve_user[0]['name']."'s " ;
                $this->set("sidebar_prefix", $sidebar_prefix);  
                $this->set("cat_details", $retrieve_cat);  
                $this->set("pending_details", $retrieve_pending);  
                $this->set("progress_details", $retrieve_progress);  
                $this->set("completed_details", $retrieve_completed);  
                $this->viewBuilder()->setLayout('user');

            }


            public function addpersonal(){
                if ($this->request->is('ajax') && $this->request->is('post') ){

                    $task_table = TableRegistry::get('task');
                    $activ_table = TableRegistry::get('activity');
                    $comment_table = TableRegistry::get('comments');
                        $task = $task_table->newEntity();
                        $task->tittle =  $this->request->data('title')  ;
                        $task->description = $this->request->data('description')  ;
                        $task->creator =  $this->Cookie->read('id') ;
                        if($this->request->data('owner') != ""){
                            $task->owner =  $this->request->data('owner') ;
                        }
                      

                        if($this->request->data('estimate') != ""){
                            $task->estimate =  $this->request->data('estimate') ;

                        }
                        
                        if($this->request->data('task_status_new_rework') != ""){
                            $task->task_status_new_rework =  $this->request->data('task_status_new_rework') ;
                        }
                        
                       // echo $task->task_status_new_rework;die("aaa");
                        
                        if($this->request->data('agent') != ""){
                            $task->assigned =  $this->request->data('agent') ;
                            $task->assignedtime = strtotime('now');
                        }
                        $task->category =  $this->request->data('category') ;
                        $task->reference =  $this->request->data('reference') ;
                        
                        
                       $task->status =  1 ; 
                        
                        
                        if($this->request->data('comment') != "" ){                                
                            $task->negative =  1 ;
                        }
                        $task->reftype =  $this->request->data('reftype') ;
                        if($this->request->data('reftype') == "yes"){
                            $task->project =  $this->request->data('project') ;
                        }
                        $task->client = $this->request->data('client')  ;
                        if($this->request->data('duedate') != ""){
                            $task->duedate =  strtotime($this->request->data('duedate')) ;

                        }
                        $task->created =  strtotime('now') ;
                        $task->modified =  strtotime('now') ;
                        if($saved = $task_table->save($task) ){
                            if($this->request->data('comment') != "" ){                                
                            $comment = $comment_table->newEntity();
                            $comment->comment =  $this->request->data('comment')  ;
                            $comment->task_id = md5($saved->id)   ;
                            $comment->user = $this->Cookie->read('id')   ;
                            $comment->created = strtotime('now');
                            $comment_table->save($comment);
                            }

                            $activity = $activ_table->newEntity();
                            $activity->action =  "Task Created"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($saved->id)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success'  ];
    
                            }
                            else{
                            $res = [ 'result' => 'activity not saved'  ];
    
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'personal task not saved'  ];
                        }
 

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            
            public function scheduled($userid = null){
                
                
                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];
                
                
                $user_view_id = $userid ;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }
                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;
                if($user_cat_id != "" && $user_cat_id != "all"  ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }
                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];  
                          $cond3 = [ ['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;

           
                $timelimit =  strtotime('tomorrow');
                
                if($user_sel_id == 'all'  || $user_sel_id == 'alltask' ){
                    $user_name = "All ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;

                }
                $retrieve_scheduled = $this->paginate($task_table->find()->select(['task.id'  , 'task.tittle' , 'task.reftype' , 'task.reference' , 'task.description' , 'task.estimate', 'task.duedate' , 'task.creator' , 'task.project' , 'task.owner' , 'task.assigned' , 'task.created' , 'task.billing', 'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.duedate >=' => $timelimit ,'task.status' => 1  , $cat_cond ,'OR' => $cond3 ]))->toArray() ;


                 $this->set("billed_hours", $this->billed_hours);  
                 $this->set("spent_hours", $this->spent_hours);  
                $this->set("user_cat_id", $user_cat_id);
                $this->set("sidebar_prefix", $user_name);
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("user_sel_id", $user_sel_id);
                $this->set("retrieve_task", $retrieve_scheduled);
                $this->set("cli_details", $retrieve_client);
                $this->viewBuilder()->setLayout('user');

            }


            public function bugs($userid = null){
                
                                 $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '10'
  ];

                
                $user_view_id = $userid ;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }
                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;
                if($user_cat_id != "" && $user_cat_id != "all"  ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }
                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid] , ['task.qagent' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];  
                          $cond3 = [ ['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;

           
                $timelimit =  strtotime('tomorrow');
                
                if($user_sel_id == 'all'  || $user_sel_id == 'alltask' ){
                    $user_name = "All ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;

                }
                $retrieve_scheduled = $this->paginate($task_table->find()->select(['task.id'  , 'task.tittle' , 'task.reftype' , 'task.reference' , 'task.description' , 'task.estimate', 'task.duedate' , 'task.creator' , 'task.project' , 'task.qrework_comment' , 'task.arework_comment', 'task.bug' , 'task.owner' , 'task.assigned' , 'task.created' , 'task.billing', 'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([  'task.status !=' => 0 , 'task.rework' => 1   , $cat_cond ,'OR' => $cond3 ]))->toArray() ;


                 $this->set("billed_hours", $this->billed_hours);  
                 $this->set("spent_hours", $this->spent_hours);  
                $this->set("user_cat_id", $user_cat_id);
                $this->set("sidebar_prefix", $user_name);
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("user_sel_id", $user_sel_id);
                $this->set("retrieve_task", $retrieve_scheduled);
                $this->set("cli_details", $retrieve_client);
                $this->viewBuilder()->setLayout('user');

            }
                       
            public function  completetaskqualityrework(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    $spent = $this->request->data('spent'); 
                    $comment = $this->request->data('comment'); 
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'rework_spent' => $spent , 'arework_comment' => $comment ,'status' => 5 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'task could not be assigned'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }
            
            public function  assignestimated(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    $estimate = $this->request->data('estimate'); 
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'rework_estimate' => $estimate ,'status' => 1 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'task could not be assigned'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }
            
            public function  approvebillingtask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    $comment = $this->request->data('comment'); 
                    $spent = $this->request->data('spent'); 
                    if(!empty($id) && !empty($comment) ){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'comment' => $comment ,'billed' => $spent ,'status' => 4,'billedon' => $modified, 'billing' => 1 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'task could not be sent for billing'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            } 

            public function  bulkapprovebillingtask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    if(!empty($id)  ){
                                $modified = strtotime('now') ;
                                foreach($id as $s_id){
                                $update_task = $task_table->query()->update()->set([ 'status' => 4, 'billing' => 1 , 'billedon' => $modified,'modified' => $modified ])->where(['id' =>  $s_id ])->execute(); 
                                }
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'task could not be sent for billing'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }
            
            public function  bulksendbillingtask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    $bill = $this->request->data('billtime');
                    if(!empty($id)){
                            $modified = strtotime('now') ;
                            $i = 0;
                            foreach($id as $s_id){
                                $billed = $bill[$i];
                                $update_task = $task_table->query()->update()->set([ 'billing' => 1 , 'billed' => $billed , 'modified' => $modified ])->where(['id' =>  $s_id ])->execute(); 
                                $i++ ;
                            }
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'task could not be sent for billing'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }


            public function  sendbillingtask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    $comment = $this->request->data('comment'); 
                    $spent = $this->request->data('spent'); 
                    
                    if(!empty($id) && !empty($comment)  ){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'comment' => $comment , 'billed' => $spent , 'billing' => 1 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'task could not be sent for billing'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }



            public function  assigntask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $comment_table = TableRegistry::get('comments');

                    $id = $this->request->data('val'); 
                    $agent = $this->request->data('assigned'); 
                    $estimate = $this->request->data('estimate'); 
                    if(!empty($id) && !empty($estimate) && !empty($agent) ){
                                $modified = strtotime('now') ;
                                $negative = ["negative" => 0 ];
                                if($this->request->data('comment') != "" ){                                
                                        $negative = ["negative" => 1 ];
                                }

                                $update_task = $task_table->query()->update()->set([ 'assigned' => $agent, $negative ,'assignedtime' => $modified , 'estimate' => $estimate , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                if($this->request->data('comment') != "" ){                                
                                    $comment = $comment_table->newEntity();
                                    $comment->comment =  $this->request->data('comment')  ;
                                    $comment->task_id = $id   ;
                                    $comment->user = $this->Cookie->read('id')   ;
                                    $comment->created = strtotime('now');
                                    $comment_table->save($comment);
                                    }
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'personal task status not assigned'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            public function editpersonal(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $comment_table = TableRegistry::get('comments');
                    $assignedtime = null ;
                    $id = $this->request->data('vista'); 
                    $title = $this->request->data('title'); 
                    $reference = $this->request->data('reference'); 
                    $category = $this->request->data('category'); 
                    $description = $this->request->data('description'); 
                    $assigned = $this->request->data('agent'); 
                    if($this->request->data('agent')!=""){
                        $assignedtime = strtotime('now');
                    }
                    $reftype  = $this->request->data('reftype');
                    $client = $this->request->data('client');
                    $project = $this->request->data('project');
                    $estimate = $this->request->data('estimate');
                    $owner = $this->request->data('owner');
                    if($this->request->data('duedate') != ""){
                        $duedate = strtotime($this->request->data('duedate'));
                    }
                    else{
                    $duedate = $this->request->data('duedate');
                    }

                    if(!empty($id) ){
                        $modified = strtotime('now') ;
                        $update_task = $task_table->query()->update()->set([ 'tittle' => $title , 'estimate' => $estimate , 'reftype' => $reftype, 'client' => $client ,'reference' => $reference , 'owner' => $owner , 'project' => $project , 'category' => $category ,'assignedtime' => $assignedtime , 'description' => $description ,'assigned' => $assigned , 'duedate' => $duedate  , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                        if($this->request->data('comment') != "" ){                                
                            $comment = $comment_table->newEntity();
                            $comment->comment =  $this->request->data('comment')  ;
                            $comment->task_id = $id   ;
                            $comment->user = $this->Cookie->read('id')   ;
                            $comment->created = strtotime('now');
                            $comment_table->save($comment);
                            }
                        $res = ['result' => 'success'];   
                    }
                    else{
                        $res = ['result' => 'personal task not found'];
                    }
                }
                    else{
                        $res = [ 'result' => 'invalid operation'  ];
        
                    }
                return $this->json($res);


            }

            public function getprojects(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $projects_table = TableRegistry::get('project');

                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                        $task_table = TableRegistry::get('task');
                        $billing_table = TableRegistry::get('billing');
                        $query = $billing_table->find();
                        // $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['md5(cid)' => $id ])->first() ;
                        // $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'client' => $id ])->first();
                        // $hours  = $retrieve_billing->hours - $count_billed->billed; 
                     
                        $retrieve_projects = $projects_table->find()->select(['id' , 'name'  ])->where(['cid' => $id , 'status' => 1 ])->toArray() ;
                        $project_list = null ;
                        if(!empty($retrieve_projects[0])){
                            $project_list = '<option value="">Select Project</option>'; 
                            foreach($retrieve_projects as $projects){
                                $project_list .= '<option value="'.md5($projects['id']).'">'.$projects['name'].'</option>'; 

                            }
                        }   
                        $res = [ 'result' => 'success' , 'data' => $project_list   ];

                    }
                    else{
                        $res = [ 'result' => 'project list not found'  ];
                    }
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];
                }
    
                return $this->json($res);
            }

            public function getprojectshours(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $projects_table = TableRegistry::get('project');

                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                        $task_table = TableRegistry::get('task');
                        $billing_table = TableRegistry::get('billing');
                        $client_table = TableRegistry::get('client');

                        $query = $billing_table->find();

                        $get_negative = $projects_table->find()->select(['nlimit' , 'ptype' , 'nallowed', 'cid'])->where([ 'md5(id)' => $id  ])->first();
                        $getclient = $client_table->find()->select(['type' ])->where([ 'md5(id)' => $get_negative->cid  ])->first();

                        if($getclient->type == 1){
                        $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['md5(pid)' => $id , 'deleted' => 0])->first() ;
                        $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'project' => $id , 'status' => 4 ])->first();
                        $hours  = ($retrieve_billing->hours)  - $count_billed->billed; 
                        if($get_negative->nallowed == "1" ){
                            $hours  = ($retrieve_billing->hours  + $get_negative->nlimit)  - $count_billed->billed; 
                        }
                        elseif($get_negative->nallowed == "0"  ){
                            if($get_negative->ptype == 2){
                                $hours = $hours + $get_negative->nlimit ; 
                            }
                            if($hours < 0 ){
                                $hours = "no" ;                                
                            }
                        }
                        }
                        elseif($getclient->type == 2){
                            $query = $billing_table->find();        
                            $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['md5(cid)' =>  $get_negative->cid , 'deleted' => 0 ])->first() ;
                            $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'client' =>  $get_negative->cid , 'status' => 4 ])->first();
                              $hours  = $retrieve_billing->hours  - $count_billed->billed; 
                        }
                        $res = [ 'result' => 'success'  , 'hours' => $hours ];
                    }
                    else{
                        $res = [ 'result' => 'project list not found'  ];
                    }
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];
                }
    
                return $this->json($res);
            }

            public function gettask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                        $retrieve_task = $task_table->find()->where(['md5(id)' => $id  ])->first() ; 
                        if($retrieve_task->duedate !=""){
                            $retrieve_task->duedate = date('m/d/Y' , $retrieve_task->duedate );
                        }

                        if($retrieve_task->frequency == 1){
                            $stringtime = strtotime($retrieve_task->frequencyvalue);
                            if($stringtime < strtotime('now')){
                                $retrieve_task->frequencyvalue = date('m/d/Y');
                            }
                        }
                        $billing_table = TableRegistry::get('billing');
                        $client_table = TableRegistry::get('client');
                        $project_table = TableRegistry::get('project');
                        $query = $billing_table->find();
                        $getclient = $client_table->find()->select(['type' ])->where([ 'md5(id)' => $retrieve_task->client ])->first();
                        $hours = 0 ;
                        if($getclient){
                        if($getclient->type == 1 ){
                            if($retrieve_task->project == ""){
                                $hours = 0 ;
                            }
                            else{
                                $getproject = $project_table->find()->select(['nlimit' , 'nallowed' ])->where([ 'md5(id)' => $retrieve_task->project ])->first();


                                    $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['md5(pid)' => $retrieve_task->project])->first() ;
                                    $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'project' => $retrieve_task->project ])->first();
            
                                    $hours  = $retrieve_billing->hours  - $count_billed->billed; 
                                if( $hours < 0 && $getproject->nallowed == 1){
                                    $hours  = ($retrieve_billing->hours + $getproject->nlimit) - $count_billed->billed; 
                                }
                                // else{
                                //     $hours = "no" ;
                                // }
    
                            }

    
                        }
                        elseif($getclient->type == 2 ){
                            $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['md5(cid)' => $retrieve_task->client])->first() ;
                            $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'client' => $retrieve_task->client ])->first();
    
                            $hours  = $retrieve_billing->hours - $count_billed->billed; 
                        }
                    }
                     
                        $res = [ 'result' => 'success' , 'task' => $retrieve_task , 'hours' => $hours ];                            

                    }
                    else{
                        $res = [ 'result' => 'personal task comment not found'  ];
                    }

                }
            
            else{
                $res = [ 'result' => 'invalid operation'  ];

            }


            return $this->json($res);
            }

            public function getcommenttask(){

                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $comment_table = TableRegistry::get('comments');
                    $id = $this->request->data('val'); 
                    $user_id = $this->Cookie->read('id') ;
                    if(!empty($id)){
                        $retrieve_comment = $comment_table->find()->select([ 'comments.comment' , 'u.name' , 'comments.user' , 'comments.created' ])->join([
                            'u' => [
                                'table' => 'users',
                                'type' => 'LEFT',
                                'conditions' =>  'md5(u.id) =  comments.user' 
                            ]
                        ])->where(['comments.task_id' => $id  ])->toArray() ; 

                        if(empty($retrieve_comment[0])){
                            $text = "<center>No comments found</center>" ;
                        }
                        else{
                            $text = '<div class="chat-app">
                            <div class="chat">
                            <div class="chat-history clearfix">
                                                            <ul class="m-b-0">' ;
                            foreach($retrieve_comment as $comment){
                                if($comment['user'] == $user_id ){
                                    $text .=  '<li class="clearfix">
                                    <div class="message-data text-right">
                                        <span class="message-data-time">'.$comment['u']['name'].' at '.date('h:i a, js F',$comment['created']).'</span>
                                    </div>
                                    <div class="message other-message float-right">'.$comment['comment'].'</div>
                                </li>' ;

                                }
                                else{
                                    $text .=  '<li class="clearfix">
                                    <div class="message-data text-left">
                                        <span class="message-data-time">'.$comment['u']['name'].' at '.date('h:i a, jS M',$comment['created']).'</span>
                                    </div>
                                    <div class="message my-message float-left">'.$comment['comment'].'</div>
                                </li>' ;
                                }
                            }
                            $text .= ' </ul>
                            </div>
                            </div>
                            </div>';
                        }

                        $res = [ 'result' => 'success' , 'comment' => $text ];                            

                    }
                    else{
                        $res = [ 'result' => 'personal task comment not found'  ];
                    }

                }
            
            else{
                $res = [ 'result' => 'invalid operation'  ];

            }


            return $this->json($res);

            }


            public function  completetask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $activ_table = TableRegistry::get('activity');
                    $comment_table = TableRegistry::get('comments');
                    $id = $this->request->data('val'); 
                    $comment = $this->request->data('comment'); 
                    $spent = $this->request->data('spent'); 
                    $ongoing = $this->request->data('repeattask'); 
                    $frequency = $this->request->data('repeattype'); 
                    $frequencyvalue = $this->request->data('repeatval'); 
                    $qualitytask = $this->request->data('qualitytask'); 
                    $durationrepeated = $this->request->data('durationrepeated');
                    //echo $durationrepeated; die("aaa");
                    
                    if(!empty($id) && !empty($spent) && !empty($comment)  ){
                                $modified = strtotime('now') ;
                                 $status = 3 ;
                                 
                                 if($qualitytask == "true"){
                                    $status = 5 ;
                                 }
                                $update_task = $task_table->query()->update()->set([ 'status' => $status , 'spent' => $spent ,'comment' => $comment , 'modified' => $modified ,'billedtime' => $modified , 'completedtime' => $modified ])->where(['md5(id)' =>  $id ])->execute();
                                $retrive_task =  $task_table->find()->where(['md5(id)' => $id  ])->first() ; 
                                
                                if($ongoing == "true" && $retrive_task->category == 4 ){
                                    $assignedtime = strtotime('now');
                                    if($frequency == "1"){
                                        $duedate = strtotime($frequencyvalue);
                                    }
                                    else if($frequency == "2"){
                                        $duedate = strtotime('tomorrow');
                                    }
                                    else if($frequency == "3"){
                                        $get_day = date('N');
                                        if($get_day < $frequencyvalue){
                                            $diff = $frequencyvalue - $get_day ;
                                            $duedate = strtotime('+'.$diff.' day');
                                        }
                                        elseif($get_day >= $frequencyvalue){
                                            $diff = (7 - $get_day) + $frequencyvalue   ;
                                            $duedate = strtotime('+'.$diff.' day');
                                            
                                        }
                                        
                                    }
                                    else if($frequency == "4"){
                                        $get_day = date('j');
                                        if($get_day < $frequencyvalue){
                                            $diff = $frequencyvalue - $get_day ;
                                            $duedate = strtotime('+'.$diff.'d');
                                        }
                                        elseif($get_day >= $frequencyvalue){
                                            $monthDay = date('t');
                                            $diff = ($monthDay - $get_day) + $frequencyvalue   ;
                                            $duedate = strtotime('+'.$diff.'d');
                                        }
                                    }

                                    /* Update status 4 annd billeeeedd 0 */
                                    
                                    //echo $retrive_task->task_status_new_rework; die("aaaa");
                                    
                                    
                                    $task = $task_table->newEntity();
                                                                        if($retrive_task->task_status_new_rework == 2)
                                    {
                                         $task->status =  4 ;
                                         $task->billed =  '0' ;

                                    }
                                 else
                                 {
                                       $task->status =  1 ;

                                 }

                                    $task->tittle =  $retrive_task->tittle  ;
                                    $task->description = $retrive_task->description  ;
                                    $task->creator =  $retrive_task->creator ;
                                    $task->owner =  $retrive_task->owner ;
                                    $task->assigned =  $retrive_task->assigned ;
                                    $task->assignedtime =  $assignedtime ;
                                    $task->category =  $retrive_task->category ;
                                    $task->reference =  $retrive_task->reference ;
                                    //$task->status =  1 ;
                                    $task->reftype =  $retrive_task->reftype ;
                                    $task->client = $retrive_task->client  ;
                                    $task->project = $retrive_task->project  ;
                                    $task->parent = $retrive_task->id  ;
                                    $task->duedate =  $duedate ;
                                    //$task->estimate =  $retrive_task->estimate ;
                                    $task->estimate =  $durationrepeated ;
                                    $task->negative =  $retrive_task->negative ;
                                    $task->task_status_new_rework =  $retrive_task->task_status_new_rework ;
                                    
                                    $task->ongoing =  $ongoing ;
                                    $task->frequency =  $frequency ;
                                    $task->frequencyvalue =  $frequencyvalue ;
                                    $task->created =  strtotime('now') ;
                                    $task->modified =  strtotime('now') ;
                                    if($savedtask = $task_table->save($task) ){
                                        if($this->request->data('repcomment') != "" ){                                
                                            $comment = $comment_table->newEntity();
                                            $comment->comment =  $this->request->data('repcomment')  ;
                                            $comment->task_id = md5($savedtask->id)   ;
                                            $comment->user = $this->Cookie->read('id')   ;
                                            $comment->created = strtotime('now');
                                            $comment_table->save($comment);
                                            }
                                        $activity = $activ_table->newEntity();
                                        $activity->action =  "Task marked complete and ongoing"  ;
                                        $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
                                        $activity->value = $id   ;
                                        $activity->origin = $this->Cookie->read('id')   ;
                                        $activity->created = strtotime('now');
                                        if($saved = $activ_table->save($activity) ){
                                            $activity = $activ_table->newEntity();
                                            $activity->action =  "Task created as ongoing"  ;
                                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
                                            $activity->value = $savedtask->id   ;
                                            $activity->origin = $this->Cookie->read('id')   ;
                                            $activity->created = strtotime('now');
                                            if($saved = $activ_table->save($activity) ){
    
                                            $res = [ 'result' => 'success'  ];
                                            }
                                            else{
                                                $res = [ 'result' => 'activity not saved'  ];
                            
                                                    }
                
                                        }
                                        else{
                                    $res = [ 'result' => 'activity not saved'  ];
                
                                        }
            
                     
                                }
                                else{
                                    $res = [ 'result' => 'task could not be created'  ];                            

                                }
                            }
                            else{
                                $res = [ 'result' => 'success'  ];

                            }
                    }
                    else{
                        $res = [ 'result' => 'personal task status not updated'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }
            

            public function  completetaskquality(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'status' => 3 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'personal task status not updated'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }


            public function  starttask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'status' => 2 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'personal task status not updated'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            
            public function  delete(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val');
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                           if($task_table->query()->update()->set([ 'status' => 0 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute()){
                            $res = [ 'result' => 'success'  ];

                           }
                             

                    }
                    else{
                        $res = [ 'result' => 'personal task not deleted'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }


            
            }
