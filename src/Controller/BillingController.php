<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\View\ClientHelper;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class BillingController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
            public function index(){
                $client_table = TableRegistry::get('client');                
                $billing_table = TableRegistry::get('billing');
                $project_table = TableRegistry::get('project');
                $task_table = TableRegistry::get('task');

                $poffset = $this->request->query('offset') ;

                $count_client = $client_table->find()->where(['status' => '1'])->count() ;

                $retrieve_client = $client_table->find()->select(['id' , 'cid' , 'fname' , 'lname' , 'type' ,'status' ,'id' ])->where(['status' => '1'])->order(['fname' => 'asc' ])->offset($poffset)->limit(10)->toArray() ;
                // foreach($retrieve_project as $check){
                //     $project_array[] =  md5($check['id']) ;
                // }
                // $project_cond = implode(',' , $project_array );
                $currentmonth = date('M y');
                $currentmonth_count = date('n') ;
                $previousmonth1 = date('M y', strtotime(date('Y-m')." -1 month"));
                $previousmonth1_count = date('n', strtotime(date('Y-m')." -1 month"));
                $previousmonth2 = date('M y', strtotime(date('Y-m')." -2 month"));
                $previousmonth2_count = date('n', strtotime(date('Y-m')." -2 month"));
                
            
                
                $new_array = array();
                // print_r($retrieve_client);
                $i = 0 ;
                $offset = ($poffset == "" ? 0 : $poffset) ;
                $limit = 10 ;
                $l = 1 ;
                $o = 0 ; 
                foreach($retrieve_client as $client){

                                   
                        if($client['type'] == 1 ){
                            $retrieve_project = $project_table->find()->select([ 'c.cid' , 'c.id',  'c.fname' , 'c.lname'  , 'project.name' , 'project.ptype' , 'project.nlimit' , 'project.status' , 'project.cid' , 'project.id'  ])->join([
                                'c' => [
                                    'table' => 'client',
                                    'type' => 'LEFT',
                                    'conditions' =>  'md5(c.id) =  project.cid' 
                                ]
                            ])->where(['project.status' => 1  , 'project.cid' => md5($client['id']) ])->toArray(); 
                            // print_r($retrieve_project);
                           
                            foreach($retrieve_project as $project){ 
                                $query = $billing_table->find();
                               
                                    // $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['cid' => $client['cid'] , 'pid' => $client['pid'] ])->toArray() ;
                                    $retrieve_billing2 = $billing_table->find()->select(['billing_date','next_payment' , 'remark'])->where(['pid' => $project['id']  , 'deleted' => 0 ])->order(['billing_date' => 'desc' ])->first() ;
                                    $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)'  ])->where([ 'project' => md5($project['id']) ,  'status' => 4 ])->first();    
                                    
                                    $count_hours = $billing_table->find()->select(['hours' => 'SUM(hours)'  ])->where([ 'pid' => $project['id'] ,  'deleted' => 0 ])->first();    
                                    $last_billed = $task_table->find()->select(['billedtime' ])->where([ 'project' => md5($project['id']) , 'status' => 4 ])->order(['id' => 'desc' ])->first();
                                    // $currentmonth_billed = $task_table->find()->select([ 'billed' => 'SUM(billed)'  ])->where(['status' => '4' , 'project' => md5($project['id']) , 'MONTH(FROM_UNIXTIME(`created`))' => $currentmonth_count ])->first() ;
                                    // $previousmonth1_billed = $task_table->find()->select(['billed' => 'SUM(billed)'  ])->where(['status' => '4' , 'project' => md5($project['id']) , 'MONTH(FROM_UNIXTIME(`created`))' => $previousmonth1_count ])->first() ;
                                    // $previousmonth2_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where(['status' => '4' , 'project' => md5($project['id']) , 'MONTH(FROM_UNIXTIME(`created`))' => $previousmonth2_count ])->first() ;
    
                                    $count_hours->hours = ($count_hours->hours ? $count_hours->hours : 0) ;  
                                    $billing['fname'] = $project['c']['fname'];
                                    $billing['lname'] = $project['c']['lname'];
                                    $billing['cid'] = $project['c']['cid'];
                                    $billing['ccid'] = md5($project['c']['id']);
                                    $billing['type'] = $client['type'];
                                    $billing['ptype'] = $project['ptype'];
                                    $billing['pid'] = $project['id'];
                                    $billing['pname'] = $project['name'];
                                    $billing['hours'] = $count_hours->hours;
                                    $billing['status'] = $client['status'];
                                    $billing['pstatus'] = $project['status'];
                                    $billing['nlimit'] = $project['nlimit'];
                                    $billing['balance'] = round($count_hours->hours,2) - round($count_billed->billed,2); 
                                    if(!isset($retrieve_billing2->next_payment)){
                                        $billing['next_payment'] = "NA"; 
                                    }
                                    else{
                                        $billing['next_payment'] = date('jS F, Y', $retrieve_billing2->next_payment); 
                                    }
                                    $billing['last_billed'] = (!isset($last_billed->billedtime) || $last_billed->billedtime == "" ? "NA" : date('jS F, Y', $last_billed->billedtime)); 
            
                                    $billing['billing_date'] = (isset($retrieve_billing2->billing_date)  ?  date('jS F, Y', $retrieve_billing2->billing_date) : "NA"  ); 
                                    $billing['remark'] = (isset($retrieve_billing2->remark) ? $retrieve_billing2->remark : "NA" ) ; 
                                    // $billing['currentmonth'] = (isset($currentmonth_billed->billed) ? $currentmonth_billed->billed :  "0"); 
                                    // $billing['previousmonth1'] = (isset($previousmonth1_billed->billed) ? $previousmonth1_billed->billed :  "0"); 
                                    // $billing['previousmonth2'] = (isset($previousmonth2_billed->billed) ? $previousmonth2_billed->billed :  "0");           
                                          $billing['currentmonth'] =  "0" ; 
                                    $billing['previousmonth1'] =    "0" ; 
                                    $billing['previousmonth2'] =   "0" ;                                
                                    $new_array[$i] = $billing;
                                    $i++ ;
                                      $l++ ;                         

                            }
                            // print_r($new_array);
    
                        }
                        elseif($client['type'] == 2 ){
                            $query = $billing_table->find();
                               
                            // $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['cid' => $client['cid'] , 'pid' => $client['pid'] ])->toArray() ;
                            $retrieve_billing2 = $billing_table->find()->select(['billing_date','next_payment' , 'remark'])->where(['cid' => $client['id']  , 'deleted' => 0 ])->order(['billing_date' => 'desc' ])->first() ;
                            $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)'  ])->where([ 'client' => md5($client['id']) ,  'status' => 4 ])->first();    
                            
                            $count_hours = $billing_table->find()->select(['hours' => 'SUM(hours)'  ])->where([ 'cid' => $client['id'] ,  'deleted' => 0 ])->first();    
                            $last_billed = $task_table->find()->select(['billedtime' ])->where([ 'client' => md5($client['id']) , 'status' => 4 ])->order(['id' => 'desc' ])->first();
    
                            // $currentmonth_billed = $task_table->find()->select([ 'billed' => 'SUM(billed)'  ])->where(['status' => '4' , 'client' => md5($client['id']) , 'MONTH(FROM_UNIXTIME(`created`))' => $currentmonth_count ])->first() ;
                            // $previousmonth1_billed = $task_table->find()->select(['billed' => 'SUM(billed)'  ])->where(['status' => '4' , 'client' => md5($client['id']) , 'MONTH(FROM_UNIXTIME(`created`))' => $previousmonth1_count ])->first() ;
                            // $previousmonth2_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where(['status' => '4' , 'client' => md5($client['id']) , 'MONTH(FROM_UNIXTIME(`created`))' => $previousmonth2_count ])->first() ;
    
                            $count_hours->hours = ($count_hours->hours ? $count_hours->hours : 0) ;  
                            $billing['fname'] = $client['fname'];
                            $billing['lname'] = $client['lname'];
                            $billing['pid'] = 0;
                            $billing['cid'] = $client['cid'];
                            $billing['ccid'] = md5($client['id']);
    
                            $billing['type'] = $client['type'];
                            $billing['ptype'] = 1;
                            $billing['pname'] = "";
                            $billing['hours'] = $count_hours->hours;
                            $billing['status'] = $client['status'];
                            $billing['pstatus'] = $client['status'];
                            $billing['nlimit'] = 0;
    
                            $billing['balance'] = round($count_hours->hours,2) - round($count_billed->billed,2); 
                            if(!isset($retrieve_billing2->next_payment)){
                                $billing['next_payment'] = "NA"; 
                            }
                            else{
                                $billing['next_payment'] = date('jS F, Y', $retrieve_billing2->next_payment); 
                            }
                            $billing['last_billed'] = (!isset($last_billed->billedtime) || $last_billed->billedtime == "" ? "NA" : date('jS F, Y', $last_billed->billedtime)); 
    
                            $billing['billing_date'] = (isset($retrieve_billing2->billing_date)  ?  date('jS F, Y', $retrieve_billing2->billing_date) : "NA"  ); 
                            $billing['remark'] = (isset($retrieve_billing2->remark) ? $retrieve_billing2->remark : "NA" ) ; 
                            // $billing['currentmonth'] = (isset($currentmonth_billed->billed) ? $currentmonth_billed->billed :  "0"); 
                            // $billing['previousmonth1'] = (isset($previousmonth1_billed->billed) ? $previousmonth1_billed->billed :  "0"); 
                            // $billing['previousmonth2'] = (isset($previousmonth2_billed->billed) ? $previousmonth2_billed->billed :  "0");       
                            
                            $billing['currentmonth'] =  "0" ; 
                            $billing['previousmonth1'] =    "0" ; 
                            $billing['previousmonth2'] =   "0" ;   
                            $new_array[$i] = $billing;
                            $i++ ;
                                $l++ ;                         

                        }
                        $pageoffset = $o ;

         
                    
                    $o++ ;

                        // print_r($new_array);

                }
                        // print_r($new_array);


              
            
                // $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' , 'status' => 4 ])->first();
                // $count_spent = $task_table->find()->select(['spent' => 'SUM(spent)' ])->first();
                // $count_balance = $billing_table->find()->select(['hours' => 'SUM(hours)' , 'deleted' => 0 ])->first();
                // $count_balance = $count_balance->hours - $count_billed->billed;
                // $this->set("billed_hours", $count_billed->billed);  
                $this->set("billed_hours", 0);  
                // $this->set("spent_hours", $count_spent->spent);  
                $this->set("spent_hours", 0);  
                $this->set("pageoffset", $offset);  
                $this->set("pagetotal", $count_client/10);  
            
                
                // $this->set("balance_hours", $count_balance);  
                $this->set("balance_hours", 0);  
                $this->set("clients", $new_array);  
                $this->set("currentmonth", $currentmonth);  
                $this->set("previousmonth1", $previousmonth1);  
                $this->set("previousmonth2", $previousmonth2);  

                $this->viewBuilder()->setLayout('user');

                
            }


            
            public function hoursdetail(){
                $project_table = TableRegistry::get('project');                
                $billing_table = TableRegistry::get('billing');
                $task_table = TableRegistry::get('task');
                $id  = $this->request->data('val') ;

                if($id){
                    
                    
                        $check_client = $billing_table->find()->select(['billing.pid' , 'hours' => 'SUM(billing.hours)' , 'p.name' ])->group('billing.pid')->join([
                            'p' => [
                                'table' => 'project',
                                'type' => 'LEFT',
                                'conditions' =>  'p.id =  billing.pid' 
                            ] 
                        ])->where(['md5(billing.cid)' => $id , 'billing.deleted' => 0 ])->toArray(); 
                        $html = '<table class="table-bordered" style="width:100%">
                        <tr class="green">
                        <th>Project</th>
                        <th>Incoming</th>
                        <th>Billed</th>
                        </tr>' ;
                        foreach($check_client as $check){
                            $project_array[] =  $check['pid'] ;
                        }
                        $project_cond = implode(',' , $project_array );
                        foreach($check_client as $check){
                            if($check['pid'] == 0 ){
                                $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'client' => $id ,  "!FIND_IN_SET('project','{$project_cond}')" ])->first();

                            }
                            else{
                                $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'client' => $id , 'project' => md5($check['pid']) ])->first();

                            }
                            // $check_client['billed'] = $count_billed->billed ;
                            // $check_client['balance'] =  $check['hours'] - $count_billed->billed ;
                            $html .=  "<tr>
                            <td>".($check['p']['name'] == NULL ? "NA" : $check['p']['name'])."</td>
                            <td>".round($check['hours'],2)."</td>
                            <td>".($count_billed->billed == "" ? 0 : round($count_billed->billed,2))."</td>
                            </tr>
                            ";
                        }
                        $html .= "</table>" ;

    
                        // if($check_client != 0 ){
                        //     $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['cid' => $client['id']])->toArray() ;
                        //     $retrieve_billing2 = $billing_table->find()->select(['billing_date','remark'])->where(['cid' => $client['id']])->order(['id' => 'desc' ])->first() ;
                        //     $retrieve_billing3 = $billing_table->find()->select(['next_payment'])->where(['cid' => $client['id']  , 'next_payment is NOT' => NULL ])->order(['id' => 'desc' ])->first() ;   
                            
                        //     $client['hours'] = $retrieve_billing[0]['hours'] - $count_billed->billed; 
                        //     if(!isset($retrieve_billing3->next_payment)){
                        //         $client['next_payment'] = "NA"; 
    
                        //     }
                        //     else{
                        //         $client['next_payment'] = date('jS F, Y', $retrieve_billing3->next_payment); 
    
                        //     }
    
                            // $client['billing_date'] = ($retrieve_billing2->billing_date == "" ? "NA" : date('jS F, Y', $retrieve_billing2->billing_date)); 
                            // $client['remark'] = $retrieve_billing2->remark; 
                            // $client['currentmonth'] = "NA"; 
                            // $client['previousmonth1'] = "NA"; 
                            // $client['previousmonth2'] = "NA";           
                            $res = ['result' => 'success' , 'data' => $html];
            
                       
                            // $new_array[] = $client;
                            
                        }
                        
                    
                
               

                // $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->first();
                // $count_spent = $task_table->find()->select(['spent' => 'SUM(spent)' ])->first();
                // $count_balance = $billing_table->find()->select(['hours' => 'SUM(hours)' ])->first();
                // $this->set("billed_hours", $count_billed->billed);  
                // $this->set("spent_hours", $count_spent->spent);  
                // $this->set("balance_hours", $count_balance->hours);  
                // $this->set("clients", $new_array);  
                // $this->set("currentmonth", $currentmonth);  
                // $this->set("previousmonth1", $previousmonth1);  
                // $this->set("previousmonth2", $previousmonth2);  

                // $this->viewBuilder()->setLayout('user');
                return $this->json($res);

                
            }



            


            public function add(){
                $client_table = TableRegistry::get('client');

                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id' ])->where(['status !=' => 0 ])->order(['fname' => 'asc'])->toArray() ; 
         
                $this->set("clients", $retrieve_client);  
                
                $this->viewBuilder()->setLayout('user');
            }
            
            public function getprojectsData(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $billing_table = TableRegistry::get('billing');
                    $project_table = TableRegistry::get('project');
                    $project  = $this->request->data('val') ;
                    if(!empty($project)){
                    $total = 0 ;
                    $paid = 0 ;
                    $pending = 0 ;
                    $query = $billing_table->find();                     
                    $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['pid' => $project  , 'deleted' => 0 ])->toArray() ;
                    $retrieve_project = $project_table->find()->select([ 'hours'   ])->where(['id' => $project])->first() ;
                    $total  = $retrieve_project->hours ;
                    $paid  = ($retrieve_billing[0]['hours'] == NULL ? "0" : $retrieve_billing[0]['hours'] )  ;
                    $pending  = $total - $paid ;
                    $res = ['result' => "success" , "total" => $total , "paid" => $paid , "pending" => $pending ] ;
                    }
                    else{
                        $res = ['result' => "empty" ] ;
                    }
                }
                else{
                    $res = ['result' => "Invalid Operation"] ;
                
                }
                return $this->json($res);

            }



            public function getprojects(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $proejct_table = TableRegistry::get('project');
                    $client  = $this->request->data('client') ;
                    $retrieve_project = $proejct_table->find()->select(['name'  , 'id' ])->where(['cid' => md5($client) ,'status !=' => 0 ])->toArray() ; 
                    if(!empty($retrieve_project[0])){
                        $html = '<option value="">Select Project</option>' ;
                        foreach($retrieve_project as $project){
                            $html .= '<option value="'.$project['id'].'"> '.$project['name'].' </option>'; 
                        }
                        $res = ['result' => "success" , 'html' => $html  ] ;
    
                    }
                    else{
                        $res = ['result' => "empty" ] ;
                    }
                }
                else{
                    $res = ['result' => "Invalid Operation"] ;
                
                }
                return $this->json($res);

            }


            public function detail($cid){
                $billing_table = TableRegistry::get('billing');
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $project_table = TableRegistry::get('project');
                $get_project = $project_table->find()->select(['id' , 'name'])->where(['cid' => $cid ,'status' => 1 ])->toArray() ;
                $check_billing = $billing_table->find()->select(['id'])->where(['md5(cid)' => $cid ,'deleted' => 0 ])->count() ;
                // if($check_billing > 0){
                    $retrieve_billing = $billing_table->find()->select(['billing.pid' , 'billing.id', 'billing.hours' ,  'billing.added_by' , 'billing.billing_date' , 'billing.next_payment' ,'billing.remark' , 'p.name' ])->join([
                        'p' => [
                            'table' => 'project',
                            'type' => 'LEFT',
                            'conditions' =>  'p.id =  billing.pid' 
                        ]
                    ])->where(['md5(billing.cid)' => $cid , 'billing.deleted' => 0  ])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' ])->where(['md5(id)' => $cid ])->first() ;
                 $client_name = ucfirst($retrieve_client->fname).' '.ucfirst($retrieve_client->lname) ;
                    foreach($retrieve_billing as $billing){
                           $check_user = $users_table->find()->select(['id'])->where(['md5(id)' => $billing['added_by'] ])->count() ;
                           if($check_user == 0){
                               $billing['uname'] = "NA" ;
                           }
                           else{
                            $retrieve_user = $users_table->find()->select(['name'])->where(['md5(id)' => $billing['added_by'] ])->first() ;
                            $billing['uname'] = $retrieve_user->name ;

                           }

                    }
                    $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'client' => $cid , 'status' => 4 ])->first();
                    $query = $billing_table->find();                     
                    $retrieve_billing_total = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['md5(cid)' => $cid , 'deleted' => 0 ])->toArray() ;
                    $retrieve_billing_next_paydate = $billing_table->find()->select([  'next_payment' , 'billing_date'  ])->where(['md5(cid)' => $cid , 'deleted' => 0 ])->order(['billing_date' => 'desc' ])->first() ;
    
                    $client['hours'] = (isset($retrieve_billing[0]['hours']) ? $retrieve_billing[0]['hours'] : "NA" ); 
                    if(!isset($retrieve_billing_next_paydate->next_payment) ){
                        $next_payment  = "NA"; 

                    }
                    else{
                        $next_payment = date('jS F, Y', $retrieve_billing_next_paydate->next_payment); 

                    }
                    if(!isset($retrieve_billing_next_paydate->next_payment)){
                        $last_payment = "NA";

                    }else{
                        $last_payment = date('jS F, Y', $retrieve_billing_next_paydate->billing_date);

                    }


                    $ref = $this->request->getQuery('ref');
                    
                    if($ref != ""){
                        $v  =  $this->request->getQuery('v');
                        if($v != ""){
                            if($ref == "p"){
                            $red  = "/bpmt/projects/detail/".$v;
                            }
                            elseif($ref == "c"){
                                $red  = "/bpmt/clients/detail/".$v;
                                }
                        }
                    } 
                    else{
                        $red  = "/bpmt/billing";
                    }
// print_r($retrieve_billing);
                    $this->set("total_hours", ($retrieve_billing_total[0]['hours'] - $count_billed->billed) );  
                    $this->set("next_payment", $next_payment);  
                    $this->set("last_payment", $last_payment);  
                    $this->set("clients", $retrieve_billing);  
                    $this->set("client_name", $client_name);  
                    $this->set("project_details", $get_project);  
                    $this->set("red", $red);  
                    $this->viewBuilder()->setLayout('user');
    
            }

            public function addbl(){
                if ($this->request->is('ajax') && $this->request->is('post') ){

                    $billing_table = TableRegistry::get('billing');
                    $activ_table = TableRegistry::get('activity');
                    $check_type  =  $this->request->data('assignpro')  ;
                    if($check_type == 'yes' ){
                        $billing = $billing_table->newEntity();
                        $billing->pid =  $this->request->data('project')  ;
                        $billing->type =  $this->request->data('payment_type')  ;
                        $billing->hours = $this->request->data('paying') ;
                        $billing->cid = $this->request->data('client') ;
                        $billing->remark = $this->request->data('remark') ;
                        $billing->next_payment =  ($this->request->data('next_date') ? strtotime($this->request->data('next_date')) : NULL) ;
                        $billing->billing_date =  strtotime($this->request->data('billing_date'));  
                        $billing->created =  strtotime('now');  
                        $billing->modified =  strtotime('now');  
                        $billing->added_by =  $this->Cookie->read('id')  ;
                        if($saved = $billing_table->save($billing) ){
                            $bid = $saved->id ;
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Billing Added"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($bid)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success'    ];
    
                            }
                            else{
                        $res = [ 'result' => 'activity not saved'  ];
    
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'billing not saved'  ];
                        }
                    } 
                    elseif($check_type == 'no' ){
                        $billing = $billing_table->newEntity();
                        $billing->pid =  0  ;
                        $billing->type =  2  ;
                        $billing->hours = $this->request->data('paying') ;
                        $billing->cid = $this->request->data('client') ;
                        $billing->remark = $this->request->data('remark') ;
                        $billing->next_payment =  NULL ;
                        $billing->billing_date =  strtotime($this->request->data('billing_date'));  
                        $billing->created =  strtotime('now');  
                        $billing->modified =  strtotime('now');  
                        $billing->added_by =  $this->Cookie->read('id')  ;
                        if($saved = $billing_table->save($billing) ){
                            $res = [ 'result' => 'success'    ];

                        }
                    }
                   

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            public function tasks($userid = null){
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }

                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;
                $completedon = $this->request->query('completedon') ;
                
                $completedon_cond =  [ 'task.completedtime >=' =>  strtotime('-1 Month') ,'task.completedtime <=' =>  strtotime('now') ] ;

                if($userid == "alltask"){
                    $user_name = "All ";
                }
                
                else{
                    $users_table = TableRegistry::get('users');

                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }


                $below_msg = "Showing ".$user_name." unbilled but completed task till date" ;

              
                if($completedon != ""  ){
                    $date_array = explode('-',$completedon);
                    $start_date = strtotime($date_array[0]); 
                    $end_date = strtotime($date_array[1]) + 84000 ;
                  $below_msg = "Showing ".$user_name." unbilled but completed task between ".$date_array[0]." to ".$date_array[1] ;
                    
                    $completedon_cond =  [ 'task.completedtime >=' =>  $start_date ,'task.completedtime <=' =>  $end_date   ];
                }

                if($user_cat_id != ""  && $user_cat_id != "all" ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }

                if($userid == 'alltask' ){
                    // $cond = [];
                    // $cond1 = [    ['task.owner is' =>  NULL],
                    // ['task.owner' =>  ''  ],
                    // ['task.assigned' =>  '' ]
                    // ];
                    // $cond2 = [];
                    $cond3 = [];
                }
                else{
                    // $cond = [['task.assigned' =>  $userid] ] ;
                    // $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    // ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    // ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    // ];
                    //   $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.assigned' =>  $userid]];

                }

                $task_table = TableRegistry::get('task');

                $retrieve_billing = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reference' , 'task.description' , 'task.duedate' , 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' , 'task.estimate', 'task.spent' , 'task.billed' , 'task.comment' , 'p.name' , 'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '3' , 'task.billing' => '1' , $completedon_cond  ,$cat_cond, 'OR' => $cond3 ])->toArray() ;

                  $count_billed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '4', $completedon_cond ,$cat_cond, 'OR' => $cond3 ])->count() ;


                  $this->set("below_message", $below_msg);  
  
                 
                  
                $this->set("completedon", $completedon);
                $this->set("user_sel_id", $user_sel_id);
                $this->set("user_cat_id", $user_cat_id);
                $this->set("retrieve_task", $retrieve_billing);  
                $this->set("count_billed", $count_billed); 
                $this->viewBuilder()->setLayout('user');

                    }

            public function billed($userid = null){
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }

                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;
                $completedon = $this->request->query('completedon') ;
                $billedon = $this->request->query('billedon') ;
                $billedon_cond = '' ;

                if($userid == "alltask"){
                    $user_name = "All ";
                }
                
                else{
                    $users_table = TableRegistry::get('users');

                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }


                $below_msg = "Showing ".$user_name." billed tasks till date" ;

                if($billedon != ""  ){
                    $bdate_array = explode('-',$billedon);
                    $bstart_date = strtotime($bdate_array[0]); 
                    $bend_date = strtotime($bdate_array[1]) + 86400 ; 
                    $billedon_cond =  [ 'task.billedon >=' =>  $bstart_date ,'task.billedon <=' =>  $bend_date   ];
                    $below_msg = "Showing ".$user_name." billed tasks, billed between ".$bdate_array[0]." and ".$bdate_array[1] ;

                }
                // $completedon_cond = '' ;
                $completedon_cond =  [ 'task.completedtime >=' =>  strtotime('-1 Month') ,'task.completedtime <=' =>  strtotime('now') ] ;

                if($completedon != ""  ){
                    $date_array = explode('-',$completedon);
                    $start_date = strtotime($date_array[0]); 
                    $end_date = strtotime($date_array[1]) + 86400 ; 
                    $completedon_cond =  [ 'task.completedtime >=' =>  $start_date ,'task.completedtime <=' =>  $end_date   ];
                    if($billedon == ""  ){
                        $below_msg = " ".$user_name." billed tasks, completed between ".$date_array[0]." and ".$date_array[1] ;

                    }
                    else{
                        $below_msg = " ".$user_name." billed tasks, completed between ".$date_array[0]." and ".$date_array[1].", and billed betweeen ".$bdate_array[0]." and ".$bdate_array[1]  ;

                    }

                }
                if($user_cat_id != ""  && $user_cat_id != "all" ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }

                if($userid == 'alltask' ){
                    // $cond = [];
                    // $cond1 = [    ['task.owner is' =>  NULL],
                    // ['task.owner' =>  ''  ],
                    // ['task.assigned' =>  '' ]
                    // ];
                    // $cond2 = [];
                    $cond3 = [];
                }
                else{
                    // $cond = [['task.assigned' =>  $userid] ] ;
                    // $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    // ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    // ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    // ];
                    //   $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.assigned' =>  $userid]];

                }
                    $task_table = TableRegistry::get('task');
    
                    $retrieve_billed = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reference' , 'task.description' , 'task.duedate' , 'task.creator', 'task.created' , 'task.comment' , 'task.completedtime' ,'task.billedtime' , 'task.billedon' ,'task.assignedtime' ,'task.billed','task.spent' , 'task.billing' , 'p.name' , 'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                        'cr' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(cr.id) =  task.creator' 
                        ],
                        'o' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(o.id) =  task.owner' 
                        ],
                        'p' => [
                            'table' => 'project',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(p.id) =  task.project' 
                        ],
                        'a' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(a.id) =  task.assigned' 
                        ],
                        'cl' => [
                            'table' => 'client',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(cl.id) =  task.client' 
                        ],
                        'c' => [
                            'table' => 'taskcategory',
                            'type' => 'LEFT',
                            'conditions' =>  'c.id =  task.category' 
                        ]
                      ])->where([ 'task.status' => '4' ,$billedon_cond   , $completedon_cond  ,$cat_cond, 'OR' => $cond3 ])->toArray() ;
    
                      $count_billing = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3' ,$completedon_cond, $billedon_cond  ,'task.billing' => 1  ,$cat_cond, 'OR' => $cond3 ])->count() ;
    
                
                  $this->set("below_message", $below_msg);  
                      
                    $this->set("billedon", $billedon);
                    $this->set("completedon", $completedon);
                    $this->set("user_sel_id", $user_sel_id);
                    $this->set("user_cat_id", $user_cat_id);                
                    $this->set("retrieve_task", $retrieve_billed);  
                    $this->set("count_billing", $count_billing); 
                    $this->viewBuilder()->setLayout('user');
    
            
            }
            public function  delete(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $billing_table = TableRegistry::get('billing');
                    $id = $this->request->data('val');
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                           if($billing_table->query()->update()->set([ 'deleted' => 1 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute()){
                            $res = [ 'result' => 'success'  ];

                           }
                             

                    }
                    else{
                        $res = [ 'result' => 'billing not deleted'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            public function getbilling(){
                if ($this->request->is('ajax') && $this->request->is('post')){
                    $billing_table = TableRegistry::get('billing');
                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                        $retrieve_billing_query = $billing_table->find()->where(['md5(id)' => $id  , 'deleted' => 0 ]) ;
                        $count_billing = $retrieve_billing_query->count() ;

                        if($count_billing > 0 ){
                            $retrieve_billing = $billing_table->find()->where(['md5(id)' => $id  , 'deleted' => 0 ])->first() ;
                            $retrieve_billing->next_payment = ($retrieve_billing->next_payment != "" ? date('m/d/Y' , $retrieve_billing->next_payment ) : $retrieve_billing->next_payment);
                            $retrieve_billing->billing_date = ($retrieve_billing->billing_date != "" ? date('m/d/Y' , $retrieve_billing->billing_date) : $retrieve_billing->billing_date );
                        
                        }

                        
                        $res = [ 'result' => 'success' , 'billing' => $retrieve_billing  ];                            

                    }
                    else{
                        $res = [ 'result' => 'billing not found'  ];
                    }

                }
            
            else{
                $res = [ 'result' => 'invalid operation'  ];

            }


            return $this->json($res);
            }



            

            public function editbilling(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $billing_table = TableRegistry::get('billing');
                    $id = $this->request->data('vista'); 
                    $hours = $this->request->data('paid'); 
                    $pid = $this->request->data('project');
                    $remark = $this->request->data('remark');
                    if($this->request->data('next_payment') != ""){
                        $next_payment = strtotime($this->request->data('next_payment'));
                    }
                    else{
                        $next_payment = "" ;
                    }
                    if($this->request->data('billing_date')!=""){
                        $billing_date = strtotime($this->request->data('billing_date'));
                    }
                    else{
                        $billing_date = "" ;
                    }

                    if(!empty($id) ){
                        $modified = strtotime('now') ;
                        $update_billing = $billing_table->query()->update()->set([ 'pid' => $pid , 'hours' => $hours , 'remark' => $remark,'billing_date' => $billing_date , 'next_payment' => $next_payment  , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                        $res = ['result' => 'success'];   
                    }
                    else{
                        $res = ['result' => 'billing entry found'];
                    }
                }
                    else{
                        $res = [ 'result' => 'invalid operation'  ];
        
                    }
                return $this->json($res);


            }



        }