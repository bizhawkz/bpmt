<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class EmployeeController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
            public function index(){
                $users_table = TableRegistry::get('users');
                $roles_table = TableRegistry::get('roles');
                $team_table = TableRegistry::get('team'); 
                
                $retrieve_teams = $team_table->find()->select(['id' , 'name'   ])->where([ 'status' => '1'  ])->toArray() ;
                $retrieve_roles = $roles_table->find()->select(['id' , 'name' ])->where([ 'status' => '1'  ])->toArray() ;
   
                $retrieve_emp = $users_table->find()->select([ 'r.name',  'users.empid' , 'users.id' , 'users.name' , 'users.email', 'users.picture' , 'users.team' , 'users.phone' , 'users.status' , 'users.join_date' ])
                    ->join(['r' => [
                        'table' => 'roles',
                        'type' => 'LEFT',
                        'conditions' =>  'r.id =  users.role' 
                    ]
                ])->toArray(); 

                $this->set("emp_details", $retrieve_emp);  
                $this->set("team_details", $retrieve_teams);  
                $this->set("role_details", $retrieve_roles);  
                $this->viewBuilder()->setLayout('user');

            }

            public function update()
            {   
                if($this->request->is('post')){

                $id = $this->request->data['id'];
                
                $users_table = TableRegistry::get('users');
                $roles_table = TableRegistry::get('roles');
                $team_table = TableRegistry::get('team'); 

                $update_users = $users_table->find()->select(['name'  , 'id' , 'empid' , 'email' , 'join_date' , 'phone' , 'role' , 'team' , 'status' , 'password'])->where(['id' => $id])->toArray(); 
                
                $data = ['name' => $update_users[0]['name'] , 'id'=>$update_users[0]['id'] , 'phone'=>$update_users[0]['phone'] ,  'email'=>$update_users[0]['email'] , 'empid'=>$update_users[0]['empid'] ,'join_date'=>$update_users[0]['join_date'] , 'status'=>$update_users[0]['status'] , 'role'=>$update_users[0]['role'], 'team'=>$update_users[0]['team'], 'password'=>$update_users[0]['password'] ];
                
                return $this->json($data);

                }  
            }

            public function managers(){
                $this->viewBuilder()->setLayout('user');
                $users_table = TableRegistry::get('users');

                $retrieve_emp = $users_table->find()->select(['id' , 'name' , 'email' , 'picture' , 'team' ])->where([ 'FIND_IN_SET(\'25\',privilages)' , 'status' => '1' ])->toArray() ;

                $this->set("emp_details", $retrieve_emp);  

            }

            public function attendance(){
                $this->viewBuilder()->setLayout('user');
               

            }

            public function add(){
                $this->viewBuilder()->setLayout('user');
            }
            public function profile(){
                $this->viewBuilder()->setLayout('user');
            }

            public function editprofile(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $users_table = TableRegistry::get('users');
                    $activ_table = TableRegistry::get('activity');
                    
                    if(!empty($this->request->data['picture']['name']))
                    {
                        $filename = $this->request->data['picture']['name'];
                        $uploadpath = 'img/';
                        $uploadfile = $uploadpath.$filename;
                        if(move_uploaded_file($this->request->data['picture']['tmp_name'], $uploadfile))
                        {
                            $this->request->data['picture'] = $filename; 
                        }
                    }else
                    {
                        $picture =  $this->request->data('apicture');
                    }

                    $picture =  $this->request->data['picture'];

                    $name =  $this->request->data('name')  ;
                    $email =  $this->request->data('email')  ;
                    $phone = $this->request->data('phone') ;
                    $opassword = $this->request->data('opassword') ;
                    $password = $this->request->data('password') ;
                    $cpassword = $this->request->data('cpassword') ;
                    $userid = $this->Cookie->read('id');
                    $modified = strtotime('now');
                    $retrieve_users = $users_table->find()->select(['id'  ])->where(['email' => $this->request->data('email'), 'md5(id) !=' => $userid  , 'status' => '1' ])->count() ;
                    if($retrieve_users == 0){
                        if($name != ""  || $email != ""  && $phone != ""  )
                        {
                            if($update_task = $users_table->query()->update()->set([  'name' => $name ,'email' => $email, 'phone' => $phone , 'picture' => $picture , 'modified' => $modified ])->where(['md5(id)' =>  $userid ])->execute()){
                                $activity = $activ_table->newEntity();
                                $activity->action =  "Employee Basic Data Updated"  ;
                                $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
            
                                $activity->value = md5($userid)   ;
                                $activity->origin = $this->Cookie->read('id')   ;
                                $activity->created = strtotime('now');
                                if($saved = $activ_table->save($activity) ){
                                    $res = [ 'result' => 'success'  ];
        
                                }
                                else{
                            $res = [ 'result' => 'activity not saved'  ];
        
                                }
                            } 
                            else{
                                $res = [ 'result' => 'profile not updated'  ];
    
                            }
    
                            if($opassword != "" && $password != "" && $cpassword  != "" && $password == $cpassword ){
                                if($update_task = $users_table->query()->update()->set([  'password' => $password , 'modified' => $modified ])->where(['md5(id)' =>  $userid ])->execute()){
                                    $activity = $activ_table->newEntity();
                                    $activity->action =  "Employee Password Updated"  ;
                                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
                
                                    $activity->value = md5($userid)   ;
                                    $activity->origin = $this->Cookie->read('id')   ;
                                    $activity->created = strtotime('now');
                                    if($saved = $activ_table->save($activity) ){
                                        $res = [ 'result' => 'success'  ];
            
                                    }
                                    else{
                                $res = [ 'result' => 'activity not saved'  ];
            
                                    }
                                } 
                                else{
                                    $res = [ 'result' => 'password not updated'  ];
        
                                }
                            }
                            else if($opassword != "" && ($password == "" || $cpassword  == "" || $password != $cpassword) ){
                                if($password == ""){
                                    $res = [ 'result' => 'password is required'  ];
                                }
                                elseif($cpassword == ""){
                                    $res = [ 'result' => 'confirm password is required'  ];
                                }
                                elseif($password != $cpassword){
                                    $res = [ 'result' => 'password and confirm password doesn\'t match'  ];
                                }
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'empty'  ];
    
                        }
                    }
                    else{
                        $res = [ 'result' => 'email'  ];

                    }
                    



            }
            else{
                $res = [ 'result' => 'Invalid Operation'  ];
            }


           return $this->json($res);

        }
            
            public function addemp(){
                if ($this->request->is('ajax') && $this->request->is('post') ){

                    $users_table = TableRegistry::get('users');
                    $activ_table = TableRegistry::get('activity');
                    $retrieve_users = $users_table->find()->select(['id'  ])->where(['email' => $this->request->data('email')  , 'status' => '1' ])->count() ;
                    if($retrieve_users ==0 ){
                        $role_table = TableRegistry::get('roles');
                        $retrieve_privilages = $role_table->find()->select(['privilage'])->where(['id' => $this->request->data('role')  ])->first() ;
                        $privilages = $retrieve_privilages->privilage ;
                        $employee = $users_table->newEntity();
                        $employee->name =  $this->request->data('name')  ;
                        $employee->email =  $this->request->data('email')  ;
                        $employee->password = $this->request->data('password') ;
                        $employee->empid =  $this->request->data('empid')  ;
                        $employee->phone =  $this->request->data('phone')  ;
                        $employee->join_date =  $this->request->data('join_date')  ;
                        $employee->role =  $this->request->data('role') ;
                        $employee->privilages =  $privilages  ;
                        $employee->type =  $this->request->data('type')  ;
                        $employee->team =  $this->request->data('team')  ;
                        $employee->status =  $this->request->data('status')  ;
                        
                        $employee->created = strtotime('now');
                        if($saved = $users_table->save($employee) ){
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Employee Created"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($saved->id)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success'  ];
    
                            }
                            else{
                        $res = [ 'result' => 'activity not saved'  ];
    
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'user not saved'  ];
                        }
                    } 
                    else{
                        $res = [ 'result' => 'email'  ];
                    }
                   

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }
            
            public function editemp(){
                if ($this->request->is('ajax') && $this->request->is('post') ){

                    $users_table = TableRegistry::get('users');
                    $activ_table = TableRegistry::get('activity');
                    $retrieve_users = $users_table->find()->select(['id'  ])->where(['email' => $this->request->data('email'), 'id IS NOT' => $this->request->data('id')  , 'status' => '1' ])->count() ;
                    if($retrieve_users == 0 ){

                        $role_table = TableRegistry::get('roles');
                        $retrieve_privilages = $role_table->find()->select(['privilage'])->where(['id' => $this->request->data('role')  ])->first() ;
                        $privilages = $retrieve_privilages->privilage ;

                        $id = $this->request->data('id');
                        $name =  $this->request->data('name')  ;
                        $email =  $this->request->data('email')  ;
                        $password = $this->request->data('password') ;
                        $empid =  $this->request->data('empid')  ;
                        $phone =  $this->request->data('phone')  ;
                        $date =  $this->request->data('join_date')  ;
                        $role =  $this->request->data('role') ;
                        $privilages =  $privilages;
                        $team =  $this->request->data('team')  ;
                        $status =  $this->request->data('status')  ;
                        $now = strtotime('now');
                        
                        if( $users_table->query()->update()->set([ 'name' => $name , 'email'=> $email , 'empid'=> $empid, 'password'=>$password, 'phone' => $phone, 'role' => $role , 'privilages'=>$privilages , 'team'=> $team , 'status'=> $status, 'join_date' =>$date, 'modified' => $now  ])->where([ 'id' => $id  ])->execute())
                        {
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Employee Updated"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($id)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) )
                            {
                                $res = [ 'result' => 'success'  ];
    
                            }
                            else
                            {
                                $res = [ 'result' => 'activity not saved'  ];
                            }
    
                        }
                        else
                        {
                            $res = [ 'result' => 'user not updated'  ];
                        }
                    } 
                    else
                    {
                        $res = [ 'result' => 'email'  ];
                    }
                   

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }
            

            }
