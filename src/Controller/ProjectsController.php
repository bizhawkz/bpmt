<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ProjectsController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
            public function index(){
                $project_table = TableRegistry::get('project');
                $billing_table = TableRegistry::get('billing');
                $task_table = TableRegistry::get('task');
                
                $this->loadComponent('Flash'); // Include the FlashComponent
  $this->loadComponent('Paginator'); // it will load Paginator

/*                
                 $retrieve_proejct = $project_table->find()->select(['project.id' , 'project.nlimit' , 'c.fname' , 'c.lname' , 'e.name' , 'e.picture' , 'project.name' , 'project.end' , 'project.status' , 'project.ptype' ,'project.created' , 'project.hours' ])->join([
                    'c' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(c.id) =  project.cid' 
                    ],
                    'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(e.id) =  project.lead' 
                    ]
                 ])->where([ 'project.status !=' => '0' , 'c.status !=' => '0' ])->toArray() ;

*/                 
                 
      $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '15'
  ];           
                 $retrieve_proejct = $this->paginate($project_table->find()->select(['project.id' , 'project.nlimit' , 'c.fname' , 'c.lname' , 'e.name' , 'e.picture' , 'project.name' , 'project.end' , 'project.status' , 'project.ptype' ,'project.created' , 'project.hours' ])->join([
                    'c' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(c.id) =  project.cid' 
                    ],
                    'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(e.id) =  project.lead' 
                    ]
                 ])->where([ 'project.status !=' => '0' , 'c.status !=' => '0' ])->order(['project.name' => 'asc']))->toArray() ;


                 foreach($retrieve_proejct as $project){
                    $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'status' => 4 , 'project' => md5($project['id']) ])->first();
                    $project['spent'] = $count_billed->billed ;
                    if($project['spent'] == 0 || $project['hours'] == 0){
                        $project['progress'] = 0 ;
                    }
                    else{
                        $project['progress'] = round(($project['spent']/$project['hours'])*100 , 2 ) ;

                    }


                    $query = $billing_table->find();                     
                    $retrieve_billing_total = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['pid' => $project['id'] , 'deleted' => 0 ])->first() ;
                    $project['balance'] = $retrieve_billing_total->hours - $project['spent']  ;

                 }

                $this->set("pj_details", $retrieve_proejct);  
                $this->viewBuilder()->setLayout('user');


            }
            public function add(){
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $team_table = TableRegistry::get('team');
                
                $retrieve_emp = $users_table->find()->select(['id' , 'name' ])->where([ 'FIND_IN_SET(\'25\',privilages)'  , 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;
                $retrieve_team = $team_table->find()->select(['id' , 'name' ])->where([ 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;

                $retrieve_client = $client_table->find()->select(['id' , 'fname' , 'lname'])->where([ 'status' => '1'])->order(['fname' => 'asc' ])->toArray() ;

                $this->set("emp_details", $retrieve_emp);  
                $this->set("team_details", $retrieve_team);  
                $this->set("cli_details", $retrieve_client);  


                $this->viewBuilder()->setLayout('user');
            }

            public function addpj(){
                if ($this->request->is('ajax') && $this->request->is('post') ){

                    $project_table = TableRegistry::get('project');
                    $activ_table = TableRegistry::get('activity');
                    $project_team = TableRegistry::get('project_team');
                        $project = $project_table->newEntity();
                        $project->name =  $this->request->data('name')  ;
                        $project->cid =  $this->request->data('client')  ;
                        $project->start = strtotime($this->request->data('sdate')) ;
                        if($this->request->data('edate') != ""){
                            $project->end =  strtotime($this->request->data('edate'))  ;
                        }
                        $project->hours =  $this->request->data('hours')  ;
                        $project->type =  $this->request->data('type')  ;
                        $project->rate =  $this->request->data('rate') ;
                        $project->lead =  $this->request->data('lead')  ;
                        $project->nlimit =  $this->request->data('nlimit')  ;
                        $project->nallowed = $this->request->data('nallowed') ;
                        if($project->nallowed != 1){
                            $project->nallowed = 0 ;
                         }
                        $project->ptype =  $this->request->data('ptype')  ;
                        $project->team =  $this->request->data('team')  ;
                        $project->description =  $this->request->data('description')  ;
                        $project->status =  1  ;
                        
                        $project->created = strtotime('now');
                        $project->modified = strtotime('now');
                        if($saved = $project_table->save($project) ){
                            $pjid = $saved->id; 
                            // $members = $this->request->data('members');
                            // foreach($members as $member){
                            //    $project_t = $project_team->newEntity();
                            //    $project_t->pid =  $pjid  ;
                            //    $project_t->uid =  $member  ;
                            //    $project_t->created =  strtotime($this->request->data('edate'))  ;
                            //    $project_team->save($project_t) ;
                            // }
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Project Created"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($pjid)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success' , 'id' => md5($pjid)  ];
    
                            }
                            else{
                        $res = [ 'result' => 'activity not saved'  ];
    
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'project not saved'  ];
                        }
                    
                   

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            public function edit($pid){
                $project_table = TableRegistry::get('project');
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $team_table = TableRegistry::get('team');
                $retrieve_project = $project_table->find()->where([ 'status !=' => '0' , 'md5(id)' => $pid  ])->first() ;
                $retrieve_emp = $users_table->find()->select(['id' , 'name' ])->where([ 'FIND_IN_SET(\'25\',privilages)'  , 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;
                $retrieve_team = $team_table->find()->select(['id' , 'name' ])->where([ 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;

                $retrieve_client = $client_table->find()->select(['id' , 'fname' , 'lname'])->where([ 'status' => '1'])->order(['fname' => 'asc' ])->toArray() ;
                $this->set("emp_details", $retrieve_emp);  
                $this->set("team_details", $retrieve_team);  
                $this->set("cli_details", $retrieve_client);  

                $this->set("project", $retrieve_project);  
               $this->viewBuilder()->setLayout('user');
            }


            public function detail($id){
                $project_table = TableRegistry::get('project');
                $project_t_table = TableRegistry::get('project_team');
                $retrieve_proejct = $project_table->find()->select(['project.id' , 'c.fname' , 'c.lname' , 'c.picture' , 'c.id' , 'e.name' , 'e.picture' ,  'e.email'  ,'project.hours', 'project.name' , 'project.start', 'project.end' , 'project.status' , 'project.created' ])->join([
                    'c' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  project.cid' 
                    ],
                    'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'e.id =  project.lead' 
                    ]
                    
                 ])->where([ 'md5(project.id)' => $id  ])->first() ;

                 $retrieve_proejct_team = $project_t_table->find()->select([   'e.name' ,  'e.email' , 'e.picture'   ])->join([
                     'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'e.id =  project_team.uid' 
                    ]
                    
                 ])->where([ 'md5(project_team.pid)' => $id  ])->toArray() ;
                 if(!isset($retrieve_proejct->id)){
                    return $this->redirect('/projects');      

                 }
                 $this->set("retrieve_project", $retrieve_proejct);  
                 $this->set("project_team", $retrieve_proejct_team);  

                $this->viewBuilder()->setLayout('user');
            }

            
            public function editpj(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $task_table = TableRegistry::get('task');
                $client_table = TableRegistry::get('client');
                $billing_table = TableRegistry::get('billing');
                $name = $this->request->data('name') ;
                $start = strtotime($this->request->data('sdate')) ;
                $end = NULL ;
                if($this->request->data('edate') != "")
                {
                    $end =  strtotime($this->request->data('edate')) ;
                }

                $type = $this->request->data('type') ;
                $hours = $this->request->data('hours') ;
                $client = $this->request->data('client') ;
                $oclient = $this->request->data('oclient') ;
                $rate = $this->request->data('rate') ;
                $lead = $this->request->data('lead') ;
                $team = $this->request->data('team') ;
                $ptype =  $this->request->data('ptype')  ;
                $nlimit = $this->request->data('nlimit') ;
                
                $nallowed = $this->request->data('nallowed') ;
                if($nallowed != 1){
                    $nallowed = 0 ;
                 }
                $description = $this->request->data('description') ;
                $pid = $this->request->data('pid') ;
                $now = strtotime('now') ;
                $retrieve_project = $project_table->find()->select(['id'  ])->where([ 'md5(id) !=' => $pid , 'status !=' => '0' ])->count() ;
                $retrieve_oclient = $client_table->find()->select(['id'])->where([ 'md5(id)' => $oclient  ])->first() ;
                $retrieve_client = $client_table->find()->select(['id'])->where([ 'md5(id)' => $client  ])->first() ;
                
                if($retrieve_project= $project_table->query()->update()->set([ 'name' => $name , 'start' => $start  , 'end' => $end , 'type' => $type , 'hours' => $hours  , 'cid' => $client ,'nlimit' => $nlimit , 'ptype' => $ptype 
                , 'rate' => $rate , 'lead' => $lead , 'team' => $team ,'nallowed' => $nallowed , 'description' => $description ,'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                $task_table->query()->update()->set([ 'client' => $client , 'modified' => $now ])->where([ 'client' => $oclient  ])->execute();
                $billing_table->query()->update()->set([ 'cid' => $retrieve_client->id  ])->where([ 'cid' => $retrieve_oclient->id , 'md5(pid)' => $pid  ])->execute();
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Project Edited"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = $pid    ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success' , 'id' => $pid   ];
    
                            }
                }
          
                return $this->json($res);
 
                
            }

            public function delete(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $pid = $this->request->data('val') ;
                $now = strtotime('now') ;
                $str = $this->request->data('str') ;

                if($retrieve_client = $project_table->query()->update()->set([ 'status' => 0 , 'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                    "CC: jyoti@outsourcingservicesusa.com";
                    mail("info@outsourcingservicesusa.com" , "Project Deleted" , "<b>".ucfirst($str)."</b> has been deleted" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Project Deleted"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $pid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) )
                    {
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not deleted'];
                }


                return $this->json($res);

            }

            public function deactivate(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $pid = $this->request->data('val') ;
                $str = $this->request->data('str') ;
                $now = strtotime('now') ;

                if($retrieve_client = $project_table->query()->update()->set([ 'status' => 2 , 'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                                "CC: jyoti@outsourcingservicesusa.com";
                    mail("info@outsourcingservicesusa.com" , "Project Deactivated" , "<b>".ucfirst($str)."</b> has been deactivated" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Project Deactivated"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $pid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) ){
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not deactivated'];
                }


                return $this->json($res);

            }

            
            public function activate(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $str = $this->request->data('str') ;

                $pid = $this->request->data('val') ;
                $now = strtotime('now') ;

                if($retrieve_client = $project_table->query()->update()->set([ 'status' => 1 , 'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                    "CC: jyoti@outsourcingservicesusa.com";
                     mail("info@outsourcingservicesusa.com" , "Project Activated" , "<b>".ucfirst($str)."</b> has been activated" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Project activated"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $pid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) ){
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not activated'];
                }


                return $this->json($res);

            }

            }
