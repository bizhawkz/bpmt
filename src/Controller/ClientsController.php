<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ClientsController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
            public function index(){
                $client_table = TableRegistry::get('client');

                $retrieve_client = $client_table->find()->select([ 'c.name'  ,  'client.fname' , 'client.lname','client.status' , 'client.id' , 'client.created' ])->join([
                   'c' => [
                       'table' => 'countries',
                       'type' => 'LEFT',
                       'conditions' =>  'c.id =  client.country' 
                   ]
                   ])->where([ 'client.status !=' => '0'  ])->toArray() ;


               $this->set("cl_details", $retrieve_client);  
                $this->viewBuilder()->setLayout('user');
            }
            public function add(){
                $this->viewBuilder()->setLayout('user');
            }
            public function edit($cid){
                $client_table = TableRegistry::get('client');
                $retrieve_client = $client_table->find()->select([ 'fname' , 'lname' , 'id' , 'country' ,'type', 'address' , 'email' , 'picture' , 'mobile'  , 'created' ])->where([ 'status' => '1' , 'md5(id)' => $cid  ])->first() ;
 
                $this->set("client", $retrieve_client);  
               $this->viewBuilder()->setLayout('user');
            }

            public function delete(){
                $client_table = TableRegistry::get('client');
                $activ_table = TableRegistry::get('activity');
                $cid = $this->request->data('val') ;
                $now = strtotime('now') ;
                $str = $this->request->data('str') ;

                if($retrieve_client = $client_table->query()->update()->set([ 'status' => 0 , 'modified' => $now ])->where([  'md5(id)' => $cid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                    "CC: jyoti@outsourcingservicesusa.com";
                     mail("info@outsourcingservicesusa.com" , "Project Deleted" , "<b>".ucfirst($str)."</b> has been deleted" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Client Deleted"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $cid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) ){
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not deleted'];
                }


                return $this->json($res);

            }

            public function deactivate(){
                $client_table = TableRegistry::get('client');
                $activ_table = TableRegistry::get('activity');
                $cid = $this->request->data('val') ;
                $now = strtotime('now') ;
                $str = $this->request->data('str') ;

                if($retrieve_client = $client_table->query()->update()->set([ 'status' => 2 , 'modified' => $now ])->where([  'md5(id)' => $cid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                    "CC: jyoti@outsourcingservicesusa.com";
                     mail("info@outsourcingservicesusa.com" , "Project Deactivated" , "<b>".ucfirst($str)."</b> has been deactivated" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Client Deactivated"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $cid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) ){
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not deactivated'];
                }


                return $this->json($res);

            }

            public function activate(){
                $client_table = TableRegistry::get('client');
                $activ_table = TableRegistry::get('activity');
                $cid = $this->request->data('val') ;
                $now = strtotime('now') ;
                $str = $this->request->data('str') ;

                if($retrieve_client = $client_table->query()->update()->set([ 'status' => 1 , 'modified' => $now ])->where([  'md5(id)' => $cid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                    "CC: jyoti@outsourcingservicesusa.com";
                     mail("info@outsourcingservicesusa.com" , "Client Activated" , "<b>".ucfirst($str)."</b> has been activated" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Client Activated"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $cid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) ){
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not activated'];
                }


                return $this->json($res);

            }

            public function editcl(){
                $client_table = TableRegistry::get('client');
                $activ_table = TableRegistry::get('activity');
                $fname = $this->request->data('fname') ;
                $lname = $this->request->data('lname') ;
                $country = $this->request->data('country') ;
                $address = $this->request->data('address') ;
                $email = $this->request->data('email') ;
                $mobile = $this->request->data('mobile') ;
                $type = $this->request->data('type') ;
                $cid = $this->request->data('cid') ;
                $now = strtotime('now') ;
                $retrieve_client = $client_table->find()->select(['id'  ])->where(['email' => $this->request->data('email')  , 'md5(id) !=' => $cid , 'status !=' => '0' ])->count() ;
                if($retrieve_client ==0 ){
                if($retrieve_client = $client_table->query()->update()->set([ 'fname' => $fname ,'type' => $type , 'lname' => $lname  , 'country' => $country , 'address' => $address , 'email' => $email  , 'mobile' => $mobile , 'modified' => $now ])->where([  'md5(id)' => $cid  ])->execute()){
                             $activity = $activ_table->newEntity();
                            $activity->action =  "Client Edited"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = $cid    ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success' , 'id' => $cid   ];
    
                            }
                }
            }
            else{
                $res = [ 'result' => 'email' ] ;
            }

                return $this->json($res);
 
                
            }

            public function detail($cid){
                $client_table = TableRegistry::get('client');
                $project_table = TableRegistry::get('project');
                $billing_table = TableRegistry::get('billing');
                $project_t_table = TableRegistry::get('project_team');
                $check_client = $client_table->find()->select(['id'])->where(['md5(id)' => $cid ])->count() ;
                if($check_client == 1){
                    $retrieve_client = $client_table->find()->select(['client.fname' ,'client.id' , 'client.lname' , 'client.email' , 'client.picture' , 'client.mobile' , 'client.address'   , 'c.name'  ])->join([
                        'c' => [
                            'table' => 'countries',
                            'type' => 'LEFT',
                            'conditions' =>  'c.id =  client.country' 
                        ]
                    ])->where(['md5(client.id)' => $cid ])->first() ;
                    $count_proejct_active = $project_table->find()->select(['id' ])->where([ 'cid' => $cid , 'status' => 1  ])->count() ;
                    $count_proejct_inactive = $project_table->find()->select(['id' ])->where([ 'cid' => $cid , 'status' => 0  ])->count() ;
                    $query = $billing_table->find();

                    $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['md5(cid)' => $cid])->first() ;
                            // print_r($retrieve_billing);
                    $retrieve_client->active = $count_proejct_active;
                    $retrieve_client->inactive = $count_proejct_inactive;
                    $spent = 0 ;
                    $retrieve_client->balance = $retrieve_billing->hours - $spent;

                    $retrieve_proejct = $project_table->find()->select(['project.id' ,  'e.name' , 'e.picture' ,  'e.email'  ,'project.hours', 'project.name', 'project.end' , 'project.status' , 'project.description' , 'project.created' ])->join([
                        'e' => [
                            'table' => 'users',
                            'type' => 'LEFT',
                            'conditions' =>  'e.id =  project.lead' 
                        ]
                        
                     ])->where([ 'project.cid' => $cid  ])->toArray() ;
                            // print_r( $retrieve_proejct)

                    foreach($retrieve_proejct as $project){
                        $retrieve_proejct_team = $project_t_table->find()->select([   'e.name' ,  'e.email' , 'e.picture'   ])->join([
                            'e' => [
                               'table' => 'users',
                               'type' => 'LEFT',
                               'conditions' =>  'e.id =  project_team.uid' 
                           ]
                           
                        ])->where([ 'project_team.pid' => $project['id']  ])->toArray() ;
                        $project['team'] = $retrieve_proejct_team ;
                        $project['spent'] = 0 ;
                        if($project['spent'] == 0){
                            $project['progress'] = 0 ;
                        }
                        else{
                            $project['progress'] = ($project['spent']/$project['hours'])*100 ;

                        }
                        $query = $billing_table->find();                     
                        $retrieve_billing_total = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['pid' => $project['id']])->first() ;
                        $project['balance'] = $retrieve_billing_total->hours - $project['spent']  ;
                        if($project['balance'] == 0 || $project['hours'] == 0){
                            $project['balance_status'] = 0  ;

                        }
                        else{
                            $project['balance_status'] = ($project['balance']/$project['hours'])*100  ;

                        }



                    }
                    

                    $ref = $this->request->getQuery('ref');
                    
                    if($ref != ""){
                        $v  =  $this->request->getQuery('v');
                        if($v != ""){
                            if($ref == "p"){
                            $red  = "/bpmt/projects/detail/".$v;
                            }
                        }
                    }
                    else{
                        $red  = "/bpmt/clients";
                    }

                    $this->set("client", $retrieve_client);  
                    $this->set("projects", $retrieve_proejct);  
                    $this->viewBuilder()->setLayout('user');
                    $this->set("red", $red);  

    
                }
                else{
                            return $this->redirect('/clients');      

                }
            }

            public function addcl(){
                if ($this->request->is('ajax') && $this->request->is('post') ){

                    $client_table = TableRegistry::get('client');
                    $activ_table = TableRegistry::get('activity');
                    $retrieve_client = $client_table->find()->select(['id'  ])->where(['email' => $this->request->data('email')  , 'status !=' => '0' ])->count() ;
                    if($retrieve_client ==0 ){
                        $client = $client_table->newEntity();
                        $client->fname =  $this->request->data('fname')  ;
                        $client->lname =  $this->request->data('lname')  ;
                        $client->email = $this->request->data('email') ;
                        $client->mobile =  $this->request->data('mobile')  ;
                        $client->address =  $this->request->data('address')  ;
                        $client->country =  $this->request->data('country')  ;
                        $client->type =  $this->request->data('type')  ;
                        $client->created = strtotime('now');
                        $client->modified = strtotime('now');
                        if($saved = $client_table->save($client) ){
                            $cid = $saved->id ;
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Client Created"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($cid)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success' , 'id' => md5($cid)   ];
    
                            }
                            else{
                        $res = [ 'result' => 'activity not saved'  ];
    
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'client not saved'  ];
                        }
                    } 
                    else{
                        $res = [ 'result' => 'email'  ];
                    }
                   

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            }
