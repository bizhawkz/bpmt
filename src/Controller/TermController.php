<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class TermController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
            public function index(){
                
                $term_table = TableRegistry::get('terms');
                $user_table = TableRegistry::get('users');
                $role_table = TableRegistry::get('roles');
                $team_table = TableRegistry::get('team');

                $retrieve_terms = $term_table->find()->select(['id' , 'name' ,  'content' , 'created'])->toArray(); 
                
                $retrieve_user = $user_table->find()->select(['id' , 'name'])->where(['status' => 1])->toArray(); 
                
                $retrieve_role = $role_table->find()->select(['id' , 'name' ])->toArray(); 
                
                $retrieve_team = $team_table->find()->select(['id' , 'name' ])->toArray(); 

                $this->set("term_details", $retrieve_terms);
                $this->set("user_details", $retrieve_user);
                $this->set("role_details", $retrieve_role);
                $this->set("team_details", $retrieve_team);
                $this->viewBuilder()->setLayout('user');
            }

            public function add(){
                $this->viewBuilder()->setLayout('user');
            }
            
            public function edit($aid){

                $term_table = TableRegistry::get('terms');

                $term_details = $term_table->find()->select([ 'name' , 'id' , 'content' ])->where(['id' => $aid])->toArray();

                $this->set("term_detail", $term_details);  
                $this->viewBuilder()->setLayout('user');
            }

            public function addterm(){
                
                $term_table = TableRegistry::get('terms');
                $activ_table = TableRegistry::get('activity');
                
                if ($this->request->is('ajax') && $this->request->is('post') )
                { 
                    $name = $this->request->data('name') ;
                    $content = $this->request->data('editor');

                    $retrieve_term = $term_table->find()->select(['id'])->where(['name' => $this->request->data('name')  ])->count() ;
                    
                    if($content)
                    {
                        if($retrieve_term == 0 )
                        {
                            $terms = $term_table->newEntity();
                            $terms->name =  $name;
                            $terms->content = $content;
                            $terms->created = strtotime('now');
                            if($saved = $term_table->save($terms) )
                            {
                                $res = [ 'result' => 'success' ];
                            }
                            else
                            {
                                $res = [ 'result' => 'failed'  ];
                            }
                        } 
                        else
                        {
                            $res = [ 'result' => 'name'  ];
                        }
                    }
                    else
                    {
                        $res = [ 'result' => 'empty'  ];
                    }
                    
                }
                else
                {
                    $res = [ 'result' => 'invalid operation'  ];

                }

                return $this->json($res);

            }

            public function editterm(){
                
                $term_table = TableRegistry::get('terms');
                $activ_table = TableRegistry::get('activity');
                
                if ($this->request->is('ajax') && $this->request->is('post') )
                {   
                    $id = $this->request->data('id');
                    $name = $this->request->data('name');
                    $content = $this->request->data('editor');
                    $now = strtotime('now');
                    $retrieve_term = $term_table->find()->select(['id'])->where(['name' => $name , 'id IS NOT ' => $id  ])->count() ;
                    
                    if($content)
                    {
                        if($retrieve_term == 0 )
                        {   
                            if( $term_table->query()->update()->set([ 'name' => $name  , 'content' => $content , 'modified' => $now  ])->where([ 'id' => $id  ])->execute())
                            {
                                $res = [ 'result' => 'success'  ];    
                            }
                            else
                            {
                                $res = [ 'result' => 'failed'  ];
                            }
                        } 
                        else
                        {
                            $res = [ 'result' => 'name'  ];
                        }
                    }
                    else
                    {
                        $res = [ 'result' => 'empty'  ];
                    }    
                }
                else
                {
                    $res = [ 'result' => 'invalid operation'  ];

                }

                return $this->json($res);

            }


            public function delete()
            {
                $tid = $this->request->data('val') ;
                $term_table = TableRegistry::get('terms');
                $activ_table = TableRegistry::get('activity');
                
                $termid = $term_table->find()->select(['id'])->where(['id'=> $tid ])->first();
                if($termid)
                {   
                    $term = $term_table->get($tid);
                    $termdel = $term_table->delete($term);
                    
                    if($termdel)
                    {
                        $activity = $activ_table->newEntity();
                        $activity->action =  "Term Deleted"  ;
                        $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
                        $activity->value = $tid    ;
                        $activity->origin = $this->Cookie->read('id')   ;
                        $activity->created = strtotime('now');

                        if($saved = $activ_table->save($activity) )
                        {
                            $res = [ 'result' => 'success'  ];
                        }
                        else
                        {
                            $res = [ 'result' => 'failed'  ];
                        }
                    }
                    else
                    {
                        $res = [ 'result' => 'not delete'  ];
                    }    
                }
                else
                {
                    $res = [ 'result' => 'error'  ];
                }

                return $this->json($res);
            }
            

            public function shareterm(){
                
                $userterm_table = TableRegistry::get('termuser');
                $user_table = TableRegistry::get('users');
                $activ_table = TableRegistry::get('activity');
    
                if ($this->request->is('ajax') && $this->request->is('post') )
                { 
                    $termid = $this->request->data('id');

                    if(!empty($this->request->data('team')))
                    {
                        $team = $this->request->data('team');
                        
                        $teamid = $user_table->find()->select(['id'])->where(['team IN'=> $team , 'status IS NOT'=> 0])->toArray();

                        $userids = array();
                        foreach($teamid as $val)
                        {
                            array_push($userids, $val['id']);
                        }    
                    }
                    elseif(!empty($this->request->data('role')))
                    {
                        $role = $this->request->data('role');
                        $roleid = $user_table->find()->select(['id'])->where(['role IN'=> $role , 'status IS NOT'=> 0])->toArray();

                        $userids = array();
                        foreach($roleid as $val)
                        {
                            array_push($userids, $val['id']);
                        }
                    }
                    elseif(!empty($this->request->data('user')))
                    {
                        $userids = $this->request->data('user');
                    }
                    /*print_r($role);
                    echo '<pre>';
                    print_r($userids);   
                    die;*/
                    if($termid)
                    {   
                        if(!empty($userids))
                        {   
                            $i=0;
                            foreach($userids as $val)
                            {   
                                $termexist = $userterm_table->find()->select(['id'])->where(['termid'=> $termid , 'userid'=> $val])->toArray();
                                if($termexist == 0)
                                {
                                    $userterm = $userterm_table->newEntity();
                                    $userterm->termid =  $termid;
                                    $userterm->userid = $val;
                                    $userterm->created = strtotime('now');
                                    if($saved = $userterm_table->save($userterm))
                                    {
                                        //$res = [ 'result' => 'success' ]; 
                                        $i=$i+1;      
                                    }
                                    else
                                    {
                                        $res = [ 'result' => 'failed'  ];
                                    }
                                }
                                else
                                {
                                    $res = [ 'result' => 'exist'  ];
                                }   
                               
                            }

                        }
                        else
                        {
                            $res = [ 'result' => 'empty'  ];
                        }

                    }
                    else
                    {
                        $res = [ 'result' => 'error'  ];
                    }
                                   
                }
                else
                {
                    $res = [ 'result' => 'invalid operation'  ];

                }
                
               if($i)
                {
                    $res = [ 'result' => 'success' ]; 
                }

                return $this->json($res);

            }


/*  
        $for deelte = $user_table->delete()->where(['id'=> $id ]); 

 */

}
