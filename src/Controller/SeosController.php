<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class SeosController  extends AppController
{

    
    
    
    
    
        
public function getWeeks($month,$year){
	$month = intval($month);				//force month to single integer if '0x'
	$suff = array('st','nd','rd','th','th','th'); 		//week suffixes
	$end = date('t',mktime(0,0,0,$month,1,$year)); 		//last date day of month: 28 - 31
  	$start = date('w',mktime(0,0,0,$month,1,$year)); 	//1st day of month: 0 - 6 (Sun - Sat)
	$last = 7 - $start; 					//get last day date (Sat) of first week
	$noweeks = ceil((($end - ($last + 1))/7) + 1);		//total no. weeks in month
	//$output = "";						//initialize string		
	$output = array();						//initialize string		
	$monthlabel = str_pad($month, 2, '0', STR_PAD_LEFT);
        
        $i = 0;
        
	for($x=1;$x<$noweeks+1;$x++){	
		if($x == 1){
			$startdate = "$year-$monthlabel-01";
			$day = $last - 6;
		}else{
			$day = $last + 1 + (($x-2)*7);
			$day = str_pad($day, 2, '0', STR_PAD_LEFT);
			$startdate = "$year-$monthlabel-$day";
		}
		if($x == $noweeks){
			$enddate = "$year-$monthlabel-$end";
		}else{
			$dayend = $day + 6;
			$dayend = str_pad($dayend, 2, '0', STR_PAD_LEFT);
			$enddate = "$year-$monthlabel-$dayend";
		}
		//$output .= "{$x}{$suff[$x-1]} week -> Start date=$startdate End date=$enddate <br />";	
		$output[$i]['week'] = "{$x}{$suff[$x-1]} week";
                $output[$i]['startdate']= strtotime($startdate);	
                $output[$i]['enddate']= strtotime($enddate);
                
                $i++;
	}
        
        //echo "<pre>";print_r($output); echo "</pre>"; die("hi");
	return $output;
}    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
            public function index(){
                $project_table = TableRegistry::get('project');
                $billing_table = TableRegistry::get('billing');
                $task_table = TableRegistry::get('task');
                
                $this->loadComponent('Flash'); // Include the FlashComponent
  $this->loadComponent('Paginator'); // it will load Paginator

/*                
                 $retrieve_proejct = $project_table->find()->select(['project.id' , 'project.nlimit' , 'c.fname' , 'c.lname' , 'e.name' , 'e.picture' , 'project.name' , 'project.end' , 'project.status' , 'project.ptype' ,'project.created' , 'project.hours' ])->join([
                    'c' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(c.id) =  project.cid' 
                    ],
                    'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(e.id) =  project.lead' 
                    ]
                 ])->where([ 'project.status !=' => '0' , 'c.status !=' => '0' ])->toArray() ;

*/                 
                 
      $this->paginate = [ // here we have define limit of the record on the page
  'limit' => '15'
  ];           
                 $retrieve_proejct = $this->paginate($project_table->find()->select(['project.id' , 'project.nlimit' , 'c.fname' , 'c.lname' , 'e.name' , 'e.picture' , 'project.name' , 'project.end' , 'project.status' , 'project.ptype' ,'project.created' , 'project.hours' ])->join([
                    'c' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(c.id) =  project.cid' 
                    ],
                    'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(e.id) =  project.lead' 
                    ]
                 ])->where([ 'project.status !=' => '0' , 'c.status !=' => '0' ])->order(['project.name' => 'asc']))->toArray() ;


                 foreach($retrieve_proejct as $project){
                    $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'status' => 4 , 'project' => md5($project['id']) ])->first();
                    $project['spent'] = $count_billed->billed ;
                    if($project['spent'] == 0 || $project['hours'] == 0){
                        $project['progress'] = 0 ;
                    }
                    else{
                        $project['progress'] = round(($project['spent']/$project['hours'])*100 , 2 ) ;

                    }


                    $query = $billing_table->find();                     
                    $retrieve_billing_total = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['pid' => $project['id'] , 'deleted' => 0 ])->first() ;
                    $project['balance'] = $retrieve_billing_total->hours - $project['spent']  ;

                 }

                $this->set("pj_details", $retrieve_proejct);  
                $this->viewBuilder()->setLayout('user');


            }
            
            
 public function addpj2($userid = null){
                
                         $uid = $this->Cookie->read('id') ;
                         
                         //echo $uid; die;

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $team_table = TableRegistry::get('team');
                                $project_table = TableRegistry::get('project');

                
$ary_week_for_reviews = array();
                  $ary_week_for_reviews =   explode(",",$this->request->data('week_for_reviews'));
               $week_first_date  = $ary_week_for_reviews[0];   
               $week_last_date  = $ary_week_for_reviews[1]; 
                
/*                
                 $retrieve_proejct = $project_table->find()->select(['project.id' , 'project.nlimit' , 'c.fname' , 'c.lname' , 'e.name' , 'e.picture' , 'project.name' , 'project.end' , 'project.status' , 'project.ptype' ,'project.created' , 'project.hours' ])->join([
                    'c' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(c.id) =  project.cid' 
                    ],
                    'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(e.id) =  project.lead' 
                    ]
                 ])->where([ 'project.created >=' => $week_first_date , 'project.created <=' => $week_last_date ])->order(['project.name' => 'asc'])->toArray() ;

*/
         $ary_weeks_previous_month = array();
                    $month_previous = date('m', strtotime('first day of last month'));
                    $year_previous = date('y', strtotime('first day of last month'));
                $ary_weeks_previous_month =  $this->getWeeks($month_previous,$year_previous);
                
                
                
                
                
                $ary_weeks_current_month = array();
                                $month_current = date('m');
                    $year_current = date("Y"); 
                $ary_weeks_current_month =  $this->getWeeks($month_current,$year_current);
                
                
                
                         
               
      $task_table = TableRegistry::get('task');
       // $timelimitlower = $day;
        
       // $timelimitupper = $timelimitlower + 86400 ;
        
        $get_projects =  $task_table->find()->distinct('u.id')->select(['cl.fname','cl.lname','u.name', 'task.project','task.description', 'u.id'])->join([
                        'u' => [
                            'table' => 'project',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(u.id) =  task.project' 
                        ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ]
                    ])
->where([ 'task.completedtime >=' =>  $week_first_date , 'task.assigned' => $uid  , 'task.completedtime <=' =>  $week_last_date ])->toArray();
                
               
               
//echo "<pre>"; print_r($get_projects);die;               
               
/*               
   $log = $task_table->find()->select(['cl.fname','cl.lname','u.name', 'task.project','task.description' ])->join([
                        'u' => [
                            'table' => 'project',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(u.id) =  task.project' 
                        ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ]
                    ])
->where([ 'task.completedtime >=' =>  $week_first_date , 'task.assigned' => $uid  , 'task.completedtime <=' =>  $week_last_date ]);
debug($log);              
*/               
               
                
                
                
                
                
                
                
                $this->set("pj_details", $get_projects);  
                $this->set("ary_weeks_previous_month", $ary_weeks_previous_month);  
$this->set("ary_weeks_current_month", $ary_weeks_current_month);  
$this->set("week_first_date", $week_first_date);  
$this->set("week_last_date", $week_last_date);  
$this->set("week_for_reviews2", $this->request->data('week_for_reviews'));  


$this->set("userid", $uid);  

                $this->viewBuilder()->setLayout('user');
            }            
            
            
            public function add($userid = null){
                
                         $uid = $this->Cookie->read('id') ;

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $team_table = TableRegistry::get('team');
                
                
            $retrieve_users = $users_table->find()->select(['id' ,'name' , 'phone'  , 'password'  , 'email' ,'role' , 'privilages' , 'sesscode' ,'terms' ,  'picture','team' ])->where(['md5(id)' => $uid ]) ; 
            $user_details = $retrieve_users->toArray() ;  

                
                $retrieve_emp = $users_table->find()->select(['id' , 'name' ])->where([ 'FIND_IN_SET(\'25\',privilages)'  , 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;
                $retrieve_team = $team_table->find()->select(['id' , 'name' ])->where([ 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;

                $retrieve_client = $client_table->find()->select(['id' , 'fname' , 'lname'])->where([ 'status' => '1'])->order(['fname' => 'asc' ])->toArray() ;

                
                
                
                                $ary_weeks_previous_month = array();
$query_date = '2020-03-01';                                
                                
                 $month_previous = date('m', strtotime('first day of last month'));
                   $year_previous = date('y', strtotime('first day of last month'));


/*
                    $month_previous = date('m', strtotime($query_date));
                    $year_previous = date('y', strtotime($query_date));
                    
*/                    
                    
                    
                $ary_weeks_previous_month =  $this->getWeeks($month_previous,$year_previous);
                
                
                
                
                
                $ary_weeks_current_month = array();
                                $month_current = date('m');
                    $year_current = date("Y"); 
                $ary_weeks_current_month =  $this->getWeeks($month_current,$year_current);
                
                
                
                
                
                
                
                
                
                $this->set("userid", $userid);  
                $this->set("team_details", $retrieve_team);  
                $this->set("cli_details", $retrieve_client);  
$this->set("user_details", $user_details);
$this->set("ary_weeks_previous_month", $ary_weeks_previous_month);  
$this->set("ary_weeks_current_month", $ary_weeks_current_month);  



                $this->viewBuilder()->setLayout('user');
            }

            public function addpj(){
                if ($this->request->is('ajax') && $this->request->is('post') ){

                    $uid=$this->request->session()->read('users_id');

                    
                    
                    $seo_stats_table = TableRegistry::get('seo_stats');
                    $activ_table = TableRegistry::get('activity');
                    
                        $seo_stats = $seo_stats_table->newEntity();
                        $seo_stats->userid =  $uid ;
                        $seo_stats->keywords =  $this->request->data('keywords')  ;
                        
                        $seo_stats->projectid =  $this->request->data('project_for_week')  ;
                        $seo_stats->week_first_date =  $this->request->data('week_first_date')  ;
                        $seo_stats->week_last_date =  $this->request->data('week_last_date')  ;
                        
                        $seo_stats->visits = $this->request->data('visits') ;
                        $seo_stats->backlinks =  $this->request->data('backlinks')  ;
                        $seo_stats->options =  $this->request->data('options')  ;
                        $seo_stats->smo_followers_insta =  $this->request->data('smo_followers_insta')  ;
                        $seo_stats->smo_twittr =  $this->request->data('smo_twittr')  ;
                        $seo_stats->smo_linkedin =  $this->request->data('smo_linkedin')  ;
                        $seo_stats->smo_optins =  $this->request->data('smo_optins')  ;
                        $seo_stats->created = strtotime('now');
                        $seo_stats->modified = strtotime('now');
                        if($saved = $seo_stats_table->save($seo_stats) ){
                            $pjid = $saved->id; 
                            // $members = $this->request->data('members');
                            // foreach($members as $member){
                            //    $project_t = $project_team->newEntity();
                            //    $project_t->pid =  $pjid  ;
                            //    $project_t->uid =  $member  ;
                            //    $project_t->created =  strtotime($this->request->data('edate'))  ;
                            //    $project_team->save($project_t) ;
                            // }
                            $activity = $activ_table->newEntity();
                            $activity->action =  "SEO SMO Stats Created"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($pjid)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success' , 'id' => md5($pjid)  ];
    
                            }
                            else{
                        $res = [ 'result' => 'activity not saved'  ];
    
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'SEO SMO Stats not saved'  ];
                        }
                    
                   

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            public function edit(){


//                        echo  "date".$this->request->data('date')  ;
//                       echo "<br>";
//                        echo  "userid".$this->request->data('userid')  ;
//                       
//                       die;

$date = $this->request->data('date') ;
$userid = $this->request->data('userid');



                    $seo_stats_table = TableRegistry::get('seo_stats');
                $retrieve_seo_stats = $seo_stats_table->find()->where([ 'userid ' => $userid , 'created' => $date  ])->order(['created' => 'DESC'])->limit(1)->toArray();

                //echo "<pre>";print_r($retrieve_seo_stats);die;
/*

                $project_table = TableRegistry::get('project');
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $team_table = TableRegistry::get('team');
                $retrieve_project = $project_table->find()->where([ 'status !=' => '0' , 'md5(id)' => $pid  ])->first() ;
                $retrieve_emp = $users_table->find()->select(['id' , 'name' ])->where([ 'FIND_IN_SET(\'25\',privilages)'  , 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;
                $retrieve_team = $team_table->find()->select(['id' , 'name' ])->where([ 'status' => '1' ])->order(['name'=>'asc'])->toArray() ;

                $retrieve_client = $client_table->find()->select(['id' , 'fname' , 'lname'])->where([ 'status' => '1'])->order(['fname' => 'asc' ])->toArray() ;
                $this->set("emp_details", $retrieve_emp);  
                $this->set("team_details", $retrieve_team);  
                $this->set("cli_details", $retrieve_client);  
                
                */

                $this->set("retrieve_seo_stats", $retrieve_seo_stats);  
               $this->viewBuilder()->setLayout('user');
            }

            
  
            public function editpj(){
                
                
                    $seo_stats_table = TableRegistry::get('seo_stats');
                    $activ_table = TableRegistry::get('activity');
                    
                       // $seo_stats = $seo_stats_table->newEntity();
                        $id =  $this->request->data('id')  ;
                        $userid =  $this->request->data('userid')  ;
                        $keywords =  $this->request->data('keywords')  ;
                        $visits = $this->request->data('visits') ;
                        $backlinks =  $this->request->data('backlinks')  ;
                        $options =  $this->request->data('options')  ;
                        $smo_followers_insta =  $this->request->data('smo_followers_insta')  ;
                        $smo_twittr =  $this->request->data('smo_twittr')  ;
                        $smo_linkedin =  $this->request->data('smo_linkedin')  ;
                        $smo_optins =  $this->request->data('smo_optins')  ;
                        $modified = strtotime('now');
                        
                        
                if($retrieve_seo_stats_rec= $seo_stats_table->query()->update()->set([ 
                    'userid' => $userid , 
                    'keywords' => $keywords ,
                    'visits' => $visits , 
                    'backlinks' => $backlinks  ,
                    'options' => $options ,
                    'smo_followers_insta' => $smo_followers_insta ,
                    'smo_twittr' => $smo_twittr ,
                    'smo_linkedin' => $smo_linkedin ,
                    'smo_optins' => $smo_optins ,
                    'modified' => $modified ])->where([  'id' => $id  ])->execute()){
                            $activity = $activ_table->newEntity();
                            $activity->action =  "SEO SMO Stats Edited"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($id)    ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success' , 'id' => $id   ];
    
                            }
                            else{
                        $res = [ 'result' => 'activity not saved'  ];
    
                            }
                }
                        else{
                            $res = [ 'result' => 'SEO SMO Stats not updated'  ];
                        }
                        
                        return $this->json($res);
            
                        
                       
                
                
                
                
                
                
                
                
                
                
                
              /*  
                
                
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $task_table = TableRegistry::get('task');
                $client_table = TableRegistry::get('client');
                $billing_table = TableRegistry::get('billing');
                $name = $this->request->data('name') ;
                $start = strtotime($this->request->data('sdate')) ;
                $end = NULL ;
                if($this->request->data('edate') != "")
                {
                    $end =  strtotime($this->request->data('edate')) ;
                }

                $type = $this->request->data('type') ;
                $hours = $this->request->data('hours') ;
                $client = $this->request->data('client') ;
                $oclient = $this->request->data('oclient') ;
                $rate = $this->request->data('rate') ;
                $lead = $this->request->data('lead') ;
                $team = $this->request->data('team') ;
                $ptype =  $this->request->data('ptype')  ;
                $nlimit = $this->request->data('nlimit') ;
                
                $nallowed = $this->request->data('nallowed') ;
                if($nallowed != 1){
                    $nallowed = 0 ;
                 }
                $description = $this->request->data('description') ;
                $pid = $this->request->data('pid') ;
                $now = strtotime('now') ;
                $retrieve_project = $project_table->find()->select(['id'  ])->where([ 'md5(id) !=' => $pid , 'status !=' => '0' ])->count() ;
                $retrieve_oclient = $client_table->find()->select(['id'])->where([ 'md5(id)' => $oclient  ])->first() ;
                $retrieve_client = $client_table->find()->select(['id'])->where([ 'md5(id)' => $client  ])->first() ;
                
                if($retrieve_project= $project_table->query()->update()->set([ 'name' => $name , 'start' => $start  , 'end' => $end , 'type' => $type , 'hours' => $hours  , 'cid' => $client ,'nlimit' => $nlimit , 'ptype' => $ptype 
                , 'rate' => $rate , 'lead' => $lead , 'team' => $team ,'nallowed' => $nallowed , 'description' => $description ,'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                $task_table->query()->update()->set([ 'client' => $client , 'modified' => $now ])->where([ 'client' => $oclient  ])->execute();
                $billing_table->query()->update()->set([ 'cid' => $retrieve_client->id  ])->where([ 'cid' => $retrieve_oclient->id , 'md5(pid)' => $pid  ])->execute();
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Project Edited"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = $pid    ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success' , 'id' => $pid   ];
    
                            }
                }
          
                return $this->json($res);
 
                */
            }            
            
            
            
            
            
            
            
            
            

            public function detail($id){
                $project_table = TableRegistry::get('project');
                $project_t_table = TableRegistry::get('project_team');
                $retrieve_proejct = $project_table->find()->select(['project.id' , 'c.fname' , 'c.lname' , 'c.picture' , 'c.id' , 'e.name' , 'e.picture' ,  'e.email'  ,'project.hours', 'project.name' , 'project.start', 'project.end' , 'project.status' , 'project.created' ])->join([
                    'c' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  project.cid' 
                    ],
                    'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'e.id =  project.lead' 
                    ]
                    
                 ])->where([ 'md5(project.id)' => $id  ])->first() ;

                 $retrieve_proejct_team = $project_t_table->find()->select([   'e.name' ,  'e.email' , 'e.picture'   ])->join([
                     'e' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'e.id =  project_team.uid' 
                    ]
                    
                 ])->where([ 'md5(project_team.pid)' => $id  ])->toArray() ;
                 if(!isset($retrieve_proejct->id)){
                    return $this->redirect('/projects');      

                 }
                 $this->set("retrieve_project", $retrieve_proejct);  
                 $this->set("project_team", $retrieve_proejct_team);  

                $this->viewBuilder()->setLayout('user');
            }

          

            public function delete(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $pid = $this->request->data('val') ;
                $now = strtotime('now') ;
                $str = $this->request->data('str') ;

                if($retrieve_client = $project_table->query()->update()->set([ 'status' => 0 , 'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                    "CC: jyoti@outsourcingservicesusa.com";
                    mail("info@outsourcingservicesusa.com" , "Project Deleted" , "<b>".ucfirst($str)."</b> has been deleted" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Project Deleted"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $pid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) )
                    {
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not deleted'];
                }


                return $this->json($res);

            }

            public function deactivate(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $pid = $this->request->data('val') ;
                $str = $this->request->data('str') ;
                $now = strtotime('now') ;

                if($retrieve_client = $project_table->query()->update()->set([ 'status' => 2 , 'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                                "CC: jyoti@outsourcingservicesusa.com";
                    mail("info@outsourcingservicesusa.com" , "Project Deactivated" , "<b>".ucfirst($str)."</b> has been deactivated" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Project Deactivated"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $pid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) ){
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not deactivated'];
                }


                return $this->json($res);

            }

            
            public function activate(){
                $project_table = TableRegistry::get('project');
                $activ_table = TableRegistry::get('activity');
                $str = $this->request->data('str') ;

                $pid = $this->request->data('val') ;
                $now = strtotime('now') ;

                if($retrieve_client = $project_table->query()->update()->set([ 'status' => 1 , 'modified' => $now ])->where([  'md5(id)' => $pid  ])->execute()){
                    $headers = "From: info@theworkroomtools.com" . "\r\n" .
                    "CC: jyoti@outsourcingservicesusa.com";
                     mail("info@outsourcingservicesusa.com" , "Project Activated" , "<b>".ucfirst($str)."</b> has been activated" , $headers );
                    $activity = $activ_table->newEntity();
                    $activity->action =  "Project activated"  ;
                    $activity->ip =  $_SERVER['REMOTE_ADDR'] ;

                    $activity->value = $pid    ;
                    $activity->origin = $this->Cookie->read('id')   ;
                    $activity->created = strtotime('now');
                    if($saved = $activ_table->save($activity) ){
                        $res = [ 'result' => 'success'     ];

                    }
                }
                else{
                    $res = [ 'result' => 'not activated'];
                }


                return $this->json($res);

            }

            }
