<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
use Cake\Controller\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\View\ClientHelper;
use Cake\ORM\TableRegistry;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class QualityController  extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
        public function initialize()
        {
        parent::initialize();

            // if(isset($this->params['url']['arg']))
                $passedArgs = $this->request->getParam('pass');
                $this->count_unassigned = 0 ;
                $this->count_assigned = 0 ;
                $this->count_progress = 0  ;
                $this->count_completed = 0 ;
                $this->count_rework = 0 ;
                $this->completedon = "" ;
                $this->qa_sel_id = "" ;

                if(isset($passedArgs[0])){
                     $userid = $passedArgs[0]; 
                }
                else{
                    $userid = $this->Cookie->read('id');
                }
                     $timelimit = strtotime('tomorrow');
                     
                    if($userid == null){
                        if($this->Cookie->read('id') == ""){
                            return $this->redirect('/login');      
                        }
                        $userid = $this->Cookie->read('id');
                        $user_view_id = $this->Cookie->read('id');
                        
                    }

                    $user_cat_id = $this->request->query('cat_id') ;
                    $this->cat_cond = "" ;
                    $this->user_cat_id =    $user_cat_id ;
                    if($user_cat_id != "" && $user_cat_id != "all" ){
                                $this->cat_cond =  ['md5(task.category)' =>  $user_cat_id ];
                    }

                    $qa_sel_id = $this->request->query('quser') ;
                    $this->user_qa_cond = "" ;
                    $this->qa_sel_id =    $qa_sel_id ;
                    if($qa_sel_id != "" && $qa_sel_id != "all" ){
                                $this->user_qa_cond =  ['task.qagent' =>  $qa_sel_id ];
                    }
    
                        $this->user_sel_id = $userid ;
                        
                        if($userid == 'alltask' ){
                            $this->assigned_cond =  [];  
                        }
                        else{
                        //  $userid = $this->Cookie->read('id');
                        $this->assigned_cond =  ['task.assigned' => $userid ];
                        }

                         $timelimitupper = strtotime('last day of this month');
                         $bend_date = strtotime('last day of this month');
                         
                        $timelimitlower = strtotime('first day of this month');
                        $bstart_date = strtotime('first day of this month');
                        $task_table = TableRegistry::get('task');
                        $btimelimitlower = strtotime('today');
                        $btimelimitupper = $btimelimitlower + 86400 ;
                        $completedon = $this->request->query('completedon') ;
                
                        $completedon_cond = '' ;
                        $assigned_cond = '' ;
                        $billed_cond = '' ;
                      
                        if($completedon != "" && $completedon != "undefined"  ){
                            $date_array = explode('-',$completedon);
                            $timelimitlower = strtotime(trim($date_array[0])); 
                            $timelimitupper = strtotime(trim($date_array[1])) + 86400 ; 
                            $btimelimitlower =  $timelimitlower ;
                            $btimelimitupper = $timelimitupper ;
                        }

                        // $completedon_cond =  [ 'task.qual >=' =>  $timelimitlower ,'task.completedtime <=' =>  $timelimitupper   ];
                        
                        // $billed_cond =  [ 'task.billedtime >=' =>  $btimelimitlower ,'task.billedtime <=' =>  $btimelimitupper   ];
                        // $billed_cond_gross =  [ 'task.billedtime >=' =>  $timelimitlower ,'task.billedtime <=' =>  $timelimitupper   ];
                        
                

                         

                        $this->count_unassigned = $task_table->find()->where([  'qagent is'  => NULL  , 'status' => 5  , $this->assigned_cond, $this->user_qa_cond , $this->cat_cond ])->count();
                        $this->count_assigned = $task_table->find()->where([  'qagent is not'  => NULL  , "status" => 5, $this->assigned_cond , $this->user_qa_cond , $this->cat_cond ])->count();

                        // $count_billed = $task_table->find()->select(['billed' => 'SUM(task.billed)' ])->where([ 'task.status' => 4 ,  $billed_cond_gross   , "OR" => $user_cond , $cat_cond ])->first();
                  
                        // $count_unassigned = $task_table->find()->select(['estimate' => 'SUM(task.estimate)' ])->where(['task.duedate <' => $timelimit ,   $cat_cond, 'task.status' => '1', 'OR' => $user_cond_unass ])->first();
                  
                       
                        // $count_pending = $task_table->find()->select(['estimate' => 'SUM(task.estimate)'   ])->where([  'task.duedate <' => $timelimit , $cat_cond , 'task.status' => '1' , 'task.assigned' => '' , 'task.owner !=' =>  '' , 'OR' =>  $user_cond ])->orWhere([ 'task.status' => '1',  'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' => $user_cond ])->first() ;
                      
                        // $count_assigned = $task_table->find()->select(['estimate' => 'SUM(task.estimate)' ])->where([$assigned_cond , $cat_cond, 'task.status' => '1','task.assigned !=' => '' , 'OR' => $user_cond ])->first();
                        

                        $this->count_progress = $task_table->find()->select(['estimate' => 'SUM(task.estimate)'])->where(['qagent is not'  => NULL  , "status" => 6 , "rework" => 0 , $this->assigned_cond , $this->user_qa_cond, $this->cat_cond ])->count() ;
                        $this->count_rework = $task_table->find()->select(['estimate' => 'SUM(task.estimate)'])->where(['qagent is not'  => NULL  , "status" => 6 , "rework" => 1 , $this->assigned_cond ,$this->user_qa_cond , $this->cat_cond ])->count() ;
                        $this->count_completed = $task_table->find()->select(['estimate' => 'SUM(task.estimate)'])->where(['qagent is not'  => NULL  , "status" => 3 , 'qualitystime is not' => NULL , $this->user_qa_cond  , $this->assigned_cond , $this->cat_cond ])->count() ;
                        

                        // $count_completed = $task_table->find()->select(['spent' => 'SUM(task.spent)'])->where([ 'task.status' => '3'  , $cat_cond , $completedon_cond ,'OR' => $user_cond  ])->first() ;

                        // $count_quality = $task_table->find()->select(['spent' => 'SUM(task.spent)'])->where([ 'task.status' => '5'  , $cat_cond , $completedon_cond ,'OR' => $user_cond  ])->first() ;
        
                        // $count_billed_day = $task_table->find()->select(['billed' => 'SUM(task.billed)' ])->where([ 'task.status' => 4 ,  $billed_cond   , "OR" => $user_cond , $cat_cond ])->first();
                  
                        
                    // $this->count_rework = 0 ;
                    $this->completedon_cond = "" ;
 
 
               

        }
            public function index($userid = null){
                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    
                }


                //  $actionName = $this->request['url'];
                //  print_r($actionName);

               
                // $user_sel_id = $userid ;
                // $user_cat_id = $this->request->query('cat_id') ;
                // $cat_cond = "" ;
                // if($user_cat_id != "" && $user_cat_id != "all" ){
                //             $cat_cond =  ['md5(task.category)' =>  $user_cat_id ];
                // }

              
                // if($userid == 'all'){
                //     if($this->Cookie->read('id') == ""){
                //         return $this->redirect('/login');      
                //     }
                //     $userid = $this->Cookie->read('id');
                //     $user_view_id = $this->Cookie->read('id');
                //     $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                //     $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                //     ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                //     ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                //     ];
                //       $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                //       $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                // }
                // else{
                //     if($userid == 'alltask' ){
                //         $cond = [];
                //         $cond1 = [    ['task.owner is' =>  NULL],
                //         ['task.owner' =>  ''  ],
                //         ['task.assigned' =>  '' ]
                //         ];
                //         $cond2 = [];
                //         $cond3 = [];
                //     }
                //     else{
                //         $cond = [['task.assigned' =>  $userid] ] ;
                //         // $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                //         // ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                //         // ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                //         // ];
                //         //   $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                //         //   $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                //     }
                // }

                

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_quality = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' , 'FIND_IN_SET(\'37\' , privilages )' ])->order(['name' => 'asc'])->toArray() ;

                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;
                $timelimit = strtotime('tomorrow');
                $retrieve_unassigned = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.description' ,'task.client','task.duedate' ,'task.project' , 'task.assigned' , 'task.completedtime' , 'task.created' ,'task.comment' , 'task.creator' ,'task.owner', 'a.name' , 'task.billing' ,'p.name', 'o.name', 'cr.name' , 'cl.fname', 'cl.lname' ,  'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',  
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([
                    'task.qagent is' => NULL ,
                    $this->cat_cond, 
                    'task.status' => '5',
                    $this->assigned_cond
                                    ])->toArray() ;
             
                $today = strtotime('today');

                if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }

                // echo $this->user_sel_id;
                
                $this->set("user_view_id", $user_view_id);
                $this->set("user_sel_id", $this->user_sel_id);
                $this->set("qa_sel_id", $this->qa_sel_id);
                $this->set("qa_details", $retrieve_quality);  
                $this->set("user_cat_id", $this->user_cat_id);
                
                
                $this->set("completedon", $this->completedon);  
                $this->set("sidebar_prefix", $user_name);  
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrieve_unassigned);  
                $this->set("count_unassigned", $this->count_unassigned);  
                $this->set("count_assigned", $this->count_assigned);  
                $this->set("count_progress", $this->count_progress);  
                $this->set("count_completed", $this->count_completed);  
                $this->set("count_rework", $this->count_rework);  
                $this->viewBuilder()->setLayout('userquality');

            }


       

            public function assigned($userid = null){
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid ;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');

                }
                
            //     $user_sel_id = $userid;
               
            //     $user_cat_id = $this->request->query('cat_id') ;
            //     $cat_cond = '' ;

            //     if($user_cat_id != "" && $user_cat_id != "all"  ){
            //                 $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
            //     }


            //     if($userid == 'all'){
            //         if($this->Cookie->read('id') == ""){
            //             return $this->redirect('/login');      
            //         }
            //         $user_sel_id = 'all' ;
            //         $userid = $this->Cookie->read('id');
            //         $user_view_id = $this->Cookie->read('id');
            //         $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
            //         $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
            //         ['task.owner' =>  '' ,'task.creator' =>  $userid ],
            //         ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
            //         ];
            //           $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
            //           $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

            //     }
            //     else{      
            //     if($userid == 'alltask' ){
            //         $cond = [];
            //         $cond1 = [    ['task.owner is' =>  NULL],
            //         ['task.owner' =>  ''  ],
            //         ['task.assigned' =>  '' ]
            //         ];
            //         $cond2 = [];
            //         $cond3 = [];
            //     }
            //     else{
            //         $cond = [['task.assigned' =>  $userid] ] ;
            //         $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
            //         ['task.owner' =>  '' ,'task.creator' =>  $userid ],
            //         ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
            //         ];
            //           $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
            //           $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

            //     }
            // }
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_quality = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' , 'FIND_IN_SET(\'37\' , privilages )' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;

          
                $today = strtotime('today');
                $retrieve_assigned = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client', 'task.qagent' ,'task.comment', 'task.description' , 'task.rework' , 'task.arework_comment' , 'task.duedate' ,'task.project', 'task.creator' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name', 'q.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '5' , 'task.qagent is not' => NULL , $this->cat_cond , $this->assigned_cond  , $this->user_qa_cond   ])->toArray() ;

                 if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }
                // $this->set("assigned_hours", $this->assigned_hours);  
                $this->set("completedon", $this->completedon);  
                $this->set("user_sel_id", $this->user_sel_id);
                // $this->set("user__id", $this->user_qa_id);
                $this->set("qa_sel_id", $this->qa_sel_id);
                $this->set("qa_details", $retrieve_quality);  
                $this->set("user_cat_id", $this->user_cat_id);

                $this->set("sidebar_prefix", $user_name);
                $this->set("user_view_id", $user_view_id);
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrieve_assigned);  
                $this->set("count_unassigned", $this->count_unassigned);  
                $this->set("count_assigned", $this->count_assigned);  
                $this->set("count_progress", $this->count_progress);  
                $this->set("count_completed", $this->count_completed);  
                $this->set("count_rework", $this->count_rework);  
               
                $this->viewBuilder()->setLayout('userquality');

            }

            
            public function ongoing($userid = null){
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid ;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');

                }
                
            //     $user_sel_id = $userid;
               
            //     $user_cat_id = $this->request->query('cat_id') ;
            //     $cat_cond = '' ;

            //     if($user_cat_id != "" && $user_cat_id != "all"  ){
            //                 $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
            //     }


            //     if($userid == 'all'){
            //         if($this->Cookie->read('id') == ""){
            //             return $this->redirect('/login');      
            //         }
            //         $user_sel_id = 'all' ;
            //         $userid = $this->Cookie->read('id');
            //         $user_view_id = $this->Cookie->read('id');
            //         $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
            //         $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
            //         ['task.owner' =>  '' ,'task.creator' =>  $userid ],
            //         ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
            //         ];
            //           $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
            //           $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

            //     }
            //     else{      
            //     if($userid == 'alltask' ){
            //         $cond = [];
            //         $cond1 = [    ['task.owner is' =>  NULL],
            //         ['task.owner' =>  ''  ],
            //         ['task.assigned' =>  '' ]
            //         ];
            //         $cond2 = [];
            //         $cond3 = [];
            //     }
            //     else{
            //         $cond = [['task.assigned' =>  $userid] ] ;
            //         $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
            //         ['task.owner' =>  '' ,'task.creator' =>  $userid ],
            //         ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
            //         ];
            //           $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
            //           $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

            //     }
            // }
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_quality = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' , 'FIND_IN_SET(\'37\' , privilages )' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;

          
                $today = strtotime('today');
                $retrieve_assigned = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client','task.project', 'task.qagent' ,'task.comment','task.description' , 'task.duedate' , 'task.creator' , 'task.owner' ,'q.name', 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '6' , 'task.rework' => 0 , 'task.qagent is not' => NULL , $this->cat_cond , $this->assigned_cond  ,  'OR' => [] ])->toArray() ;

                 if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }
                // $this->set("assigned_hours", $this->assigned_hours);  
                $this->set("completedon", $this->completedon);  
                $this->set("user_view_id", $user_view_id);
                $this->set("user_sel_id", $this->user_sel_id);
                $this->set("qa_sel_id", $this->qa_sel_id);
                $this->set("qa_details", $retrieve_quality);  
                $this->set("user_cat_id", $this->user_cat_id);
                
                $this->set("sidebar_prefix", $user_name);
         
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrieve_assigned);  
                $this->set("count_unassigned", $this->count_unassigned);  
                $this->set("count_assigned", $this->count_assigned);  
                $this->set("count_progress", $this->count_progress);  
                $this->set("count_completed", $this->count_completed);  
                $this->set("count_rework", $this->count_rework);  
               
                $this->viewBuilder()->setLayout('userquality');

            }


                        
            public function rework($userid = null){
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid ;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');

                }
                
            //     $user_sel_id = $userid;
               
            //     $user_cat_id = $this->request->query('cat_id') ;
            //     $cat_cond = '' ;

            //     if($user_cat_id != "" && $user_cat_id != "all"  ){
            //                 $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
            //     }


            //     if($userid == 'all'){
            //         if($this->Cookie->read('id') == ""){
            //             return $this->redirect('/login');      
            //         }
            //         $user_sel_id = 'all' ;
            //         $userid = $this->Cookie->read('id');
            //         $user_view_id = $this->Cookie->read('id');
            //         $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
            //         $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
            //         ['task.owner' =>  '' ,'task.creator' =>  $userid ],
            //         ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
            //         ];
            //           $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
            //           $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

            //     }
            //     else{      
            //     if($userid == 'alltask' ){
            //         $cond = [];
            //         $cond1 = [    ['task.owner is' =>  NULL],
            //         ['task.owner' =>  ''  ],
            //         ['task.assigned' =>  '' ]
            //         ];
            //         $cond2 = [];
            //         $cond3 = [];
            //     }
            //     else{
            //         $cond = [['task.assigned' =>  $userid] ] ;
            //         $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
            //         ['task.owner' =>  '' ,'task.creator' =>  $userid ],
            //         ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
            //         ];
            //           $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
            //           $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

            //     }
            // }
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_quality = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' , 'FIND_IN_SET(\'37\' , privilages )' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;

          
                $today = strtotime('today');
                $retrieve_assigned = $task_table->find()->select(['task.id', 'task.estimate'  , 'task.assignedtime' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.client' ,'task.comment', 'task.qrework_comment' , 'task.arework_comment','task.description' , 'task.duedate' , 'task.creator', 'task.qagent','task.project' , 'q.name' , 'task.owner' , 'task.created' , 'task.billing',  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '6' , 'task.rework' => 1 , 'task.qagent is not' => NULL , $this->cat_cond , $this->assigned_cond  ,  'OR' => [] ])->toArray() ;

                 if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }
                // $this->set("assigned_hours", $this->assigned_hours);  
                $this->set("completedon", $this->completedon);  
                // $this->set("billed_hours_day", $this->billed_hours_day); 
                // $this->set("count_quality", $count_quality);  
                // $this->set("quality_hours", $this->quality_hours);  
                
                // $this->set("unassigned_hours", $this->unassigned_hours);  
                // $this->set("pending_hours", $this->pending_hours);  
                // $this->set("progress_hours", $this->progress_hours);  
                // $this->set("completed_hours", $this->completed_hours);  
                // $this->set("billed_hours", $this->billed_hours);  
                // $this->set("spent_hours", $this->spent_hours);  
                $this->set("sidebar_prefix", $user_name);
                $this->set("user_view_id", $user_view_id);
                $this->set("user_sel_id", $this->user_sel_id);
                $this->set("qa_sel_id", $this->qa_sel_id);
                $this->set("qa_details", $retrieve_quality);  
                $this->set("user_cat_id", $this->user_cat_id);
                
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrieve_assigned);  
                $this->set("count_unassigned", $this->count_unassigned);  
                $this->set("count_assigned", $this->count_assigned);  
                $this->set("count_progress", $this->count_progress);  
                $this->set("count_completed", $this->count_completed);  
                $this->set("count_rework", $this->count_rework);  
               
                $this->viewBuilder()->setLayout('userquality');

            }
            
       
       
            
            public function completed($userid = null){
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }

                // $user_sel_id = $userid;
                // $user_cat_id = $this->request->query('cat_id') ;
                // $cat_cond = '' ;
                
                // if($user_cat_id != ""  && $user_cat_id != "all" ){
                //             $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                // }


                // if($userid == 'all'){
                //     if($this->Cookie->read('id') == ""){
                //         return $this->redirect('/login');      
                //     }
                //     $user_sel_id = 'all' ;
                //     $userid = $this->Cookie->read('id');
                //     $user_view_id = $this->Cookie->read('id');
                //     $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                //     $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                //     ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                //     ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                //     ];
                //       $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                //       $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                // }
                // else{
                //     if($userid == 'alltask' ){
                //         $cond = [];
                //         $cond1 = [    ['task.owner is' =>  NULL],
                //         ['task.owner' =>  ''  ],
                //         ['task.assigned' =>  '' ]
                //         ];
                //         $cond2 = [];
                //         $cond3 = [];
                //     }
                //     else{
                //         $cond = [['task.assigned' =>  $userid] ] ;
                //         $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                //         ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                //         ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                //         ];
                //           $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                //           $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                //     }

                // }

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');
                $retrieve_quality = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' , 'FIND_IN_SET(\'37\' , privilages )' ])->order(['name' => 'asc'])->toArray() ;

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->toArray() ;

                // $count_unassigned = $task_table->find()->select(['task.id' ])->where([
                //     'task.duedate <=' => $timelimit ,
                //     'task.status' => '1',
                //     $cat_cond ,
                //     'OR' => 
                //                $cond1
                           
                //  ])->count() ;
                //  dd($retrieve_unassigned);

                $today = strtotime('today');
                // $count_pending = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '1' , $cat_cond , 'task.duedate <' => $timelimit , 'task.assigned' => '' ,'task.owner !=' =>  '' , 'OR' => $cond2  ])->orWhere([ 'task.status' => '1' , $cat_cond , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->count() ;
                // $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1', $cat_cond , $this->assigned_cond , 'task.assigned !=' =>  '' ,  'OR' => $cond ])->count() ;

                // $count_progress = $task_table->find()->select(['task.id'])->where([ 'task.status' => '2' ,$cat_cond , $this->assigned_cond , 'OR' => $cond ])->count() ;


                $retrive_completed = $task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description','task.qagent' , 'q.name' , 'task.rework_spent' ,  'task.comment' , 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' ,'task.project', 'task.completedtime' , 'task.billing' ,  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '3' ,'task.qualitystime is not' => NULL, $this->completedon_cond ,$this->assigned_cond , $this->cat_cond , 'OR' => []  ])->toArray() ;
                

                  if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                } 
                  $this->set("sidebar_prefix", $user_name);
                  $this->set("user_view_id", $user_view_id);
                  $this->set("user_sel_id", $this->user_sel_id);
                  $this->set("qa_sel_id", $this->qa_sel_id);
                  $this->set("qa_details", $retrieve_quality);  
                  $this->set("user_cat_id", $this->user_cat_id);
                //   $this->set("assigned_hours", $this->assigned_hours);  
                //   $this->set("completedon", $this->completedon);  
                //   $this->set("billed_hours_day", $this->billed_hours_day); 
                //   $this->set("count_quality", $count_quality);  
                //   $this->set("quality_hours", $this->quality_hours);  

                //   $this->set("unassigned_hours", $this->unassigned_hours);  
                //   $this->set("pending_hours", $this->pending_hours);  
                //   $this->set("progress_hours", $this->progress_hours);  
                //   $this->set("completed_hours", $this->completed_hours);  
                //   $this->set("billed_hours", $this->billed_hours);  
                $this->set("completedon", $this->completedon);  
                //   $this->set("spent_hours", $this->spent_hours);  
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrive_completed);  
                $this->set("count_unassigned", $this->count_unassigned);  
                $this->set("count_assigned", $this->count_assigned);  
                $this->set("count_progress", $this->count_progress);  
                $this->set("count_completed", $this->count_completed);  
                $this->set("count_rework", $this->count_rework);  
                $this->viewBuilder()->setLayout('userquality');

            }


            public function agentquality($userid = null){
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }

                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;
                
                if($user_cat_id != ""  && $user_cat_id != "all" ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }


                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                          $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }

                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->toArray() ;

                $count_unassigned = $task_table->find()->select(['task.id' ])->where([
                    'task.duedate <=' => $timelimit ,
                    'task.status' => '1',
                    $cat_cond ,
                    'OR' => 
                               $cond1
                           
                 ])->count() ;
                //  dd($retrieve_unassigned);

                $today = strtotime('today');
                $count_pending = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '1' , $cat_cond , 'task.duedate <' => $timelimit , 'task.assigned' => '' ,'task.owner !=' =>  '' , 'OR' => $cond2  ])->orWhere([ 'task.status' => '1' , $cat_cond , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->count() ;
                $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1', $cat_cond , $this->assigned_cond , 'task.assigned !=' =>  '' ,  'OR' => $cond ])->count() ;

                $count_progress = $task_table->find()->select(['task.id'])->where([ 'task.status' => '2' ,$cat_cond , $this->assigned_cond , 'OR' => $cond ])->count() ;
                $count_completed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3' , $this->completedon_cond , $cat_cond , 'OR' => $cond   ])->count() ;


                $retrive_quality = $task_table->find()->select(['task.id' , 'task.tittle' ,'task.estimate' ,'task.spent' , 'task.assignedtime' , 'task.reftype', 'task.reference' , 'task.description' ,  'task.comment' , 'task.duedate' ,'task.owner' ,'task.client', 'task.creator', 'task.created' , 'task.completedtime' , 'task.billing' ,  'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'q' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(q.id) =  task.qagent' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '5' , $cat_cond , 'OR' => $cond  ])->toArray() ;
                  $count_billed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '4' ,  $this->billed_cond ,'OR' => $cond  ])->count() ;

                  if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }

                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond3 ])->count() ;
                $this->set("scheduled_num", $scheduled_num);  
                  $this->set("sidebar_prefix", $user_name);
                  $this->set("user_view_id", $user_view_id);
                  $this->set("user_sel_id", $user_sel_id);
                  $this->set("user_cat_id", $user_cat_id);
                  $this->set("assigned_hours", $this->assigned_hours);  
                  $this->set("completedon", $this->completedon);  
                  $this->set("billed_hours_day", $this->billed_hours_day); 
                  $this->set("quality_hours", $this->quality_hours);  

                  $this->set("unassigned_hours", $this->unassigned_hours);  
                  $this->set("pending_hours", $this->pending_hours);  
                  $this->set("progress_hours", $this->progress_hours);  
                  $this->set("completed_hours", $this->completed_hours);  
                  $this->set("billed_hours", $this->billed_hours);  
                  $this->set("spent_hours", $this->spent_hours);  
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("cli_details", $retrieve_client);  
                $this->set("retrieve_task", $retrive_quality);  
                $this->set("count_quality", count($retrive_quality));  
                $this->set("count_completed", $count_completed);  
                $this->set("count_unassigned", $count_unassigned);  
                $this->set("count_pending", $count_pending);  
                $this->set("count_assigned", $count_assigned);  
                $this->set("count_progress", $count_progress);  
                $this->set("count_billed", $count_billed);  
                $this->viewBuilder()->setLayout('usertask');

            }

            
            
            public function billed($userid = null){
                $timelimit = strtotime('tomorrow');

                $user_view_id = $userid;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }
                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;

                if($user_cat_id != "" && $user_cat_id != "all"  ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }


                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];  
                          $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }
                $users_table = TableRegistry::get('users');

                 $task_table = TableRegistry::get('task');
              
              
                $count_unassigned = $task_table->find()->select(['task.id' ])->where([ 
                    'task.duedate <=' => $timelimit ,
                    'task.status' => '1',
                    $cat_cond,
                    'OR' => 
                               $cond1
                           
                ])->count() ;
            
                $today = strtotime('today');
                $count_pending = $task_table->find()->select(['task.id'  ])->where([ 'task.status' => '1',$cat_cond , 'task.duedate <' => $timelimit , 'task.assigned' => '' , 'task.owner !=' =>  '' , 'OR' => $cond2 ])->orWhere([ 'task.status' => '1' , $cat_cond , 'task.assigned is' => NULL , 'task.owner !=' =>  '' , 'OR' =>  $cond2 ])->count() ;
                $count_assigned = $task_table->find()->select(['task.id'   ])->where([ 'task.status' => '1' ,$cat_cond , $this->assigned_cond , 'task.assigned !=' =>  '' , 'OR' => $cond])->count() ;

                $count_progress = $task_table->find()->select(['task.id'])->where([ 'task.status' => '2' , $cat_cond , 'task.duedate <' => $timelimit ,'OR' => $cond ])->count() ;
                $count_quality = $task_table->find()->select(['task.id'])->where([ 'task.status' => '5' , $cat_cond , 'OR' => $cond ])->count() ;
                
                $retrive_billed = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reftype', 'task.reference' , 'task.description' , 'task.duedate' , 'task.client' , 'task.creator', 'task.created' , 'task.completedtime'  ,'task.owner' ,'task.billedtime' ,'task.assignedtime' ,'task.billed','task.spent' , 'task.billing' , 'p.name' , 'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                  ])->where([ 'task.status' => '4' , $cat_cond , $this->billed_cond , 'OR' => $cond  ])->toArray() ;
                
                  $count_completed = $task_table->find()->select(['task.id'])->where([ 'task.status' => '3', $this->completedon_cond , $cat_cond   , 'OR' => $cond  ])->count() ;
                  if($user_view_id == "alltask"){
                    $user_name = "All's ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;
    
                }

                
                $scheduled_num =  $task_table->find()->where([ 'duedate >=' => $timelimit , $cat_cond ,'status' => 1 ,'OR' => $cond3 ])->count() ;
                $this->set("scheduled_num", $scheduled_num);  
                  $this->set("sidebar_prefix", $user_name);
                  $this->set("user_view_id", $user_view_id);
                  $this->set("user_sel_id", $user_sel_id);
                  $this->set("user_cat_id", $user_cat_id);
                  $this->set("assigned_hours", $this->assigned_hours);  
                  $this->set("completedon", $this->completedon);  
                  $this->set("billed_hours_day", $this->billed_hours_day); 
                  $this->set("count_quality", $count_quality);  
                  $this->set("quality_hours", $this->quality_hours);  
            
                  $this->set("unassigned_hours", $this->unassigned_hours);  
                  $this->set("pending_hours", $this->pending_hours);  
                  $this->set("progress_hours", $this->progress_hours);  
                  $this->set("completed_hours", $this->completed_hours);  
                  $this->set("billed_hours", $this->billed_hours);  
                  $this->set("spent_hours", $this->spent_hours);  
                        $count_billed = count($retrive_billed);
                 $this->set("retrieve_task", $retrive_billed);  
                $this->set("count_billed", $count_billed);  
                $this->set("count_unassigned", $count_unassigned);  
                $this->set("count_pending", $count_pending);  
                $this->set("count_assigned", $count_assigned);  
                $this->set("count_progress", $count_progress);
                $this->set("count_completed", $count_completed);  
                $this->viewBuilder()->setLayout('usertask');

            }

            public function employee($emp_id) {

                $users_table = TableRegistry::get('users');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_user = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' , 'md5(id)' => $emp_id ])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->toArray() ;
                $retrieve_pending = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reference' , 'task.description' , 'task.duedate' , 'u.name' , 'c.name'  ])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(u.id) =  task.assigned' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '1'   ,'OR' => [['task.assigned' =>  $emp_id], ['task.owner' =>  $emp_id]] ])->toArray() ;
                $retrieve_progress = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reference' , 'task.description' , 'task.duedate' , 'u.name' , 'c.name' ])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(u.id) =  task.assigned' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '2'  , 'OR' => [['task.assigned' =>  $emp_id], ['task.owner' =>  $emp_id]] ])->toArray() ;
                $retrieve_completed = $task_table->find()->select(['task.id' , 'task.tittle' , 'task.reference' , 'task.description' , 'task.duedate' , 'u.name' , 'c.name'])->join([
                    'u' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(u.id) =  task.assigned' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.status' => '3' ,'OR' => [['task.assigned' =>  $emp_id], ['task.owner' =>  $emp_id]] ])->toArray() ;
                 $sidebar_prefix = $retrieve_user[0]['name']."'s " ;
                $this->set("sidebar_prefix", $sidebar_prefix);  
                $this->set("cat_details", $retrieve_cat);  
                $this->set("pending_details", $retrieve_pending);  
                $this->set("progress_details", $retrieve_progress);  
                $this->set("completed_details", $retrieve_completed);  
                $this->viewBuilder()->setLayout('user');

            }


            public function addpersonal(){
                if ($this->request->is('ajax') && $this->request->is('post') ){

                    $task_table = TableRegistry::get('task');
                    $activ_table = TableRegistry::get('activity');
                    
                        $task = $task_table->newEntity();
                        $task->tittle =  $this->request->data('title')  ;
                        $task->description = $this->request->data('description')  ;
                        $task->creator =  $this->Cookie->read('id') ;
                        if($this->request->data('owner') != ""){
                            $task->owner =  $this->request->data('owner') ;
                        }
                      

                        if($this->request->data('estimate') != ""){
                            $task->estimate =  $this->request->data('estimate') ;

                        }
                        if($this->request->data('agent') != ""){
                            $task->assigned =  $this->request->data('agent') ;
                            $task->assignedtime = strtotime('now');
                        }
                        $task->category =  $this->request->data('category') ;
                        $task->reference =  $this->request->data('reference') ;
                        $task->status =  1 ;
                        $task->reftype =  $this->request->data('reftype') ;
                        if($this->request->data('reftype') == "yes"){
                            $task->project =  $this->request->data('project') ;
                        }
                        $task->client = $this->request->data('client')  ;
                        if($this->request->data('duedate') != ""){
                            $task->duedate =  strtotime($this->request->data('duedate')) ;

                        }
                        $task->created =  strtotime('now') ;
                        $task->modified =  strtotime('now') ;
                        if($saved = $task_table->save($task) ){
                            $activity = $activ_table->newEntity();
                            $activity->action =  "Task Created"  ;
                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
        
                            $activity->value = md5($saved->id)   ;
                            $activity->origin = $this->Cookie->read('id')   ;
                            $activity->created = strtotime('now');
                            if($saved = $activ_table->save($activity) ){
                                $res = [ 'result' => 'success'  ];
    
                            }
                            else{
                            $res = [ 'result' => 'activity not saved'  ];
    
                            }
    
                        }
                        else{
                            $res = [ 'result' => 'personal task not saved'  ];
                        }
 

                   
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            
            public function scheduled($userid = null){
                $user_view_id = $userid ;
                if($userid == null){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                }
                $user_sel_id = $userid;
                $user_cat_id = $this->request->query('cat_id') ;
                $cat_cond = '' ;
                if($user_cat_id != "" && $user_cat_id != "all"  ){
                            $cat_cond =  [ 'md5(task.category)' =>  $user_cat_id ];
                }
                if($userid == 'all'){
                    if($this->Cookie->read('id') == ""){
                        return $this->redirect('/login');      
                    }
                    $user_sel_id = 'all' ;
                    $userid = $this->Cookie->read('id');
                    $user_view_id = $this->Cookie->read('id');
                    $cond = [['task.assigned' =>  $userid], ['task.owner' =>  $userid] ,['task.creator' =>  $userid] ] ;
                    $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                    ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                    ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                    ];
                      $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];
                      $cond3 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                }
                else{
                    if($userid == 'alltask' ){
                        $cond = [];
                        $cond1 = [    ['task.owner is' =>  NULL],
                        ['task.owner' =>  ''  ],
                        ['task.assigned' =>  '' ]
                        ];
                        $cond2 = [];
                        $cond3 = [];
                    }
                    else{
                        $cond = [['task.assigned' =>  $userid] ] ;
                        $cond1 = [    ['task.owner is' =>  NULL ,'task.creator' =>  $userid  ],
                        ['task.owner' =>  '' ,'task.creator' =>  $userid ],
                        ['task.assigned' =>  '' , 'task.owner' =>  $userid, 'task.creator !=' =>  $userid]
                        ];
                          $cond2 = [['task.owner' =>  $userid] ,['task.creator' =>  $userid] ];  
                          $cond3 = [ ['task.owner' =>  $userid] ,['task.creator' =>  $userid],['task.assigned' =>  $userid]];

                    }

                }
                $users_table = TableRegistry::get('users');
                $client_table = TableRegistry::get('client');
                $task_table = TableRegistry::get('task');
                $category_table = TableRegistry::get('taskcategory');

                $retrieve_emp = $users_table->find()->select(['id' , 'name'  ])->where([ 'status' => '1' ])->order(['name' => 'asc'])->toArray() ;
                $retrieve_client = $client_table->find()->select(['fname' , 'lname' , 'id'  ])->where([ 'status' => '1'  ])->order(['fname' => 'asc'])->toArray() ;
                $retrieve_cat = $category_table->find()->select(['id' , 'name'  ])->order(['name' => 'asc'])->toArray() ;

           
                $timelimit =  strtotime('tomorrow');
                
                if($user_sel_id == 'all'  || $user_sel_id == 'alltask' ){
                    $user_name = "All ";
                }
                else{
                    $user_view =  $users_table->find()->select(['id' , 'name' ])->where([ 'md5(id)' => $user_view_id ])->toArray() ;
                    $user_name =  $user_view[0]['name']."'s " ;

                }
                $retrieve_scheduled = $task_table->find()->select(['task.id'  , 'task.tittle' , 'task.reftype' , 'task.reference' , 'task.description' , 'task.estimate', 'task.duedate' , 'task.creator' , 'task.owner' , 'task.assigned' , 'task.created' , 'task.billing', 'p.name' ,'o.name', 'cr.name' , 'cl.fname', 'cl.lname' , 'a.name', 'c.name' ])->join([
                    'cr' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cr.id) =  task.creator' 
                    ],
                    'o' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(o.id) =  task.owner' 
                    ],
                    'p' => [
                        'table' => 'project',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(p.id) =  task.project' 
                    ],
                    'a' => [
                        'table' => 'users',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(a.id) =  task.assigned' 
                    ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ],
                    'c' => [
                        'table' => 'taskcategory',
                        'type' => 'LEFT',
                        'conditions' =>  'c.id =  task.category' 
                    ]
                 ])->where([ 'task.duedate >=' => $timelimit ,'task.status' => 1  , $cat_cond ,'OR' => $cond3 ])->toArray() ;


                 $this->set("billed_hours", $this->billed_hours);  
                 $this->set("spent_hours", $this->spent_hours);  
                $this->set("user_cat_id", $user_cat_id);
                $this->set("sidebar_prefix", $user_name);
                $this->set("emp_details", $retrieve_emp);  
                $this->set("cat_details", $retrieve_cat);
                $this->set("user_sel_id", $user_sel_id);
                $this->set("retrieve_task", $retrieve_scheduled);
                $this->set("cli_details", $retrieve_client);
                $this->viewBuilder()->setLayout('user');

            }
            
            public function  approvebillingtask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    $comment = $this->request->data('comment'); 
                    $spent = $this->request->data('spent'); 
                    if(!empty($id) && !empty($comment) ){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'comment' => $comment ,'billed' => $spent ,'status' => 4, 'billing' => 1 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'task could not be sent for billing'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            public function  bulkapprovebillingtask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    if(!empty($id)  ){
                                $modified = strtotime('now') ;
                                foreach($id as $s_id){
                                $update_task = $task_table->query()->update()->set([ 'status' => 4, 'billing' => 1 , 'modified' => $modified ])->where(['id' =>  $s_id ])->execute(); 
                                }
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'task could not be sent for billing'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }
            
            public function  bulksendbillingtask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    $bill = $this->request->data('billtime');
                    if(!empty($id)){
                            $modified = strtotime('now') ;
                            $i = 0;
                            foreach($id as $s_id){
                                $billed = $bill[$i];
                                $update_task = $task_table->query()->update()->set([ 'billing' => 1 , 'billed' => $billed , 'modified' => $modified ])->where(['id' =>  $s_id ])->execute(); 
                                $i++ ;
                            }
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'task could not be sent for billing'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }


            public function  sendbillingtask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    $comment = $this->request->data('comment'); 
                    $spent = $this->request->data('spent'); 
                    
                    if(!empty($id) && !empty($comment)  ){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'comment' => $comment , 'billed' => $spent , 'billing' => 1 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'task could not be sent for billing'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }



            public function  assignqualitytask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    $agent = $this->request->data('assigned'); 
                    if(!empty($id) && !empty($agent) ){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'qagent' => $agent ,'qualitystime' => $modified , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'personal task status not assigned'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            public function editpersonal(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $assignedtime = null ;
                    $id = $this->request->data('vista'); 
                    $title = $this->request->data('title'); 
                    $reference = $this->request->data('reference'); 
                    $category = $this->request->data('category'); 
                    $description = $this->request->data('description'); 
                    $assigned = $this->request->data('agent'); 
                    if($this->request->data('agent')!=""){
                        $assignedtime = strtotime('now');
                    }
                    $reftype  = $this->request->data('reftype');
                    $client = $this->request->data('client');
                    $project = $this->request->data('project');
                    $estimate = $this->request->data('estimate');
                    $owner = $this->request->data('owner');
                    if($this->request->data('duedate') != ""){
                        $duedate = strtotime($this->request->data('duedate'));
                    }
                    else{
                    $duedate = $this->request->data('duedate');
                    }

                    if(!empty($id) ){
                        $modified = strtotime('now') ;
                        $update_task = $task_table->query()->update()->set([ 'tittle' => $title , 'estimate' => $estimate , 'reftype' => $reftype, 'client' => $client ,'reference' => $reference , 'owner' => $owner , 'project' => $project , 'category' => $category ,'assignedtime' => $assignedtime , 'description' => $description ,'assigned' => $assigned , 'duedate' => $duedate  , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                        $res = ['result' => 'success'];   
                    }
                    else{
                        $res = ['result' => 'personal task not found'];
                    }
                }
                    else{
                        $res = [ 'result' => 'invalid operation'  ];
        
                    }
                return $this->json($res);


            }

            public function getprojects(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $projects_table = TableRegistry::get('project');

                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                        $task_table = TableRegistry::get('task');
                        $billing_table = TableRegistry::get('billing');
                        $query = $billing_table->find();
                        $retrieve_billing = $query->select([ 'hours' => $query->func()->sum('hours')  ])->where(['md5(cid)' => $id])->first() ;
                        $count_billed = $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'client' => $id ])->first();
                        $hours  = $retrieve_billing->hours - $count_billed->billed; 
                     
                        $retrieve_projects = $projects_table->find()->select(['id' , 'name'  ])->where(['cid' => $id , 'status' => 1 ])->toArray() ;
                        $project_list = null ;
                        if(!empty($retrieve_projects[0])){
                            $project_list = '<option value="">Select Project</option>'; 
                            foreach($retrieve_projects as $projects){
                                $project_list .= '<option value="'.md5($projects['id']).'">'.$projects['name'].'</option>'; 

                            }
                        }   
                        $res = [ 'result' => 'success' , 'data' => $project_list  , 'hours' => $hours ];

                    }
                    else{
                        $res = [ 'result' => 'project list not found'  ];
                    }
                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];
                }
    
                return $this->json($res);
            }

            public function gettask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                        $retrieve_task = $task_table->find()->where(['md5(id)' => $id  ])->first() ; 
                        if($retrieve_task->duedate !=""){
                            $retrieve_task->duedate = date('m/d/Y' , $retrieve_task->duedate );
                        }

                        if($retrieve_task->frequency == 1){
                            $stringtime = strtotime($retrieve_task->frequencyvalue);
                            if($stringtime < strtotime('now')){
                                $retrieve_task->frequencyvalue = date('j/m/Y');
                            }
                        }
                        $res = [ 'result' => 'success' , 'task' => $retrieve_task  ];                            

                    }
                    else{
                        $res = [ 'result' => 'personal task comment not found'  ];
                    }

                }
            
            else{
                $res = [ 'result' => 'invalid operation'  ];

            }


            return $this->json($res);
            }

            // public function getcommenttask(){

            //     if ($this->request->is('ajax') && $this->request->is('post') ){
            //         $task_table = TableRegistry::get('task');
            //         $id = $this->request->data('val'); 
            //         if(!empty($id)){
            //             $retrieve_comment = $task_table->find()->select([ 'comment' ])->where(['md5(id)' => $id  ])->first() ; 
            //             $res = [ 'result' => 'success' , 'comment' => $retrieve_comment->comment ];                            

            //         }
            //         else{
            //             $res = [ 'result' => 'personal task comment not found'  ];
            //         }

            //     }
            
            // else{
            //     $res = [ 'result' => 'invalid operation'  ];

            // }


            // return $this->json($res);

            // }
            

            public function  completetask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $activ_table = TableRegistry::get('activity');
                    $id = $this->request->data('val'); 
                    $comment = $this->request->data('comment'); 
                    $spent = $this->request->data('spent'); 
                    $ongoing = $this->request->data('repeattask'); 
                    $frequency = $this->request->data('repeattype'); 
                    $frequencyvalue = $this->request->data('repeatval'); 
                    $qualitytask = $this->request->data('qualitytask'); 
                    
                    if(!empty($id) && !empty($spent) && !empty($comment)  ){
                                $modified = strtotime('now') ;
                                $status = 3 ;
                                if($qualitytask == "true"){
                                    $status = 5 ;
                                }
                                $update_task = $task_table->query()->update()->set([ 'status' => $status , 'spent' => $spent ,'comment' => $comment , 'modified' => $modified ,'billedtime' => $modified , 'completedtime' => $modified ])->where(['md5(id)' =>  $id ])->execute();
                                
                                if($ongoing == "true"){
                                    $assignedtime = strtotime('now');
                                    if($frequency == "1"){
                                        $duedate = strtotime($frequencyvalue);
                                    }
                                    else if($frequency == "2"){
                                        $duedate = strtotime('tomorrow');
                                    }
                                    else if($frequency == "3"){
                                        $get_day = date('N');
                                        if($get_day < $frequencyvalue){
                                            $diff = $frequencyvalue - $get_day ;
                                            $duedate = strtotime('+'.$diff.' day');
                                        }
                                        elseif($get_day >= $frequencyvalue){
                                            $diff = (7 - $get_day) + $frequencyvalue   ;
                                            $duedate = strtotime('+'.$diff.' day');
                                            
                                        }
                                        
                                    }
                                    else if($frequency == "4"){
                                        $get_day = date('j');
                                        if($get_day < $frequencyvalue){
                                            $diff = $frequencyvalue - $get_day ;
                                            $duedate = strtotime('+'.$diff.'d');
                                        }
                                        elseif($get_day >= $frequencyvalue){
                                            $monthDay = date('t');
                                            $diff = ($monthDay - $get_day) + $frequencyvalue   ;
                                            $duedate = strtotime('+'.$diff.'d');
                                        }
                                    }

                                    $retrive_task =  $task_table->find()->where(['md5(id)' => $id  ])->first() ; 
                                    $task = $task_table->newEntity();
                                    $task->tittle =  $retrive_task->tittle  ;
                                    $task->description = $retrive_task->description  ;
                                    $task->creator =  $retrive_task->creator ;
                                    $task->owner =  $retrive_task->owner ;
                                    $task->assigned =  $retrive_task->assigned ;
                                    $task->assignedtime =  $assignedtime ;
                                    $task->category =  $retrive_task->category ;
                                    $task->reference =  $retrive_task->reference ;
                                    $task->status =  1 ;
                                    $task->reftype =  $retrive_task->reftype ;
                                    $task->client = $retrive_task->client  ;
                                    $task->project = $retrive_task->project  ;
                                    $task->parent = $retrive_task->id  ;
                                    $task->duedate =  $duedate ;
                                    $task->estimate =  $retrive_task->estimate ;
                                    
                                    $task->ongoing =  $ongoing ;
                                    $task->frequency =  $frequency ;
                                    $task->frequencyvalue =  $frequencyvalue ;
                                    $task->created =  strtotime('now') ;
                                    $task->modified =  strtotime('now') ;
                                    if($savedtask = $task_table->save($task) ){
                                        $activity = $activ_table->newEntity();
                                        $activity->action =  "Task marked complete and ongoing"  ;
                                        $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
                                        $activity->value = $id   ;
                                        $activity->origin = $this->Cookie->read('id')   ;
                                        $activity->created = strtotime('now');
                                        if($saved = $activ_table->save($activity) ){
                                            $activity = $activ_table->newEntity();
                                            $activity->action =  "Task created as ongoing"  ;
                                            $activity->ip =  $_SERVER['REMOTE_ADDR'] ;
                                            $activity->value = $savedtask->id   ;
                                            $activity->origin = $this->Cookie->read('id')   ;
                                            $activity->created = strtotime('now');
                                            if($saved = $activ_table->save($activity) ){
    
                                            $res = [ 'result' => 'success'  ];
                                            }
                                            else{
                                                $res = [ 'result' => 'activity not saved'  ];
                            
                                                    }
                
                                        }
                                        else{
                                    $res = [ 'result' => 'activity not saved'  ];
                
                                        }
            
                     
                                }
                                else{
                                    $res = [ 'result' => 'task could not be created'  ];                            

                                }
                            }
                            else{
                                $res = [ 'result' => 'success'  ];

                            }
                    }
                    else{
                        $res = [ 'result' => 'personal task status not updated'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            } 
            
            
            public function  taskqualityrework(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    $rework_comment = $this->request->data('comment'); 
                    $bug = $this->request->data('bugs'); 
                    
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'rework' => 1, 'bug = bug +' => $bug , 'qrework_comment' => $rework_comment , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'personal task status not updated'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            public function  completetaskquality(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'status' => 3 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'personal task status not updated'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            
            public function  starttaskquality (){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'status' => 6 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'personal task status not updated'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }



            public function  starttask(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val'); 
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                                $update_task = $task_table->query()->update()->set([ 'status' => 2 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute(); 
                                $res = [ 'result' => 'success'  ];                            
                    }
                    else{
                        $res = [ 'result' => 'personal task status not updated'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }

            
            public function  delete(){
                if ($this->request->is('ajax') && $this->request->is('post') ){
                    $task_table = TableRegistry::get('task');
                    $id = $this->request->data('val');
                    if(!empty($id)){
                                $modified = strtotime('now') ;
                           if($task_table->query()->update()->set([ 'status' => 0 , 'modified' => $modified ])->where(['md5(id)' =>  $id ])->execute()){
                            $res = [ 'result' => 'success'  ];

                           }
                             

                    }
                    else{
                        $res = [ 'result' => 'personal task not deleted'  ];
                    }


                }
                else{
                    $res = [ 'result' => 'invalid operation'  ];

                }


                return $this->json($res);

            }


            }
