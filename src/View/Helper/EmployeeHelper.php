<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class EmployeeHelper extends Helper
{
    public function getattendance($id,$day)
    {
        // $task_table = TableRegistry::get('task');
        $attendance_table = TableRegistry::get('emp_attendance');

        $users_attendance_details = $attendance_table->find()->select([ 'id' , 'userid' , 'login' , 'logout' , 'day'  ])->where([ 'day' => $day , 'userid' => $id  ])->toArray();   

        return $users_attendance_details ;
        // echo $id;
        // return  $id; 
        // Logic to create specially formatted link goes here...
    }
    public function getspent($id,$day)
    {
        $task_table = TableRegistry::get('task');
         $timelimitlower = $day;
        
        $timelimitupper = $timelimitlower + 86400 ;
        
        $get_spent =  $task_table->find()->select(['spent' => 'SUM(spent)' ])->where([ 'completedtime >=' =>  $timelimitlower , 'assigned' => md5($id)  , 'completedtime <=' =>  $timelimitupper  ])->first();

        return $get_spent->spent;     
        // return 0 ;
    }

    public function getrework($id,$day)
    {
        $task_table = TableRegistry::get('task');
         $timelimitlower = $day;
        
        $timelimitupper = $timelimitlower + 86400 ;
        
        $get_rework =  $task_table->find()->select(['rework' => 'SUM(spent)' ])->where([ 'completedtime >=' =>  $timelimitlower , 'assigned' => md5($id)  , 'completedtime <=' =>  $timelimitupper, 'task_status_new_rework >=' => '2' ])->first();

        return $get_rework->rework;     
        // return 0 ;
    }

    public function getproject($id,$day)
    {
        $task_table = TableRegistry::get('task');
         $timelimitlower = $day;
        
        $timelimitupper = $timelimitlower + 86400 ;
        
        $get_projects =  $task_table->find()->select(['cl.fname','cl.lname','u.name', 'task.project','task.description' ])->join([
                        'u' => [
                            'table' => 'project',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(u.id) =  task.project' 
                        ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ]
                    ])
->where([ 'task.completedtime >=' =>  $timelimitlower , 'task.assigned' => md5($id)  , 'task.completedtime <=' =>  $timelimitupper ])->toArray();
      
/*       
$log = $task_table->find()->select(['cl.fname','cl.lname','u.name', 'task.project',  'task.description' ])->join([
                        'u' => [
                            'table' => 'project',
                            'type' => 'LEFT',
                            'conditions' =>  'md5(u.id) =  task.project' 
                        ],
                    'cl' => [
                        'table' => 'client',
                        'type' => 'LEFT',
                        'conditions' =>  'md5(cl.id) =  task.client' 
                    ]
                    ])
->where([ 'task.completedtime >=' =>  $timelimitlower , 'task.assigned' => md5($id)  , 'task.completedtime <=' =>  $timelimitupper ]);
debug($log);  
*/
        
//echo "<pre>";print_r($get_projects); echo "</pre>"; 
//die;
        
        return $get_projects;     
        // return 0 ;
    }

    public function getseostats($id,$startdate, $enddate)
    {
        $seo_stats_table = TableRegistry::get('seo_stats');
         $timelimitlower = $startdate;
        
      //  $timelimitupper = $timelimitlower + 86400 ;
        $timelimitupper = $enddate ;
        
        
        $get_seo_stats =  $seo_stats_table->find()->distinct('u.id')->select(['seo_stats.created', 'seo_stats.keywords','seo_stats.visits','seo_stats.backlinks','seo_stats.options','seo_stats.week_first_date','seo_stats.week_last_date','seo_stats.projectid','u.name','u.id' ])->join([
                        'u' => [
                            'table' => 'project',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  seo_stats.projectid' 
    ]])
->where([ 'seo_stats.week_first_date >=' =>  $timelimitlower , 'seo_stats.userid' => $id  , 'seo_stats.week_last_date <=' =>  $timelimitupper ])->order(['seo_stats.id' => 'DESC'])->toArray();
      
/*       
$log = $seo_stats_table->find()->select(['seo_stats.created', 'seo_stats.keywords','seo_stats.visits','seo_stats.backlinks','seo_stats.options','seo_stats.week_first_date','seo_stats.week_last_date','seo_stats.projectid','u.name','u.id' ])->join([
                        'u' => [
                            'table' => 'project',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  seo_stats.projectid' 
    ]])
->where([ 'seo_stats.week_first_date >=' =>  $timelimitlower , 'seo_stats.userid' => $id  , 'seo_stats.week_last_date <=' =>  $timelimitupper ])->order(['seo_stats.created' => 'DESC'])->limit(1);
    debug($log);  
*/
        
//echo "<pre>";print_r($get_seo_stats); echo "</pre>"; 
//die;
        
        return $get_seo_stats;     
        // return 0 ;
    }

    public function getsmostats($id,$startdate, $enddate)
    {
        $seo_stats_table = TableRegistry::get('seo_stats');
        // $timelimitlower = $day;
        
       // $timelimitupper = $timelimitlower + 86400 ;
       $timelimitlower = $startdate;
        
      //  $timelimitupper = $timelimitlower + 86400 ;
        $timelimitupper = $enddate ; 
  
/*        
        $get_smo_stats =  $seo_stats_table->find()->select(['created','smo_followers_insta', 'smo_twittr','smo_linkedin','smo_optins'])
->where([ 'created >=' =>  $timelimitlower , 'userid' => $id  , 'created <=' =>  $timelimitupper ])->order(['created' => 'DESC'])->limit(1)->toArray();
*/      
        
$get_smo_stats =  $seo_stats_table->find()->distinct('u.id')->select(['seo_stats.created', 'seo_stats.smo_followers_insta','seo_stats.smo_twittr','seo_stats.smo_linkedin','seo_stats.smo_optins','seo_stats.week_first_date','seo_stats.week_last_date','seo_stats.projectid','u.name','u.id' ])->join([
                        'u' => [
                            'table' => 'project',
                            'type' => 'LEFT',
                            'conditions' =>  'u.id =  seo_stats.projectid' 
    ]])
->where([ 'seo_stats.week_first_date >=' =>  $timelimitlower , 'seo_stats.userid' => $id  , 'seo_stats.week_last_date <=' =>  $timelimitupper ])->order(['seo_stats.id' => 'DESC'])->toArray();        
        
/*        
$log = $seo_stats_table->find()->select(['seostats', 'keywords','visits','backlinks','options' ])
->where([ 'created >=' =>  $timelimitlower , 'userid' => $id  , 'created <=' =>  $timelimitupper ])->order(['created' => 'DESC'])->limit(1);
    debug($log);  
*/
        
//echo "<pre>";print_r($get_projects); echo "</pre>"; 
//die;
        
        return $get_smo_stats;     
        // return 0 ;
    }
    
    
    
    public function getbilled($id,$day)
    {
        $task_table = TableRegistry::get('task');
        $timelimitlower = $day;
        $timelimitupper = $timelimitlower + 86400 ;
        
        $get_billed =  $task_table->find()->select(['billed' => 'SUM(billed)' ])->where([ 'billedtime >=' =>  $timelimitlower , 'assigned' => md5($id)  , 'billedtime <=' =>  $timelimitupper  ])->first();

        return $get_billed->billed;     
    }
    public function updatenegative($tid)
    {
        $task_table = TableRegistry::get('task');
        $modified = strtotime('now');
        $task_table->query()->update()->set([ 'negative' => 0 , 'modified' => $modified ])->where(['id' =>  $tid ])->execute();
        return  ;
        // return  $id; 
        // Logic to create specially formatted link goes here...
    }
    public function updatepositive($tid)
    {
        $task_table = TableRegistry::get('task');
        $modified = strtotime('now');
        $color = "red";
        $task_table->query()->update()->set([ 'negative' => 1 , 'modified' => $modified ])->where(['id' =>  $tid ])->execute();
        return  $color;
        // return  $id; 
        // Logic to create specially formatted link goes here...
    }
}

?>